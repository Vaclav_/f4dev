#include <iostream>

#include <SFML/Network.hpp>
#include "stdio.h"
#include "Server.h"

//sf::IpAddress a8 = sf::IpAddress::getLocalAddress();  // my address on the local network
//sf::IpAddress a9 = sf::IpAddress::getPublicAddress(sf::milliseconds(500));

CGame g_game;

const sf::Uint16 PORT_NUM = 54000;
sf::Uint16 GAME_VER = 10;

int main()
{
    g_game.g_clock.restart();

    std::cout << "\nServer started.\n";
/*
    CClientConn client;
    client.updateRemoteCnt(0);
    client.updateRemoteCnt(2);
    std:: cout << client.wasPacketACKed(0) << "\n";
*/


    CServer server;
    if( server.bindSocket(PORT_NUM) != 0 ) {
        return -1;
    }

    sf::Time timeout = sf::milliseconds(1000);
    sf::Clock clock;
    sf::Time wait_until = sf::seconds(0);
    sf::Time cur_t;
    clock.restart();
    while( 1 ) {
        wait_until += timeout;
        cur_t = clock.getElapsedTime();
        while( wait_until > cur_t ) {
            if( server.check( timeout ) == 0 ) {
                server.handlePacket();
                //rec = true;
            }
            cur_t = clock.getElapsedTime();
        }
        server.sendACKs();
        //std::cout << "r";
    }

    std::cout << "Goodbye.\n";



    return 0;
}
