#ifndef SERVER_H_INCLUDED
#define SERVER_H_INCLUDED

#include <SFML/Network.hpp>
#include <deque>

enum eConnectionState { CONN_NONE, CONN_INIT, CONN_EST, CONN_PEND, CONN_DISC};
enum eRefuseReason { REASON_UNKOWN=0, REASON_WRONG_VERSION, REASON_SERVER_FULL, REASON_ON_BANLIST, REASON_PLAYER_LEAVE };

const unsigned int MAX_STR_LEN = 16;
const unsigned int MAX_MESSAGE_LEN = 256;
const sf::Uint8 DEFAULT_MAX_PLAYERS = 16;

enum ePacketType {
    PC_GET_SERVER_INFO,
    PS_SERVER_INFO,
    PC_CONNECT,
    P_ACK,
    PS_SERVER_DESC,
    PS_PLAYERS_LIST,
    PS_PLAYER_CHANGE,
    PC_READY,
    P_CHAT_MESSAGE,
    PS_START_GAME,
    P_DISCONNECT
};

class CGame {
	public:
	sf::Clock g_clock;
};

//packet descriptor
//all info about a packet already sent
class CPacketDesc {
    public:
    CPacketDesc() : msg_id(0), ACKed(0) {}
    CPacketDesc( sf::Uint16 c, sf::Packet& p, sf::Time& t, sf::Uint8 m_id = 0 ) :
        cnt(c), packet(p), time(t), msg_id(m_id), ACKed(0) {}
    sf::Uint16 cnt;     //local counter of the packet
    sf::Packet packet;
    sf::Time time;      //what time it was sent
    sf::Uint8 msg_id;   //message_id, use 0 for none
    bool ACKed;         //if it was acknowledged already
};

//packet_type, need_to_ACK
extern std::map< sf::Uint8, bool> to_ACK;
void to_ACK_init();

class CClientConn {
	public:
	void error( std::string err_msg );

    void packetACK( sf::Packet& packet );
    void packetServerDesc( sf::Packet& packet, std::string& host_name, std::string& map_name );
    void packetPlayersList( sf::Packet& packet, sf::Uint8 players_num );
    void packetPlayerChange( sf::Packet& packet, sf::Uint8 player_id, sf::Uint8 join_leave );
	void packetChatMessage( sf::Packet& packet, sf::Uint8 msg_id, sf::Uint8 player_id, std::string& message );
    void packetStartGame( sf::Packet& packet);
    void packetDisconnect( sf::Packet& packet, sf::Uint8 reason );

	CClientConn();
	bool isConnected();
	int getState();

    int bindSocket( sf::Uint16 port_n );

    sf::Int32 getCntDiff( sf::Uint16 new_cnt, sf::Uint16 most_recent_cnt );
	void updateLocalACKs( sf::Uint16 new_cnt );

	bool wasPacketACKed( sf::Uint16 p_cnt );

    int send( sf::Packet& s_packet);

	//Node = the other endpoint
	sf::IpAddress node_ip;
	sf::Uint16 port;

	sf::UdpSocket socket;
	sf::Uint8 player_id;
    std::string player_name;

	//connection state
	int conn_state;
	sf::Uint16 local_cnt;
	sf::Uint16 local_ack_cnt;
	sf::Uint32 ack_bitf;
	sf::Uint16 remote_ack_cnt;
	sf::Uint32 remote_bitf;
	sf::Time last_recv_time;

    std::deque<CPacketDesc> packets_sent;
};


    void packetServerInfo( sf::Packet& packet, sf::Uint8 players_in, sf::Uint8 players_max, std::string& game_name );

    void extPacketConnect( sf::Packet& packet, sf::Uint16& rem_cnt, sf::Uint16& game_ver, std::string& p_name );
    void extPacketACK( sf::Packet& packet, sf::Uint16& seq_num, sf::Uint16& ack_num, sf::Uint32& bitfield );


class CServer {
    public:

    sf::Uint8 getNewPlayerId();
    //extract

    //true if connection is accepted
    //no reference: pass a new packet for reading
    bool alreadyConnected( sf::IpAddress& conn_ip, sf::Uint16 conn_port );
    bool decideConnection( sf::IpAddress& conn_ip, sf::Uint16 conn_port, sf::Uint16 game_version, sf::Uint8& reason );

    CServer();
    sf::UdpSocket listen_socket;
    sf::Uint16 listen_port;
	sf::IpAddress last_ip;
    sf::Uint16 last_port;

    sf::SocketSelector selector;
    //only one client for now
    CClientConn one_client;
    sf::Packet latest_packet;
    sf::Uint8 latest_player_id;

    sf::Uint8 players_connected;
    sf::Uint8 max_players;

    void error( std::string err_msg );

    int bindSocket( sf::Uint16 l_port );

    int send( sf::Packet& s_packet, sf::IpAddress& ip_addr, sf::Uint16 s_port);
    void sendACKs();

    int check( sf::Time& max_wait_time );
    void handlePacket();

};

#endif // SERVER_H_INCLUDED
