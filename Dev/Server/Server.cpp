
#include "Server.h"
#include <iostream>
#include <sstream>

extern CGame g_game;

extern sf::Uint16 GAME_VER;

CClientConn::CClientConn() :
	node_ip("0.0.0.0"),
	port(0),
    player_id(0),
    player_name("Unkown"),
	conn_state(CONN_NONE),
	local_cnt(0),
	local_ack_cnt(0),
	ack_bitf(0),
	remote_ack_cnt(0),
	remote_bitf(0),
	last_recv_time(sf::milliseconds(0))
	{
}

bool CClientConn::isConnected() {
	if( conn_state == CONN_EST || conn_state == CONN_PEND )
		return true;
	else
		return false;
}

int CClientConn::getState() {
	return conn_state;
}

int CClientConn::send( sf::Packet& s_packet) {
	if( socket.send( s_packet, node_ip, port) != sf::Socket::Done) {
		std::ostringstream port_str;
		port_str << port;
		this->error( "CClientConn Can't send packet to: IP " + node_ip.toString() + " |Port " + port_str.str() + "\n");
		return -1;
	}
	return 0;
}

void printBitf(sf::Uint32 bitfield ) {
    char c;
    sf::Uint32 num;
    sf::Uint32 mask;
    for( c=0; c<32; c++) {
        mask = 0;
        mask |= 1<<(31-c);
        num = bitfield & mask;
        num >>= (31-c);
        std::cout << num;
    }
}

void CClientConn::updateLocalACKs( sf::Uint16 new_cnt ) {
    //if the received counter is more recent
    //std::cout << "New_cnt: " << new_cnt << " local_ack_cnt: " << local_ack_cnt <<"\n";
    //std::cout << "Old bitf: ";
    //printBitf(ack_bitf); std::cout << "\n";
    sf::Uint16 diff;
    bool more_recent = false;
    if( ( new_cnt > local_ack_cnt ) && ( new_cnt-local_ack_cnt <= 0xffff/2 ) ) {
        diff = new_cnt-local_ack_cnt;
        local_ack_cnt = new_cnt;   //is bigger
        more_recent = true;
    } else if( ( local_ack_cnt > new_cnt ) && ( local_ack_cnt-new_cnt > 0xffff/2 ) ) {
        diff = 0xffff-local_ack_cnt + new_cnt + 1;
        local_ack_cnt = new_cnt;   //wrap around
        more_recent = true;
    } else {
        //no wrap around
        if( local_ack_cnt >= new_cnt ) {
            diff = local_ack_cnt - new_cnt;
        } else {//new_cnt > remote cnt wrap around
            diff = 0xffff-new_cnt + local_ack_cnt + 1;
        }
        more_recent = false;
    }

    //std::cout << "Updated cnt: " << local_ack_cnt << " More recent: " << more_recent << " diff: " << diff << "\n";

    if( more_recent ) {
        //shift
        ack_bitf <<= diff;
        //add the previous cnt to flags
        if(diff == 1) {
            ack_bitf |= 1;
        } else if(diff < 33 && diff > 0) {
            ack_bitf |= (1<<(diff-1));
        }
    } else {
        //no shift
        //add flag
        if(diff == 1) {
            ack_bitf |= 1;
        } else if(diff < 33 && diff > 0) {
            ack_bitf |= (1<<(diff-1));
        }
    }

    //std::cout << "New bitf: ";
    //printBitf(ack_bitf);
    //std::cout << "\n";

}

int CClientConn::bindSocket( sf::Uint16 port_n ) {
    if( socket.bind(port_n) != sf::Socket::Done) {
        std::cout << "Error: can not bind client UDP socket to port " << port_n << "\n";
        return -1;
    }
    std::cout << "Bound to client socket.\n";
    //socket.setBlocking(false);
    //yet add socket to selector manually
    return 0;
}

void CClientConn::error( std::string err_msg ) {
	std::cout << "Error: " << err_msg;
	if( err_msg.at(err_msg.size()-1)=='.' || err_msg.at(err_msg.size()-1)=='!' )
		std::cout << "\n";
}

sf::Int32 CClientConn::getCntDiff( sf::Uint16 new_cnt, sf::Uint16 most_recent_cnt ) {
    //newer: positive
    //older: negative
    sf::Int32 diff;
    if( ( new_cnt > most_recent_cnt ) && ( new_cnt-most_recent_cnt <= 0xffff/2 ) ) {
        //packet is more recent
        diff = new_cnt-most_recent_cnt;
    } else if( ( most_recent_cnt > new_cnt ) && ( most_recent_cnt-new_cnt > 0xffff/2 ) ) {
        //packet is more recent
        diff = 0xffff-most_recent_cnt + new_cnt + 1;
    } else {
        //packet is older or same
        //no wrap around
        if( most_recent_cnt >= new_cnt ) {
            diff =  new_cnt - most_recent_cnt;
        } else { //new_cnt > remote cnt wrap around
            diff = -(0xffff-new_cnt + most_recent_cnt + 1);
        }
    }
    return diff;
}

sf::Uint8 CServer::getNewPlayerId() {
    //TODO: check current players list..
    //get new unused ID
    return 1;
}

bool CClientConn::wasPacketACKed( sf::Uint16 p_cnt ) {
    if( p_cnt == remote_ack_cnt )
        return true;
    sf::Int32 diff = getCntDiff( p_cnt, remote_ack_cnt );
    if( diff > 0 )
        return false;
    if( diff == 0 )
        return true;
    diff = -diff;   //make it positive
    //Attention: if p_cnt is off the bitfield, function will always return false
    sf::Uint16 mask = 1<<(diff-1);
    bool wasACKed;
    //printBitf(ack_bitf); std::cout << "|bool ";
    if( ack_bitf&mask )
        wasACKed = true;
    else wasACKed = false;
    return wasACKed; //TODO: TEST test test
}

void CServer::error( std::string err_msg ) {
	std::cout << "Error: " << err_msg;
	if( err_msg.at(err_msg.size()-1)=='.' || err_msg.at(err_msg.size()-1)=='!' )
		std::cout << "\n";
}

CServer::CServer() :
    listen_port(0),
    latest_player_id(0),
    players_connected(0),
    max_players(DEFAULT_MAX_PLAYERS) {
}

int CServer::bindSocket( sf::Uint16 l_port ) {
    if( listen_socket.bind(l_port) != sf::Socket::Done) {
        std::cout << "Error: can not bind UDP socket.\n";
        return -1;
    }
    std::cout << "Bound to listen socket.\n";
    listen_port = l_port;
    selector.add(listen_socket);
    return 0;
}

int CServer::send( sf::Packet& s_packet, sf::IpAddress& ip_addr, sf::Uint16 s_port) {
    sf::UdpSocket socket;
	if( socket.send( s_packet, ip_addr, s_port) != sf::Socket::Done) {
		std::ostringstream port_str;
		port_str << s_port;
		this->error( "Can't send packet to: IP " + ip_addr.toString() + " |Port " + port_str.str() + "\n");
		return -1;
	}
	return 0;
}

int CServer::check( sf::Time& max_wait_time ) {

	if( selector.wait( max_wait_time ) ) {
		sf::IpAddress sender;
        sf::Uint16 recv_port;
	    //for.. loop through sockets
		if ( selector.isReady(listen_socket)) {
			if ( listen_socket.receive( latest_packet, sender, recv_port) != sf::Socket::Done) {
				this->error("Error receiving from listen socket.");
				return (-1);
			}
			//no player
			latest_player_id = 0;
			last_ip = sender;
			last_port = recv_port;
			//received data
			return 0;
		} else if( one_client.isConnected() && selector.isReady(one_client.socket) ) {
			if ( one_client.socket.receive( latest_packet, sender, recv_port) != sf::Socket::Done) {
				this->error("Error receiving from client socket.");
				return (-1);
			}
			latest_player_id = one_client.player_id;
			//received data
			return 0;
		}
	}
	//no data
	return 1;
}

bool CServer::alreadyConnected( sf::IpAddress& conn_ip, sf::Uint16 conn_port ) {
    //TODO: check all clients in list
    return false;
}

bool CServer::decideConnection( sf::IpAddress& conn_ip, sf::Uint16 conn_port, sf::Uint16 game_version, sf::Uint8& reason ) {
    if( game_version != GAME_VER ) {
        reason = REASON_WRONG_VERSION;
        return false;
    } else if( players_connected>= max_players ) {
        reason = REASON_SERVER_FULL;
        return false;
    } else {
        //TODO: check if ip or ip/port is on banlist
    }

    reason = 0;
    //accept connection
    return true;
}

void CServer::handlePacket() {
	sf::Uint8 packet_type;
	latest_packet >> packet_type;
	//==================
	sf::Packet read_packet;
	sf::Packet re_packet;
	std::string gn="Cool!";
	/////////////////////
    sf::Uint16 remote;
    sf::Uint16 ver;
    std::string pn;
    sf::Uint8 refuse_reason;

    if( latest_player_id == 0 ) { //latest packet came at listen socket

        switch(packet_type) {
            case PC_GET_SERVER_INFO:
                std::cout << "Packet PC_GET_SERVER_INFO\n";
                packetServerInfo( re_packet, (sf::Uint8)3, (sf::Uint8)15, gn);
                send( re_packet, last_ip, last_port);
                break;
            case PC_CONNECT:
                std::cout << "Packet PC_CONNECT\n";
                extPacketConnect( latest_packet, remote, ver, pn);

                if( !alreadyConnected( last_ip, last_port) ) {
                    if( decideConnection( last_ip, last_port, ver, refuse_reason) ) { //connected, send ack
                    //TODO: Check number of players, check game version before accepting
                    //TODO: ... new CConnState, push
                    //Connection accepted
                    one_client.conn_state = CONN_EST;
                    one_client.last_recv_time = g_game.g_clock.getElapsedTime();
                    one_client.node_ip = last_ip;
                    one_client.port = last_port;
                    one_client.player_name = pn;
                    one_client.player_id = getNewPlayerId();

                    //update remote counter
                    one_client.updateLocalACKs(remote);
                    //send ACK --> not now, but at interval
                    std::cout << "Rem " << remote << " |Ver " << ver << " |Player name " << pn << "\n";

                    one_client.bindSocket(one_client.port);
                    selector.add(one_client.socket);

                    } else {    //connection refused
                        //TODO: send disconnected packet
                        //refuse_reason
                    }

                } else {    //already connected
                    this->error("Client is already connected.");
                }

                //handle packet

                break;
            default:
                this->error("Type of received packet is unkown.");
                break;
        }

    } else if( latest_player_id == 1 ) {    //first client, for test
        if( packet_type == P_ACK ) {
            sf::Uint16 remote_cnt, ack_num;
            sf::Uint32 bitf;
            extPacketACK( latest_packet, remote_cnt, ack_num, bitf );
            one_client.updateLocalACKs( remote_cnt );
            one_client.remote_ack_cnt = ack_num;
            one_client.remote_bitf = bitf;
            std::cout << "ACK packet, remote_cnt: " <<remote_cnt <<" |remote_ack_cnt: " << ack_num << "\nRemote bitf: ";
            printBitf(bitf);
            std::cout << "\n";
            one_client.last_recv_time = g_game.g_clock.getElapsedTime();
        } else
            this->error("Unexpected packet type.");
    } else {
        this->error("Other player.");
    }

}

void CServer::sendACKs() {
    //TODO: check all clients
    if(one_client.isConnected()) {
        sf::Packet ACKpacket;
        one_client.packetACK( ACKpacket );
        one_client.send( ACKpacket );
    }
}



















