
#include "Server.h"

std::map< sf::Uint8, bool> to_ACK;
static bool to_ACK_was_setup = false;

void to_ACK_init() {
	if(!to_ACK_was_setup) {
		to_ACK[PC_GET_SERVER_INFO] = false;
		to_ACK[PS_SERVER_INFO] = false;
		to_ACK[PC_CONNECT] = true;
		to_ACK[P_ACK] = true;
		to_ACK[PS_SERVER_DESC] = true;
		to_ACK[PS_PLAYERS_LIST] = true;
		to_ACK[PS_PLAYER_CHANGE] = true;
		to_ACK[PC_READY] = true;
		to_ACK[P_CHAT_MESSAGE] = true;
		to_ACK[PS_START_GAME] = true;
		to_ACK[P_DISCONNECT] = true;
		to_ACK_was_setup = true;
	}
}

void packetServerInfo( sf::Packet& packet, sf::Uint8 players_in, sf::Uint8 players_max, std::string& game_name ) {
    sf::Uint8 t = PS_SERVER_INFO;
    packet << t;
    packet << players_in;
    packet << players_max;
    if( game_name.length() > MAX_STR_LEN ) {
        std::string game_name_cut = game_name.substr( 0, MAX_STR_LEN);
        packet << game_name_cut;
    } else packet << game_name;
}

void CClientConn::packetACK( sf::Packet& packet ) {
    sf::Uint8 type = P_ACK;
    packet << type << local_cnt << local_ack_cnt << ack_bitf;
    local_cnt++;
}

void CClientConn::packetServerDesc( sf::Packet& packet, std::string& host_name, std::string& map_name ) {
    sf::Uint8 type = PS_SERVER_DESC;
    packet << type << local_cnt;
    if( host_name.length() > MAX_STR_LEN ) {
        std::string host_name_cut = host_name.substr( 0, MAX_STR_LEN);
        packet << host_name_cut;
    } else packet << host_name;

    if( map_name.length() > MAX_STR_LEN ) {
        std::string map_name_cut = map_name_cut.substr( 0, MAX_STR_LEN);
        packet << map_name_cut;
    } else packet << map_name;
    local_cnt++;
}

void CClientConn::packetPlayersList( sf::Packet& packet, sf::Uint8 players_num ) {
    sf::Uint8 type = PS_PLAYERS_LIST;
    packet << type << local_cnt << players_num;
    local_cnt++;
}

void CClientConn::packetPlayerChange( sf::Packet& packet, sf::Uint8 player_id, sf::Uint8 join_leave ) {
    sf::Uint8 type = PS_PLAYER_CHANGE;
    packet << type << local_cnt << player_id << join_leave;
    local_cnt++;
}

void CClientConn::packetChatMessage( sf::Packet& packet, sf::Uint8 msg_id, sf::Uint8 player_id, std::string& message ) {
	sf::Uint8 type = P_CHAT_MESSAGE;
	packet << type << local_cnt << msg_id << player_id;
    if( message.length() > MAX_MESSAGE_LEN ) {
        std::string message_cut = message.substr( 0, MAX_MESSAGE_LEN);
        packet << message_cut;
    } else packet << message;
    local_cnt++;
}

void CClientConn::packetStartGame( sf::Packet& packet) {
    sf::Uint8 type = PS_START_GAME;
    packet << type << local_cnt;
    local_cnt++;
}

void CClientConn::packetDisconnect( sf::Packet& packet, sf::Uint8 reason ) {
    sf::Uint8 type = P_DISCONNECT;
    packet << type << local_cnt << reason;
    local_cnt++;
}

void extPacketConnect( sf::Packet& packet, sf::Uint16& rem_cnt, sf::Uint16& game_ver, std::string& p_name ) {
    packet >> rem_cnt >> game_ver >> p_name;
}

void extPacketACK( sf::Packet& packet, sf::Uint16& seq_num, sf::Uint16& ack_num, sf::Uint32& bitfield ) {
	packet >> seq_num >> ack_num >> bitfield;
}
