
#include "Client.h"
#include <iostream>
#include <sstream>

extern sf::Uint16 PORT_NUM;
extern sf::IpAddress SERVER_IP_ADDR;

extern CGame g_game;

void CClient::error( std::string err_msg ) {
	std::cout << "Error: " << err_msg;
	if( err_msg.at(err_msg.size()-1)=='.' || err_msg.at(err_msg.size()-1)=='!' )
		std::cout << "\n";
}

void printBitf(sf::Uint32 bitfield ) {
    char c;
    sf::Uint32 num;
    sf::Uint32 mask;
    for( c=0; c<32; c++) {
        mask = 0;
        mask |= 1<<(31-c);
        num = bitfield & mask;
        num >>= (31-c);
        std::cout << num;
    }
}

CClient::CClient() :
	conn_state(CONN_NONE),
	local_cnt(0),
	//remote_cnt(0),
	local_ack_cnt(0),
	ack_bitf(0),
	remote_ack_cnt(0),
	remote_bitf(0),
	last_recv_time(sf::milliseconds(0)),
	node_ip("0.0.0.0"),
	port(0),
	player_id(0)
	{
	socket.setBlocking(false);
}

int CClient::broadCast( sf::Uint16 port_num ) {
	//
	sf::Packet p_get_srv_info;
	sf::Uint8 p_id = PC_GET_SERVER_INFO;
	p_get_srv_info << p_id;
	if ( socket.send( p_get_srv_info, sf::IpAddress::Broadcast, port_num) != sf::Socket::Done) {
		this->error( "Error sending data: broadcast PC_GET_SERVER_INFO." );
		return -1;
	}
	return 0;
}

int CClient::send( sf::Packet& s_packet, sf::IpAddress& ip_addr, sf::Uint16 s_port) {
	if( socket.send( s_packet, ip_addr, s_port) != sf::Socket::Done) {
		std::ostringstream port_str;
		port_str << s_port;
		this->error( "Can't send packet to: IP " + ip_addr.toString() + " |Port " + port_str.str() + "\n");
		return -1;
	}
	return 0;
}

int CClient::sendServer( sf::Packet& s_packet ) {
	return 0;
}


void CClient::updateLocalACKs( sf::Uint16 new_cnt ) {
    //if the received counter is more recent
    //std::cout << "New_cnt: " << new_cnt << " local_ack_cnt: " << local_ack_cnt <<"\n";
    //std::cout << "Old bitf: ";
    //printBitf(ack_bitf); std::cout << "\n";
    sf::Uint16 diff;
    bool more_recent = false;
    if( ( new_cnt > local_ack_cnt ) && ( new_cnt-local_ack_cnt <= 0xffff/2 ) ) {
        diff = new_cnt-local_ack_cnt;
        local_ack_cnt = new_cnt;   //is bigger
        more_recent = true;
    } else if( ( local_ack_cnt > new_cnt ) && ( local_ack_cnt-new_cnt > 0xffff/2 ) ) {
        diff = 0xffff-local_ack_cnt + new_cnt + 1;
        local_ack_cnt = new_cnt;   //wrap around
        more_recent = true;
    } else {
        //no wrap around
        if( local_ack_cnt >= new_cnt ) {
            diff = local_ack_cnt - new_cnt;
        } else {//new_cnt > remote cnt wrap around
            diff = 0xffff-new_cnt + local_ack_cnt + 1;
        }
        more_recent = false;
    }

    //std::cout << "Updated cnt: " << local_ack_cnt << " More recent: " << more_recent << " diff: " << diff << "\n";

    if( more_recent ) {
        //shift
        ack_bitf <<= diff;
        //add the previous cnt to flags
        if(diff == 1) {
            ack_bitf |= 1;
        } else if(diff < 33 && diff > 0) {
            ack_bitf |= (1<<(diff-1));
        }
    } else {
        //no shift
        //add flag
        if(diff == 1) {
            ack_bitf |= 1;
        } else if(diff < 33 && diff > 0) {
            ack_bitf |= (1<<(diff-1));
        }
    }

    //std::cout << "New bitf: ";
    //printBitf(ack_bitf);
    //std::cout << "\n";

}


sf::Int32 CClient::getCntDiff( sf::Uint16 new_cnt, sf::Uint16 most_recent_cnt ) {
    //newer: positive
    //older: negative
    sf::Int32 diff;
    if( ( new_cnt > most_recent_cnt ) && ( new_cnt-most_recent_cnt <= 0xffff/2 ) ) {
        //packet is more recent
        diff = new_cnt-most_recent_cnt;
    } else if( ( most_recent_cnt > new_cnt ) && ( most_recent_cnt-new_cnt > 0xffff/2 ) ) {
        //packet is more recent
        diff = 0xffff-most_recent_cnt + new_cnt + 1;
    } else {
        //packet is older or same
        //no wrap around
        if( most_recent_cnt >= new_cnt ) {
            diff =  new_cnt - most_recent_cnt;
        } else { //new_cnt > remote cnt wrap around
            diff = -(0xffff-new_cnt + most_recent_cnt + 1);
        }
    }
    return diff;
}

bool CClient::wasPacketACKed( sf::Uint16 p_cnt ) {
    if( p_cnt == remote_ack_cnt )
        return true;
    sf::Int32 diff = getCntDiff( p_cnt, remote_ack_cnt );
    if( diff > 0 )
        return false;
    if( diff == 0 )
        return true;
    diff = -diff;   //make it positive
    //Attention: if p_cnt is off the bitfield, function will always return false
    sf::Uint16 mask = 1<<(diff-1);
    bool wasACKed;
    //printBitf(ack_bitf); std::cout << "|bool ";
    if( ack_bitf&mask )
        wasACKed = true;
    else wasACKed = false;
    return wasACKed; //TODO: TEST test test
}

//0 receive
//1 didn't receive
//-1 error
int CClient::check( sf::Time& max_wait_time ) {
	sf::IpAddress sender;
	sf::Uint16 recv_port;
	if ( socket.receive( latest_packet, sender, recv_port) != sf::Socket::Done) {
		return (1);	//didn't receive anything, this is not an error
	}
	node_ip = sender;
	port = recv_port;
	//received data
	return 0;
}

void CClient::handlePacket() {
	sf::Uint8 packet_type;
	latest_packet >> packet_type;
	//======================
	//PS_SERVER_INFO packet
	sf::Uint8 players_in;
	sf::Uint8 players_max;
	std::string game_name;
	sf::Packet re_packet;
	//////////
	std::string p_name = "Dude";
	
	if( conn_state == CONN_NONE ) {
		switch(packet_type) {
			case PS_SERVER_INFO:
				if( !(latest_packet >> players_in >> players_max >> game_name) ) {
					this->error("Packet unexpectedly short.");
					return;
				}
				std::cout << "Packet PS_SERVER_INFO\n";
				std::cout << "Players in: " << (int)players_in << " |Players max: " << (int)players_max << " |Game name: " << game_name << "\n";

				packetConnect( re_packet, GAME_VERSION, p_name );
				send( re_packet, SERVER_IP_ADDR, PORT_NUM);
				conn_state = CONN_INIT;
				last_recv_time = g_game.g_clock.getElapsedTime();

				break;
		    default:
		        this->error("Type of received packet is unkown.");
		        break;
		}
	} else if( conn_state == CONN_INIT ) {
		if(packet_type==P_ACK) {
			conn_state = CONN_EST;
			sf::Uint16 remote_cnt, ack_num;
			sf::Uint32 bitf;
			extPacketACK( latest_packet, remote_cnt, ack_num, bitf );
			updateLocalACKs( remote_cnt );
			remote_ack_cnt = ack_num;
			remote_bitf = bitf;
			std::cout << "Conn est, remote_cnt: " <<remote_cnt <<" |remote_ack_cnt: " << ack_num << "\nRemote bitf: ";
			printBitf(bitf);
			std::cout << "\n";
			last_recv_time = g_game.g_clock.getElapsedTime();
		} else this->error("Unexpected packet type in state CONN_INIT.");
	} else if( conn_state == CONN_EST ) {
		if(packet_type==P_ACK) {
			sf::Uint16 remote_cnt, ack_num;
			sf::Uint32 bitf;
			extPacketACK( latest_packet, remote_cnt, ack_num, bitf );
			updateLocalACKs( remote_cnt );
			remote_ack_cnt = ack_num;
			remote_bitf = bitf;
			std::cout << "ACK packet, remote_cnt: " <<remote_cnt <<" |remote_ack_cnt: " << ack_num << "\nRemote bitf: ";
			printBitf(bitf);
			std::cout << "\n";
			last_recv_time = g_game.g_clock.getElapsedTime();
		}
	} else this->error("Unkown Connection State.");
}

void CClient::sendAll() {
	while( !to_send.empty() ) {
		send( to_send.front(), node_ip, port);
		to_send.pop();
	}
}

bool CClient::isConnected() {
	if( conn_state == CONN_EST || conn_state == CONN_PEND )
		return true;
	else
		return false;
}

int CClient::getState() {
	return conn_state;
}

int CClient::send( sf::Packet& s_packet ) {
	if( socket.send( s_packet, node_ip, port) != sf::Socket::Done) {
		std::ostringstream port_str;
		port_str << port;
		this->error( "CClientConn Can't send packet to: IP " + node_ip.toString() + " |Port " + port_str.str() + "\n");
		return -1;
	}
	return 0;
}

void CClient::disconnect() {
	conn_state = CONN_DISC;
	sf::Packet packet;
	packetDisconnect( packet, REASON_PLAYER_LEAVE );
	send( packet );
}

void CClient::sendACK() {
    if(isConnected()) {
        sf::Packet ACKpacket;
        packetACK( ACKpacket );
		send( ACKpacket );
    }
}

