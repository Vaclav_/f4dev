
#ifndef CLIENT_H_
#define CLIENT_H_

#include <SFML/Network.hpp>
#include <vector>
#include <map>
#include <deque>
#include <queue>

enum eRefuseReason { REASON_UNKOWN=0, REASON_WRONG_VERSION, REASON_SERVER_FULL, REASON_ON_BANLIST, REASON_PLAYER_LEAVE };

const sf::Uint16 GAME_VERSION = 10;
const unsigned int MAX_STR_LEN = 16;
const unsigned int MAX_MESSAGE_LEN = 256;

enum ePacketType {
    PC_GET_SERVER_INFO,
    PS_SERVER_INFO,
    PC_CONNECT,
    P_ACK,
    PS_SERVER_DESC,
    PS_PLAYERS_LIST,
    PS_PLAYER_CHANGE,
    PC_READY,
    P_CHAT_MESSAGE,
    PS_START_GAME,
    P_DISCONNECT
};

enum eConnectionState { CONN_NONE, CONN_INIT, CONN_EST, CONN_PEND, CONN_DISC};

class CGame {
	public:
	sf::Clock g_clock;
};

//packet_type, need_to_ACK
extern std::map< sf::Uint8, bool> to_ACK;
void to_ACK_init();

void packetGetServerInfo( sf::Packet& packet );
void extPacketACK( sf::Packet& packet, sf::Uint16& seq_num, sf::Uint16& ack_num, sf::Uint32& bitfield );

//packet descriptor
//all info about a packet already sent
class CPacketDesc {
    public:
    CPacketDesc() : msg_id(0), ACKed(0) {}
    CPacketDesc( sf::Uint16 c, sf::Packet& p, sf::Time& t, sf::Uint8 m_id = 0 ) :
        cnt(c), packet(p), time(t), msg_id(m_id), ACKed(0) {}
    sf::Uint16 cnt;     //local counter of the packet
    sf::Packet packet;
    sf::Time time;      //what time it was sent
    sf::Uint8 msg_id;   //message_id, use 0 for none
    bool ACKed;         //if it was acknowledged already
};


class CClient {
	public:
	CClient();

	void packetConnect( sf::Packet& packet, sf::Uint16 game_version, std::string& player_name );
	void packetACK( sf::Packet& packet );
	void packetReady( sf::Packet& packet, sf::Uint8 ready_un_r );
	void packetChatMessage( sf::Packet& packet, sf::Uint8 msg_id, sf::Uint8 player_id, std::string& message );
	void packetDisconnect( sf::Packet& packet, sf::Uint8 reason );

	int broadCast( sf::Uint16 port_num );
	bool connect( sf::IpAddress& ip_addr, sf::Uint16 port_num, unsigned int retries );
	bool isConnected();
	int getState();
	
	int send( sf::Packet& s_packet );
	int send( sf::Packet& s_packet, sf::IpAddress& ip_addr, sf::Uint16 s_port);
	int sendServer( sf::Packet& s_packet );
	int check( sf::Time& max_wait_time );
	void handlePacket();
	void sendAll();

	void disconnect();

	void error( std::string err_msg );

	void sendACK();

    sf::Int32 getCntDiff( sf::Uint16 new_cnt, sf::Uint16 most_recent_cnt );
	void updateLocalACKs( sf::Uint16 new_cnt );

	bool wasPacketACKed( sf::Uint16 p_cnt );
	
	sf::Packet latest_packet;
	
	//connection state
	int conn_state;
	sf::UdpSocket socket;
	//our sequence number
	sf::Uint16 local_cnt;
	//the node's seq number received from acks
	//the newest packet we will ack
//	sf::Uint16 remote_cnt;
	//newest packet's sequence number that was acked
	sf::Uint16 local_ack_cnt;
	//our acks
	sf::Uint32 ack_bitf;
	sf::Uint16 remote_ack_cnt;
	//acks received from node
	sf::Uint32 remote_bitf;
	sf::Time last_recv_time;
	
	//Node = the other endpoint
	sf::IpAddress node_ip;
	sf::Uint16 port;

	sf::Uint8 player_id;
	std::string player_name;	//16 characters max

	std::deque<CPacketDesc> packets_sent;
	std::queue<sf::Packet> to_send;
	
};

#endif
