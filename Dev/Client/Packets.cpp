
#include "Client.h"

std::map< sf::Uint8, bool> to_ACK;
static bool to_ACK_was_setup = false;

void to_ACK_init() {
	if(!to_ACK_was_setup) {
		to_ACK[PC_GET_SERVER_INFO] = false;
		to_ACK[PS_SERVER_INFO] = false;
		to_ACK[PC_CONNECT] = true;
		to_ACK[P_ACK] = true;
		to_ACK[PS_SERVER_DESC] = true;
		to_ACK[PS_PLAYERS_LIST] = true;
		to_ACK[PS_PLAYER_CHANGE] = true;
		to_ACK[PC_READY] = true;
		to_ACK[P_CHAT_MESSAGE] = true;
		to_ACK[PS_START_GAME] = true;
		to_ACK[P_DISCONNECT] = true;
		to_ACK_was_setup = true;
	}
}

void packetGetServerInfo( sf::Packet& packet ) {
	sf::Uint8 type = PC_GET_SERVER_INFO;
	packet << type;
}
void CClient::packetConnect( sf::Packet& packet, sf::Uint16 game_version, std::string& player_name ) {
	sf::Uint8 type = PC_CONNECT;
	packet << type << local_cnt << game_version;
    if( player_name.length() > MAX_STR_LEN ) {
        std::string player_name_cut = player_name.substr( 0, MAX_STR_LEN);
        packet << player_name_cut;
    } else packet << player_name;
	local_cnt++;
}

void CClient::packetReady( sf::Packet& packet, sf::Uint8 ready_un_r ) {
	sf::Uint8 type = PC_READY;
	packet << type << local_cnt << ready_un_r;
	local_cnt++;
}

void CClient::packetACK( sf::Packet& packet ) {
    sf::Uint8 type = P_ACK;
    packet << type << local_cnt << local_ack_cnt << ack_bitf;
	local_cnt++;
}

void CClient::packetChatMessage( sf::Packet& packet, sf::Uint8 msg_id, sf::Uint8 player_id, std::string& message ) {
	sf::Uint8 type = P_CHAT_MESSAGE;
	packet << type << local_cnt << msg_id << player_id;
    if( message.length() > MAX_MESSAGE_LEN ) {
        std::string message_cut = message.substr( 0, MAX_MESSAGE_LEN);
        packet << message_cut;
    } else packet << message;
	local_cnt++;
}

void CClient::packetDisconnect( sf::Packet& packet, sf::Uint8 reason ) {
    sf::Uint8 type = P_DISCONNECT;
    packet << type << local_cnt << reason;
	local_cnt++;
}

void extPacketACK( sf::Packet& packet, sf::Uint16& seq_num, sf::Uint16& ack_num, sf::Uint32& bitfield ) {
	packet >> seq_num >> ack_num >> bitfield;
}
