#include <iostream>
#include <SFML/Network.hpp>
#include "Client.h"

sf::Uint16 PORT_NUM = 54000;
sf::IpAddress SERVER_IP_ADDR = "127.0.0.1";

CGame g_game;

int main(void)
{

	g_game.g_clock.restart();

    std::cout << "Client started.\n";

	to_ACK_init();
	CClient client;
	sf::Packet packet;
	sf::Uint8 type = PC_GET_SERVER_INFO;
	packet << type;
	client.send( packet, SERVER_IP_ADDR, PORT_NUM);

    sf::Time timeout = sf::milliseconds(500);

	sf::sleep( sf::milliseconds(200) );
    if( client.check( timeout ) == 0 ) {
        client.handlePacket();
    }

	sf::sleep( sf::milliseconds(200) );
    if( client.check( timeout ) == 0 ) {
        client.handlePacket();
    }

    sf::Time wait_until;
	sf::Time cur_t;
	int rec_n = 0;
    while( client.isConnected() ) {
		wait_until += timeout;
		while( wait_until > cur_t ) {
		    if( client.check( timeout ) == 0 ) {
		        client.handlePacket();
				rec_n++;
		    }
			cur_t = g_game.g_clock.getElapsedTime();
		}
		if( rec_n > 5 )
			client.disconnect();
		client.sendACK();
    }
	
	client.sendAll();
	
    std::cout << "Finished";

    return 0;
}
