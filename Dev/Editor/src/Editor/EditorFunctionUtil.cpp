
#include <Box2D/Box2D.h>
#include <vector>
#include <cmath>

bool checkIfClockwise(const b2Vec2* vs, int32 count) {
    //Code from Line 74 of Box2D/Box2D/Collision/Shapes/b2PolygonShape.cpp
	b2Assert(count >= 3);
	b2Vec2 c; c.Set(0.0f, 0.0f);
	float32 area = 0.0f;
	b2Vec2 pRef(0.0f, 0.0f);
	const float32 inv3 = 1.0f / 3.0f;
	for (int32 i = 0; i < count; ++i)
	{
		b2Vec2 p1 = pRef;
		b2Vec2 p2 = vs[i];
		b2Vec2 p3 = i + 1 < count ? vs[i+1] : vs[0];
		b2Vec2 e1 = p2 - p1;
		b2Vec2 e2 = p3 - p1;
		float32 D = b2Cross(e1, e2);
		float32 triangleArea = 0.5f * D;
		area += triangleArea;
		c += triangleArea * inv3 * (p1 + p2 + p3);
	}
	if(area > b2_epsilon)
        return true;
    else
        return false;
}

static int signFunc(float X) { return( X<0 ? -1 : 1); }

//One convexity test is to conceptually walk along the polygon edges
//and check if the sign of the change in x and y changes more than twice.
//If it does than the polygon is convex.
bool isPolygonConvex(const std::vector<b2Vec2>& O) {
	if(O.size()<3) return false;
	int XCh=0,YCh=0;
	b2Vec2 A = O.at(0)-O.at(1);
	b2Vec2 B;
	unsigned int I;
	for( I=1; I<O.size()-1; I++) {
        B = O.at(I)-O.at(I+1);
        if(signFunc(A.x)!=signFunc(B.x)) XCh++;
        if(signFunc(A.y)!=signFunc(B.y)) YCh++;
        A=B;
    }
    B = O.at(I)-O.at(0);
    if(signFunc(A.x)!=signFunc(B.x)) XCh++;
    if(signFunc(A.y)!=signFunc(B.y)) YCh++;
	return (XCh<=2 && YCh<=2);
}

//only for convex polygons
float getPolygonArea( const std::vector<b2Vec2>& points, b2Vec2& centroid) {
    std::vector<b2Vec2> triangles;
    unsigned int i;
    for( i=0; i<points.size()-1; i++) {
        triangles.push_back(centroid);
        triangles.push_back(points.at(i));
        triangles.push_back(points.at(i+1));
    }
    //the last triangle
    triangles.push_back(centroid);
    triangles.push_back(points.back());
    triangles.push_back(points.at(0));

    float area = 0.0f;
    float triangle_area;
    float s;
    b2Vec2 side_a, side_b, side_c;
    float a, b, c;
    for( i=0; i<triangles.size()-2; i+=3) {
        side_a = triangles.at(i+1)-triangles.at(i);
        side_b = triangles.at(i+2)-triangles.at(i+1);
        side_c = triangles.at(i)-triangles.at(i+2);
        a = side_a.Length();
        b = side_b.Length();
        c = side_c.Length();
        s = (a+b+c)/2;
        //Heron's formula
        triangle_area = s*(s-a)*(s-b)*(s-c);
        triangle_area = std::sqrt(triangle_area);
        area += triangle_area;
    }

    return area;
}
