
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <vector>
#include <assert.h>

#include "../Game.h"
#include "EditorObj.h"
#include "EditorFunction.h"
#include "b2dJson/b2dJson.h"
#include "b2dJson/b2dJsonImage.h"

void jsonDumpObjectProps( std::vector<CEditorObj*>& objects, b2dJson & json);
void jsonLoadObjectProps( std::vector<CEditorObj*>& objects, b2dJson & json);

void dumpEditorSprite( CEditorSprite* cur_sprite, b2dJson& json ) {
    assert( cur_sprite->body != NULL );

    json.setCustomInt( cur_sprite->body, "type", cur_sprite->type );

    switch(cur_sprite->type) {
        case EOBJ_GRAPHIC:
            json.setBodyName( cur_sprite->body, "t_graphic");
            break;
        case EOBJ_GND:
            json.setBodyName( cur_sprite->body, "t_sensor");
            json.setCustomInt( cur_sprite->body, "gnd_t", cur_sprite->gnd_type);
            break;
        case EOBJ_DYNAMIC:
            json.setBodyName( cur_sprite->body, "t_dymanic");
            break;
        case EOBJ_STATIC:
            json.setBodyName( cur_sprite->body, "t_static");
            break;
    }
    //for all
    json.setCustomInt( cur_sprite->body, "fR", cur_sprite->fill_color.r );
    json.setCustomInt( cur_sprite->body, "fG", cur_sprite->fill_color.g );
    json.setCustomInt( cur_sprite->body, "fB", cur_sprite->fill_color.b );
    json.setCustomInt( cur_sprite->body, "fA", cur_sprite->fill_color.a );
    json.setCustomInt( cur_sprite->body, "lR", cur_sprite->line_color.r );
    json.setCustomInt( cur_sprite->body, "lG", cur_sprite->line_color.g );
    json.setCustomInt( cur_sprite->body, "lB", cur_sprite->line_color.b );
    json.setCustomInt( cur_sprite->body, "lA", cur_sprite->line_color.a );
}

void jsonDumpObjectProps( std::vector<CEditorObj*>& objects, b2dJson& json) {
    unsigned int i;
    CEditorSprite* cur_sprite = NULL;
    CStartPoint* cur_sp = NULL;
    CFinishLine* cur_fl = NULL;
    for( i=0; i<objects.size(); i++) {
        assert( objects.at(i) != NULL );
        if( isEditorSprite(objects.at(i) ) ) {
            cur_sprite = (CEditorSprite*)objects.at(i);
            dumpEditorSprite( cur_sprite, json);
        } else if( objects.at(i)->type == EOBJ_START_POINT ) {
            cur_sp = (CStartPoint*)objects.at(i);
            json.setCustomInt( cur_sp->body, "type", objects.at(i)->type );
            json.setBodyName( cur_sp->body, "t_start_point");
            json.setCustomInt( cur_sp->body, "number", cur_sp->number );
        } else if( objects.at(i)->type == EOBJ_FINISH_LINE ) {
            cur_fl = (CFinishLine*)objects.at(i);
            json.setCustomInt( cur_fl->body, "type", objects.at(i)->type );
            json.setBodyName( cur_fl->body, "t_finish_line");
            json.setCustomVector( cur_fl->body, "point2", cur_fl->b2point2 );
        } else {
            g_game.error << "jsonDumpObjectProps: No such object type.";
        }
    }
}

void loadEditorSprite( int obj_type, b2Body* body, CEditorSprite* cur_sprite, b2dJson& json ) {
        b2Fixture* fx;
        cur_sprite->body = body;
        cur_sprite->type = obj_type;
        switch( obj_type ) {
            case EOBJ_GRAPHIC:
                break;
            case EOBJ_GND:
                cur_sprite->gnd_type = json.getCustomInt( body, "gnd_t");
                break;
            case EOBJ_DYNAMIC:
                break;
            case EOBJ_STATIC:
                break;
        }
        cur_sprite->fill_color.r = json.getCustomInt( body, "fR");
        cur_sprite->fill_color.g = json.getCustomInt( body, "fG");
        cur_sprite->fill_color.b = json.getCustomInt( body, "fB");
        cur_sprite->fill_color.a = json.getCustomInt( body, "fA");
        cur_sprite->line_color.r = json.getCustomInt( body, "lR");
        cur_sprite->line_color.g = json.getCustomInt( body, "lG");
        cur_sprite->line_color.b = json.getCustomInt( body, "lB");
        cur_sprite->line_color.a = json.getCustomInt( body, "lA");
        //should contain only one fixture
        fx = body->GetFixtureList();
        if(fx!=NULL)
            fx->SetUserData( cur_sprite );
}

void jsonLoadObjectProps( b2World* world, std::vector<CEditorObj*>& objects, b2dJson & json, float b2d_to_gfx) {
    objects.erase( objects.begin(), objects.end() );
    assert( world != NULL );
    //for all bodies in world
    b2Body* body;
    int obj_type;
    CEditorSprite* cur_sprite = NULL;
    CStartPoint* cur_sp = NULL;
    CFinishLine* cur_fl = NULL;
    for( body = world->GetBodyList(); body; body = body->GetNext() ) {
        obj_type = json.getCustomInt( body, "type");
        if( obj_type==EOBJ_DYNAMIC || obj_type==EOBJ_STATIC ||
           obj_type==EOBJ_GRAPHIC || obj_type==EOBJ_GND ) {
            cur_sprite = new CEditorSprite();
            objects.push_back( (CEditorObj*)cur_sprite );
            loadEditorSprite( obj_type, body, cur_sprite, json);
        } else if( obj_type == EOBJ_START_POINT ) {
            cur_sp = new CStartPoint();
            objects.push_back( (CEditorObj*)cur_sp );
            cur_sp->body = body;
            cur_sp->number = json.getCustomInt( body, "number");
            cur_sp->position.x = body->GetPosition().x*b2d_to_gfx;
            cur_sp->position.y = body->GetPosition().y*b2d_to_gfx;
        } else if( obj_type == EOBJ_FINISH_LINE ) {
            cur_fl = new CFinishLine();
            objects.push_back( (CEditorObj*)cur_fl );
            cur_fl->body = body;
            cur_fl->point1.x = body->GetPosition().x*b2d_to_gfx;
            cur_fl->point1.y = body->GetPosition().y*b2d_to_gfx;
            cur_fl->b2point2 = json.getCustomVector( body, "point2");
            cur_fl->point2.x = cur_fl->b2point2.x*b2d_to_gfx;
            cur_fl->point2.y = cur_fl->b2point2.y*b2d_to_gfx;
        }
    }
}
