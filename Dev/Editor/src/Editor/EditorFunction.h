#ifndef EDITORFUNCTION_H_INCLUDED
#define EDITORFUNCTION_H_INCLUDED

#include <Box2D/Box2D.h>
#include "EditorObj.h"

bool isEditorSprite( CEditorObj* obj );
void renderObject( CEditorObj* obj, sf::RenderWindow* win, sf::Font* r_font, float b2d_to_gfx, bool selection );
void renderEditorSprite( CEditorSprite* sprite_p, sf::RenderWindow* win, float b2d_to_gfx, bool selection );
void renderStartPoint( CStartPoint* start_p, sf::RenderWindow* win, sf::Font* r_font, float b2d_to_gfx, bool selection );
void renderFinishLine( CFinishLine* finish_l, sf::RenderWindow* win, float b2d_to_gfx, bool selection );
CEditorObj* getSelectedObject( std::vector<CEditorObj*>& e_objects, b2World* world_p, sf::Vector2f px_point, float gfx_to_b2d, sf::RenderWindow* win );

void createBox( CEObjDef& def, CEditorObj*& obj, b2World* world_p, sf::Vector2f p1, sf::Vector2f p2, float gfx_to_b2d );
void createCircle( CEObjDef& def, CEditorObj*& obj, b2World* world_p, sf::Vector2f p1, sf::Vector2f p2, float gfx_to_b2d );
void createChain( CEObjDef& def, CEditorObj*& obj, b2World* world_p, std::vector<sf::Vertex>& vertices, float gfx_to_b2d, bool looped );
void createPolygon( CEObjDef& def, CEditorObj*& obj, b2World* world_p, std::vector<sf::Vertex>& vertices, float gfx_to_b2d );
void createStartPoint( CEditorObj*& obj, b2World* world_p, int num, sf::Vector2f p1, float gfx_to_b2d );
void createFinishLine( CEditorObj*& obj, b2World* world_p, sf::Vector2f p1, sf::Vector2f p2, float gfx_to_b2d );

int getNextStartPoint( std::vector<CEditorObj*>& e_objects );
void removeFinishLine( std::vector<CEditorObj*>& e_objects );

#endif // EDITORFUNCTION_H_INCLUDED
