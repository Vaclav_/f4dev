
#include <sstream>

#include "../Game.h"
#include <Box2D/Box2D.h>
#include "EditorFunction.h"
#include "EditorObj.h"

#define RADTODEG 180.0f/3.14159265359f

extern bool checkIfClockwise(const b2Vec2* vs, int32 count);
extern bool isPolygonConvex(const std::vector<b2Vec2>& O);
extern float getPolygonArea( const std::vector<b2Vec2>& points, b2Vec2& centroid);

static sf::Color sel_fill_color = sf::Color::Red;
static sf::Color sel_line_color = sf::Color::Magenta;
static float sel_width = 4.0f;
static float line_width = 0.0f;

static float start_pos_sel_radius = 18.0f;

bool isEditorSprite( CEditorObj* obj ) {
    if( obj->type==EOBJ_DYNAMIC || obj->type==EOBJ_GRAPHIC ||
          obj->type==EOBJ_STATIC || obj->type==EOBJ_GND )
        return true;
    else return false;
}

static void normalizeVect( sf::Vector2f& vect ) {
		float length = b2Sqrt( vect.x * vect.x + vect.y * vect.y);
		if (length < b2_epsilon) {
			return;
		}
		float invLength = 1.0f / length;
		vect.x *= invLength;
		vect.y *= invLength;
		return;
}

void renderObject( CEditorObj* obj, sf::RenderWindow* win, sf::Font* r_font, float b2d_to_gfx, bool selection ) {

    if( obj == NULL )  { g_game.error << "renderObject: obj is NULL."; return; }

    if( isEditorSprite(obj) ) {
        //it's safe to cast now
        CEditorSprite* sprite_p = (CEditorSprite*)obj;
        renderEditorSprite( sprite_p, win, b2d_to_gfx, selection );
    } else if( obj->type == EOBJ_START_POINT ) {
        CStartPoint* sp = (CStartPoint*)obj;
        renderStartPoint( sp, win, r_font, b2d_to_gfx, selection );
    } else if( obj->type == EOBJ_FINISH_LINE ) {
        CFinishLine* fl = (CFinishLine*)obj;
        renderFinishLine( fl, win, b2d_to_gfx, selection );
    } else {
        g_game.error << "renderObject: unkown object type: " << obj->type << " .";
    }

}

void renderStartPoint( CStartPoint* start_p, sf::RenderWindow* win, sf::Font* r_font, float b2d_to_gfx, bool selection ) {
    float w_zoom = win->getView().getSize().x/(float)g_game.scrnW;

    if( selection ) {
        sf::CircleShape circle(start_pos_sel_radius*w_zoom);
        circle.setFillColor(sf::Color::Magenta);
        sf::FloatRect circle_size = circle.getLocalBounds();
        circle.setOrigin( circle_size.width/2, circle_size.height/2 );
        circle.setPosition( start_p->position );
        win->draw( circle );
        return;
    }

    float len = 20.0f*w_zoom;
    float width = 2.0f*w_zoom;
    float outline = 4.0f*w_zoom;
    sf::RectangleShape line1(sf::Vector2f( width, len));
    sf::RectangleShape line2(sf::Vector2f( width, len));
    sf::RectangleShape outline1(sf::Vector2f( width+outline, len+outline));
    sf::RectangleShape outline2(sf::Vector2f( width+outline, len+outline));
    line1.setFillColor(sf::Color::Red);
    line2.setFillColor(sf::Color::Red);
    outline1.setFillColor(sf::Color::Black);
    outline2.setFillColor(sf::Color::Black);
    line1.setOrigin( width/2, len/2);
    line2.setOrigin( width/2, len/2);
    outline1.setOrigin( (width+outline)/2, (len+outline)/2);
    outline2.setOrigin( (width+outline)/2, (len+outline)/2);
    line1.setRotation( 45.0f );
    line2.setRotation( 135.0f );
    outline1.setRotation( 45.0f );
    outline2.setRotation( 135.0f );
    line1.setPosition( start_p->position );
    line2.setPosition( start_p->position );
    outline1.setPosition( start_p->position );
    outline2.setPosition( start_p->position );
    win->draw( outline1 );
    win->draw( outline2 );
    win->draw( line1 );
    win->draw( line2 );

    sf::Text n_label;
    sf::Text text_outline;

    if(r_font!=NULL) {
        n_label.setFont(*r_font);
        text_outline.setFont(*r_font);
    }
    else
        return;

    float t_outline_size = 5.0f;
    sf::Vector2f text_offset( 15.0f*w_zoom, 15.0f*w_zoom );
    std::stringstream ss;

    ss << start_p->number;
    n_label.setString(ss.str());
    text_outline.setString(ss.str());
    //!TODO: this gives blurred result for too small size, but the other cases look good
    n_label.setCharacterSize(15.0f*w_zoom);
    text_outline.setCharacterSize((15.0f+t_outline_size)*w_zoom);
    n_label.setColor(sf::Color::White);
    text_outline.setColor(sf::Color::Black);
    sf::FloatRect l_size = n_label.getLocalBounds();
    n_label.setOrigin( l_size.left+l_size.width/2, l_size.top+l_size.height/2 );
    l_size = text_outline.getLocalBounds();
    text_outline.setOrigin( l_size.left+l_size.width/2, l_size.top+l_size.height/2 );
    n_label.setPosition( start_p->position - text_offset );
    text_outline.setPosition( start_p->position - text_offset );
    win->draw( text_outline );
    win->draw( n_label );

}

void renderFinishLine( CFinishLine* finish_l, sf::RenderWindow* win, float b2d_to_gfx, bool selection ) {
    float w_zoom = win->getView().getSize().x/(float)g_game.scrnW;

    if( selection ) {
        sf::CircleShape circle(start_pos_sel_radius*w_zoom);
        circle.setFillColor(sf::Color::Magenta);
        sf::FloatRect circle_size = circle.getLocalBounds();
        circle.setOrigin( circle_size.width/2, circle_size.height/2 );
        circle.setPosition( finish_l->point1 );
        win->draw( circle );
        return;
    }

    std::vector<sf::Vertex> line;
    line.push_back(sf::Vertex());
    line.back().position = finish_l->point1;
    line.back().color = sf::Color::Red;
    line.push_back(sf::Vertex());
    line.back().position = finish_l->point2;
    line.back().color = sf::Color::Red;
    win->draw( &line.at(0), line.size(), sf::Lines);

    float len = 10.0f*w_zoom;
    float width = 2.0f*w_zoom;
    float outline = 4.0f*w_zoom;
    sf::RectangleShape line1(sf::Vector2f( width, len));
    sf::RectangleShape line2(sf::Vector2f( width, len));
    sf::RectangleShape outline1(sf::Vector2f( width+outline, len+outline));
    sf::RectangleShape outline2(sf::Vector2f( width+outline, len+outline));
    line1.setFillColor(sf::Color::Blue);
    line2.setFillColor(sf::Color::Blue);
    outline1.setFillColor(sf::Color::Black);
    outline2.setFillColor(sf::Color::Black);
    line1.setOrigin( width/2, len/2);
    line2.setOrigin( width/2, len/2);
    outline1.setOrigin( (width+outline)/2, (len+outline)/2);
    outline2.setOrigin( (width+outline)/2, (len+outline)/2);
    line1.setRotation( 45.0f );
    line2.setRotation( 135.0f );
    outline1.setRotation( 45.0f );
    outline2.setRotation( 135.0f );
    line1.setPosition( finish_l->point1 );
    line2.setPosition( finish_l->point1 );
    outline1.setPosition( finish_l->point1 );
    outline2.setPosition( finish_l->point1 );
    win->draw( outline1 );
    win->draw( outline2 );
    win->draw( line1 );
    win->draw( line2 );

}

void renderEditorSprite( CEditorSprite* sprite_p, sf::RenderWindow* win, float b2d_to_gfx, bool selection ) {

    if( sprite_p->body == NULL )  { g_game.error << "renderObject: obj->body is NULL."; return; }

    //the body should contain only one fixture, render the first fixture only
    b2Fixture* fx = sprite_p->body->GetFixtureList();
    b2CircleShape* circle_shape;
    sf::CircleShape render_circle;
    b2PolygonShape* polygon_shape;
    sf::ConvexShape r_convex;
    b2ChainShape* chain_shape;
    b2Vec2 b2_point;
    int32 cur_p;
    sf::Transform rotation;
    sf::Vector2f t_vect;
    int t = fx->GetType();
    const int t_polygon = b2Shape::e_polygon;
    const int t_circle = b2Shape::e_circle;
    const int t_chain = b2Shape::e_chain;
    float w_zoom;
    b2Vec2 length_vect;

    switch( t ) {
        case t_polygon:
            polygon_shape = (b2PolygonShape*) fx->GetShape();
            w_zoom = win->getView().getSize().x/(float)g_game.scrnW;
            if(selection) {
                r_convex.setFillColor(sel_fill_color);
                r_convex.setOutlineColor(sel_line_color);
                r_convex.setOutlineThickness(w_zoom*sel_width);
            } else {
                r_convex.setFillColor(sprite_p->fill_color);
                r_convex.setOutlineColor(sprite_p->line_color);
                r_convex.setOutlineThickness(w_zoom*line_width);
            }
            rotation.rotate( sprite_p->body->GetAngle()*RADTODEG );
            // sprite_p->body->GetAngle()
            r_convex.setPointCount(polygon_shape->GetVertexCount());
            for( cur_p = 0; cur_p<polygon_shape->GetVertexCount(); cur_p++ ) {
                b2_point = polygon_shape->GetVertex(cur_p);
                t_vect.x = b2d_to_gfx*b2_point.x;
                t_vect.y = b2d_to_gfx*b2_point.y;
                t_vect = rotation.transformPoint(t_vect);
                t_vect.x += b2d_to_gfx*sprite_p->body->GetPosition().x;
                t_vect.y += b2d_to_gfx*sprite_p->body->GetPosition().y;
                r_convex.setPoint( cur_p, t_vect);
            }
            win->draw(r_convex);

        break;
        case t_circle:
            circle_shape = (b2CircleShape*)fx->GetShape();
            w_zoom = win->getView().getSize().x/(float)g_game.scrnW;
            render_circle.setRadius( circle_shape->m_radius*b2d_to_gfx );
            if(selection) {
                render_circle.setFillColor(sel_fill_color);
                render_circle.setOutlineColor(sel_line_color);
                render_circle.setOutlineThickness(w_zoom*sel_width);
            } else {
                render_circle.setFillColor(sprite_p->fill_color);
                render_circle.setOutlineColor(sprite_p->line_color);
                render_circle.setOutlineThickness(w_zoom*line_width);
            }
            b2_point =sprite_p->body->GetPosition() + circle_shape->m_p;
            b2_point.x-=circle_shape->m_radius;
            b2_point.y-=circle_shape->m_radius;
            render_circle.setPosition( b2_point.x*b2d_to_gfx, b2_point.y*b2d_to_gfx);
            win->draw(render_circle);
        break;
        case t_chain:
            rotation.rotate( sprite_p->body->GetAngle()*RADTODEG );
/*
            w_zoom = win->getView().getSize().x/(float)g_game.scrnW;*/

            chain_shape = (b2ChainShape*)fx->GetShape();
            std::vector<sf::Vertex> vertices;
            if( !selection ) {
                for( cur_p=0; cur_p<chain_shape->m_count; cur_p++) {
                    vertices.push_back(sf::Vertex());
                    t_vect.x = (*(chain_shape->m_vertices+cur_p)).x*b2d_to_gfx;
                    t_vect.y = (*(chain_shape->m_vertices+cur_p)).y*b2d_to_gfx;
                    t_vect = rotation.transformPoint(t_vect);
                    vertices.back().position.x = t_vect.x+sprite_p->body->GetPosition().x*b2d_to_gfx;
                    vertices.back().position.y = t_vect.y+sprite_p->body->GetPosition().y*b2d_to_gfx;
                    vertices.back().color = sprite_p->fill_color;
                }
                win->draw( &vertices.at(0), vertices.size(), sf::LinesStrip);
            } else { //render selection with sf::Quads
                sf::Vector2f normal_vect;
                sf::Vector2f first_p;
                sf::Vector2f prev_point;
                w_zoom = win->getView().getSize().x/(float)g_game.scrnW;
                float temp;

                for( cur_p=0; cur_p<chain_shape->m_count; cur_p++) {
                    t_vect.x = (*(chain_shape->m_vertices+cur_p)).x*b2d_to_gfx;
                    t_vect.y = (*(chain_shape->m_vertices+cur_p)).y*b2d_to_gfx;
                    t_vect = rotation.transformPoint(t_vect);
                    t_vect.x += sprite_p->body->GetPosition().x*b2d_to_gfx;
                    t_vect.y += sprite_p->body->GetPosition().y*b2d_to_gfx;
                    if( cur_p!= 0 ) {
                        //load it
                        if( cur_p>1 )
                            first_p = prev_point;
                        //save it
                        prev_point = t_vect;
                        normal_vect = t_vect-first_p;
                        normalizeVect( normal_vect );
                        normal_vect.x *= w_zoom*sel_width/2.0f;
                        normal_vect.y *= w_zoom*sel_width/2.0f;
                        first_p -= normal_vect;
                        t_vect += normal_vect;
                        temp = normal_vect.x;
                        normal_vect.x = - normal_vect.y;
                        normal_vect.y = temp;
                        normalizeVect( normal_vect );
                        normal_vect.x *= w_zoom*sel_width/2.0f;
                        normal_vect.y *= w_zoom*sel_width/2.0f;
                        vertices.push_back(sf::Vertex());
                        vertices.back().position = first_p+normal_vect;
                        vertices.back().color = sel_line_color;
                        vertices.push_back(sf::Vertex());
                        vertices.back().position = first_p-normal_vect;
                        vertices.back().color = sel_line_color;
                        vertices.push_back(sf::Vertex());
                        vertices.back().position = t_vect-normal_vect;
                        vertices.back().color = sel_line_color;
                        vertices.push_back(sf::Vertex());
                        vertices.back().position = t_vect+normal_vect;
                        vertices.back().color = sel_line_color;
                    } else
                        first_p = t_vect;
                }
                win->draw( &vertices.at(0), vertices.size(), sf::Quads);
            }
        break;
    }
}


CEditorObj* getSelectedObject( std::vector<CEditorObj*>& e_objects, b2World* world_p, sf::Vector2f px_point, float gfx_to_b2d, sf::RenderWindow* win ) {
    if(world_p==NULL) { g_game.error << "getSelectedObject: world_p is NULL."; return NULL; }
    CEditorObj* obj = NULL;
    CEditorSprite* sprite_p = NULL;
    b2Vec2 b2d_point;
    b2d_point.x = px_point.x*gfx_to_b2d;
    b2d_point.y = px_point.y*gfx_to_b2d;
    b2Body* c_body;
    b2Fixture* fx;
    int i;
    b2ChainShape* chain_shape;
    b2AABB edge_aabb;
    b2Transform body_transform;
    bool got_it = false;    //found the first intersecting object
    bool is_start_point = false;

    CStartPoint* sp = NULL;
    CFinishLine* fl = NULL;
    b2Vec2 diff_vect;
    float w_zoom = win->getView().getSize().x/(float)g_game.scrnW;
    float sel_radius = start_pos_sel_radius*w_zoom;

    //check Starting Points first
    for( i=0; (unsigned int)i<e_objects.size(); i++) {
        if( e_objects.at(i)->type == EOBJ_START_POINT ) {
            sp = (CStartPoint*)e_objects.at(i);
            diff_vect.x = sp->position.x - px_point.x;
            diff_vect.y = sp->position.y - px_point.y;
            if( diff_vect.Length() < sel_radius ) {
                obj = e_objects.at(i);
                got_it = true;
                is_start_point = true;
                break;
            }
        }
        if( e_objects.at(i)->type == EOBJ_FINISH_LINE ) {
            fl = (CFinishLine*)e_objects.at(i);
            diff_vect.x = fl->point1.x - px_point.x;
            diff_vect.y = fl->point1.y - px_point.y;
            if( diff_vect.Length() < sel_radius ) {
                obj = e_objects.at(i);
                got_it = true;
                is_start_point = true;
                break;
            }
        }
    }

    for( c_body = world_p->GetBodyList(); c_body && !got_it; c_body = c_body->GetNext()) {
        for ( fx = c_body->GetFixtureList(); fx; fx = fx->GetNext()) {
            if( fx->TestPoint(b2d_point) ) {
                sprite_p = (CEditorSprite*)fx->GetUserData();
                got_it = true;
                break;
            } else if( fx->GetType() == b2Shape::e_chain ) { //if it's chainshape
                //Loop through each child edge
                chain_shape = (b2ChainShape*)fx->GetShape();
                for ( i = 0; i < chain_shape->GetChildCount(); i++) {
                    body_transform = c_body->GetTransform();
                    chain_shape->ComputeAABB( &edge_aabb, body_transform, i);
                    if( edge_aabb.lowerBound.x<b2d_point.x && b2d_point.x<edge_aabb.upperBound.x &&
                       edge_aabb.lowerBound.y<b2d_point.y && b2d_point.y<edge_aabb.upperBound.y ) {
                       sprite_p = (CEditorSprite*)fx->GetUserData();
                       got_it = true;
                       break;
                    }
                }
                if(got_it) break;
            }
        }
        if(got_it) break;
    }

    if( got_it && !is_start_point )
        obj = (CEditorObj*)sprite_p;
    return obj;
}

void createBox( CEObjDef& def, CEditorObj*& obj, b2World* world_p, sf::Vector2f p1, sf::Vector2f p2, float gfx_to_b2d ) {
    CEditorSprite* sprite_p = new CEditorSprite();
    obj = (CEditorObj*)sprite_p;
    sprite_p->type = def.type;
    sprite_p->fill_color = def.fill_color;
    sprite_p->line_color = def.line_color;
    sprite_p->gnd_type = def.gnd_type;
    b2Vec2 pos, b_size;
    b_size.x = (p2.x-p1.x)/2.0f;
    b_size.y = (p1.y-p2.y)/2.0f;
    pos.x = (p1.x+ b_size.x)*gfx_to_b2d;
    pos.y = (p2.y+ b_size.y)*gfx_to_b2d;
    b_size = (double)gfx_to_b2d*b_size;
    if(b_size.x<0.0f) b_size.x*=-1.0f;
    if(b_size.y<0.0f) b_size.y*=-1.0f;
    b2BodyDef myBodyDef;
    b2FixtureDef boxFixtureDef;
    switch( def.type ) {
        case EOBJ_DYNAMIC:
            myBodyDef.type = b2_dynamicBody;
        break;
        case EOBJ_GND:
            boxFixtureDef.isSensor = true;
        break;
        case EOBJ_GRAPHIC:
        break;
        case EOBJ_STATIC:
            myBodyDef.type = b2_staticBody;
        break;
        default:
        break;
    }
    myBodyDef.linearDamping = def.lin_damping;
    myBodyDef.angularDamping = def.ang_damping;
    myBodyDef.position.Set( pos.x, pos.y );
    myBodyDef.angle = 0.0f;
    sprite_p->body = world_p->CreateBody(&myBodyDef);
    b2PolygonShape boxShape;
    boxShape.SetAsBox( b_size.x, b_size.y );
    boxFixtureDef.shape = &boxShape;
    boxFixtureDef.restitution = def.restitution;
    float box_density = def.mass/(b_size.x*gfx_to_b2d*b_size.y*gfx_to_b2d*4);
    if(box_density<0.0f) box_density*=-1.0f;
    boxFixtureDef.density = box_density;
    b2Fixture* fx;
    fx = sprite_p->body->CreateFixture(&boxFixtureDef);
    fx->SetUserData(sprite_p);
}

void createCircle( CEObjDef& def, CEditorObj*& obj, b2World* world_p, sf::Vector2f p1, sf::Vector2f p2, float gfx_to_b2d ) {
    CEditorSprite* sprite_p = new CEditorSprite();
    obj = (CEditorObj*)sprite_p;
    sprite_p->type = def.type;
    sprite_p->fill_color = def.fill_color;
    sprite_p->line_color = def.line_color;
    sprite_p->gnd_type = def.gnd_type;
    b2Vec2 r, pos;
    r.x = (p2.x-p1.x)/2;
    r.y = (p2.y-p1.y)/2;
    pos.x = p1.x+r.x;
    pos.y = p1.y+r.y;
    b2BodyDef myBodyDef;
    b2FixtureDef myFixtureDef;
    switch( def.type ) {
        case EOBJ_DYNAMIC:
            myBodyDef.type = b2_dynamicBody;
        break;
        case EOBJ_GND:
            myFixtureDef.isSensor = true;
        break;
        case EOBJ_GRAPHIC:
        break;
        case EOBJ_STATIC:
            myBodyDef.type = b2_staticBody;
        break;
        default:
        break;
    }
    myBodyDef.linearDamping = def.lin_damping;
    myBodyDef.angularDamping = def.ang_damping;
    myBodyDef.position.Set( pos.x*gfx_to_b2d, pos.y*gfx_to_b2d );
    myBodyDef.angle = 0.0f;
    sprite_p->body = world_p->CreateBody(&myBodyDef);
    b2CircleShape circleShape;
    circleShape.m_radius = r.Length()*gfx_to_b2d;
    myFixtureDef.shape = &circleShape;
    myFixtureDef.restitution = def.restitution;
    //V = m/(r^2*pi)
    float circle_density = def.mass/(r.Length()*gfx_to_b2d*r.Length()*gfx_to_b2d*b2_pi);
    myFixtureDef.density = circle_density;
    b2Fixture* fx;
    fx = sprite_p->body->CreateFixture(&myFixtureDef);
    fx->SetUserData(sprite_p);
}

void createStartPoint( CEditorObj*& obj, b2World* world_p, int num, sf::Vector2f p1, float gfx_to_b2d ) {
    CStartPoint* sp = new CStartPoint( num, p1);
    obj = (CEditorObj*)sp;
    b2BodyDef myBodyDef;
    myBodyDef.position.Set( p1.x*gfx_to_b2d, p1.y*gfx_to_b2d);
    sp->body = world_p->CreateBody( &myBodyDef );
}

void createFinishLine( CEditorObj*& obj, b2World* world_p, sf::Vector2f p1, sf::Vector2f p2, float gfx_to_b2d ) {
    CFinishLine* fl = new CFinishLine( p1, p2);
    obj = (CEditorObj*)fl;
    b2BodyDef myBodyDef;
    myBodyDef.position.Set( p1.x*gfx_to_b2d, p1.y*gfx_to_b2d);
    fl->body = world_p->CreateBody( &myBodyDef );
    fl->b2point2.x = p2.x*gfx_to_b2d;
    fl->b2point2.y = p2.y*gfx_to_b2d;
}

static float CCW( b2Vec2& a, b2Vec2& b, b2Vec2& c) {
    return (b.x-a.x)*(c.y-a.y) - (b.y-a.y)*(c.x-a.x);
}
static bool intersect(b2Vec2& a, b2Vec2& b, b2Vec2& c, b2Vec2& d) {
    return (CCW(a,b,c)*CCW(a,b,d)<0 && CCW(c,d,b)*CCW(c,d,a)<0);
}

void createChain( CEObjDef& def, CEditorObj*& obj, b2World* world_p, std::vector<sf::Vertex>& vertices, float gfx_to_b2d, bool looped ) {

    std::vector<b2Vec2> vs;
    unsigned int i;
    for( i=0; i<vertices.size(); i++)
        vs.push_back(b2Vec2( (vertices.at(i).position.x-vertices.at(0).position.x)*gfx_to_b2d, (vertices.at(i).position.y-vertices.at(0).position.y)*gfx_to_b2d));

    unsigned int i_base = 0;
    unsigned int i_top;
    bool inters = false;
    for( i_top = 3; i_top<vs.size(); i_top++ ) {
        for( i_base=0; i_base<i_top-2; i_base++) {
            if( intersect(vs.at(i_base), vs.at(i_base+1), vs.at(i_top), vs.at(i_top-1)) ) {
                inters = true;
                break;
            }
        }
        if(inters) break;
    }
    if(inters) {
        obj = NULL;
        return;
    }

    CEditorSprite* sprite_p = new CEditorSprite();
    obj = (CEditorObj*)sprite_p;

    sprite_p->type = def.type;
    sprite_p->fill_color = def.fill_color;
    sprite_p->line_color = def.line_color;
    sprite_p->gnd_type = def.gnd_type;

    b2ChainShape chain;
    if(looped)
        chain.CreateLoop( &vs.at(0), vs.size());
    else
        chain.CreateChain( &vs.at(0), vs.size());

    b2BodyDef myBodyDef;
    b2FixtureDef myFixtureDef;
    switch( def.type ) {
        case EOBJ_DYNAMIC:
            //CHAIN SHAPE MUST BE STATIC
            myBodyDef.type = b2_staticBody;
            g_game.error << "Chain shapes must be static bodies.";
        break;
        case EOBJ_GND:
            //A Chain shape being a sensor has no sense
            //myFixtureDef.isSensor = true;
        break;
        case EOBJ_GRAPHIC:
            //Grahpic is ok
        break;
        case EOBJ_STATIC:
            myBodyDef.type = b2_staticBody;
        break;
        default:
        break;
    }
    myBodyDef.linearDamping = def.lin_damping;
    myBodyDef.angularDamping = def.ang_damping;
    myBodyDef.position.Set( vertices.at(0).position.x*gfx_to_b2d, vertices.at(0).position.y*gfx_to_b2d );
    myBodyDef.angle = 0.0f;
    sprite_p->body = world_p->CreateBody(&myBodyDef);
    myFixtureDef.shape = &chain;
    myFixtureDef.restitution = def.restitution;
    b2Fixture* fx;
    fx = sprite_p->body->CreateFixture(&myFixtureDef);
    fx->SetUserData(sprite_p);
}

//check point count DONE
//check if convex DONE
//check for clockw order DONE
void createPolygon( CEObjDef& def, CEditorObj*& obj, b2World* world_p, std::vector<sf::Vertex>& vertices, float gfx_to_b2d ) {

    std::vector<b2Vec2> vs;
    unsigned int i;
    b2Vec2 centroid( 0.0f, 0.0f);
    for( i=0; i<vertices.size(); i++) {
        centroid.x += vertices.at(i).position.x;
        centroid.y += vertices.at(i).position.y;
    }
    centroid.x /= vertices.size();
    centroid.y /= vertices.size();

    for( i=0; i<vertices.size(); i++)
        vs.push_back(b2Vec2( (vertices.at(i).position.x-centroid.x)*gfx_to_b2d, (vertices.at(i).position.y-centroid.y)*gfx_to_b2d));

    //check number of points, return if invalid
    if( vs.size()<3 || vs.size()>b2_maxPolygonVertices ) {
        obj = NULL;
        return;
    }
    //the polygon must be convex
    if( !isPolygonConvex( vs ) ) {
        obj = NULL;
        return;
    }
    //check if it's clockwise
    if( !checkIfClockwise( &vs.at(0), vs.size() ) ) {
        //try to reverse it and check again
        std::vector<b2Vec2> vs2;
        for( i=vs.size()-1; 1; i--) {
            vs2.push_back(vs.at(i));
            if(i==0) break;
        }
        //then copy back to original
        for( i=0; i<vs.size(); i++) {
            vs.at(i) = vs2.at(i);
        }
        //check again, return if invalid
        if( !checkIfClockwise( &vs.at(0), vs.size() ) ) {
            obj = NULL;
            return;
        }
    }

    CEditorSprite* sprite_p = new CEditorSprite();
    obj = (CEditorObj*)sprite_p;

    sprite_p->type = def.type;
    sprite_p->fill_color = def.fill_color;
    sprite_p->line_color = def.line_color;
    sprite_p->gnd_type = def.gnd_type;

    b2PolygonShape polygon;
    polygon.Set( &vs.at(0), vs.size());

    b2BodyDef myBodyDef;
    b2FixtureDef myFixtureDef;
    switch( def.type ) {
        case EOBJ_DYNAMIC:
            myBodyDef.type = b2_dynamicBody;
        break;
        case EOBJ_GND:
            myFixtureDef.isSensor = true;
        break;
        case EOBJ_GRAPHIC:
        break;
        case EOBJ_STATIC:
            myBodyDef.type = b2_staticBody;
        break;
        default:
        break;
    }
    myBodyDef.linearDamping = def.lin_damping;
    myBodyDef.angularDamping = def.ang_damping;
    myBodyDef.position.Set( centroid.x*gfx_to_b2d, centroid.y*gfx_to_b2d );
    myBodyDef.angle = 0.0f;
    sprite_p->body = world_p->CreateBody(&myBodyDef);
    myFixtureDef.shape = &polygon;
    if( def.type == EOBJ_DYNAMIC ) {
        float area = getPolygonArea( vs, centroid);
        //R = m/V
        if(area != 0.0f)
            myFixtureDef.density = def.mass/area;
    }
    myFixtureDef.restitution = def.restitution;
    b2Fixture* fx;
    fx = sprite_p->body->CreateFixture(&myFixtureDef);
    fx->SetUserData(sprite_p);
}


int getNextStartPoint( std::vector<CEditorObj*>& e_objects ) {

    std::vector<int> numbers;
    unsigned int i;
    CStartPoint* sp = NULL;

    for( i=0; i<e_objects.size(); i++) {
        if( e_objects.at(i)!=NULL ) {
            if( e_objects.at(i)->type == EOBJ_START_POINT ) {
                sp = (CStartPoint*)e_objects.at(i);
                numbers.push_back( sp->number );
            }
        }
    }

    std::sort( numbers.begin(), numbers.end() );
    int cur_n = 1;
    for( i=0; i<numbers.size(); i++) {
        if( numbers.at(i) != cur_n )
            return cur_n;   //there is a gap..
        else
            cur_n++;
    }

    return cur_n;    //the number after the biggest

}

void removeFinishLine( std::vector<CEditorObj*>& e_objects ) {
    unsigned int i;
    for( i=0; i<e_objects.size(); i++) {
        if( e_objects.at(i)->type == EOBJ_FINISH_LINE ) {
            e_objects.erase( e_objects.begin()+i, e_objects.begin()+i+1 );
            break;
        }
    }
}
