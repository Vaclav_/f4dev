
#include "EditorObj.h"

CEditorSprite::CEditorSprite() :
    gnd_type(0),
    fill_color( sf::Color::White ),
    line_color( sf::Color::Yellow )
    {
}

void CEditorSprite::move( sf::Vector2f dir, float gfx_to_b2d ) {
    if(body!=NULL) {
        b2Vec2 cur_pos = body->GetPosition();
        b2Vec2 move_offs;
        move_offs.x = dir.x*gfx_to_b2d;
        move_offs.y = dir.y*gfx_to_b2d;
        body->SetTransform( cur_pos+move_offs, body->GetAngle());
    }
}

void CEditorSprite::rotate( sf::Vector2f dir, float gfx_to_b2d ) {
    if(body!=NULL) {
        float ang = body->GetAngle();
        b2Vec2 cur_pos = body->GetPosition();
        b2Vec2 rot_offs;
        rot_offs.x = dir.x*gfx_to_b2d;
        rot_offs.y = dir.y*gfx_to_b2d;
        if(dir.y<0.0f)
            ang -= (rot_offs.Length()*0.075f);
        else
            ang += (rot_offs.Length()*0.075f);

        body->SetTransform( cur_pos, ang );
    }
}

bool fetchEditorObjProp( CEditorObj* obj, CEObjDef& obj_def ) {
    if( obj == NULL ||
        ( obj->type!=EOBJ_DYNAMIC && obj->type!=EOBJ_GRAPHIC &&
          obj->type!=EOBJ_STATIC && obj->type!=EOBJ_GND) ) {
        obj_def.type = 0;
        obj_def.gnd_type = 0;
        obj_def.restitution = 0.0f;
        obj_def.mass = 1.0f;
        obj_def.lin_damping = 0.0f;
        obj_def.ang_damping = 0.0f;
        obj_def.fill_color = sf::Color::White;
        obj_def.line_color = sf::Color::Black;
        return false;
    }
    obj_def.type = obj->type;
    //it's safe to cast now
    CEditorSprite* sprite_p = (CEditorSprite*)obj;

    //error
    if( sprite_p->body == NULL || sprite_p->body->GetFixtureList() == NULL )
        return false;

    obj_def.gnd_type = sprite_p->gnd_type;
    b2Fixture* fx;
    //the body should contain only one fixture
    fx = sprite_p->body->GetFixtureList();
    obj_def.restitution = fx->GetRestitution();
    obj_def.mass = sprite_p->body->GetMass();
    obj_def.lin_damping = sprite_p->body->GetLinearDamping();
    obj_def.ang_damping = sprite_p->body->GetAngularDamping();
    obj_def.fill_color = sprite_p->fill_color;
    obj_def.line_color = sprite_p->line_color;

    return true;
}

void setEditorObjProp( CEditorObj* obj, CEObjDef& obj_def ) {
    if( obj == NULL ||
       ( obj->type!=EOBJ_DYNAMIC && obj->type!=EOBJ_GRAPHIC &&
          obj->type!=EOBJ_STATIC && obj->type!=EOBJ_GND) ) {
        return;
    }

    //don't set type
    //it's safe to cast now
    CEditorSprite* sprite_p = (CEditorSprite*)obj;

    if( sprite_p->body == NULL || sprite_p->body->GetFixtureList() == NULL )    //error
        return;

    sprite_p->gnd_type = obj_def.gnd_type;

    b2Fixture* fx;
    b2MassData md;
    sprite_p->body->GetMassData( &md );
    md.mass = obj_def.mass;
    sprite_p->body->SetMassData( &md );
    //the body should contain only one fixture
    fx = sprite_p->body->GetFixtureList();
    fx->SetRestitution(obj_def.restitution);
    sprite_p->body->SetLinearDamping(obj_def.lin_damping);
    sprite_p->body->SetAngularDamping(obj_def.ang_damping);
    sprite_p->fill_color = obj_def.fill_color;
    sprite_p->line_color = obj_def.line_color;
}

