#ifndef EDITORSAVESTATE_H_INCLUDED
#define EDITORSAVESTATE_H_INCLUDED

#include <SFML/Graphics.hpp>
#include "../GameState/GameState.h"
#include "../Graphics/GUI.h"
#include "../Util.h"
#include "EditorGUI.h"

//implements pre singleplayer state
//inherits from CGameState, see description there
class CEditorSaveState : public CGameState {

	public:
	CEditorSaveState() : do_sleep(true), next_state(STATE_EDITOR_SAVE), path_error(false) {}
	~CEditorSaveState() {}

	int loadState();
	void leaveState();

	void handleEvent( sf::Event& event );
	void step();
	void render();

	//runs only if do_sleep is false, must not block
	void idle( sf::Time remaining_time );

	void setNextState( int new_next_state ) { next_state = new_next_state; }
	int getNextState() { return next_state; }

    //if true, the app sleeps until the next frame, else it runs the idle function
	bool do_sleep;

	protected:
	int next_state;		//next game state
	CTextureManager manager;

    bool path_error;
	sf::Text label0;
	sf::Text label_error;
	CInputBox inp_path;
	CButton button_back;
	CButton button_ok;
};

#endif // EDITORSAVESTATE_H_INCLUDED
