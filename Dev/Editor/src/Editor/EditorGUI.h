#ifndef EDITORGUI_H_INCLUDED
#define EDITORGUI_H_INCLUDED

#include <SFML/Graphics.hpp>

//base class
class CGUIBox {
    public:
    CGUIBox() {}
    virtual ~CGUIBox() {}

    virtual bool mouseDown( int cX, int cY) = 0;
    virtual bool isActive() = 0;
    virtual void select() = 0;
    virtual void deSelect() = 0;
};

class CNumInput : public CGUIBox {
public:
	CNumInput();
	~CNumInput() {}

	//void setTexture( sf::Texture* tex );

	void setFont( sf::Font* font );
	//pX: x position; pY: y position (top left corner)
	//sX: x size, sY: ySize (of the panel, NOT the texture)
	void setTextureParam( unsigned int digits, int pX, int pY, int sY, sf::Color col = sf::Color( 0xff, 0xff, 0xff) );
	//text properties
	void setLabelParam( std::string default_text, unsigned char text_size, sf::Color col = sf::Color( 0xff, 0xff, 0xff) );
	void textEntered( char ch );
    //mouse left click is held event, at screen coordinates x: cY, y: cY
	bool mouseDown( int cX, int cY);
	//activate by software
	void select();
	void deSelect();

    void eventEnter();
    void eventDel(); //delete character
    void moveCursorLeft();
    void moveCursorRight();

    bool keyPress( int key_code );

    bool isActive();

    void enable();
    bool disable();

    void erase();
    void setContent( int num);
    int getContent();

    void setMax( int m);

	//draw the button
	void render( sf::RenderWindow* r_window );

private:
    int max_value;
    bool enabled;
    unsigned int max_digits;
	bool active;
	std::string content;
	bool content_changed;
	sf::Text render_text;
	//sf::Sprite panel;
    sf::RectangleShape panel;
    sf::RectangleShape cursor;
	int pos_x, pos_y;
	int size_x, size_y;
	//cursor positin, in characters
	unsigned int cur_pos;
	bool cur_blink_state;
	unsigned int blink_period;  //in ms
	unsigned int blink_cur_time;
	sf::Color panel_color;
};

class CChoiceBox : public CGUIBox {
public:
	CChoiceBox();
	~CChoiceBox() {}
	//void setTexture( sf::Texture* tex );
	void setFont( sf::Font* font );
	//pX: x position; pY: y position (top left corner)
	//sX: x size, sY: ySize (of the panel, NOT the texture)
	void setTextureParam( int pX, int pY, int sX, int sY, sf::Color color_pas = sf::Color( 0xff, 0xff, 0xff), sf::Color color_act = sf::Color( 0xff, 0x0, 0x0) );
	//text properties
	void setLabelParam( unsigned char text_size, sf::Color col = sf::Color( 0xff, 0xff, 0xff) );
    //mouse left click is held event, at screen coordinates x: cY, y: cY
	bool mouseDown( int cX, int cY);
	//activate by software
	void select();
	void deSelect();
    void eventEnter();
    void buttonLeft();
    void buttonRight();
    bool isActive();
    void setChoice( int c);
    int getChoice();
    void keyPress( int key_code );
	//draw the button
	void render( sf::RenderWindow* r_window );
    std::vector<std::string> elements;

    void enable();
    bool disable();

    void disable_one( int which );
    void enable_one( int which );
    void enable_all();

    bool onList( int w );
    int getNextLeft( int cur );
    int getNextRight( int cur );


private:
    bool enabled;
	bool active;
	bool content_changed;
	sf::Text render_text;
    sf::RectangleShape panel;
	int pos_x, pos_y;
	int size_x, size_y;
	int cur_element;
	sf::Color col_active;
	sf::Color col_passive;
	std::vector<int> disabled_elements;
};

class CSelectBox : public CGUIBox {
public:
	CSelectBox();
	~CSelectBox();
	void setTexture( sf::Texture* tex );
	//pX: x position; pY: y position (both are CENTER);
	//sX: x size, sY: ySize (of the button, NOT the texture)
	void setTextureParam( int pX, int pY, int sX, int sY, sf::Color col_pas = sf::Color( 0xff, 0xff, 0xff), sf::Color col_act = sf::Color( 0xff, 0x00, 0x00) );
    bool mouseDown( int cX, int cY);
    bool isActive();
    void select();
    void deSelect();
	void render( sf::RenderWindow* r_window );

private:
	bool state;
	sf::Sprite sprite;
	int x, y;
	int sizeX, sizeY;
	sf::Color col_passive;
	sf::Color col_active;
};

#endif // EDITORGUI_H_INCLUDED
