#ifndef EDITOROBJ_H_INCLUDED
#define EDITOROBJ_H_INCLUDED

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

enum eEditorObjType { EOBJ_GRAPHIC, EOBJ_GND, EOBJ_DYNAMIC, EOBJ_STATIC, EOBJ_START_POINT, EOBJ_FINISH_LINE};

class CEObjDef {
    public:
    int type;
    int gnd_type;
    float restitution;
    float mass;
    float lin_damping;
    float ang_damping;
    sf::Color fill_color;
    sf::Color line_color;
};

class CEditorObj {
    public:
    CEditorObj() : type(0), body(NULL) {}
    int type;
    b2Body* body;
};

class CEditorSprite : public CEditorObj {
    public:
    CEditorSprite();
    int gnd_type;
    sf::Color fill_color;
    sf::Color line_color;

    void move( sf::Vector2f dir, float gfx_to_b2d );
    void rotate( sf::Vector2f dir, float gfx_to_b2d );
};

class CStartPoint : public CEditorObj {
    public:
    CStartPoint() : number(0) {  type = EOBJ_START_POINT; }
    CStartPoint( int n, sf::Vector2f& pos ) :
        number(n),
        position(pos) {
            type = EOBJ_START_POINT;
    }

    int number;
    sf::Vector2f position;
};

class CFinishLine : public CEditorObj {
    public:
    CFinishLine() { type = EOBJ_FINISH_LINE; }
    CFinishLine( sf::Vector2f& p1, sf::Vector2f& p2) :
        point1(p1),
        point2(p2),
        b2point2( 0.0f, 0.0f) {
        type = EOBJ_FINISH_LINE;
    }

    //p1 is the first point, it must be at the inner side of the track
    sf::Vector2f point1;
    sf::Vector2f point2;
    //first point in the body position
    b2Vec2 b2point2;
};

bool fetchEditorObjProp( CEditorObj* obj, CEObjDef& obj_def );
void setEditorObjProp( CEditorObj* obj, CEObjDef& obj_def );

#endif // EDITOROBJ_H_INCLUDED
