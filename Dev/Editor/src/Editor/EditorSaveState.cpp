#include <SFML/Graphics.hpp>
#include "../Game.h"
#include "EditorSaveState.h"
#include "../Util.h"
#include <vector>
#include <string>
#include <stdio.h>

extern std::string g_json_str;

static int COL_OFFSET = 35;
static int COL_HEIGHT = 35;
static int ROW_OFFSET = 35;
static int ROW_WIDTH = 175;
static int LABEL_Y_OFFSET = 7;

int CEditorSaveState::loadState() {
    sf::Font* font = manager.getFont("gfx/DejaVuSansMono.ttf");
    if(font!=NULL)
        label0.setFont(*font);
    label0.setCharacterSize(14);
    label0.setColor(sf::Color::White);
    label0.setPosition( ROW_OFFSET, COL_OFFSET+LABEL_Y_OFFSET);
    label0.setString( "Save path (relative to app directory)" );

    if(font!=NULL)
        label_error.setFont(*font);
    label_error.setCharacterSize(14);
    label_error.setColor(sf::Color::Red);
    label_error.setStyle(sf::Text::Bold);
    label_error.setPosition( ROW_OFFSET, COL_OFFSET+COL_HEIGHT*2+LABEL_Y_OFFSET);
    label_error.setString( "Error: no such directory!" );

    if(font!=NULL)
        inp_path.setFont(font);
    inp_path.setTextureParam( ROW_OFFSET, COL_OFFSET+COL_HEIGHT, 400, 30, sf::Color(245,245,245) );
    inp_path.setLabelParam( "maps/file.map", 14, 48, sf::Color::Black );

    sf::Texture* loaded_tex = NULL;
    loaded_tex = manager.getTexture("gfx/Exit.png");
    if( loaded_tex != NULL )
        button_back.setTexture(loaded_tex);
    button_back.setTextureParam( ROW_OFFSET+25, COL_OFFSET+COL_HEIGHT*4, 32, 32 );

    loaded_tex = manager.getTexture("gfx/OK.bmp");
    if( font!=NULL )
        button_ok.setFont(font);
    if( loaded_tex != NULL )
        button_ok.setTexture(loaded_tex);
    button_ok.setTextureParam( ROW_OFFSET+ROW_WIDTH, COL_OFFSET+COL_HEIGHT*4, 64, 32 );
    button_ok.setLabelParam( "Save", 16, sf::Color::Black );

	return 0;
}

void CEditorSaveState::leaveState() {
}

void CEditorSaveState::handleEvent( sf::Event& event ) {
    switch(event.type) {
        case sf::Event::KeyPressed:
            if( inp_path.isActive() ) {
                if( event.key.code == sf::Keyboard::Left )
                    inp_path.moveCursorLeft();
                else if( event.key.code == sf::Keyboard::Right )
                    inp_path.moveCursorRight();
                else if( event.key.code == sf::Keyboard::Delete )
                    inp_path.eventDel();
            } else if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape )
                next_state = STATE_EDITOR;
        break;

        case sf::Event::MouseButtonPressed:
            button_back.mouseDown(event.mouseButton.x, event.mouseButton.y);
            button_ok.mouseDown(event.mouseButton.x, event.mouseButton.y);
            inp_path.mouseDown( event.mouseButton.x, event.mouseButton.y);
        break;

        case sf::Event::MouseButtonReleased:
            button_back.mouseUp( event.mouseButton.x, event.mouseButton.y);
            button_ok.mouseUp( event.mouseButton.x, event.mouseButton.y);
        break;

        case sf::Event::TextEntered:
        if (event.text.unicode < 128) {
            if( inp_path.isActive() )
                inp_path.textEntered(static_cast<char>(event.text.unicode));
        }
        break;
        default:
        break;
    }
}

void CEditorSaveState::step() {
    if( button_back.check() )
        next_state = STATE_EDITOR;
    if( button_ok.check() ) {
        //check if path is valid
        std::string path_str;
        inp_path.getContent(path_str);
        FILE* fp = fopen( path_str.c_str(), "w");
        if(fp!=NULL) {//seems to be OK
            fprintf( fp, "%s", g_json_str.c_str());
            fclose(fp);
            path_error = false;
            next_state = STATE_EDITOR;
        }
        else {
            path_error = true;
        }
    }
}

void CEditorSaveState::render() {
    g_game.window->clear(sf::Color::Black);
    g_game.window->draw( label0 );
    if(path_error)
        g_game.window->draw( label_error );
    inp_path.render( g_game.window );
    button_back.render( g_game.window );
    button_ok.render( g_game.window );
    g_game.window->display();
}

//runs only if do_sleep is false, must not block
void CEditorSaveState::idle( sf::Time remaining_time ) {
}
