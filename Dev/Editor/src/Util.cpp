
#include "Game.h"
#include "Util.h"
#include <stdio.h>
#include <iostream>

//return number of characters read or EOF
int scanWord( FILE* fp, int max_len, char* buf) {
    if( fp == NULL || buf == NULL ) return 0;
    if( max_len==0 ) { *buf = 0; return 0; }
    int i;
    bool end_of_f = false;
    for( i=0; i<max_len-1; i++) {
        //get one char
        if( fscanf( fp, "%c", buf+i ) == EOF ) {
            end_of_f = true;
            break;
        }
        //read until
        if( *(buf+i)==' ' || *(buf+i)=='\t' || *(buf+i)=='\n' ) {
            break; //we will overwrite this character with 0
        }
    }
    //0 terminate the string
    *(buf+i) = 0;
    if(end_of_f)
        return EOF;
    else
        return i;
}

COutput COutput::operator<<(const std::string& msg) {
    if( turned_on ) {
        if( is_newline && type_msg != "" ) {
            std::cout << type_msg;
            is_newline = false;
        }
        std::cout << msg;
        char lastCh = *msg.rbegin();
        if( insert_newline && (lastCh == '.' || lastCh == '!' || lastCh == '?')) {
            std::cout << "\n";
            is_newline = true;
        } else if( lastCh == '\n')
            is_newline = true;
    }
    return *this;
};
/*
COutput COutput::operator<<(const bool b) {
    if( turned_on ) {
        if(b)
            std::cout << "TRUE";
        else
            std::cout << "FALSE";
    }
    return *this;
}
*/
COutput COutput::operator<<(const unsigned ui) {
    if( turned_on )
        std::cout << ui;
    return *this;
}

COutput COutput::operator<<(const int i) {
    if( turned_on )
        std::cout << i;
    return *this;
}

COutput COutput::operator<<(const float f) {
    if( turned_on )
        std::cout << f;
    return *this;
}

COutput COutput::operator<<(const double d) {
    if( turned_on )
        std::cout << d;
    return *this;
}

COutput COutput::operator<<(const b2Vec2& vec2) {
    if( turned_on )
        std::cout << "x " << vec2.x << " |y " << vec2.y;
    return *this;
}

sf::Texture* CTextureManager::addTexture( const sf::Texture& tex, std::string& name ) {
    textures.push_back( new sf::Texture() );
	if( textures.back() == NULL ) {
		g_game.error <<  "Allocating has failed.";
		textures.pop_back();
		return NULL;
	}
	//copy the texture
    *textures.back() = tex;
    texture_paths.push_back( name );
    return textures.back();
}

//if the texture at parameter path hasn't been loaded yet
//then loads it and returns a pointer
//if it was loaded already, finds it and returns a pointer
sf::Texture* CTextureManager::getTexture( std::string path ) {

	//search for texure name in std::vector<std::string> texture_paths
	unsigned int i;
	bool found = false;
	for( i=0; i<texture_paths.size() && !found; i++) {
        if( texture_paths.at(i) == path )
            found = true;
	}

	i--;
    if(found) {
        return textures.at(i);
    }
    //else load it

	textures.push_back( new sf::Texture() );

	if( textures.back() == NULL ) {
		g_game.error <<  "Allocating on the heap has failed when trying to open: " + path + "." ;
		textures.pop_back();
		return NULL;
	}

	//.back is the last element of the vector
	if ( !textures.back()->loadFromFile( path.c_str() ) )
	{
		g_game.error << "Can't load texture: " + path + "." ;
		textures.pop_back();
		return NULL;
	} else {
		texture_paths.push_back( path );
		return textures.back();
	}

//	} else return &textures.at(0);
}

//the same as getTexture(..) but with fonts
sf::Font* CTextureManager::getFont( std::string path ) {

	//search for texure name in font_paths
	unsigned int i;
	bool found = false;
	for( i=0; i<font_paths.size() && !found; i++) {
        if( font_paths.at(i) == path )
            found = true;
	}

	i--;
    if(found) {
        return fonts.at(i);
    }
    //else load it

    fonts.push_back( new sf::Font() );

	if( fonts.back() == NULL ) {
		g_game.error <<  "Allocating on the heap has failed when trying to open: " + path + "." ;
		fonts.pop_back();
		return NULL;
	}

	//.back is the last element
	if ( !fonts.back()->loadFromFile( path.c_str() ) )
	{
		g_game.error << "Can't load font: " + path + ".";
		fonts.pop_back();
		return NULL;
	} else {
		font_paths.push_back( path );
		return fonts.back();
	}
}

void CTextureManager::freeTextures() {
    while( textures.size() != 0 ) {
        if( textures.back() != NULL )
            delete textures.back();
        textures.pop_back();
    }
    while( texture_paths.size() != 0 )
        texture_paths.pop_back();
}

void CTextureManager::freeFonts() {
    while( fonts.size() != 0 ) {
        if( fonts.back() != NULL )
            delete fonts.back();
        fonts.pop_back();
    }
    while( font_paths.size() != 0 ) {
        font_paths.pop_back();
    }
}

void CTextureManager::freeAll() {
    freeTextures();
    freeFonts();
}

//delay until cur_clock reaches time, precise
void delayUntil( sf::Clock& cur_clock, const sf::Time& time ) {
    //try to sleep as little as possible
    while( cur_clock.getElapsedTime() < time-sf::milliseconds(5) ) {
        sf::sleep( sf::milliseconds(1) );
    }
    //burn the remaining time
    while( cur_clock.getElapsedTime() < time ) {
    }
}
