#ifndef _UTIL_H_
#define _UTIL_H_

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <iostream>

class COutput {
    public:
    COutput() : turned_on(true), type_msg(""), is_newline(true), insert_newline(true) {
        std::cout.precision(1);
        std::cout << std::fixed;
    };
    COutput operator<<(const std::string& msg);
    COutput operator<<(const unsigned ui);
    COutput operator<<(const int i);
    COutput operator<<(const float f);
    COutput operator<<(const double d);
    COutput operator<<(const b2Vec2& vec2);

    bool turned_on;
    std::string type_msg;
    private:
    bool is_newline;
    bool insert_newline;
};

//use this class, and NOT SFML directly to load textures/fonts!
//prevents to load a texture or a font twice
class CTextureManager {

	public:
	//if the texture at parameter path hasn't been loaded yet
	//then loads it and returns a pointer
	//if it was loaded already, finds it and returns a pointer
	sf::Texture* getTexture( std::string path );
	sf::Font* getFont( std::string path );
    sf::Texture* addTexture( const sf::Texture& tex, std::string& name );

    //delete textures/fonts -> free memory
    void freeTextures();
    void freeFonts();
    void freeAll();

	private:
	std::vector<sf::Texture*> textures;
	std::vector<std::string> texture_paths;
	std::vector<sf::Font*> fonts;
	std::vector<std::string> font_paths;

};

//delay until cur_clock reaches time, precise
void delayUntil( sf::Clock& cur_clock, const sf::Time& time );

#endif

