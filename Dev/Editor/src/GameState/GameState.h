#ifndef _STATE_H_
#define _STATE_H_

#include <SFML/Graphics.hpp>
#include "../Util.h"


//CGameState types
//int next_state in CGameState
//and int active_state in CGame
enum eGameStates { STATE_MENU, STATE_PRE_SP, STATE_SP, STATE_PRE_MP, STATE_MP_LOBBY, STATE_MP, STATE_SETTINGS,
    STATE_CREDITS, STATE_EDITOR, STATE_EDITOR_SAVE, STATE_EDITOR_LOAD };

//ancestor (ős) class for gamestates
class CGameState {

	public:
	CGameState() {}
	virtual ~CGameState() {}
	virtual int loadState() { return -1; }
	virtual void leaveState() {}

    //handle keyboard/mouse/window events, one event at once
	virtual void handleEvent( sf::Event& event ) {}
	//calculations for current frame go here
	virtual void step() {}
	//draw frame
	virtual void render() {}

	//runs only if do_sleep is false, must not block
	virtual void idle( sf::Time remaining_time ) {}

    //set next_state to this state to stay
    //and another to leave the state
	virtual void setNextState( int new_next_state ) { next_state = new_next_state; }
	virtual int getNextState() { return next_state; }

    //if true, the app sleeps until the next frame, else it runs the idle function
	bool do_sleep;

	protected:
	int next_state;		//next game state
	//always load textures/fonts with this class, not with SFML directly
	CTextureManager manager;
};

#endif
