
#include <SFML/Graphics.hpp>
#include "../Game.h"
#include "EditorState.h"
#include "../Util.h"
#include "../Editor/EditorFunction.h"
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cmath>

static bool DEBUG_DRAW_ON = false;

std::string g_json_str = "";

extern void jsonDumpObjectProps( std::vector<CEditorObj*>& objects, b2dJson & json);
extern void jsonLoadObjectProps( b2World* world, std::vector<CEditorObj*>& objects, b2dJson & json, float b2d_to_gfx);

CEditorState::CEditorState() :
    do_sleep(true),
    next_state(STATE_EDITOR),
    b2d_to_gfx(25.0f),
    m_world(NULL),
    COL_OFFSET(10),
    COL_HEIGHT(35),
    ROW_WIDTH(175),
    ROW_OFFSET(375),
    LABEL_Y_OFFSET(7),
    LABEL_COLOR(sf::Color::Yellow),
    LABEL_CH_SIZE(14),
    LABELS_NUM(23),
    selected_obj(NULL),
    editor_state(ESTATE_NONE),
    prev_editor_state(ESTATE_NONE),
    move_object(false),
    rot_object(false),
    ref_body(NULL) {
    cur_pos_vert.color = sf::Color::Red;
}

int CEditorState::loadState() {

    sf::Font* font;
    font = manager.getFont("gfx/DejaVuSansMono.ttf");
    editor_font = font;

    sf::FloatRect bounds;

    unsigned int l;
    for( l=0; l<LABELS_NUM; l++) {
        labels.push_back(sf::Text());
        if(font!=NULL)
            labels.at(l).setFont(*font);
        labels.at(l).setCharacterSize(LABEL_CH_SIZE);
        labels.at(l).setColor(LABEL_COLOR);
    }

    labels.at(0).setString("Object type:");
    labels.at(0).setPosition( ROW_OFFSET, COL_OFFSET+LABEL_Y_OFFSET);

    type_selector.setFont(font);
    type_selector.setTextureParam( ROW_OFFSET+ROW_WIDTH, COL_OFFSET, 100, 30, sf::Color(245,245,245, 255) );
    type_selector.setLabelParam( 14, sf::Color::Black );
    type_selector.elements.push_back("<Graphic>");
    type_selector.elements.push_back("<Sensor>");
    type_selector.elements.push_back("<Dynamic>");
    type_selector.elements.push_back("<Static>");

    gnd_selector.setFont(font);
    gnd_selector.setTextureParam( ROW_OFFSET+80, COL_OFFSET+COL_HEIGHT*2, 75, 30, sf::Color(245,245,245, 255) );
    gnd_selector.setLabelParam( 14, sf::Color::Black );
    gnd_selector.elements.push_back("<Road>");
    gnd_selector.elements.push_back("<Dirt>");

    labels.at(1).setString("Sensor");
    labels.at(1).setPosition( ROW_OFFSET, COL_OFFSET+COL_HEIGHT+LABEL_Y_OFFSET);
    labels.at(2).setString("Static");
    labels.at(2).setPosition( ROW_OFFSET+ROW_WIDTH, COL_OFFSET+COL_HEIGHT+LABEL_Y_OFFSET);
    labels.at(3).setString("Dynamic");
    labels.at(3).setPosition( ROW_OFFSET+ROW_WIDTH*2, COL_OFFSET+COL_HEIGHT+LABEL_Y_OFFSET);
    labels.at(4).setString("Graphics");
    labels.at(4).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT+LABEL_Y_OFFSET);
    labels.at(5).setString("Line color");
    labels.at(5).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*2+LABEL_Y_OFFSET);
    labels.at(6).setString("R");
    labels.at(6).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*3+LABEL_Y_OFFSET);
    labels.at(7).setString("G");
    labels.at(7).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*4+LABEL_Y_OFFSET);
    labels.at(8).setString("B");
    labels.at(8).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*5+LABEL_Y_OFFSET);
    labels.at(9).setString("A");
    labels.at(9).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*6+LABEL_Y_OFFSET);
    labels.at(10).setString("Fill color");
    labels.at(10).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*7+LABEL_Y_OFFSET);
    labels.at(11).setString("R");
    labels.at(11).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*8+LABEL_Y_OFFSET);
    labels.at(12).setString("G");
    labels.at(12).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*9+LABEL_Y_OFFSET);
    labels.at(13).setString("B");
    labels.at(13).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*10+LABEL_Y_OFFSET);
    labels.at(14).setString("A");
    labels.at(14).setPosition( ROW_OFFSET+ROW_WIDTH*3, COL_OFFSET+COL_HEIGHT*11+LABEL_Y_OFFSET);
    labels.at(15).setString("Mass");
    labels.at(15).setPosition( ROW_OFFSET+ROW_WIDTH*2, COL_OFFSET+COL_HEIGHT*2+LABEL_Y_OFFSET);
    labels.at(16).setString("L damp");
    labels.at(16).setPosition( ROW_OFFSET+ROW_WIDTH*2, COL_OFFSET+COL_HEIGHT*3+LABEL_Y_OFFSET);
    labels.at(17).setString("A damp");
    labels.at(17).setPosition( ROW_OFFSET+ROW_WIDTH*2, COL_OFFSET+COL_HEIGHT*4+LABEL_Y_OFFSET);
    labels.at(18).setString("Rest.");
    labels.at(18).setPosition( ROW_OFFSET+ROW_WIDTH, COL_OFFSET+COL_HEIGHT*2+LABEL_Y_OFFSET);
    labels.at(19).setString("Gnd type");
    labels.at(19).setPosition( ROW_OFFSET, COL_OFFSET+COL_HEIGHT*2+LABEL_Y_OFFSET);
    labels.at(20).setString(".");
    labels.at(20).setPosition( ROW_OFFSET+ROW_WIDTH*2+95, COL_OFFSET+COL_HEIGHT*3+LABEL_Y_OFFSET);
    labels.at(20).setColor(sf::Color::Black);
    labels.at(21).setString(".");
    labels.at(21).setPosition( ROW_OFFSET+ROW_WIDTH*2+95, COL_OFFSET+COL_HEIGHT*4+LABEL_Y_OFFSET);
    labels.at(21).setColor(sf::Color::Black);
    labels.at(22).setString(".");
    labels.at(22).setColor(sf::Color::Black);
    labels.at(22).setPosition( ROW_OFFSET+ROW_WIDTH+95, COL_OFFSET+COL_HEIGHT*2+LABEL_Y_OFFSET);

    int C_OFFS = 25;

    if(font!=NULL) {
        inp_line_col_R.setFont(font);
        inp_line_col_R.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*3+C_OFFS, COL_OFFSET+COL_HEIGHT*3, 30, sf::Color(245,245,245) );
        inp_line_col_R.setLabelParam( "0", 14, sf::Color::Black );
        inp_line_col_R.setMax(255);
        inp_line_col_R.disable();

        inp_line_col_G.setFont(font);
        inp_line_col_G.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*3+C_OFFS, COL_OFFSET+COL_HEIGHT*4, 30, sf::Color(245,245,245) );
        inp_line_col_G.setLabelParam( "0", 14, sf::Color::Black );
        inp_line_col_G.setMax(255);
        inp_line_col_G.disable();

        inp_line_col_B.setFont(font);
        inp_line_col_B.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*3+C_OFFS, COL_OFFSET+COL_HEIGHT*5, 30, sf::Color(245,245,245) );
        inp_line_col_B.setLabelParam( "0", 14, sf::Color::Black );
        inp_line_col_B.setMax(255);
        inp_line_col_B.disable();

        inp_line_col_A.setFont(font);
        inp_line_col_A.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*3+C_OFFS, COL_OFFSET+COL_HEIGHT*6, 30, sf::Color(245,245,245) );
        inp_line_col_A.setLabelParam( "255", 14, sf::Color::Black );
        inp_line_col_A.setMax(255);
        inp_line_col_A.disable();

        inp_fill_col_R.setFont(font);
        inp_fill_col_R.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*3+C_OFFS, COL_OFFSET+COL_HEIGHT*8, 30, sf::Color(245,245,245) );
        inp_fill_col_R.setLabelParam( "255", 14, sf::Color::Black );
        inp_fill_col_R.setMax(255);

        inp_fill_col_G.setFont(font);
        inp_fill_col_G.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*3+C_OFFS, COL_OFFSET+COL_HEIGHT*9, 30, sf::Color(245,245,245) );
        inp_fill_col_G.setLabelParam( "255", 14, sf::Color::Black );
        inp_fill_col_G.setMax(255);

        inp_fill_col_B.setFont(font);
        inp_fill_col_B.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*3+C_OFFS, COL_OFFSET+COL_HEIGHT*10, 30, sf::Color(245,245,245) );
        inp_fill_col_B.setLabelParam( "255", 14, sf::Color::Black );
        inp_fill_col_B.setMax(255);

        inp_fill_col_A.setFont(font);
        inp_fill_col_A.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*3+C_OFFS, COL_OFFSET+COL_HEIGHT*11, 30, sf::Color(245,245,245) );
        inp_fill_col_A.setLabelParam( "255", 14, sf::Color::Black );
        inp_fill_col_A.setMax(255);

        inp_mass.setFont(font);
        inp_mass.setTextureParam( 6, ROW_OFFSET+ROW_WIDTH*2+65, COL_OFFSET+COL_HEIGHT*2, 30, sf::Color(245,245,245) );
        inp_mass.setLabelParam( "1", 14, sf::Color::Black );

        inp_lin_damp1.setFont(font);
        inp_lin_damp1.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*2+65, COL_OFFSET+COL_HEIGHT*3, 30, sf::Color(245,245,245) );
        inp_lin_damp1.setLabelParam( "", 14, sf::Color::Black );

        inp_lin_damp2.setFont(font);
        inp_lin_damp2.setTextureParam( 1, ROW_OFFSET+ROW_WIDTH*2+50+46, COL_OFFSET+COL_HEIGHT*3, 30, sf::Color(245,245,245) );
        inp_lin_damp2.setLabelParam( "0", 14, sf::Color::Black );

        inp_ang_damp1.setFont(font);
        inp_ang_damp1.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH*2+65, COL_OFFSET+COL_HEIGHT*4, 30, sf::Color(245,245,245) );
        inp_ang_damp1.setLabelParam( "", 14, sf::Color::Black );

        inp_ang_damp2.setFont(font);
        inp_ang_damp2.setTextureParam( 1, ROW_OFFSET+ROW_WIDTH*2+50+46, COL_OFFSET+COL_HEIGHT*4, 30, sf::Color(245,245,245) );
        inp_ang_damp2.setLabelParam( "0", 14, sf::Color::Black );

        inp_rest1.setFont(font);
        inp_rest1.setTextureParam( 3, ROW_OFFSET+ROW_WIDTH+65, COL_OFFSET+COL_HEIGHT*2, 30, sf::Color(245,245,245) );
        inp_rest1.setLabelParam( "", 14, sf::Color::Black );

        inp_rest2.setFont(font);
        inp_rest2.setTextureParam( 1, ROW_OFFSET+ROW_WIDTH+50+46, COL_OFFSET+COL_HEIGHT*2, 30, sf::Color(245,245,245) );
        inp_rest2.setLabelParam( "0", 14, sf::Color::Black );
    }

    num_inputs.push_back(&inp_line_col_R);
    num_inputs.push_back(&inp_line_col_G);
    num_inputs.push_back(&inp_line_col_B);
    num_inputs.push_back(&inp_line_col_A);
    num_inputs.push_back(&inp_fill_col_R);
    num_inputs.push_back(&inp_fill_col_G);
    num_inputs.push_back(&inp_fill_col_B);
    num_inputs.push_back(&inp_fill_col_A);
    num_inputs.push_back( &inp_mass );
    num_inputs.push_back( &inp_lin_damp1 );
    num_inputs.push_back( &inp_lin_damp2 );
    num_inputs.push_back( &inp_ang_damp1 );
    num_inputs.push_back( &inp_ang_damp2 );
    num_inputs.push_back( &inp_rest1 );
    num_inputs.push_back( &inp_rest2 );

    if( g_json_str == "" ) {
        b2Vec2 gravity( 0.0f, 0.0f);
        m_world = new b2World(gravity);
    } else {
        std::string err_msg;
        m_world = json.readFromString( g_json_str, err_msg);
        jsonLoadObjectProps( m_world, editor_objects, json, b2d_to_gfx);
    }

/*
    ///Create reference body
    b2BodyDef myBodyDef;
    myBodyDef.position.Set( 0.0f, -5.0f); //set the starting position
    myBodyDef.angle = b2_pi/2; //set the starting angle
    ref_body = m_world->CreateBody(&myBodyDef);
    b2PolygonShape boxShape;
    boxShape.SetAsBox( 2.0f/2.0f, 4.6f/2.0f);
    b2FixtureDef boxFixtureDef;
    boxFixtureDef.shape = &boxShape;
    boxFixtureDef.density = 1;
    ref_body->CreateFixture(&boxFixtureDef);
    */

	uint32 flags = 0;
	flags |= b2Draw::e_shapeBit;
	debug_draw.SetFlags(flags);
	debug_draw.setScale(b2d_to_gfx);
    //debug_draw.setReducedMode();
    m_world->SetDebugDraw( &debug_draw );
    debug_draw.setScale( b2d_to_gfx);
    debug_draw.setFont(font);

    world_view.setSize( 1027.0f, 768.0f);
    world_view.zoom(2.5f);
    g_game.window->setView(world_view);

    place_type_gui.resize(6);
    sf::Texture* tex_p;
    tex_p = manager.getTexture( "gfx/CreateBox.bmp");
    place_type_gui.at(0).setTexture(tex_p);
    tex_p = manager.getTexture( "gfx/CreateCircle.bmp");
    place_type_gui.at(1).setTexture(tex_p);
    tex_p = manager.getTexture( "gfx/CreateChain.bmp");
    place_type_gui.at(2).setTexture(tex_p);
    tex_p = manager.getTexture( "gfx/CreatePolygon.bmp");
    place_type_gui.at(3).setTexture(tex_p);
    tex_p = manager.getTexture( "gfx/StartPoint.bmp");
    place_type_gui.at(4).setTexture(tex_p);
    tex_p = manager.getTexture( "gfx/FinishLine.bmp");
    place_type_gui.at(5).setTexture(tex_p);

    unsigned int i;
    for( i=0; i<place_type_gui.size(); i++)
        place_type_gui.at(i).setTextureParam( 50, 100+i*42, 32, 32 );

    gui_obj.push_back( &type_selector );
    gui_obj.push_back( &gnd_selector );
    for( i=0; i<place_type_gui.size(); i++)
        gui_obj.push_back(&place_type_gui.at(i));
    gui_obj.push_back(&inp_line_col_R);
    gui_obj.push_back(&inp_line_col_G);
    gui_obj.push_back(&inp_line_col_B);
    gui_obj.push_back(&inp_line_col_A);
    gui_obj.push_back(&inp_fill_col_R);
    gui_obj.push_back(&inp_fill_col_G);
    gui_obj.push_back(&inp_fill_col_B);
    gui_obj.push_back(&inp_fill_col_A);
    gui_obj.push_back( &inp_mass );
    gui_obj.push_back( &inp_lin_damp1 );
    gui_obj.push_back( &inp_lin_damp2 );
    gui_obj.push_back( &inp_ang_damp1 );
    gui_obj.push_back( &inp_ang_damp2 );
    gui_obj.push_back( &inp_rest1 );
    gui_obj.push_back( &inp_rest2 );

    sf::Texture* loaded_tex = NULL;
    loaded_tex = manager.getTexture("gfx/Load.png");
    if( loaded_tex != NULL )
        button_load.setTexture(loaded_tex);
    button_load.setTextureParam( 32, 32, 32, 32 );

    loaded_tex = manager.getTexture("gfx/Save.png");
    if( loaded_tex != NULL )
        button_save.setTexture(loaded_tex);
    button_save.setTextureParam( 75, 32, 32, 32 );

    loaded_tex = manager.getTexture("gfx/Exit.png");
    if( loaded_tex != NULL )
        button_exit.setTexture(loaded_tex);
    button_exit.setTextureParam( 150, 32, 32, 32 );

    gnd_selector.disable();
    inp_rest1.disable();
    inp_rest2.disable();
    inp_mass.disable();
    inp_lin_damp1.disable();
    inp_lin_damp2.disable();
    inp_ang_damp1.disable();
    inp_ang_damp2.disable();

	return 0;
}

void CEditorState::leaveState() {
    manager.freeAll();
    if( m_world != NULL) delete m_world;
    m_world = NULL;
    g_game.window->setView( g_game.window->getDefaultView() );
}

void CEditorState::handleEvent( sf::Event& event ) {

    unsigned int i;
    bool num_inp_interact = false;
    int new_type_choice;
    class CEObjDef obj_def;
    sf::Vector2i scrn_coords;

    switch (event.type)
    {
        // window closed
        case sf::Event::Closed:
            g_game.setRunning(false);
            break;

        // key pressed
        case sf::Event::KeyPressed:
            for( i=0; i<num_inputs.size(); i++ ) {
                if( num_inputs.at(i)->isActive() ) {
                    //if became inactive
                    if( !num_inputs.at(i)->keyPress( event.key.code ) ) {
                        editor_state = ESTATE_NONE;
                        //Escape/Enter: save settings to object
                        if( selected_obj != NULL ) {
                            fetchEObjDef( obj_def );
                            setEditorObjProp( selected_obj, obj_def );
                        }
                    }
                    num_inp_interact = true;
                }
                if( event.key.code == sf::Keyboard::T ) {
                    if(ref_body!=NULL)
                        m_world->DestroyBody(ref_body);
                    ref_body = NULL;
                }
            }
            if( num_inp_interact ) { //intentionally blank
            } else if( type_selector.isActive() ) {
                type_selector.keyPress( event.key.code );
                if( event.key.code == sf::Keyboard::Left || event.key.code == sf::Keyboard::Right ) {
                    new_type_choice = type_selector.getChoice();
                    switch( new_type_choice ) {
                    case EOBJ_GRAPHIC: //graphic
                        if( gnd_selector.disable() || inp_rest1.disable() ||
                           inp_rest2.disable() || inp_mass.disable() ||
                           inp_lin_damp1.disable() || inp_lin_damp2.disable() ||
                           inp_ang_damp1.disable() || inp_ang_damp2.disable() )
                            editor_state = ESTATE_NONE;
                    break;
                    case EOBJ_GND: //sensor
                        gnd_selector.enable();
                        if( inp_rest1.disable() || inp_rest2.disable() || inp_mass.disable() ||
                           inp_lin_damp1.disable() || inp_lin_damp2.disable() ||
                           inp_ang_damp1.disable() || inp_ang_damp2.disable() )
                            editor_state = ESTATE_NONE;
                    break;
                    case EOBJ_DYNAMIC: //dynamic
                        inp_rest1.enable();
                        inp_rest2.enable();
                        inp_mass.enable();
                        inp_lin_damp1.enable();
                        inp_lin_damp2.enable();
                        inp_ang_damp1.enable();
                        inp_ang_damp2.enable();
                        if( gnd_selector.disable() )
                            editor_state = ESTATE_NONE;
                    break;
                    case EOBJ_STATIC: //static
                        inp_rest1.enable();
                        inp_rest2.enable();
                        if( gnd_selector.disable() || inp_mass.disable() ||
                           inp_lin_damp1.disable() || inp_lin_damp2.disable() ||
                           inp_ang_damp1.disable() || inp_ang_damp2.disable() )
                           editor_state = ESTATE_NONE;
                    break;
                    default:
                    break;
                    }
                }
            } else if( gnd_selector.isActive() )
                gnd_selector.keyPress( event.key.code );
            else if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape )
                g_game.setRunning(false);
            else if( event.key.code == sf::Keyboard::Delete ) {
                if(selected_obj!=NULL) {
                    if(selected_obj->body!=NULL) {
                        m_world->DestroyBody(selected_obj->body);
                    }
                    //pop obj from obj list
                    int del_num=-1;
                    for( i=0; i<editor_objects.size(); i++) {
                        if( editor_objects.at(i) == selected_obj )
                            del_num = i;
                    }
                    if( del_num >= 0 )
                        editor_objects.erase( editor_objects.begin()+del_num, editor_objects.begin()+del_num+1);
                    //std::cout << "DN: "<<del_num<<"\n";
                    //std::cout <<"Size: " << editor_objects.size()<<"\n";
                    delete selected_obj;
                    selected_obj = NULL;
                }
            }
            else if( event.key.code == sf::Keyboard::Space ) {
                editor_state = ESTATE_NONE;
                for( i=0; i<place_type_gui.size(); i++)
                    place_type_gui.at(i).deSelect();
            }
            else if( event.key.code == sf::Keyboard::Return ) {
                if( editor_state == ESTATE_CHAIN ) {
                    if(temp_points.size()>=2) {
                        fetchEObjDef(obj_def);
                        editor_objects.push_back(NULL);
                        createChain( obj_def, editor_objects.back(), m_world, temp_points, 1.0f/b2d_to_gfx, false );
                        if( editor_objects.back()== NULL )
                            editor_objects.pop_back();
                        else
                            editor_objects.back()->type = type_selector.getChoice();
                    }
                    temp_points.clear();
                } else if( editor_state == ESTATE_POLYGON ) {
                    if(temp_points.size()>=3) {
                        fetchEObjDef(obj_def);
                        editor_objects.push_back(NULL);
                        createPolygon( obj_def, editor_objects.back(), m_world, temp_points, 1.0f/b2d_to_gfx );
                        if( editor_objects.back()== NULL )
                            editor_objects.pop_back();
                        else
                            editor_objects.back()->type = type_selector.getChoice();
                    }
                    temp_points.clear();
                }
            } else if( event.key.code == sf::Keyboard::L ) {
                if( editor_state == ESTATE_CHAIN ) {
                    if(temp_points.size()>=2) {
                        fetchEObjDef(obj_def);
                        editor_objects.push_back(NULL);
                        createChain( obj_def, editor_objects.back(), m_world, temp_points, 1.0f/b2d_to_gfx, true );
                        if( editor_objects.back()== NULL )
                            editor_objects.pop_back();
                        else
                            editor_objects.back()->type = type_selector.getChoice();
                    }
                    temp_points.clear();
                }
            } else if( event.key.code == sf::Keyboard::Num1 ) {
                for( i=0; i<place_type_gui.size(); i++)
                    place_type_gui.at(i).deSelect();
                place_type_gui.at(0).select();
                editor_state = ESTATE_BOX;
                selected_obj = NULL;
            } else if( event.key.code == sf::Keyboard::Num2 ) {
                for( i=0; i<place_type_gui.size(); i++)
                    place_type_gui.at(i).deSelect();
                place_type_gui.at(1).select();
                editor_state = ESTATE_CIRCLE;
                selected_obj = NULL;
            } else if( event.key.code == sf::Keyboard::Num3 ) {
                for( i=0; i<place_type_gui.size(); i++)
                    place_type_gui.at(i).deSelect();
                place_type_gui.at(2).select();
                editor_state = ESTATE_CHAIN;
                selected_obj = NULL;
            } else if( event.key.code == sf::Keyboard::Num4 ) {
                for( i=0; i<place_type_gui.size(); i++)
                    place_type_gui.at(i).deSelect();
                place_type_gui.at(3).select();
                editor_state = ESTATE_POLYGON;
                selected_obj = NULL;
            } else if( event.key.code == sf::Keyboard::Num5 ) {
                for( i=0; i<place_type_gui.size(); i++)
                    place_type_gui.at(i).deSelect();
                place_type_gui.at(4).select();
                editor_state = ESTATE_START_POINT;
                selected_obj = NULL;
            } else if( event.key.code == sf::Keyboard::Num6 ) {
                for( i=0; i<place_type_gui.size(); i++)
                    place_type_gui.at(i).deSelect();
                place_type_gui.at(5).select();
                editor_state = ESTATE_FINISH_LINE;
                selected_obj = NULL;
            }
            break;

        case sf::Event::MouseButtonPressed:
            if( event.mouseButton.button == sf::Mouse::Left ) {
                handleLeftClick( event.mouseButton.x, event.mouseButton.y);
            } else if( event.mouseButton.button == sf::Mouse::Right ) {
                /*scrn_coords.x = event.mouseButton.x;
                scrn_coords.y = event.mouseButton.y;
                last_right_btn = g_game.window->mapPixelToCoords(scrn_coords);*/
            }
            break;

        case sf::Event::MouseButtonReleased:
            if( event.mouseButton.button == sf::Mouse::Left ) {
                move_object = false;
                button_load.mouseUp( event.mouseButton.x, event.mouseButton.y);
                button_save.mouseUp( event.mouseButton.x, event.mouseButton.y);
                button_exit.mouseUp( event.mouseButton.x, event.mouseButton.y);
            } else if( event.mouseButton.button == sf::Mouse::Right ) {
                rot_object = false;
                /*scrn_coords.x = event.mouseButton.x;
                scrn_coords.y = event.mouseButton.y;
                if(selected_obj!=NULL) {
                    selected_obj->rotate( g_game.window->mapPixelToCoords(scrn_coords)-last_right_btn, 1.0f/b2d_to_gfx );
                }*/
            }
        break;

        case sf::Event::TextEntered:
            if (event.text.unicode < 128) {
                for( i=0; i<num_inputs.size(); i++ ) {
                    if( num_inputs.at(i)->isActive() )
                        num_inputs.at(i)->textEntered(static_cast<char>(event.text.unicode));
                }
            }
            break;

        default:
            break;
    }

}

void CEditorState::handleLeftClick( int pX, int pY) {
    bool was_active;
    bool is_active;
    unsigned int i, j;
    bool gui_click = false;
    CEObjDef eobj_def;
    do {
        button_load.mouseDown( pX, pY);
        button_save.mouseDown( pX, pY);
        button_exit.mouseDown( pX, pY);
        for( i=0; i<num_inputs.size(); i++ ) {
            was_active = num_inputs.at(i)->isActive();
            is_active = num_inputs.at(i)->mouseDown( pX, pY);
            //if it was selected, change to new editor_state
            if( !was_active && is_active ) {
                editor_state = ESTATE_TYPING;
                //deactivate other gui objects
                for( j=0; j<gui_obj.size(); j++)
                    gui_obj.at(j)->deSelect();
                num_inputs.at(i)->select();
                gui_click = true;
            }
        }

        was_active = type_selector.isActive();
        is_active = type_selector.mouseDown( pX, pY);
        if( !was_active && is_active ) {
            editor_state = ESTATE_TYPING;
            for( i=0; i<gui_obj.size(); i++)
                gui_obj.at(i)->deSelect();
            type_selector.select();
            gui_click = true;
        }

        was_active = gnd_selector.isActive();
        is_active = gnd_selector.mouseDown( pX, pY);
        if( !was_active && is_active ) {
            editor_state = ESTATE_TYPING;
            for( i=0; i<gui_obj.size(); i++)
                gui_obj.at(i)->deSelect();
            gnd_selector.select();
            gui_click = true;
        }

        for( i=0; i<place_type_gui.size(); i++) {
            was_active = place_type_gui.at(i).isActive();
            is_active = place_type_gui.at(i).mouseDown( pX, pY);

            if( !was_active && is_active ) {
                for( j=0; j<gui_obj.size(); j++)
                    gui_obj.at(j)->deSelect();
                switch(i) {
                    case 0: editor_state = ESTATE_BOX; place_type_gui.at(0).select(); break;
                    case 1: editor_state = ESTATE_CIRCLE; place_type_gui.at(1).select(); break;
                    case 2: editor_state = ESTATE_CHAIN; place_type_gui.at(2).select(); break;
                    case 3: editor_state = ESTATE_POLYGON; place_type_gui.at(3).select(); break;
                    case 4: editor_state = ESTATE_START_POINT; place_type_gui.at(4).select(); break;
                    case 5: editor_state = ESTATE_FINISH_LINE; place_type_gui.at(5).select(); break;
                    default: break;
                }
                type_selector.enable();
                selected_obj = NULL;
                gui_click = true;
            }
        }

     } while(0);

   //if nothing is selected..
    bool gui_selection = false;
    for( i=0; i<gui_obj.size(); i++) {
        if( gui_obj.at(i)->isActive() ) {
            gui_selection = true;
            break;
        }
    }
    if(!gui_selection)
        editor_state = ESTATE_NONE;

    //if the GUI was clicked, nothing else to do, it's handled already
    if( gui_click ) {
        if( prev_editor_state == ESTATE_TYPING && selected_obj != NULL ) {
            CEObjDef def;
            fetchEObjDef( def );
            setEditorObjProp( selected_obj, def );
        }
        move_object = false;
        return;
    } else {
        move_object = true;
    }

    if( prev_editor_state == ESTATE_TYPING && editor_state == ESTATE_NONE && selected_obj != NULL ) {
        CEObjDef def2;
        fetchEObjDef( def2 );
        setEditorObjProp( selected_obj, def2 );
    }

    sf::Vector2f gfx_pos;
    sf::Vertex vert;
    sf::Vector2f px_pos;
    CEObjDef obj_def;
    //if no GUI interaction: do sg with editor_state
    switch(editor_state) {
        case ESTATE_NONE:
            //look if an object was selected
            px_pos = g_game.window->mapPixelToCoords(sf::Vector2i( pX, pY));
            selected_obj = getSelectedObject( editor_objects, m_world, px_pos, 1.0f/b2d_to_gfx, g_game.window);
            if(selected_obj==NULL) break;
            switch(selected_obj->type) {
               case EOBJ_GRAPHIC: //graphic
                    type_selector.enable();
                    type_selector.setChoice(EOBJ_GRAPHIC);
                    if( type_selector.disable() || gnd_selector.disable() || inp_rest1.disable() ||
                       inp_rest2.disable() || inp_mass.disable() ||
                       inp_lin_damp1.disable() || inp_lin_damp2.disable() ||
                       inp_ang_damp1.disable() || inp_ang_damp2.disable() )
                        editor_state = ESTATE_NONE;
                break;
                case EOBJ_GND: //sensor
                    type_selector.enable();
                    type_selector.setChoice(EOBJ_GND);
                    gnd_selector.enable();
                    if( type_selector.disable() || inp_rest1.disable() || inp_rest2.disable() || inp_mass.disable() ||
                       inp_lin_damp1.disable() || inp_lin_damp2.disable() ||
                       inp_ang_damp1.disable() || inp_ang_damp2.disable() )
                        editor_state = ESTATE_NONE;
                break;
                case EOBJ_DYNAMIC: //dynamic
                    type_selector.enable();
                    type_selector.setChoice(EOBJ_DYNAMIC);
                    type_selector.disable();
                    inp_rest1.enable();
                    inp_rest2.enable();
                    inp_mass.enable();
                    inp_lin_damp1.enable();
                    inp_lin_damp2.enable();
                    inp_ang_damp1.enable();
                    inp_ang_damp2.enable();
                    if( gnd_selector.disable() )
                        editor_state = ESTATE_NONE;
                break;
                case EOBJ_STATIC: //static
                    type_selector.enable();
                    type_selector.setChoice(EOBJ_STATIC);
                    type_selector.disable();
                    inp_rest1.enable();
                    inp_rest2.enable();
                    if( gnd_selector.disable() || inp_mass.disable() ||
                       inp_lin_damp1.disable() || inp_lin_damp2.disable() ||
                       inp_ang_damp1.disable() || inp_ang_damp2.disable() )
                       editor_state = ESTATE_NONE;
                break;
                default:
                break;
            }
            fetchEditorObjProp( selected_obj, eobj_def );
            setGUIToObjProp( eobj_def );
            break;
        case ESTATE_BOX: //add point
            gfx_pos = g_game.window->mapPixelToCoords(sf::Vector2i( pX, pY));
            vert.position.x = gfx_pos.x;
            vert.position.y = gfx_pos.y;
            vert.color = sf::Color::Red;
            temp_points.push_back( vert );
            if(temp_points.size()>=2) {
                fetchEObjDef(obj_def);
                editor_objects.push_back(NULL);
                createBox( obj_def, editor_objects.back(), m_world, temp_points.at(0).position, temp_points.at(1).position, 1.0f/b2d_to_gfx);
                temp_points.clear();
                editor_objects.back()->type = type_selector.getChoice();
            }
            break;
        case ESTATE_CIRCLE:
            gfx_pos = g_game.window->mapPixelToCoords(sf::Vector2i( pX, pY));
            vert.position.x = gfx_pos.x;
            vert.position.y = gfx_pos.y;
            vert.color = sf::Color::Red;
            temp_points.push_back( vert );
            if(temp_points.size()>=2) {
                fetchEObjDef(obj_def);
                editor_objects.push_back(NULL);
                createCircle( obj_def, editor_objects.back(), m_world, temp_points.at(0).position, temp_points.at(1).position, 1.0f/b2d_to_gfx );
                temp_points.clear();
                editor_objects.back()->type = type_selector.getChoice();
            }

            break;
        case ESTATE_CHAIN:
            gfx_pos = g_game.window->mapPixelToCoords(sf::Vector2i( pX, pY));
            vert.position.x = gfx_pos.x;
            vert.position.y = gfx_pos.y;
            vert.color = sf::Color::Red;
            temp_points.push_back( vert );
            break;
        case ESTATE_POLYGON:
            gfx_pos = g_game.window->mapPixelToCoords(sf::Vector2i( pX, pY));
            vert.position.x = gfx_pos.x;
            vert.position.y = gfx_pos.y;
            vert.color = sf::Color::Red;
            temp_points.push_back( vert );
            break;
        case ESTATE_START_POINT:
            gfx_pos = g_game.window->mapPixelToCoords(sf::Vector2i( pX, pY));
            editor_objects.push_back(NULL);
            createStartPoint( editor_objects.back(), m_world, getNextStartPoint(editor_objects), gfx_pos, 1.0f/b2d_to_gfx );
            break;
        case ESTATE_FINISH_LINE:
            gfx_pos = g_game.window->mapPixelToCoords(sf::Vector2i( pX, pY));
            vert.position.x = gfx_pos.x;
            vert.position.y = gfx_pos.y;
            vert.color = sf::Color::Red;
            temp_points.push_back( vert );
            if(temp_points.size()>=2) {
                removeFinishLine( editor_objects );
                editor_objects.push_back(NULL);
                createFinishLine( editor_objects.back(), m_world, temp_points.at(0).position, temp_points.at(1).position, 1.0f/b2d_to_gfx );
                temp_points.clear();
            }
            break;
        //case ESTATE_SELECTION:
            //de-selected?
            //break;
        default:
            break;
    }

}


void CEditorState::step() {

    CEditorSprite* w_sprite = NULL;

    sf::Vector2i scrn_coords = sf::Mouse::getPosition(*g_game.window);
    sf::Vector2f cur_pos = g_game.window->mapPixelToCoords(scrn_coords);
    if( sf::Mouse::isButtonPressed(sf::Mouse::Left) ) {
        if(move_object) {
            if( selected_obj!=NULL ) {
                if( isEditorSprite(selected_obj) ) {
                    w_sprite = (CEditorSprite*)selected_obj;
                    w_sprite->move( cur_pos-last_cur_pos, 1.0f/b2d_to_gfx );
                } //TODO: move other types
            }
        } else
            move_object = true;
    }
    if( sf::Mouse::isButtonPressed(sf::Mouse::Right) ) {
        if(rot_object) {
            if( selected_obj!=NULL && !sf::Mouse::isButtonPressed(sf::Mouse::Left) ) {
                if( isEditorSprite(selected_obj) ) {
                    w_sprite = (CEditorSprite*)selected_obj;
                    w_sprite->rotate( g_game.window->mapPixelToCoords(scrn_coords)-last_cur_pos, 1.0f/b2d_to_gfx );
                }
            }
        } else
            rot_object = true;
    }

    last_cur_pos = cur_pos;

    debug_draw.checkIfRendered();

    float move_magnitude = 40.0f;
    float w_zoom = g_game.window->getView().getSize().x/(float)g_game.scrnW;
    move_magnitude *= w_zoom;
    if( editor_state != ESTATE_TYPING && sf::Keyboard::isKeyPressed(sf::Keyboard::Left) )
        world_view.move( -move_magnitude, 0.0f);
    if( editor_state != ESTATE_TYPING && sf::Keyboard::isKeyPressed(sf::Keyboard::Right) )
        world_view.move( move_magnitude, 0.0f);
    if( editor_state != ESTATE_TYPING && sf::Keyboard::isKeyPressed(sf::Keyboard::Up) )
        world_view.move( 0.0f, -move_magnitude);
    if( editor_state != ESTATE_TYPING && sf::Keyboard::isKeyPressed(sf::Keyboard::Down) )
        world_view.move( 0.0f, move_magnitude);
    if( editor_state != ESTATE_TYPING && sf::Keyboard::isKeyPressed(sf::Keyboard::X) )
        world_view.zoom(0.95f);
    if( editor_state != ESTATE_TYPING && sf::Keyboard::isKeyPressed(sf::Keyboard::C) )
        world_view.zoom(1.05f);

    sf::Vector2i local_m_pos = sf::Mouse::getPosition(*g_game.window);
    sf::Vector2f gfx_pos = g_game.window->mapPixelToCoords( local_m_pos );
    cur_pos_vert.position.x = gfx_pos.x;
    cur_pos_vert.position.y = gfx_pos.y;

    /*float32 timeStep = 1.0f/50.0f;
    int32 velocityIterations = 8;
    int32 positionIterations = 3;
    m_world->Step( timeStep, velocityIterations, positionIterations);*/

    std::stringstream ss;
    ss << "State: " << editor_state;
    debug_draw.dynamic_lines.push_back(ss.str());

    if( button_load.check() ) {
        next_state = STATE_EDITOR_LOAD;
        jsonDumpObjectProps( editor_objects, this->json);
        g_json_str.erase( g_json_str.begin(), g_json_str.end() );
        g_json_str = json.writeToString( m_world );
    }
    else if( button_save.check() ) {
        jsonDumpObjectProps( editor_objects, this->json);
        g_json_str.erase( g_json_str.begin(), g_json_str.end() );
        g_json_str = json.writeToString( m_world );
        next_state = STATE_EDITOR_SAVE;
    }
    else if( button_exit.check() )
        g_game.setRunning(false);

    if( prev_editor_state != editor_state )
        temp_points.clear();
    if( prev_editor_state!=ESTATE_CHAIN && editor_state==ESTATE_CHAIN ) {
        type_selector.disable_one(1); //chains can't be sensor or dynamic
        type_selector.disable_one(2);
    } else if( prev_editor_state==ESTATE_CHAIN && editor_state!=ESTATE_CHAIN ) {
        type_selector.enable_one(1);
        type_selector.enable_one(2);
    }
    prev_editor_state = editor_state;
}

void CEditorState::renderTemp( int type, sf::RenderWindow* win ) {
    bool loop = false;
    if( type == ESTATE_POLYGON || type == ESTATE_CHAIN || type == ESTATE_FINISH_LINE ) {
        if(type == ESTATE_POLYGON)
            loop=true;
        if( temp_points.size()!=0 ) {
            temp_points.push_back(cur_pos_vert);
            if(loop)
                temp_points.push_back(temp_points.at(0));
            win->draw( &temp_points.at(0), temp_points.size(), sf::LinesStrip);
            temp_points.pop_back();
            if(loop)
                temp_points.pop_back();
        }
    } else if( type == ESTATE_BOX ) {
        if( temp_points.size()==1 ) {
            temp_points.push_back(cur_pos_vert);
            float width, height;
            sf::Vector2f pos;
            pos.x = std::min( temp_points.at(0).position.x, temp_points.at(1).position.x );
            pos.y = std::min( temp_points.at(0).position.y, temp_points.at(1).position.y );
            width = std::abs( temp_points.at(1).position.x-temp_points.at(0).position.x );
            height = std::abs( temp_points.at(1).position.y-temp_points.at(0).position.y );
            sf::RectangleShape rectangle( sf::Vector2f( width, height) );
            rectangle.setPosition(pos);
            rectangle.setFillColor(sf::Color(225, 0, 0, 140));
            rectangle.setOutlineThickness(2);
            rectangle.setOutlineColor(sf::Color::Red);
            win->draw( rectangle );
            temp_points.pop_back();
        }
    } else if( type == ESTATE_CIRCLE ) {
        if( temp_points.size()==1 ) {
            temp_points.push_back(cur_pos_vert);
            sf::Vector2f pos;
            b2Vec2 t;
            float r;
            pos.x = temp_points.at(0).position.x + (temp_points.at(1).position.x - temp_points.at(0).position.x)/2.0f;
            pos.y = temp_points.at(0).position.y + (temp_points.at(1).position.y - temp_points.at(0).position.y)/2.0f;
            t.x = temp_points.at(1).position.x-temp_points.at(0).position.x;
            t.y = temp_points.at(1).position.y-temp_points.at(0).position.y;
            r = t.Length()/2;
            sf::CircleShape circle(r);
            circle.setOrigin( r, r);
            circle.setPosition(pos);
            circle.setFillColor(sf::Color(225, 0, 0, 140));
            circle.setOutlineThickness(2);
            circle.setOutlineColor(sf::Color::Red);
            win->draw( circle );
            temp_points.pop_back();
        }
    }
}

void CEditorState::render() {
    unsigned int i;
    g_game.window->setView(world_view);
    g_game.window->clear(sf::Color::Black);

    if( DEBUG_DRAW_ON ) {
        m_world->DrawDebugData();
        debug_draw.renderWorld( g_game.window );
    }

    for( i=0; i<editor_objects.size(); i++) {
        if( editor_objects.at(i) != selected_obj )
            renderObject( editor_objects.at(i), g_game.window, editor_font, b2d_to_gfx, false);
    }
    if( selected_obj != NULL ) { //render selection and the selected object
        renderObject( selected_obj, g_game.window, editor_font, b2d_to_gfx, true);
        renderObject( selected_obj, g_game.window, editor_font, b2d_to_gfx, false);
    }

    //render temp shape
    renderTemp( editor_state, g_game.window);

    //render User Interface
    g_game.window->setView( g_game.window->getDefaultView() );

    type_selector.render( g_game.window );
    gnd_selector.render( g_game.window );

    for( i=0; i<num_inputs.size(); i++ )
        num_inputs.at(i)->render( g_game.window );

    for( i=0; i<labels.size(); i++)
        g_game.window->draw( labels.at(i) );

    for( i=0; i<place_type_gui.size(); i++)
        place_type_gui.at(i).render( g_game.window );

    button_load.render( g_game.window );
    button_save.render( g_game.window );
    button_exit.render( g_game.window );

    debug_draw.renderText( g_game.window );
    g_game.window->display();
    g_game.window->setView(world_view);
}

//runs only if do_sleep is false, must not block
void CEditorState::idle( sf::Time remaining_time ) {
}

void CEditorState::fetchEObjDef( CEObjDef& def ) {

    def.type = type_selector.getChoice();
    def.gnd_type = gnd_selector.getChoice();

    def.restitution = (float)inp_rest1.getContent() + (float)inp_rest2.getContent()/10.0f;
    def.mass = (float)inp_mass.getContent();
    def.lin_damping = (float)inp_lin_damp1.getContent() + (float)inp_lin_damp2.getContent()/10.0f;
    def.ang_damping = (float)inp_ang_damp1.getContent() + (float)inp_ang_damp2.getContent()/10.0f;
    int r,g,b,a;
    r = inp_fill_col_R.getContent();
    g = inp_fill_col_G.getContent();
    b = inp_fill_col_B.getContent();
    a = inp_fill_col_A.getContent();
    if( r>255 ) r=255;
    if( g>255 ) g=255;
    if( b>255 ) b=255;
    if( a>255 ) a=255;
    def.fill_color = sf::Color( (sf::Uint8)r, (sf::Uint8)g, (sf::Uint8)b, (sf::Uint8)a );
    r = inp_line_col_R.getContent();
    g = inp_line_col_G.getContent();
    b = inp_line_col_B.getContent();
    a = inp_line_col_A.getContent();
    if( r>255 ) r=255;
    if( g>255 ) g=255;
    if( b>255 ) b=255;
    if( a>255 ) a=255;
    def.line_color = sf::Color( (sf::Uint8)r, (sf::Uint8)g, (sf::Uint8)b, (sf::Uint8)a );
}

static float round(float f) {
  return std::floor(f + 0.5f);
}
void CEditorState::setGUIToObjProp( CEObjDef& def ) {
    float rad;
    switch(def.type) {
        case EOBJ_DYNAMIC:
            inp_mass.setContent( def.mass );
            inp_lin_damp1.setContent( (int)def.lin_damping );
            rad = def.lin_damping-(int)def.lin_damping;
            rad*=10.0f;
            inp_lin_damp2.setContent( round(rad) );
            inp_ang_damp1.setContent( (int)def.ang_damping );
            rad = def.ang_damping-(int)def.ang_damping;
            rad*=10.0f;
            inp_ang_damp2.setContent( round(rad) );
            inp_rest1.setContent( (int)def.restitution );
            rad = def.restitution-(int)def.restitution;
            rad*=10.0f;
            inp_rest2.setContent( round(rad) );
            break;
        case EOBJ_GND:
            gnd_selector.setChoice(def.gnd_type);
            break;
        case EOBJ_GRAPHIC:
            break;
        case EOBJ_STATIC:
            inp_rest1.setContent( (int)def.restitution );
            rad = def.restitution-(int)def.restitution;
            rad*=10.0f;
            inp_rest2.setContent( round(rad) );
            break;
        default:
            g_game.error << "setGUIToObjProp: No such object type!";
            break;
    }
    inp_fill_col_R.setContent( def.fill_color.r );
    inp_fill_col_G.setContent( def.fill_color.g );
    inp_fill_col_B.setContent( def.fill_color.b );
    inp_fill_col_A.setContent( def.fill_color.a );

    inp_line_col_R.setContent( def.line_color.r );
    inp_line_col_G.setContent( def.line_color.g );
    inp_line_col_B.setContent( def.line_color.b );
    inp_line_col_A.setContent( def.line_color.a );

}
