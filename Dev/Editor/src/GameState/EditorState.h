#ifndef SETTINGS_STATE_H_
#define SETTINGS_STATE_H_

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "../Graphics/GUI.h"
#include "../Util.h"
#include "../Editor/b2dJson/b2dJson.h"
#include "../Graphics/DebugDraw.h"
#include "../Editor/EditorGUI.h"
#include "../Editor/EditorObj.h"

//removed ESTATE_SELECTION (not used)
enum eEditorStates{ ESTATE_NONE, ESTATE_BOX, ESTATE_CIRCLE, ESTATE_CHAIN, ESTATE_POLYGON, ESTATE_START_POINT, ESTATE_FINISH_LINE,
    ESTATE_TYPING };

//implements pre singleplayer state
//inherits from CGameState, see description there
class CEditorState : public CGameState {

	public:
	CEditorState();
	~CEditorState() {}

	int loadState();
	void leaveState();

	void handleEvent( sf::Event& event );
	void handleLeftClick( int pX, int pY);
	void step();
	void render();

	//runs only if do_sleep is false, must not block
	void idle( sf::Time remaining_time );

	void setNextState( int new_next_state ) { next_state = new_next_state; }
	int getNextState() { return next_state; }

	void renderTemp( int type, sf::RenderWindow* win );

	void fetchEObjDef( CEObjDef& def );
	void setGUIToObjProp( CEObjDef& def );

    //if true, the app sleeps until the next frame, else it runs the idle function
	bool do_sleep;

	protected:
    int next_state;		//next game state

    //GRAPHICS
    sf::View world_view;
    CTextureManager manager;
    sf::Font* editor_font;

    //PHYSICS
    float b2d_to_gfx;
    b2World* m_world;
    CDebugDraw debug_draw;

    //GUI
    int COL_OFFSET;
    int COL_HEIGHT;
    int ROW_WIDTH;
    int ROW_OFFSET;
    int LABEL_Y_OFFSET;
    sf::Color LABEL_COLOR;
    int LABEL_CH_SIZE;

    std::vector<CGUIBox*> gui_obj;

	CChoiceBox type_selector;
	unsigned int LABELS_NUM;
    std::vector<sf::Text> labels;

    std::vector<CNumInput*> num_inputs;
	CNumInput inp_line_col_R;
	CNumInput inp_line_col_G;
	CNumInput inp_line_col_B;
	CNumInput inp_line_col_A;
	CNumInput inp_fill_col_R;
	CNumInput inp_fill_col_G;
	CNumInput inp_fill_col_B;
	CNumInput inp_fill_col_A;
	CNumInput inp_mass;
	CNumInput inp_lin_damp1;
	CNumInput inp_lin_damp2;
	CNumInput inp_ang_damp1;
	CNumInput inp_ang_damp2;
	CNumInput inp_rest1;
	CNumInput inp_rest2;

	CChoiceBox gnd_selector;

	CButton button_load;
	CButton button_save;
	CButton button_exit;

    std::vector<CSelectBox> place_type_gui;

	//EDITOR
	 b2dJson json;
    CEditorObj* selected_obj;
	std::vector<CEditorObj*> editor_objects;
	int editor_state;
	int prev_editor_state;
	std::vector<sf::Vertex> temp_points;
	sf::Vertex cur_pos_vert;
	bool move_object;
	bool rot_object;
	sf::Vector2f last_cur_pos; //in gfx coordinates

	b2Body* ref_body; //reference body
};

#endif
