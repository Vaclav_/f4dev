#ifndef DEBUG_DRAW_H_
#define DEBUG_DRAW_H_

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <vector>

struct b2AABB;

class CModelMatrix : public sf::Shape {
    unsigned int getPointCount() const;
    sf::Vector2f getPoint(unsigned int index) const;
};

enum eDebugDrawMode { DDRAW_NORMAL, DDRAW_REDUCED };

//This class implements debug drawing callbacks that are invoked
//inside b2World::Step.
//To enable debug draw:
//fooDrawInstance.SetFlags( b2DebugDraw::e_shapeBit );
//world->SetDebugDraw( &fooDrawInstance );
//and at rendering:
//world->DrawDebugData();
/*
e_shapeBit				= 0x0001,	///< draw shapes
e_jointBit				= 0x0002,	///< draw joint connections
e_aabbBit				= 0x0004,	///< draw axis aligned bounding boxes
e_pairBit				= 0x0008,	///< draw broad-phase pairs
e_centerOfMassBit		= 0x0010	///< draw center of mass frame
*/
class CDebugDraw : public b2Draw {
	public:
	CDebugDraw();
	void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
	void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
	void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
	void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
	void DrawTransform(const b2Transform& xf);
    void DrawPoint(const b2Vec2& p, float32 size, const b2Color& color);
    void DrawString(int x, int y, const char* string, ...);
    void DrawAABB(b2AABB* aabb, const b2Color& color);

    //returns current translation as a 2D vector
    //b2Vec2: vector of two floats
    b2Vec2 getTranslation();
    //returns current rotation in radians
    float getRotation();
    //returns zoom as float, 1.0f means 1:1
    float getScale();

    //translation = eltolás
    //relative to current translation
    //always in default direction and default scale
    void translate( float tX, float tY );
    //absolute translation
    void setTranslation( float absX, float absY );

    //rotate around the origin ( 0, 0)
    //relative to current rotation, in degrees
    void rotate( float rot_deg );
    //absolute rotation
    void setRotation( float abs_rot_deg );

    //nagyitas/kicsinyites
    //relative to current scale, 1.0f means 1:1
    void scale( float zoom );
    //absolute scale
    void setScale( float abs_zoom );

    void setFont( sf::Font* f );

    void checkIfRendered();
    void renderWorld( sf::RenderWindow* r_window );
    void renderText( sf::RenderWindow* r_window );
    void setReducedMode();
    int mode;

    std::vector<std::string> static_lines;
    sf::Text statix_test;
    bool static_text_changed;
    std::vector<std::string> dynamic_lines;
std::vector<sf::Vertex> lines_vect;
    private:
    bool rendered_world;
    bool rendered_text;
    unsigned int circle_approx_n;
    sf::Uint32 lines_buffer_size;

    sf::Color b2SFColor(const b2Color &color, int alpha = 255);
    CModelMatrix matrix;
    sf::Font* font;
    sf::Text static_debug_text;
};

#endif
