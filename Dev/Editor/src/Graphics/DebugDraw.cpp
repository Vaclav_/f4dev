
#include <cmath>

#include "DebugDraw.h"
#include "../Game.h"
#include <sstream>

CDebugDraw::CDebugDraw() :
    mode(DDRAW_NORMAL),
    static_text_changed(true),
    rendered_world(false),
    rendered_text(false),
    circle_approx_n(16),
    lines_buffer_size(1000),
    font(NULL)
    {
    matrix.setPosition( 0.0f, 0.0f);
}

void CDebugDraw::setFont( sf::Font* f ) {
    font = f;
}

void CDebugDraw::setReducedMode() {
    mode = DDRAW_REDUCED;
    lines_vect.reserve(lines_buffer_size);
}

void CDebugDraw::checkIfRendered() {
    if(!rendered_world)
        lines_vect.clear();
    if(!rendered_text)
        dynamic_lines.clear();
    rendered_world = false;
    rendered_text = false;
}

void CDebugDraw::renderWorld( sf::RenderWindow* r_window ) {
    if( lines_vect.size()!=0 ) {
        sf::RenderStates states;
        states.transform = matrix.getTransform();
        r_window->draw( &lines_vect.at(0), lines_vect.size(), sf::Lines, states);
        lines_vect.clear();
    }
    rendered_world = true;
}

void CDebugDraw::renderText( sf::RenderWindow* r_window ) {
    float x=15.0f, y=330.0f;
    unsigned int i;
    if( static_text_changed ) {
        std::stringstream s_text;
        for( i=0; i<static_lines.size(); i++) {
            s_text << static_lines.at(i);
            if(i+1<static_lines.size())
                s_text << "\n";
        }
        if(font!=NULL)
            static_debug_text.setFont(*font);
        static_debug_text.setString(s_text.str().c_str());
        static_debug_text.setCharacterSize(12);
        static_debug_text.setPosition( x, y);
        static_debug_text.setColor(sf::Color::Yellow);
        //if it's screen coordinates, transformation shouldn't be applied
        static_text_changed = false;
    }

    r_window->draw(static_debug_text);
    y+=static_lines.size()*14.0f;

    for( i=0; i<dynamic_lines.size(); i++) {
        DrawString( x, y, dynamic_lines.at(i).c_str() );
        y+= 14.0f;
    }
    dynamic_lines.clear();

    rendered_text = true;
}

//convert a Box2D (float 0.0f - 1.0f range) color to a SFML color (uint8 0 - 255 range)
sf::Color CDebugDraw::b2SFColor(const b2Color &color, int alpha) {
	sf::Color result((sf::Uint8)(color.r*255), (sf::Uint8)(color.g*255), (sf::Uint8)(color.b*255), (sf::Uint8) alpha);
	return result;
}

void CDebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {

    int32 i;
    if( mode == DDRAW_NORMAL ) {
        sf::ConvexShape polygon(vertexCount);
        for ( i=0; i<vertexCount; i++) {
            b2Vec2 vertex = vertices[i];
            polygon.setPoint(i, sf::Vector2f(vertex.x, vertex.y));
        }
        polygon.setFillColor(sf::Color::Transparent);
        polygon.setOutlineColor(this->b2SFColor(color));
        float w_zoom = g_game.window->getView().getSize().x/(float)g_game.scrnW;
        polygon.setOutlineThickness(w_zoom*1.0f/getScale());
        g_game.window->draw( polygon, matrix.getTransform());
    } else if( mode == DDRAW_REDUCED ) {
        //add for 0
        i = vertexCount-1;
        lines_vect.push_back(sf::Vertex(sf::Vector2f( vertices[i].x, vertices[i].y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( vertices[0].x, vertices[0].y), sf::Color::Green));
        for ( i=1; i<vertexCount; i++) {
            lines_vect.push_back(sf::Vertex(sf::Vector2f( vertices[i-1].x, vertices[i-1].y), sf::Color::Green));
            lines_vect.push_back(sf::Vertex(sf::Vector2f( vertices[i].x, vertices[i].y), sf::Color::Green));
        }
    }

}

void CDebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) {

    int32 i;
    if( mode == DDRAW_NORMAL ) {
        sf::ConvexShape polygon(vertexCount);
        for ( i=0; i<vertexCount; i++) {
            b2Vec2 vertex = vertices[i];
            polygon.setPoint(i, sf::Vector2f(vertex.x, vertex.y));
        }

        polygon.setFillColor(this->b2SFColor(color, 50));
        polygon.setOutlineColor(this->b2SFColor(color));
        float w_zoom = g_game.window->getView().getSize().x/(float)g_game.scrnW;
        polygon.setOutlineThickness(w_zoom*1.0f/getScale());

        g_game.window->draw( polygon, matrix.getTransform());
    } else if( mode == DDRAW_REDUCED ){
        i = vertexCount-1;
        lines_vect.push_back(sf::Vertex(sf::Vector2f( vertices[i].x, vertices[i].y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( vertices[0].x, vertices[0].y), sf::Color::Green));
        for ( i=1; i<vertexCount; i++) {
            lines_vect.push_back(sf::Vertex(sf::Vector2f( vertices[i-1].x, vertices[i-1].y), sf::Color::Green));
            lines_vect.push_back(sf::Vertex(sf::Vector2f( vertices[i].x, vertices[i].y), sf::Color::Green));
        }
    }

}

void CDebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color) {

    if( mode == DDRAW_NORMAL ) {
        sf::CircleShape circle((radius));
        circle.setPosition(center.x-radius, center.y-radius);
        circle.setFillColor(sf::Color::Transparent);
        circle.setOutlineColor(this->b2SFColor(color));
        float w_zoom = g_game.window->getView().getSize().x/(float)g_game.scrnW;
        circle.setOutlineThickness(w_zoom*1.0f/getScale());
        g_game.window->draw( circle, matrix.getTransform());
    } else if( mode == DDRAW_REDUCED ) {
        float theta = 0.0f;
        float d_fi = 2.0*3.14159265/circle_approx_n;
        float p_x, p_y;

        p_x = center.x + radius*cos(theta);
        p_y = center.y + radius*sin(theta);
        lines_vect.push_back(sf::Vertex(sf::Vector2f( p_x, p_y), sf::Color::Green));

        for( theta=d_fi; theta<2.0*3.14159265; theta+=d_fi) {
            p_x = center.x + radius*cos(theta);
            p_y = center.y + radius*sin(theta);
            lines_vect.push_back(sf::Vertex(sf::Vector2f( p_x, p_y), sf::Color::Green));
            lines_vect.push_back(sf::Vertex(sf::Vector2f( p_x, p_y), sf::Color::Green));
        }
        p_x = center.x + radius*cos(theta);
        p_y = center.y + radius*sin(theta);
        lines_vect.push_back(sf::Vertex(sf::Vector2f( p_x, p_y), sf::Color::Green));
    }

}

void CDebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color) {

    if( mode == DDRAW_NORMAL ) {
        //no converion in cordinates of center and upper left corner, Circle in sfml is managed by default with the center
        sf::CircleShape circle(radius);
        circle.setPosition(center.x - radius, center.y - radius);
        circle.setFillColor(this->b2SFColor(color, 50));
        circle.setOutlineColor(this->b2SFColor(color));
        float w_zoom = g_game.window->getView().getSize().x/(float)g_game.scrnW;
        circle.setOutlineThickness(w_zoom*1.0f/getScale());

        // line of the circle wich shows the angle
        b2Vec2 p = center + (radius * axis);
        this->DrawSegment(center, p, color);

        g_game.window->draw( circle, matrix.getTransform());
    } else if( mode == DDRAW_REDUCED ) {
        float theta = 0.0f;
        float d_fi = 2.0*3.14159265/circle_approx_n;
        float p_x, p_y;

        p_x = center.x + radius*cos(theta);
        p_y = center.y + radius*sin(theta);
        lines_vect.push_back(sf::Vertex(sf::Vector2f( p_x, p_y), sf::Color::Green));

        for( theta=d_fi; theta<2.0*3.14159265; theta+=d_fi) {
            p_x = center.x + radius*cos(theta);
            p_y = center.y + radius*sin(theta);
            lines_vect.push_back(sf::Vertex(sf::Vector2f( p_x, p_y), sf::Color::Green));
            lines_vect.push_back(sf::Vertex(sf::Vector2f( p_x, p_y), sf::Color::Green));
        }
        p_x = center.x + radius*cos(theta);
        p_y = center.y + radius*sin(theta);
        lines_vect.push_back(sf::Vertex(sf::Vector2f( p_x, p_y), sf::Color::Green));
    }

}

void CDebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) {

    if( mode == DDRAW_NORMAL ) {
        sf::VertexArray line(sf::Lines);
        line.append(sf::Vertex(sf::Vector2f(p1.x, p1.y), this->b2SFColor(color)));
        line.append(sf::Vertex(sf::Vector2f(p2.x, p2.y), this->b2SFColor(color)));
        g_game.window->draw( line, matrix.getTransform());
    } else if( mode == DDRAW_REDUCED ) {
        lines_vect.push_back(sf::Vertex(sf::Vector2f( p1.x, p1.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( p2.x, p2.y), sf::Color::Green));
    }

}

void CDebugDraw::DrawTransform(const b2Transform& xf) {
    float w_zoom = g_game.window->getView().getSize().x/(float)g_game.scrnW;
    float line_proportion = w_zoom*15.0f/getScale();
    b2Vec2 p1 = xf.p, p2;

	//x axis
	p2 = p1 + (line_proportion * xf.q.GetXAxis());
	b2Color col_red( 1.0f, 0.0f, 0.0f);
    this->DrawSegment(p1, p2, col_red);

	//y axis
	p2 = p1 + (line_proportion * xf.q.GetYAxis());
	b2Color col_green( 0.0f, 1.0f, 0.0f);
    this->DrawSegment(p1, p2, col_green);
}

void CDebugDraw::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color) {

    if( mode == DDRAW_NORMAL ) {
        sf::VertexArray vertex(sf::Points);
        vertex.append( sf::Vertex(sf::Vector2f( p.x, p.y)));
        vertex[0].color = b2SFColor(color);
        g_game.window->draw( vertex, matrix.getTransform());
    } else if( mode == DDRAW_REDUCED ) {
        lines_vect.push_back(sf::Vertex(sf::Vector2f( p.x, p.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( p.x, p.y), sf::Color::Green));
    }

}

void CDebugDraw::DrawString(int x, int y, const char* string, ...) {
    sf::Text text;
    if(font!=NULL)
        text.setFont(*font);
    text.setString(string);
    text.setCharacterSize(12);
    text.setPosition( (float)x, (float)y);
    text.setColor(sf::Color::White);
    //if it's screen coordinates, transformation shouldn't be applied
    g_game.window->draw(text);
}

void CDebugDraw::DrawAABB(b2AABB* aabb, const b2Color& color) {

    if( mode == DDRAW_NORMAL ) {
        sf::ConvexShape polygon(4);
        polygon.setPoint(0, sf::Vector2f(aabb->lowerBound.x, aabb->lowerBound.y));
        polygon.setPoint(1, sf::Vector2f(aabb->upperBound.x, aabb->lowerBound.y));
        polygon.setPoint(2, sf::Vector2f(aabb->upperBound.x, aabb->upperBound.y));
        polygon.setPoint(3, sf::Vector2f(aabb->lowerBound.x, aabb->upperBound.y));

        polygon.setFillColor(this->b2SFColor(color, 50));
        polygon.setOutlineColor(this->b2SFColor(color));
        float w_zoom = g_game.window->getView().getSize().x/(float)g_game.scrnW;
        polygon.setOutlineThickness(w_zoom*1.0f/getScale());
        g_game.window->draw( polygon, matrix.getTransform());
    } else if( mode == DDRAW_REDUCED ) {
        //lower left v
        lines_vect.push_back(sf::Vertex(sf::Vector2f( aabb->lowerBound.x, aabb->lowerBound.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( aabb->lowerBound.x, aabb->upperBound.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( aabb->lowerBound.x, aabb->upperBound.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( aabb->upperBound.x, aabb->upperBound.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( aabb->upperBound.x, aabb->upperBound.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( aabb->upperBound.x, aabb->lowerBound.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( aabb->upperBound.x, aabb->lowerBound.y), sf::Color::Green));
        lines_vect.push_back(sf::Vertex(sf::Vector2f( aabb->lowerBound.x, aabb->lowerBound.y), sf::Color::Green));


    }
}

//returns current translation as a 2D vector
//b2Vec2: vector of two floats
b2Vec2 CDebugDraw::getTranslation() {
    sf::Vector2f pos = matrix.getPosition();
    b2Vec2 ret_pos( pos.x, pos.y);
    return ret_pos;
}

//returns current rotation in radians
float CDebugDraw::getRotation() {
    return matrix.getRotation();
}

//returns zoom as float, 1.0f means 1:1
float CDebugDraw::getScale() {
    sf::Vector2f sc = matrix.getScale();
    float ret_sc = (sc.x+sc.y)/2;
    return ret_sc;
}

//translation = eltolás
//relative to current translation
//always in default direction and default scale
void CDebugDraw::translate( float tX, float tY ) {
    matrix.move( -tX, -tY);
}

//absolute translation
void CDebugDraw::setTranslation( float absX, float absY ) {
    matrix.setPosition( -absX, -absY);
}

//rotate around the origin ( 0, 0)
//relative to current rotation, in degrees
void CDebugDraw::rotate( float rot_deg ) {
    matrix.rotate( rot_deg );
}

//absolute rotation, in degrees
void CDebugDraw::setRotation( float abs_rot_deg ) {
    matrix.setRotation( abs_rot_deg );
}

//nagyitas/kicsinyites
//relative to current scale
void CDebugDraw::scale( float zoom ) {
   matrix.scale( zoom, zoom);
}

//absolute scale
void CDebugDraw::setScale( float abs_zoom ) {
    matrix.setScale( abs_zoom, abs_zoom);
}

unsigned int CModelMatrix::getPointCount() const {
    return 1;
}

sf::Vector2f CModelMatrix::getPoint(unsigned int index) const {
    return sf::Vector2f( 0.0f, 0.0f);
}
