//compiling
//g++ Main.cpp Game.cpp MenuState.cpp GUI.cpp Util.cpp -o game -lsfml-system -lsfml-window -lsfml-graphics

#include <iostream>
#include <SFML/Graphics.hpp>
#include "GameState/GameState.h"
#include "Game.h"
/////////////////
#include "GameState/EditorState.h"
#include "Editor/EditorSaveState.h"
#include "Editor/EditorLoadState.h"
/////////////////

#define FPS_DEBUG_TIME 100000

CGame g_game;

int main(int argc, char* argv[]) {

	CGameState* cur_state = NULL;
	sf::Event cur_event;

	//time elapsed since the start of the app
	sf::Clock game_timer;
	//the time when the next frame has to be drawn
	sf::Time next_frame_t;

	sf::Clock debug_timer;
	unsigned int fps_cnt = 0;

	g_game.init();

    while( g_game.isRunning() ) {

        //create new gamestate
        switch( g_game.active_state ) {
            case STATE_EDITOR:
                g_game.debug << "New state: STATE_EDITOR.";
                g_game.active_state = STATE_EDITOR;
                cur_state = new CEditorState;
                cur_state->loadState();
                break;
            case STATE_EDITOR_SAVE:
                g_game.debug << "New state: STATE_EDITOR_SAVE.";
                g_game.active_state = STATE_EDITOR_SAVE;
                cur_state = new CEditorSaveState;
                cur_state->loadState();
                break;
            case STATE_EDITOR_LOAD:
                g_game.debug << "New state: STATE_EDITOR_LOAD.";
                g_game.active_state = STATE_EDITOR_LOAD;
                cur_state = new CEditorLoadState;
                cur_state->loadState();
                break;
            default:
                g_game.error << "Unknown gamestate, can't be loaded.";
                g_game.error << g_game.active_state << "\n";
                g_game.setRunning(0);
                break;
        }

        fps_cnt = 0;
        debug_timer.restart();
        next_frame_t = sf::milliseconds(0);
        game_timer.restart();

        //while we stay in the same state, and the game is running
        while( g_game.isRunning() && g_game.active_state == cur_state->getNextState() ) {
            //the time in SDL_Ticks when the next frame comes
            //until then we have time for this frame
            next_frame_t += g_game.getFrameTime();

            while( g_game.window->pollEvent( cur_event ) ) {
                cur_state->handleEvent( cur_event );
            }

            cur_state->step();
            //Render only if we have enough time for it
            if( game_timer.getElapsedTime() < next_frame_t ) {
                cur_state->render();
            } else g_game.debug << "Skipping frame...";

            //regulating FPS
            if( cur_state->do_sleep ) { //sleep
                delayUntil( game_timer, next_frame_t);
            } else {                    //idle
                while( game_timer.getElapsedTime() < next_frame_t ) {
                    //pass the remaining time until next frame
                    cur_state->idle( next_frame_t-game_timer.getElapsedTime() );
                }
            }

            fps_cnt++;
            if( debug_timer.getElapsedTime() > sf::milliseconds(FPS_DEBUG_TIME) ) {
                std::cout << "FPS: " << fps_cnt*1000.0/FPS_DEBUG_TIME << "\n";
                fps_cnt = 0;
                debug_timer.restart();
            }

        }		//end while in current state

        if( cur_state != NULL ) {
            //set the name of the next state
            g_game.active_state = cur_state->getNextState();
            g_game.debug << "Leaving current gamestate...";
            //delete current gamestate, to create the next in the next loop
            cur_state->leaveState();
            delete cur_state;
            cur_state = NULL;
        }


    }   //end while game is running

	g_game.end();


	return 0;
}


