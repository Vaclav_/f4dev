
#include <assert.h>
#include <Box2D/Box2D.h>
#include "CarPhysics.h"
#include "GroundType.h"
#include "Telemetry.h"

	#include <iostream>
	#include <stdio.h>

	char print_str[100];
	char str_debug_fl[100];
	char str_debug_fr[100];
	char str_debug_rl[100];
	char str_debug_rr[100];
	char str_debug_drive[50];
	char str_debug_brake[50];
	char str_debug_controls[50];

	float PHYSICS_TIMESTEP = (1.0f/50.0f);	//seconds

    //all dimensions in meters
	float CAR_WIDTH;
	float CAR_LENGTH;
	float TIRE_WIDTH;
	float TIRE_LENGTH;
    //should be TIRE_LENGTH/2
	float TIRE_RADIUS;
    //position from the car's center
	float FRONT_TIRE_X;
	float FRONT_TIRE_Y;
	float REAR_TIRE_X;
	float REAR_TIRE_Y;

    //in kilogramms
	float CAR_MASS;
	float TIRE_MASS;

    int CAR_DRIVE_TYPE;
    float CAR_RESTITUTION;
    float WALL_RESTITUTION;
    float CAR_FRICTION;
    float REVERSE_TORQUE;

	float AIR_RESISTANCE_CONST;

    //drive torque = DRIVE_TARGET_ANG_VEL-ang_vel
	float DRIVE_TARGET_ANG_VEL;
    float DRIVE_TORQUE_SATURATION;
    //max is MAX_BRAKE_TORQUE*100
	float MAX_BRAKE_TORQUE;

    //in degrees
	float MAX_STEERING_ANGLE;
	float STEERING_ANG_VEL;
    
///PARAMETERS DEBEPNDING ON GROUND TYPE
struct SGroundData {
	float MAX_FWD_FRICTION;
	float FWD_FRICTION_FALLBACK;
	float MAX_LAT_FRICTION;
	float LAT_FRICTION_FALLBACK;
    //allows the wheels to accelerate or slow down the car by pushing the road
	//also effects impulses to the car, slows them (collisions)    
	float TIRE_FWD_FRICTION_MULT;
	float ROAD_TO_TIRE_TORQUE;
	float FRICTION_DRAWBACK_CONST;
} ground_data[GTYPE_MAX];

//////////////

    float BOX_SIZE;
    float BOX_MASS;
    float BOX_RESTITUTION;
    float BOX_FRICTION;
    float BOX_FRICTION_DRAWBACK;
    float BOX_ANG_DAMPING;

	extern CTelemetry telemetry;

//Transimmion paramters data
std::vector<float> TR_ANG_VEL_MIN;
std::vector<float> TR_ANG_VEL_MAX;
std::vector<float> TR_MAX_TORQUE;
std::vector<float> TR_MIN_TORQUE;

//TR_ANG_VEL_DIFF.at(n) = TR_ANG_VEL_MAX.at(n) - TR_ANG_VEL_MIN.at(n)
std::vector<float> TR_ANG_VEL_DIFF;

int CTirePhysics::getType() { return POBJECT_TIRE; }
void CTirePhysics::setBody( b2Body* s_body ) { body = s_body; }
b2Body* CTirePhysics::getBody() { return body; }
void CTirePhysics::setGround( int g_type ) { ground_type = g_type; }
int CTirePhysics::getGround() { return ground_type; }

CTirePhysics::CTirePhysics( b2World* world, int tire_number, float t_radius, float mass, float width, float length, b2Vec2 pos, float ang ) :
	ground_type(0),
	ang_vel(0.0f),
    torque(0.0f),
	inertia((mass * t_radius * t_radius)/2.0), //intertia of a wheel shape
    tire_n(tire_number),
    radius(t_radius),
	is_skidding(false),
	is_skidding_fwd(false),
	is_skidding_lat(false)
    {
	
	b2BodyDef body_def;
	body_def.position = pos;
	body_def.angle = ang;
	body_def.type = b2_dynamicBody;
	body = world->CreateBody(&body_def);

	b2PolygonShape polygon_shape;
	//all units are in meters
	polygon_shape.SetAsBox( width/2, length/2 );
	float tire_density = mass/(width*length);
	b2Fixture* tire_fixture = body->CreateFixture( &polygon_shape, tire_density);
    b2Filter tire_filter;
    tire_filter.categoryBits = COLLENTITY_TIRE;
    tire_fixture->SetFilterData(tire_filter);
	body->SetUserData( this );
}

void CTirePhysics::startSkidding() {
	is_skidding = true;
}

void CTirePhysics::stopSkidding() {
	is_skidding = false;
}

void CTirePhysics::addTorque( float add_t ) {
	torque += add_t;
}

void CTirePhysics::updateFriction() {

	//===========forward friction force================

	b2Vec2 cur_fwd_normal = body->GetWorldVector( b2Vec2(0,1) );

	//calculate speed of tire patch at the ground, relative to the wheel centre
	b2Vec2 patch_fwd_vel_vect = ang_vel * radius * (-cur_fwd_normal);
	float patch_fwd_vel = b2Dot( patch_fwd_vel_vect, cur_fwd_normal );

	//ground velocity = -wheel/car velocity
	//rel. to the wheel
	//project onto fwd normal
	float gnd_fwd_vel = -b2Dot( getForwardVelocity(), cur_fwd_normal );
	//get velocity difference between ground and the tire patch
	float vel_diff = patch_fwd_vel - gnd_fwd_vel;

	//calculate friction forces
	//the friction force seems to be proportional to the speed difference between the tire patch and the road
	float fwd_response_force = -vel_diff * ground_data[ground_type].TIRE_FWD_FRICTION_MULT;
	//limit friction: allow skidding
	//FWD_FRICTION_FALLBACK: a súrlódási erő kisebb, mint a tapadási
	if( ground_data[ground_type].MAX_FWD_FRICTION < fwd_response_force ) {
		fwd_response_force = ground_data[ground_type].FWD_FRICTION_FALLBACK;
		is_skidding_fwd = true;
	} else if( fwd_response_force < -ground_data[ground_type].MAX_FWD_FRICTION ) {
		fwd_response_force = -ground_data[ground_type].FWD_FRICTION_FALLBACK;
		is_skidding_fwd = true;
	} else
		is_skidding_fwd = false;

	//apply force
	body->ApplyForce( fwd_response_force * cur_fwd_normal, body->GetWorldCenter() );

	//calculate torque on the wheel
	torque += vel_diff * radius * ground_data[ground_type].ROAD_TO_TIRE_TORQUE;

	//debug
	if( tire_n == TIRE_FL )
		sprintf( str_debug_fl, "FL: Torque|%i|Ang.vel|%i|RForce|%i", (int)torque, (int)ang_vel, (int)fwd_response_force);
	if( tire_n == TIRE_FR )
		sprintf( str_debug_fr, "FR: Torque|%i|Ang.vel|%i|RForce|%i", (int)torque, (int)ang_vel, (int)fwd_response_force);
	if( tire_n == TIRE_RL )
		sprintf( str_debug_rl, "RL: Torque|%i|Ang.vel|%i|RForce|%i", (int)torque, (int)ang_vel, (int)fwd_response_force);
	if( tire_n == TIRE_RR )
		sprintf( str_debug_rr, "RR: Torque|%i|Ang.vel|%i|RForce|%i", (int)torque, (int)ang_vel, (int)fwd_response_force);

	//==================================================
	//==============Kill lateral velocity==============

	//I = m*v = m*(F/m)*t = F*t
	//F = m*v/t = I/t
	b2Vec2 impulse = body->GetMass() * -getLateralVelocity();
	b2Vec2 tire_lateral_force;
	tire_lateral_force.x = impulse.x/PHYSICS_TIMESTEP;
	tire_lateral_force.y = impulse.y/PHYSICS_TIMESTEP;
	//limit lateral friction -> skidding
	if ( tire_lateral_force.Length() > ground_data[ground_type].MAX_LAT_FRICTION ) {
		tire_lateral_force *= ground_data[ground_type].LAT_FRICTION_FALLBACK / tire_lateral_force.Length();
		is_skidding_lat = true;
	} else
		is_skidding_lat = false;

	//kill lateral velocity
	body->ApplyForce( tire_lateral_force, body->GetWorldCenter() );

	//==================================================
	//================friction drawback=================
	//rolling resistance

	//Fstop = m*v/t --> force needed to stop in this timestep
	//if Fstop is less than the friction force, apply only Fstop
	b2Vec2 f_stop = (-body->GetMass()/PHYSICS_TIMESTEP)*body->GetLinearVelocity();
	if( ground_data[ground_type].FRICTION_DRAWBACK_CONST < f_stop.Length() ) {
		b2Vec2 fwd_dir = body->GetWorldVector( b2Vec2( 0.0f, 1.0f) );
		fwd_dir.Normalize();
		//F = u*m*g/r
		//replaced with a constant
		//depends on ground type
		b2Vec2 friction_drawback = ground_data[ground_type].FRICTION_DRAWBACK_CONST*(-fwd_dir);
		body->ApplyForce( friction_drawback, body->GetWorldCenter() );
	} else {
		body->ApplyForce( f_stop, body->GetWorldCenter() );
	}

}

void CTirePhysics::step() {
	//integrate torque into wheel angular velocity
	ang_vel += torque / inertia * PHYSICS_TIMESTEP;
	//zero after integrating
	torque = 0;
}

//====================================================================
//==========CDrive====================================================

CDrive::CDrive( int drive_t, std::vector<CTirePhysics*> set_tires, float t_ang_vel, float torque_sat, float rev_torque, float m_brake_torque ) :
	drive_type(drive_t),
	tires(set_tires),
	target_ang_vel(t_ang_vel),
	torque_saturation(torque_sat),
	reverse_torque(rev_torque),
	max_brake_torque(m_brake_torque),
    cur_gear(0),
    gear_num(5), //number of gears
    transmission_state(TR_READY),
    shift_time_cnt(0),
	filter_tap_num(6),
	cur_filter_pos(0)
	{
	filter_values.resize( filter_tap_num, 0.0f);
}

void CDrive::checkTrShift() {
    if( transmission_state != TR_READY ) {
        shift_time_cnt--;
        if( shift_time_cnt <= 0 ) {
            if( transmission_state == TR_GEAR_UP ) {
                cur_gear++;
                transmission_state = TR_READY;
            }
            else if( transmission_state == TR_GEAR_DOWN ) {
                cur_gear--;
                transmission_state = TR_READY;
            } else {
                std::cout << "Error: no transmission_state.\n";
                assert( 0 && "Error: no transmission_state." );
            }
        }
    }
}

float CDrive::applyFilter( float tr_torque ) {
	filter_values.at( cur_filter_pos ) = tr_torque;
	
	cur_filter_pos++;
	if( cur_filter_pos >= filter_values.size() )
		cur_filter_pos = 0;
		
    float filtered_torque = 0.0f;
	for( unsigned int i=0; i<filter_values.size(); i++)
		filtered_torque += filter_values.at(i);
		
	filtered_torque /= (float)filter_values.size();
	
	return filtered_torque;
}

float CDrive::getTransmissionTorque( float throttle, float ang_vel) {
    //if( ang_vel < 0.0f) 
    assert( throttle >= 0.0f );
    assert( throttle <= 100.0f );
    assert( cur_gear <= gear_num );
    assert( cur_gear >= 0 );
	
	//ang_vel (x) amount to fall 500 torque, if ang_vel exceeds TR_ANG_VEL_MAX.at(cur_gear)
	float TORQUE_FALLOFF_RATE = 10.0f;
    
    float tr_torque = 0.0f;
    if( ang_vel >= TR_ANG_VEL_MIN.at(cur_gear) ) {
		if( ang_vel <= TR_ANG_VEL_MAX.at(cur_gear) ) {
			// 1.0f*TR_MAX_TORQUE at w min and 0.0f at w max
			// ( w max - w) / ( w max - w min )
			 tr_torque = ( TR_ANG_VEL_MAX.at(cur_gear) - ang_vel ) / ( TR_ANG_VEL_MAX.at(cur_gear) - TR_ANG_VEL_MIN.at(cur_gear) );
			 tr_torque = b2Clamp( tr_torque, 0.0f, 1.0f);
			 float torque_diff = TR_MAX_TORQUE.at(cur_gear) - TR_MIN_TORQUE.at(cur_gear);
			 tr_torque = tr_torque * torque_diff + TR_MIN_TORQUE.at(cur_gear);
		 } else {
			tr_torque = TR_MIN_TORQUE.at(cur_gear);
			tr_torque -= TR_ANG_VEL_MAX.at(cur_gear)/TORQUE_FALLOFF_RATE * 500.0f;
			if( tr_torque < 0.0f ) tr_torque = 0.0f;
		 }
    } else if( cur_gear == 0 ) {
        tr_torque = TR_MAX_TORQUE.at(0);
    } else {
        // 0.0f at 0.0f and 1.0f at w min
        tr_torque = ang_vel / TR_ANG_VEL_MIN.at(cur_gear);
        tr_torque = b2Clamp( tr_torque, 0.0f, 1.0f);
        float torque_diff = TR_MAX_TORQUE.at(cur_gear) - TR_MIN_TORQUE.at(cur_gear);
        tr_torque = tr_torque * torque_diff + TR_MIN_TORQUE.at(cur_gear);
    }
    
    tr_torque = tr_torque * throttle / 100.0f;
	
    return tr_torque;    
}


//throttle from 0 to 100 %
void CDrive::applyDriveTorque( float throttle, float brake, bool rev ) {
	float drive_torque;
	float ang_vel_avg;
    //reverse direction
    if( rev && throttle < 0.1f && brake < 0.1f ) {
        drive_torque = -reverse_torque;
    } else {
        //forward direction
        if( drive_type==DRIVE_FRONT ) {
            ang_vel_avg = tires.at(TIRE_FL)->ang_vel + tires.at(TIRE_FR)->ang_vel;
            ang_vel_avg /= 2.0f;
        } else if ( drive_type==DRIVE_REAR ) {
            ang_vel_avg = tires.at(TIRE_RL)->ang_vel + tires.at(TIRE_RR)->ang_vel;
            ang_vel_avg /= 2.0f;
        } else if( drive_type == DRIVE_ALL ) {
            ang_vel_avg = tires.at(TIRE_FL)->ang_vel + tires.at(TIRE_FR)->ang_vel;
            ang_vel_avg += tires.at(TIRE_RL)->ang_vel + tires.at(TIRE_RR)->ang_vel;
            ang_vel_avg /= 4.0f;
        } else {
            std::cout << "CDrive::applyDriveTorque: ERROR: unkown drive type.\n";
            ang_vel_avg = 0.0f;
        }

        if( throttle > 0.0f ) {
            if(throttle>100.0f)	throttle = 100.0f;
            //1, drive_torque = throttle*( target_ang_vel-ang_vel_avg );
            //2, drive_torque = throttle*target_ang_vel;
            if( transmission_state == TR_READY ) {
                drive_torque = getTransmissionTorque( throttle, ang_vel_avg);
                drive_torque = b2Clamp( drive_torque, 0.0f, torque_saturation);
            }
            else
                drive_torque = 0.0f;
            
        } else drive_torque = 0.0f;
        
    }
	
	drive_torque = applyFilter(drive_torque);
    
    autoGear( throttle, ang_vel_avg );
    checkTrShift();

	//debug
	sprintf( str_debug_drive, "Drive torque: %i", (int)drive_torque);

	telemetry << drive_torque;
    telemetry << ang_vel_avg * 10.0f;

	if( drive_type==DRIVE_FRONT ) {
		tires.at(TIRE_FL)->addTorque(drive_torque);
		tires.at(TIRE_FR)->addTorque(drive_torque);
	} else if ( drive_type==DRIVE_REAR ) {
		tires.at(TIRE_RL)->addTorque(drive_torque);
		tires.at(TIRE_RR)->addTorque(drive_torque);
	} else {	//DRIVE_ALL
		tires.at(TIRE_FL)->addTorque(drive_torque);
		tires.at(TIRE_FR)->addTorque(drive_torque);
		tires.at(TIRE_RL)->addTorque(drive_torque);
		tires.at(TIRE_RR)->addTorque(drive_torque);
	}

}

//braking: from 0 to 100 % positive
void CDrive::applyBrakes( float braking ) {

	float brake_torque;
	//d(ang_vel) = (M/I)*t
	//Mstop = ang_vel*I/t

	float M_stop;
	int tire_n;
	for( tire_n=0; tire_n<4; tire_n++ ) {
		if ( braking > 0.0f ) {
			if( braking>100.0f)	braking = 100.0f;
			brake_torque = braking * max_brake_torque;	//always positive
			M_stop = tires.at(tire_n)->ang_vel*tires.at(tire_n)->inertia/PHYSICS_TIMESTEP;

			//make it positive
			if( M_stop < 0.0f )
				M_stop *= (-1.0f);
			if( brake_torque < M_stop ) {	//so the wheel won't stop yet
				if( tires.at(tire_n)->ang_vel > 0.0f ) {
					sprintf( str_debug_brake, "Brake torque: %i", (int)-brake_torque);
					tires.at(tire_n)->addTorque(-brake_torque);
				} else {
					sprintf( str_debug_brake, "Brake torque: %i", (int)brake_torque);
					tires.at(tire_n)->addTorque(brake_torque);
				}
			} else {	//so apply less torque, not to start rolling in the opposite direction
				if( tires.at(tire_n)->ang_vel > 0.0f ) {
					sprintf( str_debug_brake, "Brake torque: %i", (int)-M_stop);
					tires.at(tire_n)->addTorque(-M_stop);
				} else {
					sprintf( str_debug_brake, "Brake torque: %i", (int)M_stop);
					tires.at(tire_n)->addTorque(M_stop);
				}
			}

		} else sprintf( str_debug_brake, "Brake torque: %i", 0);

	}

}


//====================================================================
//==========CCarDef===================================================

CCarDef::CCarDef() :
	drive_t(-1),
	car_mass(0.0f),
	car_width(0.0f),
	car_length(0.0f),
	tire_rad(0.0f),
	tire_mass(0.0f),
	tire_width(0.0f),
	tire_length(0.0f),
	front_tire_x(0.0f),
	front_tire_y(0.0f),
	rear_tire_x(0.0f),
	rear_tire_y(0.0f),
	drive_target_ang_vel(0.0f),
	drive_torque_sat(0.0f),
	rev_torque(0.0f),
	max_brake_torque(0.0f)
	{
}

bool CCarDef::isValid() {
	bool valid = true;
	if( drive_t<0 || drive_t>DRIVE_ALL ) valid = false;
	if( car_mass == 0.0f ) valid = false;
	if( car_width == 0.0f ) valid = false;
	if( car_length == 0.0f ) valid = false;
	if( tire_rad == 0.0f ) valid = false;
	if( tire_mass == 0.0f ) valid = false;
	if( tire_width == 0.0f ) valid = false;
	if( tire_length == 0.0f ) valid = false;
	if( front_tire_x == 0.0f ) valid = false;
	if( front_tire_y == 0.0f ) valid = false;
	if( rear_tire_x == 0.0f ) valid = false;
	if( rear_tire_y == 0.0f ) valid = false;
	if( drive_target_ang_vel == 0.0f ) valid = false;
	if( drive_torque_sat == 0.0f ) valid = false;
	if( rev_torque == 0.0f ) valid = false;
	if( max_brake_torque == 0.0f ) valid = false;
	return valid;
}

//====================================================================
//==========CCarPhysics===============================================

int CCarPhysics::getType() { return POBJECT_CAR; }
void CCarPhysics::setBody( b2Body* s_body ) { body = s_body; }
b2Body* CCarPhysics::getBody() { return body; }
void CCarPhysics::setGround( int g_type ) { }
int CCarPhysics::getGround() { return GTYPE_NONE; }

CCarPhysics::CCarPhysics( b2World* world, CCarDef& car_def, b2Vec2 pos, float ang ) :
    m_world(world),
    body(NULL),
    drive_type(car_def.drive_t),
	throttle(0.0f),
	braking(0.0f),
	steering(0.0f),
    reverse(false),
    drive(NULL)
    {

	assert(car_def.isValid());

	//create car body
	b2BodyDef body_def;
	body_def.position = pos;
	body_def.angle = ang;
	body_def.type = b2_dynamicBody;
	body = world->CreateBody(&body_def);
	//create car shape
	b2PolygonShape polygon_shape;
	polygon_shape.SetAsBox( car_def.car_width/2, car_def.car_length/2 );	//half sizes
	float car_density = car_def.car_mass/(car_def.car_width*car_def.car_length);
    b2Fixture* fixture = body->CreateFixture(&polygon_shape, car_density);
    fixture->SetRestitution(CAR_RESTITUTION);
    fixture->SetFriction(CAR_FRICTION);
    body->SetUserData( this );

	b2RevoluteJoint* new_joint;
	b2RevoluteJointDef joint_def;
	joint_def.bodyA = body;
	joint_def.enableLimit = true;
	joint_def.lowerAngle = 0;		//if we set these both to the same value
	joint_def.upperAngle = 0;		//then the joint will not move
	joint_def.localAnchorB.SetZero(); //joint anchor in tire is always center

	//FL Front Lieft tire
	b2Vec2 tire_pos( pos.x-car_def.front_tire_x, pos.y+car_def.front_tire_y);
	//world*, tire_n, float t_radius, float mass, float width, float length
	CTirePhysics* tire_p = new CTirePhysics( world, TIRE_FL,  car_def.tire_rad, car_def.tire_mass, car_def.tire_width, car_def.tire_length, pos, ang );
	joint_def.bodyB = tire_p->body;
	joint_def.localAnchorA.Set( -car_def.front_tire_x, car_def.front_tire_y );
	
	new_joint = (b2RevoluteJoint*)world->CreateJoint( &joint_def );
	tires.push_back(tire_p);
	joints.push_back(new_joint);

	//FR
	tire_pos.x = pos.x+car_def.front_tire_x;
	tire_pos.y = pos.y+car_def.front_tire_y;
	tire_p = new CTirePhysics( world, TIRE_FR, car_def.tire_rad, car_def.tire_mass, car_def.tire_width, car_def.tire_length, pos, ang );
	joint_def.bodyB = tire_p->body;
	joint_def.localAnchorA.Set( car_def.front_tire_x, car_def.front_tire_y );
	new_joint = (b2RevoluteJoint*)world->CreateJoint( &joint_def );
	tires.push_back(tire_p);
	joints.push_back(new_joint);

	//RL
	tire_pos.x = pos.x-car_def.rear_tire_x;
	tire_pos.y = pos.y-car_def.rear_tire_y;
	tire_p = new CTirePhysics( world, TIRE_RL, car_def.tire_rad, car_def.tire_mass, car_def.tire_width, car_def.tire_length, pos, ang );
	joint_def.bodyB = tire_p->body;
	joint_def.localAnchorA.Set( -car_def.rear_tire_x, -car_def.rear_tire_y );
	new_joint = (b2RevoluteJoint*)world->CreateJoint( &joint_def );
	tires.push_back(tire_p);
	joints.push_back(new_joint);

	//RR
	tire_pos.x = pos.x+car_def.rear_tire_x;
	tire_pos.y = pos.y-car_def.rear_tire_y;
	tire_p = new CTirePhysics( world, TIRE_RR, car_def.tire_rad, car_def.tire_mass, car_def.tire_width, car_def.tire_length, pos, ang );
	joint_def.bodyB = tire_p->body;
	joint_def.localAnchorA.Set( car_def.rear_tire_x, -car_def.rear_tire_y );
	new_joint = (b2RevoluteJoint*)world->CreateJoint( &joint_def );
	tires.push_back(tire_p);
	joints.push_back(new_joint);

	//int drive_t, std::vector<CTirePhysics*> set_tires, float t_ang_vel, float torque_sat, float rev_torque, float m_brake_torque
	drive = new CDrive( car_def.drive_t, tires, car_def.drive_target_ang_vel, car_def.drive_torque_sat, car_def.rev_torque, car_def.max_brake_torque );
}


CCarPhysics::~CCarPhysics() {

    while(joints.size()!=0) {
        if(joints.back()!=0) {
            m_world->DestroyJoint( joints.back() );
        }
        joints.pop_back();
    }

	while(tires.size()!=0) {
		//the last element
		if(tires.back()!=NULL) {
			delete tires.back();
		}
		tires.pop_back();
	}

	if(body!=NULL) {
        m_world->DestroyBody(body);
        body = NULL;
	}

	if( drive != NULL ) {
		delete drive;
		drive = NULL;
	}
	std::cout << "CAR DESTRUCTOR\n" ;
}

void CCarPhysics::setSteering(float thr, float br, float steer, bool rev) {
	throttle = thr;
	braking = br;
	steering = steer;
    reverse = rev;
}


void CCarPhysics::step() {
	//Set front wheels according to steering
	float lock_angle = MAX_STEERING_ANGLE * DEGTORAD;
	float steering_turn_per_sec = STEERING_ANG_VEL * DEGTORAD;//from lock to lock in 0.25 sec //320
	float steering_turn_per_timestep = steering_turn_per_sec / 50.0f;
	float desired_angle = 0;
 
	desired_angle = -lock_angle*steering/100.0f;

	float cur_angle = joints.at(TIRE_FL)->GetJointAngle();
	float angle_to_turn = desired_angle - cur_angle;
	angle_to_turn = b2Clamp( angle_to_turn, -steering_turn_per_timestep, steering_turn_per_timestep );
	float new_angle = cur_angle + angle_to_turn;
	joints.at(TIRE_FL)->SetLimits( new_angle, new_angle );
	joints.at(TIRE_FR)->SetLimits( new_angle, new_angle );

	//Apply air resistance
	//air resistance = (v^2)*constant
	//direction is opposite of velocity
	float car_vel_mag = body->GetLinearVelocity().Length();
	float air_force_mag = car_vel_mag*car_vel_mag*AIR_RESISTANCE_CONST;
	b2Vec2 air_drag = body->GetLinearVelocity();
	air_drag.Normalize();
	air_drag *= (-air_force_mag);
	body->ApplyForce( air_drag, body->GetWorldCenter() );

	drive->applyDriveTorque( throttle, braking, reverse );
	drive->applyBrakes( braking );

	for ( unsigned int i = 0; i < tires.size(); i++) {
		tires.at(i)->updateFriction();
		tires.at(i)->step();
	}

}


//====================================================================
//==========CBox======================================================

int CBox::getType() { return POBJECT_BOX; }
void CBox::setBody( b2Body* s_body ) { body = s_body; }
b2Body* CBox::getBody() { return body; }
void CBox::setGround( int g_type ) { ground_type = g_type; }
int CBox::getGround() { return ground_type; }

CBox::CBox( b2World* world, float mass, float size_x, float size_y ) {
	b2BodyDef body_def;
	body_def.type = b2_dynamicBody;
	body_def.linearDamping = BOX_FRICTION_DRAWBACK;
	body_def.angularDamping = BOX_ANG_DAMPING;
	body = world->CreateBody(&body_def);
	b2PolygonShape polygon_shape;
	polygon_shape.SetAsBox( size_x/2, size_y/2 );
	float box_density = mass/(size_x*size_y);
    b2Fixture* fixture = body->CreateFixture(&polygon_shape, box_density);
    fixture->SetRestitution(BOX_RESTITUTION);
    fixture->SetFriction(BOX_FRICTION);
    b2Filter box_filter;
	//make it collide with ground sensors
    box_filter.categoryBits = COLLENTITY_TIRE;
    fixture->SetFilterData(box_filter);
    body->SetUserData( this );
}

void CBox::step() {

	/*//TODO: make friction/vel damping depend on ground type
    b2Vec2 vel = body->GetLinearVelocity();
    b2Vec2 drawback_f = vel;
    drawback_f.Normalize();
    drawback_f *= BOX_FRICTION_DRAWBACK;
    //Fstop = m*v/t
    float f_stop_mag = body->GetMass()*vel.Length()/PHYSICS_TIMESTEP;
    if( f_stop_mag < drawback_f.Length() )
        drawback_f *= (f_stop_mag/drawback_f.Length());
    body->ApplyForce( -drawback_f, body->GetWorldCenter());
    
    float ang_vel = body->GetAngularVelocity();
    //Mstop = ang_vel*I/t
    float m_stop = b2Abs(ang_vel)*body->GetInertia()/PHYSICS_TIMESTEP;
    float m_damping = BOX_ANG_DAMPING;
    if( m_stop < m_damping )
        m_damping = m_stop;
    if( ang_vel > 0.0f )
        m_damping *= (-1.0f);
    body->ApplyTorque( m_damping );*/
}

