#ifndef CARTEST_H_
#define CARTEST_H_

	#include "CarPhysics.h"
	#include <vector>
	#include <iostream>
	#include "stdio.h"
	#include "Test.h"
    #include "GroundType.h"
	#include "Telemetry.h"
   
	class CCarTest : public Test
	{
		//only for graphics
		b2Body* panel_body;
		CCarPhysics* car0;
		CCarPhysics* car1;
        CBox* box0;
        CGroundSensor* gnd0;
        bool ctrl_reverse;
        CContactListener contact_listener_inst;

		//throttle from 0 to 100
		float getThrottle();
	
		//brake from 0 to 100
		float getBrake();
        
        //from -100 to +100
		float getSteering();

		public:
		CCarTest();
        ~CCarTest();

		void Step(Settings* settings);

		void Keyboard(unsigned char key) {
			switch (key) {
				case 'a' :
                    ctrl_reverse = true;
                    break;
                case 'y':
                    car0->drive->gearUp();
                    break;
                case 'x':
                    car0->drive->gearDown();
                    break;
				default: Test::Keyboard(key);
			}
		}

		void KeyboardUp(unsigned char key) {
			switch (key) {
				case 'a' :
                    ctrl_reverse = false;
                    break;
				default: Test::Keyboard(key);
			}
		}

		static Test* Create()
		{
			return new CCarTest;
		}
	};


#endif
