
#include "CarTest.h"
#include "glui/glui.h"
#include <sstream>

	extern int load_settings();
    extern int save_settings();
    
    const int PANEL_HALFSIZE = 67+7;
	const int PANEL_HALF_DEADBAND = 7;
	const int PANEL_MID_X = 650;
	const int PANEL_MID_Y = 550;
    
    extern int CAR_DRIVE_TYPE;
    extern float WALL_RESTITUTION;

	extern float CAR_WIDTH;
	extern float CAR_LENGTH;
	extern float TIRE_WIDTH;
	extern float TIRE_LENGTH;
	extern float TIRE_RADIUS;
	extern float FRONT_TIRE_X;
	extern float FRONT_TIRE_Y;
	extern float REAR_TIRE_X;
	extern float REAR_TIRE_Y;

	extern float CAR_MASS;
	extern float TIRE_MASS;

   	extern float REVERSE_TORQUE;
	extern float DRIVE_TARGET_ANG_VEL;
    extern float DRIVE_TORQUE_SATURATION;
	extern float MAX_BRAKE_TORQUE;

	extern float BOX_MASS;
	extern float BOX_SIZE;
    
	extern b2Vec2 ConvertScreenToWorld(int32 x, int32 y);

	extern char print_str[100];
	extern char str_debug_fl[100];
	extern char str_debug_fr[100];
	extern char str_debug_rl[100];
	extern char str_debug_rr[100];
	extern char str_debug_drive[50];
	extern char str_debug_brake[50];
	extern char str_debug_controls[50];

	CTelemetry telemetry;

    //throttle from 0 to 100
    float CCarTest::getThrottle() {
        int fwd_px = PANEL_MID_Y-m_mouseWorld.y;
        if( -PANEL_HALF_DEADBAND <= fwd_px && fwd_px <= PANEL_HALF_DEADBAND )
            return 0.0f;
        int fwd_active_band = PANEL_HALFSIZE-PANEL_HALF_DEADBAND;
        //normalize
        float fwd_mag;
        if( fwd_px > 0.0f ) {
            fwd_mag = (fwd_px-PANEL_HALF_DEADBAND)/(float)fwd_active_band*100.0f;
        } else return 0.0f; //else is braking

        if( fwd_mag > 100.0f )
            fwd_mag = 100.0f;

        return fwd_mag;
    }
    
    //brake from 0 to 100
    float CCarTest::getBrake() {
        int fwd_px = PANEL_MID_Y-m_mouseWorld.y;
        if( -PANEL_HALF_DEADBAND <= fwd_px && fwd_px <= PANEL_HALF_DEADBAND )
            return 0.0f;
        int fwd_active_band = PANEL_HALFSIZE-PANEL_HALF_DEADBAND;
        //normalize
        float fwd_mag;
        if( fwd_px < 0.0f ) {
            fwd_mag = (fwd_px+PANEL_HALF_DEADBAND)/(float)fwd_active_band*100.0f;
        } else return 0.0f; //it's throttle

        if( fwd_mag < -100.0f )
            fwd_mag = -100.0f;
        //make it positive
        fwd_mag *= -1.0f;
        return fwd_mag;
    }

    //from -100 to +100
    float CCarTest::getSteering() {
        int side_px = m_mouseWorld.x-PANEL_MID_X;
        if( -PANEL_HALF_DEADBAND <= side_px && side_px <= PANEL_HALF_DEADBAND )
            return 0.0f;
        int side_active_band = PANEL_HALFSIZE-PANEL_HALF_DEADBAND;
        //normalize
        float side_mag;
        if( side_px > 0.0f ) {
            side_mag = (side_px-PANEL_HALF_DEADBAND)/(float)side_active_band*100.0f;
        } else {
            side_mag = (side_px+PANEL_HALF_DEADBAND)/(float)side_active_band*100.0f;
        }
        if( side_mag > 100.0f )
            side_mag = 100.0f;
        else if( side_mag < -100.0f )
            side_mag = -100.0f;
        return side_mag;
    }
    
    CCarTest::CCarTest() {

        telemetry.writeHeader();

        //for ground data not in the parameters file
        initGroundData();
        load_settings();
    
        ctrl_reverse = false;
        
        m_mouseWorld.x = 0.0f;
        m_mouseWorld.y = 0.0f;


		CCarDef car_def;
		car_def.drive_t = CAR_DRIVE_TYPE;
		car_def.car_mass = CAR_MASS;
		car_def.car_width = CAR_WIDTH;
		car_def.car_length = CAR_LENGTH;

		car_def.tire_rad = TIRE_RADIUS;
		car_def.tire_mass = TIRE_MASS;
		car_def.tire_width = TIRE_WIDTH;
		car_def.tire_length = TIRE_LENGTH;

		car_def.front_tire_x = FRONT_TIRE_X;
		car_def.front_tire_y = FRONT_TIRE_Y;
		car_def.rear_tire_x = REAR_TIRE_X;
		car_def.rear_tire_y = REAR_TIRE_Y;

		car_def.drive_target_ang_vel = DRIVE_TARGET_ANG_VEL;
		car_def.drive_torque_sat = DRIVE_TORQUE_SATURATION;
		car_def.rev_torque = REVERSE_TORQUE;
		car_def.max_brake_torque = MAX_BRAKE_TORQUE;

		if(car_def.isValid())
			std::cout << "car_def is valid\n";
		else
			std::cout << "Error: car_def is invalid\n";

		car1 = new CCarPhysics( m_world, car_def, b2Vec2( 50.0f, 15.0f), 0.0f );

        car0 = new CCarPhysics( m_world, car_def, b2Vec2( -0.0f, 0.0f), 0.0f );
//( b2World* world, float mass, float size_x, float size_y )
        CBox* panel_obj = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        b2BodyDef body_def;
        //will be moved to PANEL_MID_X, PANEL_MID_X screen coordinates in step function
        body_def.position.Set( 0.0f, 0.0f );
        panel_body = m_world->CreateBody( &body_def );

        b2PolygonShape polygon_shape;
        b2FixtureDef fixture_def;
        fixture_def.shape = &polygon_shape;
        fixture_def.isSensor = true;

        polygon_shape.SetAsBox( 10, 10);
        panel_body->CreateFixture(&fixture_def);
        panel_body->SetUserData(panel_obj);
        m_world->DestroyBody(panel_obj->body);
        panel_obj->body = panel_body;

        //create some ground only for graphics (no effect on physics)
        CBox* gnd_pattern = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        body_def.position.Set( 25.0f, 25.0f );
        b2Body* new_body = m_world->CreateBody( &body_def );
        new_body->SetUserData(gnd_pattern);
        m_world->DestroyBody(gnd_pattern->body);
        gnd_pattern->body = new_body;

        gnd_pattern = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        polygon_shape.SetAsBox( 25, 25);
        new_body->CreateFixture(&fixture_def);
        body_def.position.Set( 30.0f, -30.0f );
        new_body = m_world->CreateBody( &body_def );
        new_body->CreateFixture(&fixture_def);
        new_body->SetUserData(gnd_pattern);
        m_world->DestroyBody(gnd_pattern->body);
        gnd_pattern->body = new_body;

        gnd_pattern = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        body_def.position.Set( -40.0f, 35.0f );
        new_body = m_world->CreateBody( &body_def );
        new_body->CreateFixture(&fixture_def);
        new_body->SetUserData(gnd_pattern);
        m_world->DestroyBody(gnd_pattern->body);
        gnd_pattern->body = new_body;

        gnd_pattern = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        body_def.position.Set( -40.0f, -45.0f );
        new_body = m_world->CreateBody( &body_def );
        new_body->CreateFixture(&fixture_def);
        new_body->SetUserData(gnd_pattern);
        m_world->DestroyBody(gnd_pattern->body);
        gnd_pattern->body = new_body;

        gnd_pattern = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        body_def.position.Set( 95.0f, 85.0f );
        new_body = m_world->CreateBody( &body_def );
        new_body->CreateFixture(&fixture_def);
        new_body->SetUserData(gnd_pattern);
        m_world->DestroyBody(gnd_pattern->body);
        gnd_pattern->body = new_body;

        gnd_pattern = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        body_def.position.Set( -95.0f, -85.0f );
        new_body = m_world->CreateBody( &body_def );
        new_body->CreateFixture(&fixture_def);
        new_body->SetUserData(gnd_pattern);
        m_world->DestroyBody(gnd_pattern->body);
        gnd_pattern->body = new_body;

        gnd_pattern = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        body_def.position.Set( 5.0f, 100.0f );
        new_body = m_world->CreateBody( &body_def );
        new_body->CreateFixture(&fixture_def);
        new_body->SetUserData(gnd_pattern);
        m_world->DestroyBody(gnd_pattern->body);
        gnd_pattern->body = new_body;

        gnd_pattern = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        body_def.position.Set( -5.0f, -100.0f );
        new_body = m_world->CreateBody( &body_def );
        new_body->CreateFixture(&fixture_def);
        new_body->SetUserData(gnd_pattern);
        m_world->DestroyBody(gnd_pattern->body);
        gnd_pattern->body = new_body;
        
        box0 = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        box0->body->SetTransform( b2Vec2( -15.0f, 15.0f), 0.0f);
        
        CBox* wall = new CBox( m_world, BOX_MASS, BOX_SIZE, BOX_SIZE);
        fixture_def.isSensor = false;
        body_def.type = b2_dynamicBody;
        polygon_shape.SetAsBox( 15, 15);
        body_def.position.Set( -50.0f, 50.0f );
        new_body = m_world->CreateBody( &body_def );
        b2Fixture* fx = new_body->CreateFixture( &polygon_shape, 100000.0f ); //some huge density
        fx->SetFriction(0.5f);
        fx->SetRestitution(WALL_RESTITUTION);
        new_body->SetUserData(wall);
        m_world->DestroyBody(wall->body);
        wall->body = new_body;
        /*
        std::vector<b2Vec2> p;
        p.push_back( b2Vec2( -10.0f, -10.0f) );
        p.push_back( b2Vec2( 10.0f, -10.0f) );
        p.push_back( b2Vec2( 10.0f, 10.0f) );
        p.push_back( b2Vec2( -10.0f, 10.0f) );
        gnd0 = new CGroundSensor( m_world, GTYPE_ROAD, p );
        gnd0->body->SetTransform( b2Vec2( 0.0f, 15.0f), 0.0f);
        */
        std::vector<b2Vec2> p1;
        p1.push_back( b2Vec2( -10.0f, -10.0f) );
        p1.push_back( b2Vec2( 10.0f, -10.0f) );
        p1.push_back( b2Vec2( 10.0f, 10.0f) );
        p1.push_back( b2Vec2( -10.0f, 10.0f) );
        gnd0 = new CGroundSensor( m_world, GTYPE_DIRT, p1 );
        gnd0->body->SetTransform( b2Vec2( 15.0f, 15.0f), 0.0f);
        
        m_world->SetContactListener(&contact_listener_inst);
    }

    CCarTest::~CCarTest() {
        save_settings();
		if(car0 != NULL) {
			delete car0;
			car0 = NULL;
		}
		if(car1 != NULL) {
			delete car1;
			car1 = NULL;
		}
    }
    
	static int last_fps_time = -1;
	static int fps_cnt = 0;
	static std::string fps_string;

    void CCarTest::Step(Settings* settings) {
    
        box0->step();

        if( settings->debug_controls && settings->control_on )
            sprintf( str_debug_controls, "Throttle %i|Brake %i|Steering %i", (int)getThrottle(), (int)getBrake(), (int)getSteering());
        else
            sprintf( str_debug_controls, "Throttle %i|Brake %i|Steering %i", 0, 0, 0);
            

        if( !settings->pause || settings->singleStep ) {
            //car0 has to be stepped last for the debug messages not to be cleared
            car1->setSteering( 0.0f, 0.0f, 0.0f, false);
            if( settings->control_on )
                car0->setSteering( getThrottle(), getBrake(), getSteering(), ctrl_reverse );
            else
                car0->setSteering( 0.0f, 0.0f, 0.0f, ctrl_reverse );
            //car1->step();
            car0->step();
        }
        
        telemetry << car0->body->GetLinearVelocity().Length();
        telemetry.newRow();

    //will be moved to PANEL_MID_X, PANEL_MID_X screen coordinates in step function


        int j,k;
        b2Vec2 pos;
        b2BodyDef body_def;
        b2Body* body_p;
        b2PolygonShape polygon_shape;
        b2FixtureDef fixture_def;
        fixture_def.shape = &polygon_shape;
        fixture_def.isSensor = true;
        polygon_shape.SetAsBox( 0.5f, 0.5f);
        for(j=0;j<4;j++) {
            if( car0->tires.at(j)->is_skidding || car0->tires.at(j)->is_skidding_fwd || car0->tires.at(j)->is_skidding_lat ) {
                pos = car0->tires.at(j)->body->GetPosition();
                body_def.position.Set( pos.x, pos.y );
                body_p = m_world->CreateBody( &body_def );
                fixture_def.filter.maskBits = COLLMASK_NOTHING;
                body_p->CreateFixture(&fixture_def);
            }
        }

        //run the default physics and rendering
        Test::Step(settings);

        //center view on the car
        settings->viewCenter = car0->body->GetPosition();

        //move the panel to stay relative to the screen
        b2Vec2 new_panel_pos = ConvertScreenToWorld( PANEL_MID_X, PANEL_MID_Y);
        panel_body->SetTransform( new_panel_pos, panel_body->GetAngle());
        
        //if control is on
        //don't let the cursor go out of the box
        if(settings->control_on) {
            if( m_mouseWorld.x > PANEL_MID_X+PANEL_HALFSIZE+10 )
                glutWarpPointer( PANEL_MID_X+PANEL_HALFSIZE+10, m_mouseWorld.y );
            if( m_mouseWorld.x < PANEL_MID_X-PANEL_HALFSIZE-10 )
                glutWarpPointer( PANEL_MID_X-PANEL_HALFSIZE-10, m_mouseWorld.y );
            if( m_mouseWorld.y > PANEL_MID_Y+PANEL_HALFSIZE+10 )
                glutWarpPointer( m_mouseWorld.x, PANEL_MID_Y+PANEL_HALFSIZE+10 );
            if( m_mouseWorld.y < PANEL_MID_Y-PANEL_HALFSIZE-10 )
                glutWarpPointer( m_mouseWorld.x, PANEL_MID_Y-PANEL_HALFSIZE-10 );
        }

		if( last_fps_time < 0 )
			last_fps_time = glutGet(GLUT_ELAPSED_TIME);
		else {
			int cur_time = glutGet(GLUT_ELAPSED_TIME);
			if ( cur_time - last_fps_time > 1000) {
				fps_cnt++;
				std::stringstream fps_ss;
				fps_ss << "FPS: " << fps_cnt;
				fps_string = fps_ss.str();
				last_fps_time = cur_time;				
				fps_cnt = 0;
			} else
				fps_cnt++;

            m_debugDraw.DrawString(5, m_textLine, fps_string.c_str());
            m_textLine += 15;
		}


        if(settings->debug_controls) {
            m_debugDraw.DrawString(5, m_textLine, str_debug_controls);
            m_textLine += 15;
        }
        if(settings->debug_front_tires) {
            m_debugDraw.DrawString(5, m_textLine, str_debug_fl);
            m_textLine += 15;
            m_debugDraw.DrawString(5, m_textLine, str_debug_fr);
            m_textLine += 15;
        }
        if(settings->debug_rear_tires) {
            m_debugDraw.DrawString(5, m_textLine, str_debug_rl);
            m_textLine += 15;
            m_debugDraw.DrawString(5, m_textLine, str_debug_rr);
            m_textLine += 15;
        }
        if(settings->debug_drive) {
            m_debugDraw.DrawString(5, m_textLine, str_debug_drive);
            m_textLine += 15;
        }
        if(settings->debug_brake) {
            m_debugDraw.DrawString(5, m_textLine, str_debug_brake);
            m_textLine += 15;
        }
        
		
		m_textLine += 10;
        std::stringstream gear_ss;
        gear_ss << "Gear: " << car0->drive->cur_gear+1;
        m_debugDraw.DrawString(5, m_textLine, gear_ss.str().c_str());
        m_textLine += 15;

    }
