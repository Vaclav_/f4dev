#include "CarTest.h"

extern Settings settings;

	extern float TIRE_WIDTH;
	extern float TIRE_LENGTH;
	extern float TIRE_RADIUS;
	extern float CAR_WIDTH;
	extern float CAR_LENGTH;
	extern float FRONT_TIRE_X;
	extern float FRONT_TIRE_Y;
	extern float REAR_TIRE_X;
	extern float REAR_TIRE_Y;

	extern float CAR_MASS;
	extern float TIRE_MASS;
    
    extern int CAR_DRIVE_TYPE;
    extern float CAR_RESTITUTION;
    extern float WALL_RESTITUTION;
    extern float CAR_FRICTION;
    extern float REVERSE_TORQUE;

	extern float AIR_RESISTANCE_CONST;	

	extern float DRIVE_TARGET_ANG_VEL;
    extern float DRIVE_TORQUE_SATURATION;
	extern float MAX_BRAKE_TORQUE;

	extern float MAX_STEERING_ANGLE;
	extern float STEERING_ANG_VEL;
    
    //
    extern float BOX_SIZE;
    extern float BOX_MASS;
    extern float BOX_RESTITUTION;
    extern float BOX_FRICTION;
    extern float BOX_FRICTION_DRAWBACK;
    extern float BOX_ANG_DAMPING;

	extern std::vector<float> TR_ANG_VEL_MIN;
	extern std::vector<float> TR_ANG_VEL_MAX;
	extern std::vector<float> TR_MAX_TORQUE;
	extern std::vector<float> TR_MIN_TORQUE;
	extern std::vector<float> TR_ANG_VEL_DIFF;

extern struct SGroundData {
	float MAX_FWD_FRICTION;
	float FWD_FRICTION_FALLBACK;
	float MAX_LAT_FRICTION;
	float LAT_FRICTION_FALLBACK;
	float TIRE_FWD_FRICTION_MULT;
	float ROAD_TO_TIRE_TORQUE;
	float FRICTION_DRAWBACK_CONST;
} ground_data[GTYPE_MAX];


int save_settings() {

    std::cout << "Saving settings...\n";
	FILE* fp = NULL;
	fp = fopen( "debug.cfg", "w");
	if(fp==NULL) {
		std::cout << "Couldn't open settings file!\n";
		return -1;
	}
    
    fprintf( fp, "# Debug settings\n" );
    fprintf( fp, "DEBUG_CONTROLS %i\n", settings.debug_controls);
    fprintf( fp, "DEBUG_FRONT_TIRES %i\n", settings.debug_front_tires);
    fprintf( fp, "DEBUG_REAR_TIRES %i\n", settings.debug_rear_tires);
    fprintf( fp, "DEBUG_DRIVE %i\n", settings.debug_drive);
    fprintf( fp, "DEBUG_BRAKE %i", settings.debug_brake);
    //don't write \n at the end of the file
    
    std::cout << "Saved settings succesfully.\n";
    
    fclose(fp);
    
    return 0;
}

int load_settings() {

	FILE* fp = NULL;
	fp = fopen( "param.cfg", "r");
	if(fp==NULL) {
		std::cout << "Couldn't open parameters file!\n";
		return -1;
	}

	std::cout << "Loading parameters...\n";

	float d;
	char in_buffer[300];
	int i;

	std::string temp_str;

	while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {

		if( in_buffer[0] == '#' ) {	// # marks comment
			//read until \n
			while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {
				if( in_buffer[0] == '\n' )
					break;
			}
			continue;
		}

		//go back and get param name+float
		fseek( fp, -1, SEEK_CUR );
		fscanf( fp, "%s", in_buffer);
		fscanf( fp, "%f", &d);

		temp_str = in_buffer;

		if(temp_str == "CAR_WIDTH") {
			CAR_WIDTH = d;
		} else if(temp_str == "CAR_LENGTH") {
			CAR_LENGTH = d;
		} else if(temp_str == "TIRE_WIDTH") {
			TIRE_WIDTH = d;
		} else if(temp_str == "TIRE_LENGTH") {
			TIRE_LENGTH = d;
		} else if(temp_str == "TIRE_RADIUS") {
			TIRE_RADIUS = d;
		} else if(temp_str == "FRONT_TIRE_X") {
			FRONT_TIRE_X = d;
		} else if(temp_str == "FRONT_TIRE_Y") {
			FRONT_TIRE_Y = d;
		} else if(temp_str == "REAR_TIRE_X") {
			REAR_TIRE_X = d;
		} else if(temp_str == "REAR_TIRE_Y") {
			REAR_TIRE_Y = d;
		} else if(temp_str == "CAR_MASS") {
			CAR_MASS = d;
		} else if(temp_str == "TIRE_MASS") {
			TIRE_MASS = d;
		} else if(temp_str == "MAX_FWD_FRICTION") {
			ground_data[GTYPE_ROAD].MAX_FWD_FRICTION = d;
		} else if(temp_str == "FWD_FRICTION_FALLBACK") {
			ground_data[GTYPE_ROAD].FWD_FRICTION_FALLBACK = d;
		} else if(temp_str == "MAX_LAT_FRICTION") {
			ground_data[GTYPE_ROAD].MAX_LAT_FRICTION = d;
		} else if(temp_str == "LAT_FRICTION_FALLBACK") {
			ground_data[GTYPE_ROAD].LAT_FRICTION_FALLBACK = d;
		} else if(temp_str == "TIRE_FWD_FRICTION_MULT") {
			ground_data[GTYPE_ROAD].TIRE_FWD_FRICTION_MULT = d;
		} else if(temp_str == "ROAD_TO_TIRE_TORQUE") {
			ground_data[GTYPE_ROAD].ROAD_TO_TIRE_TORQUE = d;
		} else if(temp_str == "FRICTION_DRAWBACK_CONST") {
			ground_data[GTYPE_ROAD].FRICTION_DRAWBACK_CONST = d;
		} else if(temp_str == "AIR_RESISTANCE_CONST") {
			AIR_RESISTANCE_CONST = d;
		} else if(temp_str == "DRIVE_TARGET_ANG_VEL") {
			DRIVE_TARGET_ANG_VEL = d;
		} else if(temp_str == "DRIVE_TORQUE_SATURATION") {
			DRIVE_TORQUE_SATURATION = d;
		} else if(temp_str == "MAX_BRAKE_TORQUE") {
			MAX_BRAKE_TORQUE = d;
		} else if(temp_str == "MAX_STEERING_ANGLE") {
			MAX_STEERING_ANGLE = d;
		} else if(temp_str == "STEERING_ANG_VEL") {
			STEERING_ANG_VEL = d;
		} else if(temp_str == "CAR_DRIVE_TYPE") {
			CAR_DRIVE_TYPE = d;
		} else if(temp_str == "CAR_RESTITUTION") {
			CAR_RESTITUTION = d;
		} else if(temp_str == "WALL_RESTITUTION") {
			WALL_RESTITUTION = d;
		} else if(temp_str == "CAR_FRICTION") {
			CAR_FRICTION = d;
		} else if(temp_str == "REVERSE_TORQUE") {
			REVERSE_TORQUE = d;
		} else if(temp_str == "BOX_SIZE") {
			BOX_SIZE = d;
		} else if(temp_str == "BOX_MASS") {
			BOX_MASS = d;
		} else if(temp_str == "BOX_RESTITUTION") {
			BOX_RESTITUTION = d;
		} else if(temp_str == "BOX_FRICTION") {
			BOX_FRICTION = d;
		} else if(temp_str == "BOX_FRICTION_DRAWBACK") {
			BOX_FRICTION_DRAWBACK = d;
		} else if(temp_str == "BOX_ANG_DAMPING") {
			BOX_ANG_DAMPING = d;
		} else if( temp_str == "TR_ANG_VEL_MIN" ) {
			TR_ANG_VEL_MIN.resize(5);
			TR_ANG_VEL_MIN.at(0) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MIN.at(1) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MIN.at(2) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MIN.at(3) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MIN.at(4) = d;
		} else if( temp_str == "TR_ANG_VEL_MAX" ) {
			TR_ANG_VEL_MAX.resize(5);
			TR_ANG_VEL_MAX.at(0) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MAX.at(1) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MAX.at(2) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MAX.at(3) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MAX.at(4) = d;
		} else if( temp_str == "TR_MAX_TORQUE" ) {
			TR_MAX_TORQUE.resize(5);
			TR_MAX_TORQUE.at(0) = d;
			fscanf( fp, "%f", &d);
			TR_MAX_TORQUE.at(1) = d;
			fscanf( fp, "%f", &d);
			TR_MAX_TORQUE.at(2) = d;
			fscanf( fp, "%f", &d);
			TR_MAX_TORQUE.at(3) = d;
			fscanf( fp, "%f", &d);
			TR_MAX_TORQUE.at(4) = d;
		} else if( temp_str == "TR_MIN_TORQUE" ) {
			TR_MIN_TORQUE.resize(5);
			TR_MIN_TORQUE.at(0) = d;
			fscanf( fp, "%f", &d);
			TR_MIN_TORQUE.at(1) = d;
			fscanf( fp, "%f", &d);
			TR_MIN_TORQUE.at(2) = d;
			fscanf( fp, "%f", &d);
			TR_MIN_TORQUE.at(3) = d;
			fscanf( fp, "%f", &d);
			TR_MIN_TORQUE.at(4) = d;
		} else {
			std::cout << "ERROR: unkown parameter:\n>>>>";
		}
        
		std::cout << temp_str << ": " << d << "\n";

		//read /n
		while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {
			if( in_buffer[0] == '\n' )
				break;
		}

	}

	TR_ANG_VEL_DIFF.resize(5);
	for( int n=0; n<5; n++) {
		if( n>=TR_ANG_VEL_MAX.size() || n>=TR_ANG_VEL_MIN.size() )
			break;
		TR_ANG_VEL_DIFF.at(n) = TR_ANG_VEL_MAX.at(n) - TR_ANG_VEL_MIN.at(n);
	}

	fclose( fp );

	std::cout << "Loaded parameters succesfully.\n";
      
	fp = NULL;
	fp = fopen( "debug.cfg", "r");
	if(fp==NULL) {
        //file doesn't exist: do nothing
        //not an error
		return 0;
	}
    
    std::cout << "Loading debug settings...\n";
    
	while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {

		if( in_buffer[0] == '#' ) {	// # marks comment
			//read until \n
			while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {
				if( in_buffer[0] == '\n' )
					break;
			}
			continue;
		}

		//go back and get param name+float
		fseek( fp, -1, SEEK_CUR );
		fscanf( fp, "%s", in_buffer);
		fscanf( fp, "%f", &d);

		temp_str = in_buffer;
        
        if(temp_str == "DEBUG_CONTROLS") {
			settings.debug_controls = (int)d;
		} else if(temp_str == "DEBUG_FRONT_TIRES") {
			settings.debug_front_tires = (int)d;
		} else if(temp_str == "DEBUG_REAR_TIRES") {
			settings.debug_rear_tires = (int)d;
		}  else if(temp_str == "DEBUG_DRIVE") {
			settings.debug_drive = (int)d;
		}  else if(temp_str == "DEBUG_BRAKE") {
			settings.debug_brake = (int)d;
		} else {
			std::cout << "ERROR: unkown setting:\n>>>>";
		}

		std::cout << temp_str << ": " << d << "\n";

		//read /n
		while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {
			if( in_buffer[0] == '\n' )
				break;
		}
        
    }

	return 0;
}

