/*
/*
* Copyright (c) 2006-2007 Erin Catto http://www.box2d.org
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
* 1. The origin of this software must not be misrepresented; you must not
* claim that you wrote the original software. If you use this software
* in a product, an acknowledgment in the product documentation would be
* appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
* misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
*/
// Slight modifications by Václav for custom use

#include <iostream>
#include "Render.h"
#include "Test.h"
#include "glui/glui.h"

#include <cstdio>
using namespace std;

const char* TESTBED_VERSION = "__v0.14__";

GLUI_Checkbox* control_checkbox;

	int32 testIndex = 0;
	int32 testSelection = 0;
	int32 testCount = 0;
	TestEntry* entry = NULL;
	Test* test = NULL;
	Settings settings;
	int32 width = 1024;
	int32 height = 768;
	int32 framePeriod = 20; //ms
	int32 mainWindow = 0;
	float settingsHz = 50.0;
	GLUI *glui = NULL;
	float32 viewZoom = 2.0f;
	int tx = 0, ty = 0, tw = 0, th = 0;
	bool rMouseDown = false;
	b2Vec2 lastp( 0.0f, 0.0f);

static void Resize(int32 w, int32 h)
{

	width = w;
	height = h;

	GLUI_Master.get_viewport_area(&tx, &ty, &tw, &th);
	glViewport(tx, ty, tw, th);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	float32 ratio = float32(tw) / float32(th);

	b2Vec2 extents(ratio * 25.0f, 25.0f);
	extents *= viewZoom;

	b2Vec2 lower = settings.viewCenter - extents;
	b2Vec2 upper = settings.viewCenter + extents;

	// L/R/B/T
	gluOrtho2D(lower.x, upper.x, lower.y, upper.y);
	
	GLdouble projection[16];
	GLdouble modelview[16];
	GLdouble  screen_coords[3];
	GLdouble world_coords[3];
	GLint viewport[4];
	world_coords[0]=0.0;
	world_coords[1]=0.0;
	world_coords[2]=0.0;
	
/*	//convert world coordinates to screen coordinates
	glGetDoublev(GL_PROJECTION_MATRIX, projection);
	glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
	glGetIntegerv( GL_VIEWPORT, viewport);
	
gluProject(world_coords[0], world_coords[1], world_coords[2],
    modelview, projection, viewport,
    screen_coords, screen_coords + 1, screen_coords + 2);
	
	std::cout << "X " << screen_coords[0] <<" |Y "<< screen_coords[1] <<" |Z "<<screen_coords[2] <<"\n";
*/
}

//static
b2Vec2 ConvertScreenToWorld(int32 x, int32 y)
{
	float32 u = x / float32(tw);
	float32 v = (th - y) / float32(th);

	float32 ratio = float32(tw) / float32(th);
	b2Vec2 extents(ratio * 25.0f, 25.0f);
	extents *= viewZoom;

	b2Vec2 lower = settings.viewCenter - extents;
	b2Vec2 upper = settings.viewCenter + extents;

	b2Vec2 p;
	p.x = (1.0f - u) * lower.x + u * upper.x;
	p.y = (1.0f - v) * lower.y + v * upper.y;
	return p;
}

// This is used to control the frame rate
static void Timer(int)
{
	glutSetWindow(mainWindow);
	glutPostRedisplay();
	glutTimerFunc(framePeriod, Timer, 0);
}

static void SimulationLoop()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	test->SetTextLine(30);
	b2Vec2 oldCenter = settings.viewCenter;
	settings.hz = settingsHz;
	test->Step(&settings);
	if (oldCenter.x != settings.viewCenter.x || oldCenter.y != settings.viewCenter.y)
	{
		Resize(width, height);
	}

	test->DrawTitle(5, 15, entry->name);

	glutSwapBuffers();

	if (testSelection != testIndex)
	{
		testIndex = testSelection;
		delete test;
		entry = g_testEntries + testIndex;
		test = entry->createFcn();
		viewZoom = 1.0f;
		settings.viewCenter.Set(0.0f, 20.0f);
		Resize(width, height);
	}
}

static void Restart(int)
{

	delete test;
	entry = g_testEntries + testIndex;
	test = entry->createFcn();
	Resize(width, height);
	glutWarpPointer( 650, 550);

}

static void SingleStep(int)
{
	settings.pause = 1;
	settings.singleStep = 1;
}

static void Exit(int code)
{
	// TODO: freeglut is not building on OSX
#ifdef FREEGLUT
	glutLeaveMainLoop();
    if(test!=NULL)
        delete test;
#endif
	exit(code);
}

static void Keyboard(unsigned char key, int x, int y)
{
	B2_NOT_USED(x);
	B2_NOT_USED(y);

	switch (key)
	{
	case 27:
        Exit(0);
		break;

	//toggle control
	case 'c':
		//settings.control_on = !settings.control_on;
		control_checkbox->set_int_val(!settings.control_on);
		break;
	
	case 's':
		SingleStep(0);
		break;

		// Press 'r' to reset.
	case 'r':
		Restart(0);
		break;

	case 'p':
		settings.pause = !settings.pause;
		break;
		
	default:
		if (test)
		{
			test->Keyboard(key);
		}
	}
}

static void KeyboardSpecial(int key, int x, int y)
{
	B2_NOT_USED(x);
	B2_NOT_USED(y);

//	switch (key)
//	{
//	case GLUT_ACTIVE_SHIFT:
//	case GLUT_KEY_LEFT:
//	case GLUT_KEY_RIGHT:
//	case GLUT_KEY_DOWN:
//	case GLUT_KEY_UP:
//	case GLUT_KEY_HOME:
//	}

}

static void KeyboardUp(unsigned char key, int x, int y)
{
	B2_NOT_USED(x);
	B2_NOT_USED(y);

	if (test)
	{
		test->KeyboardUp(key);
	}
}

static void Mouse(int32 button, int32 state, int32 x, int32 y)
{
}

static void MouseMotion(int32 x, int32 y)
{
	b2Vec2 p( x, y);
//	p = ConvertScreenToWorld( 650, 550);
	test->MouseMove(p);
	
	if (rMouseDown)
	{
		b2Vec2 diff = p - lastp;
		settings.viewCenter.x -= diff.x;
		settings.viewCenter.y -= diff.y;
		Resize(width, height);
		lastp = ConvertScreenToWorld(x, y);
	}
}

static void MouseWheel(int wheel, int direction, int x, int y)
{
	B2_NOT_USED(wheel);
	B2_NOT_USED(x);
	B2_NOT_USED(y);
	if (direction > 0)
	{
		viewZoom /= 1.1f;
	}
	else
	{
		viewZoom *= 1.1f;
	}
	Resize(width, height);
}

static void Pause(int)
{
	settings.pause = !settings.pause;
}

int main(int argc, char** argv)
{
	testCount = 0;
	while (g_testEntries[testCount].createFcn != NULL)
	{
		++testCount;
	}

	testIndex = b2Clamp(testIndex, 0, testCount-1);
	testSelection = testIndex;

	entry = g_testEntries + testIndex;
	test = entry->createFcn();

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize(width, height);

	mainWindow = glutCreateWindow("2D Car Testbed");
	//enable fullscreen
	//glutFullScreen();
	//glutSetOption (GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	glutDisplayFunc(SimulationLoop);
	GLUI_Master.set_glutReshapeFunc(Resize);  
	GLUI_Master.set_glutKeyboardFunc(Keyboard);
	GLUI_Master.set_glutSpecialFunc(KeyboardSpecial);
	GLUI_Master.set_glutMouseFunc(Mouse);
#ifdef FREEGLUT
	glutMouseWheelFunc(MouseWheel);
#endif
	//called only when a mouse button is pressed 
//	glutMotionFunc(MouseMotion);
	//called when no mouse button is pressed
	glutPassiveMotionFunc(MouseMotion);

	glutKeyboardUpFunc(KeyboardUp);

	glui = GLUI_Master.create_glui_subwindow( mainWindow, 
		GLUI_SUBWINDOW_RIGHT );

	glui->add_statictext("2D Car Testbed");
	glui->add_statictext(TESTBED_VERSION);
	char title[32];
	sprintf(title, "Box2D Version %d.%d.%d", b2_version.major, b2_version.minor, b2_version.revision);
	glui->add_statictext(title);

	glui->add_separator();
	glui->add_statictext("Vel Iters   8");
	glui->add_statictext("Pos Iters   3");
	glui->add_statictext("Sim freq   50");

	glui->add_separator();

	GLUI_Panel* drawPanel =	glui->add_panel("Debug settings");

	control_checkbox = glui->add_checkbox_to_panel(drawPanel, "Controls ON (c)", &settings.control_on);
	glui->add_checkbox_to_panel(drawPanel, "Debug controls", &settings.debug_controls);
	glui->add_checkbox_to_panel(drawPanel, "Front wheels", &settings.debug_front_tires);
	glui->add_checkbox_to_panel(drawPanel, "Rear wheels", &settings.debug_rear_tires);
	glui->add_checkbox_to_panel(drawPanel, "Drive torque", &settings.debug_drive);
	glui->add_checkbox_to_panel(drawPanel, "Brake torque", &settings.debug_brake);
	glui->add_checkbox_to_panel(drawPanel, "Statistics", &settings.drawStats);
	glui->add_checkbox_to_panel(drawPanel, "Profile", &settings.drawProfile);

	glui->add_button("Pause (p)", 0, Pause);
	glui->add_button("Single Step (s)", 0, SingleStep);
	GLUI_Button* restart_btn = glui->add_button("Restart (r)", 0, Restart);

	glui->add_button("Quit (esc)", 0,(GLUI_Update_CB)Exit);
    
    glui->add_separator();
    glui->add_statictext("   Press button 'c' to" ); 
    glui->add_statictext("   unlock the mouse." ); 
    
	glui->add_separator();
    glui->add_statictext("   Press button 'a'" ); 
    glui->add_statictext("   for reverse gear" );
    glui->add_statictext("   (and no throttle nor" );
    glui->add_statictext("   brake at the same time)" );
    
	glui->set_main_gfx_window( mainWindow );

	// Use a timer to control the frame rate.
	glutTimerFunc(framePeriod, Timer, 0);

	//wrap mouse to position
	glutWarpPointer( 650, 550);

	glutMainLoop();

	return 0;
}
