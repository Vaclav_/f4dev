#ifndef TELEMETRY_H_
#define TELEMETRY_H_

#include <stdio.h>
#include <string>

static const bool TELEMETRY_ON = true;

class CTelemetry {
	public:
	CTelemetry() :
		output_on(TELEMETRY_ON),
		file_path("telemetry.dat"),
		fp(NULL),
        time_step(1.0f/50.0f),
        step_cnt(0)
	{
		if( !output_on )
			return;
		fp = fopen( file_path, "w" );
		if( fp != NULL )
			fprintf( fp, "# ");
		else
			printf( "Error: couldn't open telemetry file for writing: %s", file_path );
	}

	~CTelemetry() {
		if( fp != NULL )
			fclose(fp);
		fp = NULL;
	}
    
    void writeHeader() {
		if( !output_on )
			return;
		
        addColumn( "Time [s]");
		addColumn( "Engine t");
		addColumn( "Engine vel");
		addColumn( "Car vel");
		newRow();
    }

	void addColumn( const char* col_name ) {
    	if( output_on && fp != NULL )
			fprintf( fp, "%s\t", col_name );
	}

	void newRow() {
    	if( output_on && fp != NULL )
			fprintf( fp, "\n%f\t", step_cnt*time_step);
        step_cnt++;
	}

	CTelemetry& operator<<(const float f) {
    	if( output_on && fp != NULL )
    	    fprintf( fp, "%f\t", f);
    	return *this;
	}

	private:
	bool output_on;
	const char* file_path;
	FILE* fp;
    float time_step;
    unsigned int step_cnt;
};

#endif
