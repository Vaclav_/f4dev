#ifndef CAR_PHYSICS_H_
#define CAR_PHYSICS_H_

#include <Box2D/Box2D.h>
#include <vector>
#include "PhysicalObject.h"

#define DEGTORAD b2_pi/180.0f

enum eTire { TIRE_FL=0, TIRE_FR, TIRE_RL, TIRE_RR };
enum eDriveType { DRIVE_REAR=0, DRIVE_FRONT, DRIVE_ALL };
enum eTransmissionState { TR_READY, TR_GEAR_UP, TR_GEAR_DOWN };

extern std::vector<float> TR_ANG_VEL_MIN;
extern std::vector<float> TR_ANG_VEL_MAX;
extern std::vector<float> TR_ANG_VEL_DIFF;
extern std::vector<float> TR_MAX_TORQUE;

class CTirePhysics : public CPhysicalObject {
	public:
	//inherited functions
    int getType();
    void setBody( b2Body* s_body );
    b2Body* getBody();
    void setGround( int g_type );
    int getGround();

	//constructor: create body with fixture
	CTirePhysics( b2World* world, int tire_number, float t_radius, float mass, float width, float length, b2Vec2 pos = b2Vec2( 0.0f, 0.0f), float ang = 0.0f );

	b2Body* body;
	//type of the ground currently under the tire
    int ground_type;
	//angular velocity, how fast the wheel is spinning
	//around horizontal axis
	float ang_vel;
	//torque applied to the wheel, integrated every timestep
	float torque;
	//inertia for spinning around horizontal axis
	float inertia;

	//parameters
	//which tire
	int tire_n;
	float radius;

	//for skidding effect
	bool is_skidding;
	bool is_skidding_fwd;
	bool is_skidding_lat;
	void startSkidding();
	void stopSkidding();

	b2Vec2 getLateralVelocity() {
		b2Vec2 right_normal = body->GetWorldVector( b2Vec2( 1.0f, 0.0f) );
		return b2Dot( right_normal, body->GetLinearVelocity() ) * right_normal;
	}

	b2Vec2 getForwardVelocity() {
		b2Vec2 forward_normal = body->GetWorldVector( b2Vec2( 0.0f, 1.0f) );
		return b2Dot( forward_normal, body->GetLinearVelocity() ) * forward_normal;
	}

	void addTorque( float add_t );

	//kill lateral velocity of the tire
	//add fwd/backward friction
	void updateFriction();
	void step();	//integrate torque 

	~CTirePhysics() {
		if(body!=NULL)
			body->GetWorld()->DestroyBody(body);
        body = NULL;
	}
}; //CTirePhysics


class CDrive {
	public:
	CDrive( int drive_t, std::vector<CTirePhysics*> set_tires, float t_ang_vel, float torque_sat, float rev_torque, float m_brake_torque );
	int drive_type;
	std::vector<CTirePhysics*> tires;
	float target_ang_vel;
	float torque_saturation;
	float reverse_torque;
	float max_brake_torque;

	void applyDriveTorque( float throttle, float brake, bool rev );
	void applyBrakes( float braking );

	//for transmission
	void gearUp() {
        if( cur_gear < gear_num-1 && transmission_state == TR_READY ) {
            transmission_state = TR_GEAR_UP;
            shift_time_cnt = 15;
        }
    }
	void gearDown() {
        if( cur_gear > 0 && transmission_state == TR_READY ) {
            transmission_state = TR_GEAR_DOWN;
            shift_time_cnt = 10;
        }
    }
	inline float getTransmissionTorque( float throttle, float ang_vel);
    void autoGear( float throttle, float ang_vel_avg ) {    //automatic transmission, shift up or down
        //gear up
        if( ang_vel_avg > TR_ANG_VEL_MIN.at(cur_gear)+TR_ANG_VEL_DIFF.at(cur_gear)*0.75f && throttle > 25.0f)
            gearUp();
        else if( ang_vel_avg < TR_ANG_VEL_MIN.at(cur_gear)*0.75f )
            gearDown();
    }
    //do the actual shift if shift delay time is up
    void checkTrShift();
    int cur_gear;
    int gear_num;
    int transmission_state;
    int shift_time_cnt;     //time until shift is done (0 is for done), time is frame count
	
	//Add a simple low pass filter to the engine torque
	//All weigthts are 1.0, so no need to use them
	float applyFilter( float tr_torque );
	unsigned int filter_tap_num;
	unsigned int cur_filter_pos;	//current element in the filter_values
	std::vector<float> filter_values;
};

class CCarDef {
	public:
	CCarDef();
	bool isValid();

	int drive_t;
	float car_mass;
	float car_width;
	float car_length;

	float tire_rad;
	float tire_mass;
	float tire_width;
	float tire_length;

	float front_tire_x;
	float front_tire_y;
	float rear_tire_x;
	float rear_tire_y;

	float drive_target_ang_vel;
	float drive_torque_sat;
	float rev_torque;
	float max_brake_torque;
};

class CCarPhysics : public CPhysicalObject {
    public:
    int getType();
    void setBody( b2Body* s_body );
    b2Body* getBody();
    void setGround( int g_type );
    int getGround();

	public:
	b2World* m_world;
	b2Body* body;
	std::vector<CTirePhysics*> tires;
	std::vector<b2RevoluteJoint*> joints;
	int drive_type;

	//controls
	float throttle;
	float braking;
	float steering;
    bool reverse;

	CDrive* drive;

	public:
	CCarPhysics( b2World* world, CCarDef& car_def, b2Vec2 pos = b2Vec2( 0.0f, 0.0f), float ang = 0.0f );
    ~CCarPhysics();

	//set new steering every timestep
	void setSteering( float thr, float br, float steer, bool rev);
	//update drive, break, steering, friction
	//throttle/braking, steering: between 0 and 100
	void step();
};

class CBox : public CPhysicalObject {

    int getType();
    void setBody( b2Body* s_body );
    b2Body* getBody();
    void setGround( int g_type );
    int getGround();

    public:
    b2Body* body;
    int ground_type;
    CBox( b2World* world, float mass, float size_x, float size_y );
    void step();
};

#endif
