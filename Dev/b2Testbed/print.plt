set rmargin 7
set bmargin 3
set title "Gyorsulás teszt"
set label "t; sec" at 7.12,-250
plot [] [0:3200] "telemetry.dat" using 1:2 with line title "M(t)", "" using 1:3 with line title "v(t)"
pause -1 "Press enter."