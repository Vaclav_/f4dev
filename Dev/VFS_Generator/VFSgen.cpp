
#ifndef __linux__
#include "inc/dirent.h"
#else
#include <dirent.h>
#endif

#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>

const bool DEBUG_ON = 1;

const char* OUT_FILE_NAME = "out.vfs";

const char* VFS_STR = "VFS";
const unsigned char VFS_VER = 1;
const std::string ROOT_DIR = "Res/";
int error( const char* msg );
unsigned int getFileSize( const char* f_path );
int copyFile( FILE* fp, const char* f_path);
int writeFiles( FILE* out_fp, std::vector<std::string>& cur_files);
void printUInt32( FILE* fp, unsigned int num);

int main() {

	std::string cur_path = ROOT_DIR;
	
	//Files in current working directory
	std::vector<std::string> cur_files;
	//Directories in root directory
	std::vector<std::string> dir_base;
	
	FILE* out_fp = fopen( OUT_FILE_NAME, "wb" );
	if( out_fp == NULL ) {
		error("Couldn't create output file.");
		return -1;
	}
	//Write file header
	fprintf( out_fp, "%s%c", VFS_STR, VFS_VER);

    DIR *dir;
    struct dirent *ent;
	std::string t_str;
	std::size_t found;
	
	//Get directories
    if ((dir = opendir (cur_path.c_str())) != NULL) {
      // print all the files and directories within directory
		while ((ent = readdir(dir)) != NULL) {
			t_str = ent->d_name;
			if( t_str == "." || t_str == "..")
				continue;
			found = t_str.find('.');
			if (found!=std::string::npos) {
				//found '.', so it's a file
				printf ("File: %s\n", t_str.c_str());
				cur_files.push_back(t_str);
			} else {
				//no '.', so it must be a directory
				printf ("Dir: %s\n", t_str.c_str());
				dir_base.push_back(t_str);
			}
		}
		closedir(dir);
    } else {
	  error("Could not open directory.");
	  fclose(out_fp);
      return -1;
    }
	
	writeFiles( out_fp, cur_files);

	//Loop all Level 1 directories
	for( unsigned int cur_dir_n=0; cur_dir_n<dir_base.size(); cur_dir_n++) {

		cur_files.erase( cur_files.begin(), cur_files.end() );
		//enter dir
		cur_path = ROOT_DIR + dir_base.at(cur_dir_n) + "/";

		if(DEBUG_ON)
			std::cout << "Entering directory: " << cur_path << "\n";
		
		//Get directories
	    if ((dir = opendir (cur_path.c_str())) != NULL) {

			//Get files
			while ((ent = readdir(dir)) != NULL) {
				t_str = ent->d_name;
				if( t_str == "." || t_str == "..")
					continue;
				t_str = dir_base.at(cur_dir_n) + '/' + t_str;
				found = t_str.find('.');
				if (found!=std::string::npos) {
					//found '.', so it's a file
					printf ("File: %s\n", t_str.c_str());
					cur_files.push_back(t_str);
				} else {
					//no '.', so it must be a directory
					std::string err_dir = "Level 2 directories aren't supported yet: " + t_str;
					error(err_dir.c_str());
					return -1;
				}
			}
			closedir(dir);

			if( writeFiles( out_fp, cur_files) ) {
				error("Error in writeFiles.");
				return -1;
			}
		}
	}

	fclose( out_fp );
	
	std::cout << "==================================\nCreated vfs file \
Succesfully.\n==================================";
	
    return 0;
}

int error( const char* msg ) {
	std::cout << "Error: " << msg << "\n";
}

unsigned int getFileSize( const char* f_path ) {
	if( f_path == NULL ) {
		error( "getFileSize: f_path is NULL.");
		return 0;
	}

	unsigned int size;
	std::string full_path = ROOT_DIR + f_path;
	
	FILE* fp = fopen( full_path.c_str(), "rb" );
	if( fp == NULL ) {
		std::string temp = "getFileSize: can't open file: ";
		temp += f_path;
		error( temp.c_str() );
		return 0;
	}
	
	fseek( fp, 0, SEEK_END);
	size = ftell(fp);
	fclose(fp);
	
	return size;
}

int copyFile( FILE* fp, const char* f_path) {
	if( fp==NULL || f_path==NULL ) {
		error("copyFile: param NULL.");
		return -1;
	}
	
	std::string full_path = ROOT_DIR + f_path;
	
	FILE* read_fp = fopen( full_path.c_str(), "rb");
	if( read_fp == NULL ) {
		std::string temp = "copyFile: can't open file: ";
		temp += f_path;
		error( temp.c_str() );
		return -2;
	}
	
	unsigned char byte;
	while( fscanf( read_fp, "%c", &byte) != EOF ) {
		fprintf( fp, "%c", byte);
	}
	
	fclose(read_fp);
	
	return 0;
}

int writeFiles( FILE* out_fp, std::vector<std::string>& cur_files) {
	//Write all files in directory
	unsigned int f_size;
	unsigned int path_size;
	unsigned int next_file_offset;
	for( unsigned int k=0; k<cur_files.size(); k++) {
		f_size = getFileSize( cur_files.at(k).c_str() );

		if(DEBUG_ON)
			std::cout << "Writing: " << cur_files.at(k) << " |Size: " << f_size;

		path_size = cur_files.at(k).length() + 1;	//+1 for 0

		if(DEBUG_ON)
			std::cout << " |Path length: " << path_size << "\n";

		if( path_size > 255 ) {
			error("File path size can't be greater than 255.");
			fclose(out_fp);
			return -1;
		}

		next_file_offset = path_size + f_size + 4 + 1;
		printUInt32( out_fp, next_file_offset);		//4B
		fprintf( out_fp, "%c", (char)path_size);	//1B
		fprintf( out_fp, "%s", cur_files.at(k).c_str() );
		fprintf( out_fp, "%c", (char)0);			//NULL
		if( copyFile( out_fp, cur_files.at(k).c_str()) ) {
			error("Error calling copyFile.");
			return -1;
		}
	}
}

void printUInt32( FILE* fp, unsigned int num) {
	if( fp == NULL ) {
		error( "printUInt32: fp is NULL.");
		return;
	}
	
	unsigned char ch;
	unsigned int temp;
	for( int i=0; i<4; i++) {
		temp = num>>(8*i);
		ch = (unsigned char)(temp);
		fprintf( fp, "%c", ch);
	}
}