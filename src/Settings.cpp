
#include <stdio.h>
#include <string>

#include "Settings.h"
#include "Game.h"

extern int scanWord( FILE* fp, int max_len, char* buf);

CSettings::CSettings() :
    fullscreen(false),
    reduced_gfx(false),
    show_fps(false) {
}

bool CSettings::loadFromFile( const char* path ) {

	FILE* fp = NULL;
	fp = fopen( path, "r");
	if(fp==NULL) {
		g_game.error << "Couldn't open settings file!";
		return false;
	}

	g_game.debug << "Loading settings...";

	float d;
	char in_buffer[300];

	std::string temp_str;

	while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {

		if( in_buffer[0] == '#' ) {	// # marks comment
			//read until \n
			while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {
                if( in_buffer[0] == '\n' )
					break;
			}
			continue;
		}

		//go back and get param name+float
		fseek( fp, -1, SEEK_CUR );
		scanWord( fp, 300, &in_buffer[0]);
		fscanf( fp, "%f", &d);

		temp_str = in_buffer;

        if(temp_str == "FULLSCREEN") {
            fullscreen = (bool)((int)d);
        } else if(temp_str == "REDUCED_GFX") {
			reduced_gfx = (bool)((int)d);
		} else if(temp_str == "SHOW_FPS") {
			show_fps = (bool)((int)d);
		} else {
			g_game.error << "Loading: unkown setting:\n>>>>";
		}

		g_game.debug << temp_str << ": " << d << "\n";

		//read /n
		while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {
			if( in_buffer[0] == '\n' )
				break;
		}
	}

	fclose( fp );

	g_game.debug << "Loaded settings succesfully.";

    return true;
}

bool CSettings::saveToFile( const char* path ) {

    g_game.debug << "Saving settings...";
	FILE* fp;
	fp = fopen( "settings.cfg", "w");
	if(fp==NULL) {
		g_game.error << "Couldn't open settings file for writing!";
		return false;
	}

    fprintf( fp, "#Settings file\n" );
    fprintf( fp, "FULLSCREEN %i\n", (int)fullscreen);
    fprintf( fp, "SHOW_FPS %i\n", (int)show_fps);
    fprintf( fp, "REDUCED_GFX %i", (int)reduced_gfx);
    //don't write \n at the end of the file

    fclose(fp);

    g_game.debug << "Saved settings succesfully.";

    return 0;
}
