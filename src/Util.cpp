
#include "Game.h"
#include "Util.h"
#include <stdio.h>
#include <iostream>


//return number of characters read or EOF
int scanWord( FILE* fp, int max_len, char* buf) {
    if( fp == NULL || buf == NULL ) return 0;
    if( max_len==0 ) { *buf = 0; return 0; }
    int i;
    bool end_of_f = false;
    for( i=0; i<max_len-1; i++) {
        //get one char
        if( fscanf( fp, "%c", buf+i ) == EOF ) {
            end_of_f = true;
            break;
        }
        //read until
        if( *(buf+i)==' ' || *(buf+i)=='\t' || *(buf+i)=='\n' ) {
            break; //we will overwrite this character with 0
        }
    }
    //0 terminate the string
    *(buf+i) = 0;
    if(end_of_f)
        return EOF;
    else
        return i;
}

COutput COutput::operator<<(const std::string& msg) {
    if( turned_on ) {
        if( is_newline && type_msg != "" ) {
            std::cout << type_msg;
            is_newline = false;
        }
        std::cout << msg;
        char lastCh = *msg.rbegin();
        if( insert_newline && (lastCh == '.' || lastCh == '!' || lastCh == '?')) {
            std::cout << "\n";
            is_newline = true;
        } else if( lastCh == '\n')
            is_newline = true;
    }
    return *this;
};

COutput COutput::operator<<(const unsigned ui) {
    if( turned_on )
        std::cout << ui;
    return *this;
}

COutput COutput::operator<<(const int i) {
    if( turned_on )
        std::cout << i;
    return *this;
}

COutput COutput::operator<<(const float f) {
    if( turned_on )
        std::cout << f;
    return *this;
}

COutput COutput::operator<<(const double d) {
    if( turned_on )
        std::cout << d;
    return *this;
}

COutput COutput::operator<<(const b2Vec2& vec2) {
    if( turned_on )
        std::cout << "x " << vec2.x << " |y " << vec2.y;
    return *this;
}

//==================================================

int CVFSStream::getString( FILE* fp, std::string& str) {
    if( fp == NULL ) return -1;

    str = "";
    char ch=1;
    int ret=1;
    while( ch != 0 && ret > 0) {
        ret = fscanf( fp, "%c", &ch);
        if( ch!=0 )
            str += ch;
    }

    return 0;
}

unsigned int CVFSStream::getUInt32( FILE* fp) {
    if( fp == NULL) {
        g_game.error << "getUInt32: fp is NULL.";
        return 0;
    }

    unsigned char ch;
    unsigned int num = 0;
    unsigned int temp;

    for( int i=0; i<4; i++) {
        fscanf( fp, "%c", &ch);
        temp = 0;
        temp |= ch;
        temp = temp<<(i*8);
        num |= temp;
    }

    return num;
}

int CVFSStream::open( const std::string& filename) {
    //fp must be an already open file
    //This function finds the file offset in the VFS
    if(vfs_fp == NULL) {
        g_game.error << "CVFSStream::open: vfs_fp is NULL.";
        return -1;
    }

    sf::Int64 vfs_end_pos;
    if( fseek( vfs_fp, 0, SEEK_END) ) {
        g_game.error << "CVFSStream::open: error using fseek on file pointer.";
        return -4;
    }
    vfs_end_pos = ftell( vfs_fp );
    fseek( vfs_fp, 0, SEEK_SET);
    //g_game.debug << "vfs_end_pos: " << (int)vfs_end_pos << "\n";

    char ch;
    //scrap the header
    for( int i=0; i<4; i++) {
        if( fscanf( vfs_fp, "%c", &ch) <= 0 ) {
            g_game.error << "CVFSStream::open: error reading VFS file 1.";
            return -2;
        }
    }

    sf::Int64 full_offset = 4;
    unsigned int next_file_offset;
    unsigned char path_len;
    bool found = false;

    while( !found && full_offset < vfs_end_pos ) {
        //g_game.debug << "Loop.";

        next_file_offset = getUInt32( vfs_fp ); //4B

        if( fscanf( vfs_fp, "%c", &path_len) <= 0 ) {   //1B
            g_game.error << "CVFSStream::open: error reading VFS file 3.";
            return -2;
        }

        std::string read_f_name;
        if( getString( vfs_fp, read_f_name) ) {
            g_game.error << "CVFSStream::open: error reading VFS file 4.";
            return -2;
        }

        //g_game.debug << "Read fname: " << read_f_name << "\n";
        if( read_f_name == filename) {
            found = true;
        }
        else {
            full_offset += next_file_offset;
            fseek( vfs_fp, full_offset, SEEK_SET );
            //g_game.debug << "Seek: " << (int)full_offset << "\n";
        }
    }

    if(found) {
        file_offset = full_offset + 4 + 1 + path_len;
        file_size = next_file_offset - path_len - 4 - 1;
        g_game.debug << "Found file in FVS. Name: " << filename << " Offset: " << (int)file_offset << "\n";
    }
    else {
        file_offset = 0;
        g_game.error << "Can't find filename in VFS.";
        return -3;
    }

    return 0;
}

//read must extract size bytes of data from the stream,
//and copy them to the supplied data address; it returns
//the number of bytes read, or -1 on error.
sf::Int64 CVFSStream::read(void* data, sf::Int64 size) {

    if( vfs_fp == NULL ) {
        g_game.error << "VFS file pointer is NULL.";
        return -1;
    }
    if( data == NULL ) {
        g_game.error << "CVFSStream::read: *data is NULL.";
        return -1;
    }

    sf::Int64 num_read;

    if( cur_pos >= file_size) {
        g_game.error << "CVFSStream::read: range out of virtual file.\nVirtual file size: "
            << (int)file_size << "\nRequested position: " << (int)cur_pos << "\n";
        return -1;
    }
    fseek( vfs_fp, file_offset+cur_pos, SEEK_SET);

    unsigned char ch;
    for( num_read=0; num_read<size; num_read++) {
        fscanf( vfs_fp, "%c", &ch);
        *((unsigned char*)data+num_read) = ch;
        cur_pos++;
        if( cur_pos >= file_size) {
            //end of file, no error
            return num_read;
        }
    }

    return num_read;
}

//seek must change the current reading position in the stream;
//its position argument is the absolute byte offset to jump to
//(so it is relative to the beginning of the data, not to the current
// position); it returns the new position, or -1 on error.
sf::Int64 CVFSStream::seek(sf::Int64 position) {

    if( vfs_fp == NULL ) {
        g_game.error << "VFS file pointer is NULL.";
        return -1;
    }
    if( position > file_size) {
        g_game.error << "CVFSStream::seek: requested posiiton is greater than virtual file size.\nReq. pos: "
            << (int)position << "Virtual file size: " << (int) file_size << "\n";
        return -1;
    }

    cur_pos = position;

    if( fseek( vfs_fp, file_offset+cur_pos, SEEK_SET) ) {
        g_game.error << "CVFSStream::seek: fseek error to position: " << (int)(file_offset+cur_pos) << "\n";
        return -1;
    }
    else
        return cur_pos;
}

//tell must return the current reading position (in bytes) in the stream,
// or -1 on error.
sf::Int64 CVFSStream::tell() {
    if( vfs_fp == NULL ) {
        g_game.error << "VFS file pointer is NULL.";
        return -1;
    }
    return cur_pos;
}

//getSize must return the total size (in bytes) of the data which
//is contained in the stream, or -1 on error.
sf::Int64 CVFSStream::getSize() {
    if( vfs_fp == NULL ) {
        g_game.error << "VFS file pointer is NULL.";
        return -1;
    }
    g_game.debug << "File size: " << (file_size) << "\n";
    return file_size;
}

sf::Texture* CTextureManager::addTexture( const sf::Texture& tex, std::string& name ) {
    textures.push_back( new sf::Texture() );
	if( textures.back() == NULL ) {
		g_game.error <<  "Allocating has failed.";
		textures.pop_back();
		return NULL;
	}
	//copy the texture
    *textures.back() = tex;
    texture_paths.push_back( name );
    return textures.back();
}

//if the texture at parameter path hasn't been loaded yet
//then loads it and returns a pointer
//if it was loaded already, finds it and returns a pointer
sf::Texture* CTextureManager::getTexture( std::string path ) {

	//search for texure name in std::vector<std::string> texture_paths
	unsigned int i;
	bool found = false;
	for( i=0; i<texture_paths.size() && !found; i++) {
        if( texture_paths.at(i) == path )
            found = true;
	}

	i--;
    if(found) {
        return textures.at(i);
    }
    //else load it

	textures.push_back( new sf::Texture() );

	if( textures.back() == NULL ) {
		g_game.error <<  "Allocating on the heap has failed when trying to open: " + path + "." ;
		textures.pop_back();
		return NULL;
	}

    CVFSStream texture_stream(vfs);
    texture_stream.open(path.c_str());

	//.back is the last element of the vector
	if ( !textures.back()->loadFromStream( texture_stream ) )
	{
		g_game.error << "Can't load texture: " + path + "." ;
		textures.pop_back();
		return NULL;
	} else {
		texture_paths.push_back( path );
		return textures.back();
	}

//	} else return &textures.at(0);
}

//the same as getTexture(..) but with fonts
sf::Font* CTextureManager::getFont( std::string path ) {

	//search for texure name in font_paths
	unsigned int i;
	bool found = false;
	for( i=0; i<font_paths.size() && !found; i++) {
        if( font_paths.at(i) == path )
            found = true;
	}

	i--;
    if(found) {
        return fonts.at(i);
    }
    //else load it

    fonts.push_back( new sf::Font() );

	if( fonts.back() == NULL ) {
		g_game.error <<  "Allocating on the heap has failed when trying to open: " + path + "." ;
		fonts.pop_back();
		return NULL;
	}

    font_streams.push_back(CVFSStream(vfs));
    font_streams.back().open(path.c_str());

	//.back is the last element
	if ( !fonts.back()->loadFromStream( font_streams.back() ) )
	{
		g_game.error << "Can't load font: " + path + ".";
		fonts.pop_back();
		return NULL;
	} else {
		font_paths.push_back( path );
		return fonts.back();
	}
}

void CTextureManager::freeTextures() {
    while( textures.size() != 0 ) {
        if( textures.back() != NULL )
            delete textures.back();
        textures.pop_back();
    }
    while( texture_paths.size() != 0 )
        texture_paths.pop_back();
}

void CTextureManager::freeFonts() {
    while( fonts.size() != 0 ) {
        if( fonts.back() != NULL )
            delete fonts.back();
        fonts.pop_back();
    }
    while( font_paths.size() != 0 ) {
        font_paths.pop_back();
    }

    font_streams.erase( font_streams.begin(), font_streams.end());
}

void CTextureManager::freeAll() {
    freeTextures();
    freeFonts();
}

//delay until cur_clock reaches time, precise
void delayUntil( sf::Clock& cur_clock, const sf::Time& time ) {
    //try to sleep as little as possible
    while( cur_clock.getElapsedTime() < time-sf::milliseconds(5) ) {
        sf::sleep( sf::milliseconds(1) );
    }
    //burn the remaining time
    while( cur_clock.getElapsedTime() < time ) {
    }
}
