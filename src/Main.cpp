
#include <iostream>
#include <SFML/Graphics.hpp>
#include "GameState/GameState.h"
#include "GameState/MenuState.h"
#include "Game.h"
#include "GameState/PreSPState.h"
#include "GameState/SPState.h"
#include "GameState/PreMPState.h"
#include "GameState/SettingsState.h"
#include "GameState/MPLobbyState.h"
#include "GameState/TutorialState.h"

static const unsigned int FPS_DEBUG_TIME = 2000;

CGame g_game;

int main(int argc, char* argv[]) {

	CGameState* cur_state = NULL;
	sf::Event cur_event;

	//time elapsed since the start of the app
	sf::Clock game_timer;
	//the time when the next frame has to be drawn
	sf::Time next_frame_t;

	sf::Clock debug_timer;
	unsigned int fps_cnt = 0;

	g_game.init();

    while( g_game.isRunning() ) {

        //create new gamestate
        switch( g_game.active_state ) {
            case STATE_MENU:
                g_game.debug << "New state: STATE_MENU.";
                cur_state = new CMenuState;
                cur_state->loadState();
                break;
            case STATE_TUTORIAL:
                g_game.debug << "New state: STATE_TUTORIAL.";
                cur_state = new CTutorialState;
                cur_state->loadState();
                break;
            case STATE_PRE_SP:
                g_game.debug << "New state: STATE_PRE_SP.";
                cur_state = new CPreSPState;
                cur_state->loadState();
                break;
            case STATE_SP:
                g_game.debug << "New state: STATE_SP.";
                cur_state = new CSPState;
                cur_state->loadState();
                break;
            case STATE_PRE_MP:
                g_game.debug << "New state: STATE_PRE_MP.";
                g_game.active_state = STATE_PRE_MP;
                cur_state = new CPreMPState;
                cur_state->loadState();
                break;
            case STATE_MP_LOBBY:
                g_game.debug << "New state: STATE_MP_LOBBY.";
                g_game.active_state = STATE_MP_LOBBY;
                cur_state = new CMPLobbyState;
                cur_state->loadState();
                break;
            case STATE_SETTINGS:
                g_game.debug << "New state: STATE_SETTINGS.";
                g_game.active_state = STATE_SETTINGS;
                cur_state = new CSettingsState;
                cur_state->loadState();
                break;
            case STATE_CREDITS:
                g_game.error << "CREDITS is not yet implemented.";
                g_game.active_state = STATE_MENU;
                cur_state = new CMenuState;
                cur_state->loadState();
                break;
            default:
                g_game.error << "Unknown gamestate, can't be loaded.";
                g_game.setRunning(false);
                std::cout << g_game.active_state << "\n";
                break;
        }

        fps_cnt = 0;
        debug_timer.restart();
        next_frame_t = sf::milliseconds(0);
        game_timer.restart();

        //while we stay in the same state, and the game is running
        while( g_game.isRunning() && g_game.active_state == cur_state->getNextState() ) {
            //the time in SDL_Ticks when the next frame comes
            //until then we have time for this frame
            next_frame_t += g_game.getFrameTime();

            while( g_game.window->pollEvent( cur_event ) ) {
                cur_state->handleEvent( cur_event );
            }

            cur_state->step();
            //Render only if we have enough time for it
            if( game_timer.getElapsedTime() < next_frame_t ) {
                cur_state->render();
                fps_cnt++;
            } else g_game.debug << "Skipping frame...";

            //regulating FPS
            if( cur_state->do_sleep ) { //sleep
                delayUntil( game_timer, next_frame_t);
            } else {                    //idle
                while( game_timer.getElapsedTime() < next_frame_t ) {
                    //pass the remaining time until next frame
                    cur_state->idle( next_frame_t-game_timer.getElapsedTime() );
                }
            }

            if( debug_timer.getElapsedTime() > sf::milliseconds(FPS_DEBUG_TIME) ) {
                g_game.fps_measured = fps_cnt*1000.0/FPS_DEBUG_TIME;
                fps_cnt = 0;
                debug_timer.restart();
            }

        }		//end while in current state

        if( cur_state != NULL ) {
            //set the name of the next state
            g_game.debug << "Leaving current gamestate...";
            //delete current gamestate, to create the next in the next loop
            cur_state->leaveState();
            g_game.active_state = cur_state->getNextState();
            //note: virtual destructor means
            //that only the destructor of the derived class will be run
            delete cur_state;
            cur_state = NULL;
        }


    }   //end while game is running

	g_game.end();


	return 0;
}


