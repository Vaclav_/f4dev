#ifndef CONTROL_H_INCLUDED
#define CONTROL_H_INCLUDED

#include <SFML/Graphics.hpp>

class CControlPanel {
        public:
        CControlPanel();
        void setTexture(sf::Texture* tex);
		//throttle from 0 to 100
		float getThrottle();
		//brake from 0 to 100
		float getBrake();
        //from -100 to +100
		float getSteering();

        void render();

        sf::Uint32 half_size;
        int half_deadband;
        sf::Vector2i position;

        sf::Color color_steering_deadband;
        sf::Color color_steering_active;
        sf::Color color_fwd_deadband;
        sf::Color color_fwd_throttle;
        sf::Color color_fwd_brake;

        sf::RectangleShape panel;
        sf::RectangleShape steering_rect;
        sf::RectangleShape forward_rect;
};

#endif // CONTROL_H_INCLUDED
