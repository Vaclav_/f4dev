#ifndef _GAME_H_
#define _GAME_H_ 0

#include <SFML/Graphics.hpp>
#include <string>
#include "Util.h"
#include "Settings.h"
#include "SinglePlayer/SPConfig.h"

//class containing information related to the whole game
//there's only one instance of it, CGame g_game and it's global
class CGame {

	public:
	CGame();
	~CGame();
	//init the whole game
	int init();
	void end();

	COutput debug;
	COutput error;

    //if the game is running, or it is about to stop (quit)
	bool isRunning();
	//use setRunning(false) to quit the whole game
	void setRunning( bool game_running );

	//sets the frame_rate and frame_time
	void setFrameRate( int new_frame_rate );
	int getFrameRate();
	sf::Time getFrameTime();

    sf::Time getElapsedTime();

    //current executing gamestate (state that inherits from CGameState)
    //see eGameStates in GameState.h
	int active_state;

    //the window of the game, always render here
	sf::RenderWindow* window;

	//screen properties
	int scrnW;      //width
	int scrnH;      //height
	int scrnBpp;    //bits per pixel

    //if debug messages should be shown in console or not
    bool debug_on;

    sf::Time frame_time;		//time between two frames

    CSettings settings;

    CSPConfig SPConfig;

    unsigned int fps_measured;

    float b2d_to_gfx;
    float gfx_to_b2d;

	private:
	bool running;
	std::string game_name;
	int frame_rate;

	sf::Clock game_time;
};

extern CGame g_game;

#endif
