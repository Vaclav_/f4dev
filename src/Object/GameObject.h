
#ifndef GAMEOBJ_H_
#define GAMEOBJ_H_

#include <SFML/Graphics.hpp>
#include <vector>
#include "../Graphics/Texture.h"
#include "../Physics/PhysicalObject.h"
#include "../Physics/CarPhysics.h"

enum EGameObjects { GAMEOBJ_NONE, GAMEOBJ_CAR, GAMEOBJ_SIMPLE, GAMEOBJ_STARTPOINT, GAMEOBJ_FINISHLINE };


//This is a top level object
//any object in the game with
    //Graphics
    //Physics
    //Custom Properties
class CGameObject {
    public:
    CGameObject() : type(0) {}
    virtual ~CGameObject() {}
    int type;
    virtual void render( sf::RenderWindow* win, sf::FloatRect& view_rect ) = 0;
    virtual void step( float time_sec ) = 0;
};


class CSimpleGameObject : public CGameObject {
    public:
    CSimpleGameObject( CGraphicObject* gfx, CPhysicalObject* phys) :
        graphics(gfx),
        physics(phys) {
        type = GAMEOBJ_SIMPLE;
    }
    ~CSimpleGameObject() {
        if(graphics!=NULL)
            delete graphics;
        if(physics!=NULL)
            delete physics;
        graphics = NULL;
        physics = NULL;
    }
    void render( sf::RenderWindow* win, sf::FloatRect& view_rect );
    void step( float time_sec );

    CGraphicObject* graphics;
    CPhysicalObject* physics;
};

class CFinishLineObject : public CGameObject {
public:
    CFinishLineObject( b2Vec2& p1, b2Vec2& p2, float b2d_to_gfx) {
        type = GAMEOBJ_FINISHLINE;

        point1 = p1;
        point2 = p2;
        line = new CVertexArray( sf::Lines, b2d_to_gfx);
        line->vertices.push_back(sf::Vertex());
        line->vertices.back().position.x = p1.x*b2d_to_gfx;
        line->vertices.back().position.y = p1.y*b2d_to_gfx;
        line->vertices.back().color = sf::Color::Red;
        line->vertices.push_back(sf::Vertex());
        line->vertices.back().position.x = p2.x*b2d_to_gfx;
        line->vertices.back().position.y = p2.y*b2d_to_gfx;
        line->vertices.back().color = sf::Color::Red;
    }

    ~CFinishLineObject() {
        if(line!=NULL)
            delete line;
        line = NULL;
    }

    void render( sf::RenderWindow* win, sf::FloatRect& view_rect );
    void step( float time_sec );

    CVertexArray* line;

    b2Vec2 point1;
    b2Vec2 point2;
};

class CStartPointObject : public CGameObject {
public:
    CStartPointObject( b2Vec2& point, int num, float b2d_to_gfx ) {
        type = GAMEOBJ_STARTPOINT;

        position = point;
        number = num;
        mark = new CVertexArray( sf::Lines, b2d_to_gfx);
        mark->vertices.push_back(sf::Vertex());
        mark->vertices.back().position.x = point.x*b2d_to_gfx-25;
        mark->vertices.back().position.y = point.y*b2d_to_gfx-25;
        mark->vertices.back().color = sf::Color::Red;
        mark->vertices.push_back(sf::Vertex());
        mark->vertices.back().position.x = point.x*b2d_to_gfx+25;
        mark->vertices.back().position.y = point.y*b2d_to_gfx+25;
        mark->vertices.back().color = sf::Color::Red;
        mark->vertices.push_back(sf::Vertex());
        mark->vertices.back().position.x = point.x*b2d_to_gfx+25;
        mark->vertices.back().position.y = point.y*b2d_to_gfx-25;
        mark->vertices.back().color = sf::Color::Red;
        mark->vertices.push_back(sf::Vertex());
        mark->vertices.back().position.x = point.x*b2d_to_gfx-25;
        mark->vertices.back().position.y = point.y*b2d_to_gfx+25;
        mark->vertices.back().color = sf::Color::Red;
    }

    ~CStartPointObject() {
        if( mark != NULL )
            delete mark;
        mark = NULL;
    }

    void render( sf::RenderWindow* win, sf::FloatRect& view_rect );
    void step( float time_sec );

    b2Vec2 position;
    int number;

    CVertexArray* mark;
};

#endif

