#ifndef CAR_H_INCLUDED
#define CAR_H_INCLUDED

#include "GameObject.h"

class CCar : public CGameObject {
    public:
    CCar( CCarPhysics* car, CTexture* tex, float tire_w, float tire_l);
    ~CCar() {
        if(car_texture!=NULL)
            delete car_texture;
        if(car_physics!=NULL)
            delete car_physics;
        for( int i=0; i<4; i++) {
            if(tires_gfx[i]!=NULL)
                delete tires_gfx[i];
        }
    }

    void render( sf::RenderWindow* win, sf::FloatRect& view_rect );
    void step( float time_sec );

    CGraphicShape* tires_gfx[4];
    CCarPhysics* car_physics;
    CTexture* car_texture;
};

#endif // CAR_H_INCLUDED
