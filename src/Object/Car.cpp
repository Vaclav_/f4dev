
#include "Car.h"
#include "../Game.h"

CCar::CCar( CCarPhysics* car, CTexture* tex, float tire_w, float tire_l) :
    car_physics(car),
    car_texture(tex) {
    type = GAMEOBJ_CAR;

    sf::RectangleShape* shape;
    CGraphicShape* gfx;
    for( int i=0; i<4; i++) {
        shape = new sf::RectangleShape( sf::Vector2f(tire_w*g_game.b2d_to_gfx, tire_l*g_game.b2d_to_gfx));
        shape->setFillColor(sf::Color::Black);
        shape->setOrigin( tire_w*g_game.b2d_to_gfx/2.0f, tire_l*g_game.b2d_to_gfx/2.0f);
        gfx = new CGraphicShape( (sf::Shape*)shape, g_game.b2d_to_gfx);
        tires_gfx[i] = gfx;
        if(car!=NULL)
            tires_gfx[i]->attach(car->tires.at(i)->body );
    }
}

void CCar::render( sf::RenderWindow* win, sf::FloatRect& view_rect ) {
    for( int i=0; i<4; i++) {
        if(tires_gfx[i]!=NULL)
            tires_gfx[i]->render( win, view_rect);
    }
    if(car_texture!=NULL)
        car_texture->render( win, view_rect);
}

void CCar::step( float time_sec ) {
    //if( car_physics!=NULL )
    //    car_physics->step(time_sec);
}
