
#include "GameObject.h"
#include "../Game.h"

//================================================

void CSimpleGameObject::render( sf::RenderWindow* win, sf::FloatRect& view_rect ) {
    if(graphics!=NULL)
        graphics->render( win, view_rect);
}
void CSimpleGameObject::step( float time_sec ) {
    if(graphics!=NULL)
        graphics->step(time_sec);
    if(physics!=NULL)
        physics->step(time_sec);
}

//================================================

void CFinishLineObject::render( sf::RenderWindow* win, sf::FloatRect& view_rect ) {
    if( line != NULL )
        line->render( win, view_rect);
}

void CFinishLineObject::step( float time_sec ) {
}

//================================================

void CStartPointObject::render( sf::RenderWindow* win, sf::FloatRect& view_rect ) {
    if( g_game.debug_on && mark != NULL )
        mark->render( win, view_rect);
}

void CStartPointObject::step( float time_sec ) {
}
