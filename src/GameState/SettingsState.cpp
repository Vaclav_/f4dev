#include <SFML/Graphics.hpp>
#include "../Game.h"
#include "SettingsState.h"
#include "../Util.h"
#include <vector>
#include <string>

static int COL_OFFSET = 210;
static int ROW_HEIGHT = 35;
static int ROW_OFFSET = 325;
static int COL_WIDTH = 175;
static int LABEL_Y_OFFSET = 12;

static sf::Color LABEL_COLOR = sf::Color::White;
static int LABEL_CHAR_SIZE = 14;

int CSettingsState::loadState() {

    sf::Font* font;
    font = manager.getFont("gfx/DejaVuSansMono.ttf");
    if( font!=NULL )
        label_settings.setFont(*font);
    label_settings.setCharacterSize(LABEL_CHAR_SIZE);
    label_settings.setString("Settings");
    label_settings.setColor( LABEL_COLOR );
    label_settings.setPosition( ROW_OFFSET, COL_OFFSET+LABEL_Y_OFFSET);

    if( font!=NULL )
        label_fullscreen.setFont(*font);
    label_fullscreen.setCharacterSize(LABEL_CHAR_SIZE);
    label_fullscreen.setString("Fullscreen");
    label_fullscreen.setColor( LABEL_COLOR );
    label_fullscreen.setPosition( ROW_OFFSET, COL_OFFSET+ROW_HEIGHT+LABEL_Y_OFFSET);

    if( font!=NULL )
        label_fps.setFont(*font);
    label_fps.setCharacterSize(LABEL_CHAR_SIZE);
    label_fps.setString("Show FPS counter");
    label_fps.setColor( LABEL_COLOR );
    label_fps.setPosition( ROW_OFFSET, COL_OFFSET+ROW_HEIGHT*2+LABEL_Y_OFFSET);

    if( font!=NULL )
        label_reduced.setFont(*font);
    label_reduced.setCharacterSize(LABEL_CHAR_SIZE);
    label_reduced.setString("Debug graphics");
    label_reduced.setColor( LABEL_COLOR );
    label_reduced.setPosition( ROW_OFFSET, COL_OFFSET+ROW_HEIGHT*3+LABEL_Y_OFFSET);

    sf::Texture* loaded_tex = manager.getTexture("gfx/Checkbox.png");

    chb_fullscreen.setTexture(loaded_tex);
    chb_fullscreen.setTextureParam( ROW_OFFSET+COL_WIDTH, COL_OFFSET+ROW_HEIGHT, 32, 32);
    chb_fullscreen.setArea( ROW_OFFSET+COL_WIDTH+2, COL_OFFSET+ROW_HEIGHT+7, 23, 23);

    chb_fps.setTexture(loaded_tex);
    chb_fps.setTextureParam( ROW_OFFSET+COL_WIDTH, COL_OFFSET+ROW_HEIGHT*2, 32, 32);
    chb_fps.setArea( ROW_OFFSET+COL_WIDTH+2, COL_OFFSET+ROW_HEIGHT*2+7, 23, 23);

    chb_reduced.setTexture(loaded_tex);
    chb_reduced.setTextureParam( ROW_OFFSET+COL_WIDTH, COL_OFFSET+ROW_HEIGHT*3, 32, 32);
    chb_reduced.setArea( ROW_OFFSET+COL_WIDTH+2, COL_OFFSET+ROW_HEIGHT*3+7, 23, 23);

    loaded_tex = manager.getTexture("gfx/Exit2.png");
    button_back.setTexture(loaded_tex);
    button_back.setTextureParam( ROW_OFFSET+16, COL_OFFSET+ROW_HEIGHT*5, 32, 32 );

    g_game.settings.loadFromFile("settings.cfg");
    chb_fullscreen.setState(g_game.settings.fullscreen);
    chb_reduced.setState(g_game.settings.reduced_gfx);
    chb_fps.setState(g_game.settings.show_fps);

    loaded_tex = manager.getTexture("gfx/main_menu_BLUE_1024.png");
    if( loaded_tex != NULL )
        background.setTexture(*loaded_tex);
    background.setPosition( 0.0f, 0.0f );

    return 0;
}

void CSettingsState::leaveState() {
    g_game.settings.fullscreen = chb_fullscreen.getState();
    g_game.settings.reduced_gfx = chb_reduced.getState();
    g_game.settings.show_fps = chb_fps.getState();
    g_game.settings.saveToFile("settings.cfg");
    manager.freeAll();
}

void CSettingsState::handleEvent( sf::Event& event ) {


    switch (event.type)
    {
        // window closed
        case sf::Event::Closed:
            g_game.setRunning(false);
            break;

        // key pressed
        case sf::Event::KeyPressed:
            if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape )
                button_back.pressButton();
            break;

        case sf::Event::MouseButtonPressed:
            if( event.mouseButton.button == sf::Mouse::Left ) {
                chb_fullscreen.mouseDown( event.mouseButton.x, event.mouseButton.y );
                chb_reduced.mouseDown( event.mouseButton.x, event.mouseButton.y );
                chb_fps.mouseDown( event.mouseButton.x, event.mouseButton.y );
                button_back.mouseDown( event.mouseButton.x, event.mouseButton.y );
            }
            break;
        case sf::Event::MouseButtonReleased:
            if( event.mouseButton.button == sf::Mouse::Left ) {
                button_back.mouseUp( event.mouseButton.x, event.mouseButton.y );
            }
        break;
        default:
            break;
    }

}

void CSettingsState::step() {

    sf::Vector2i cursor = sf::Mouse::getPosition(*g_game.window);

    if( button_back.check( cursor.x, cursor.y) )
        next_state = STATE_MENU;

}

void CSettingsState::render() {
    //g_game.window->clear(sf::Color::Blue);
    g_game.window->draw(background);
    g_game.window->draw(label_fullscreen);
    g_game.window->draw(label_settings);
    g_game.window->draw(label_fps);
    g_game.window->draw(label_reduced);
    chb_fullscreen.render( g_game.window );
    chb_reduced.render( g_game.window );
    chb_fps.render( g_game.window );
    button_back.render( g_game.window );
    g_game.window->display();
}

//runs only if do_sleep is false, must not block
void CSettingsState::idle( sf::Time remaining_time ) {
}
