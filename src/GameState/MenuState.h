#ifndef _MENU_H_
#define _MENU_H_

#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "../Graphics/GUI.h"
#include "../Util.h"

//MBUTTON = Menu Button
//enum eMenuButtons { MBUTTON_SP, MBUTTON_MP, MBUTTON_SETTINGS, MBUTTON_CREDITS, MBUTTON_QUIT, MBUTTON_MAX };
enum eMenuButtons { MBUTTON_TUTORIAL, MBUTTON_SP, MBUTTON_MP, MBUTTON_EDITOR, MBUTTON_SETTINGS, MBUTTON_CREDITS, MBUTTON_QUIT, MBUTTON_MAX };

//implements main menu
//inherits from CGameState, see description there
class CMenuState : public CGameState {

	public:
	CMenuState() : do_sleep(true), next_state(STATE_MENU) {}
	~CMenuState() {}

	int loadState();
	void leaveState();

	void handleEvent( sf::Event& event );
	void step();
	void render();

	//runs only if do_sleep is false, must not block
	void idle( sf::Time remaining_time );

	void setNextState( int new_next_state ) { next_state = new_next_state; }
	int getNextState() { return next_state; }

    //if true, the app sleeps until the next frame, else it runs the idle function
	bool do_sleep;

	protected:
	int next_state;		//next game state
	CTextureManager manager;

	private:
	CButton buttons[MBUTTON_MAX];
	sf::Sprite background;
};

#endif
