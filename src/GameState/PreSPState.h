#ifndef PRE_SP_STATE_H_
#define PRE_SP_STATE_H_

#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "../Graphics/GUI.h"
#include "../Util.h"
#include "../Editor/EditorGUI.h"

//implements pre singleplayer state
//inherits from CGameState, see description there
class CPreSPState : public CGameState {

	public:
	CPreSPState() : do_sleep(true), next_state(STATE_PRE_SP), name_valid(true), map_valid(true) {}
	~CPreSPState() {}

	int loadState();
	void leaveState();

	void handleEvent( sf::Event& event );
	void step();
	void render();

	//runs only if do_sleep is false, must not block
	void idle( sf::Time remaining_time );

	void setNextState( int new_next_state ) { next_state = new_next_state; }
	int getNextState() { return next_state; }

    //if true, the app sleeps until the next frame, else it runs the idle function
	bool do_sleep;

	protected:
	int next_state;		//next game state
	CTextureManager manager;

    private:
    bool checkName(std::string name);
    bool checkFile(std::string path);

    bool name_valid;
    bool map_valid;

    std::vector<sf::Color> p_colors; //primary
    std::vector<sf::Color> s_colors; //secondary

    sf::Sprite background;
	std::vector<sf::Text> labels;
	sf::Text label_name_inv;
	sf::Text label_map_inv;
	CInputBox name_input;
	CInputBox map_input;
	CButton btn_load;
	CButton btn_back;
	CButton btn_go;
	CChoiceBox choice_car;
	CChoiceBox choice_pcolor;
	CChoiceBox choice_scolor;
};

#endif
