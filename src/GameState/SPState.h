#ifndef SP_STATE_H_
#define SP_STATE_H_

#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "../Graphics/GUI.h"
#include "../Util.h"
#include <Box2D/Box2D.h>
#include "../Graphics/DebugDraw.h"
#include "../Physics/CarPhysics.h"
#include "../Physics/GroundType.h"
#include "../Control.h"
#include "../Graphics/Camera.h"
#include "../Graphics/Texture.h"
#include "../Object/GameObject.h"
#include "../Object/Car.h"
#include "../Editor/Map/MapInfo.h"
#include "../Graphics/TireTracks.h"
#include "../SinglePlayer/SPHUD.h"

//implements pre singleplayer state
//inherits from CGameState, see description there
//Class SingplePlayer State
class CSPState : public CGameState {

	public:
	CSPState() : do_sleep(true), next_state(STATE_SP) {}
	~CSPState() {}

	int loadState();
	void leaveState();

	void handleEvent( sf::Event& event );
	void step();
	void render();

	//runs only if do_sleep is false, must not block
	void idle( sf::Time remaining_time );

	void setNextState( int new_next_state ) { next_state = new_next_state; }
	int getNextState() { return next_state; }

    //if true, the app sleeps until the next frame, else it runs the idle function
	bool do_sleep;

	protected:
	int next_state;		//next game state
	CTextureManager manager;

    private:
    CContactListener contact_listener;

    std::vector<CGameObject*> static_objects;
    std::list<CGameObject*> dynamic_objects;

	float b2d_to_gfx;

    CCamera camera;
    CDebugDraw debug_draw;
    //test physics
    int velocity_iter;   //how strongly to correct velocity
    int position_iter;   //how strongly to correct position
    b2World* world;
    //two bodies just for testing debug draw..
    b2Body* test_body0;
    b2Body* test_body1;

    CCarPhysics* car0;
    CTexture* car_texture;

    bool ctrl_reverse;
    CControlPanel control_panel;

    CMapInfo map_info;

    CTireTracks tracks;

    CSPHUD HUD_obj;
};

#endif
