#include <SFML/Graphics.hpp>
#include "../Game.h"
#include "PreSPState.h"
#include "../Util.h"
#include <vector>
#include <string>
#include "../SinglePlayer/SPConfig.h"
#include <stdio.h>

static int LABEL_CHAR_SIZE = 14;
static sf::Color LABEL_COLOR = sf::Color::Yellow;
static int X_OFFSET = 325;
static int Y_OFFSET = 225;
static int ROW_HEIGHT = 40;
static int COL_WIDTH = 125;
static int LABEL_Y_OFFSET = 11;

int CPreSPState::loadState() {

    sf::Texture* loaded_tex = manager.getTexture("gfx/main_menu_BLUE_1024.png");
    if( loaded_tex != NULL )
        background.setTexture(*loaded_tex);
    background.setPosition( 0.0f, 0.0f );

    sf::Font* font = manager.getFont("gfx/DejaVuSansMono.ttf");

    sf::Text init_label;

    if( font != NULL ) {
        init_label.setFont(*font);
        init_label.setCharacterSize(LABEL_CHAR_SIZE);
        init_label.setColor(LABEL_COLOR);
    }

    std::vector<std::string> label_str;
    label_str.push_back("Player name");
    label_str.push_back("Map path");
    label_str.push_back("Car type");
    label_str.push_back("First color");
    label_str.push_back("Second color");

    unsigned int i;
    for( i=0; i<label_str.size(); i++) {
        labels.push_back(init_label);
        labels.back().setString(label_str.at(i));
        labels.back().setPosition( X_OFFSET, Y_OFFSET+LABEL_Y_OFFSET+i*ROW_HEIGHT);
    }

    label_name_inv = init_label;
    label_name_inv.setColor(sf::Color::Red);
    label_name_inv.setString("INVALID NAME");
    label_name_inv.setStyle(sf::Text::Bold);
    label_name_inv.setPosition( X_OFFSET+3*COL_WIDTH, Y_OFFSET+LABEL_Y_OFFSET);

    label_map_inv = init_label;
    label_map_inv.setColor(sf::Color::Red);
    label_map_inv.setString("INVALID PATH");
    label_map_inv.setStyle(sf::Text::Bold);
    label_map_inv.setPosition( X_OFFSET+3*COL_WIDTH, Y_OFFSET+LABEL_Y_OFFSET+1*ROW_HEIGHT);

    name_input.setFont(font);
    name_input.setTextureParam( X_OFFSET+COL_WIDTH, Y_OFFSET, 135, 30, sf::Color(245,245,245, 255) );
    name_input.setLabelParam( "Player", 14, 15, sf::Color::Black );
    name_input.setActive();

    map_input.setFont(font);
    map_input.setTextureParam( X_OFFSET+COL_WIDTH, Y_OFFSET+ROW_HEIGHT, 220, 30, sf::Color(245,245,245, 255) );
    map_input.setLabelParam( "maps/file.map", 14, 26, sf::Color::Black );

    loaded_tex = manager.getTexture("gfx/button_RED_small.png");

    btn_back.setTexture(loaded_tex);
    btn_back.setFont(font);
    btn_back.setTextureParam( X_OFFSET+40, Y_OFFSET+(i+1)*ROW_HEIGHT, 218, 35);
    btn_back.setLabelParam( "Back", 14, sf::Color::White, -10, -4 );

    btn_go.setTexture(loaded_tex);
    btn_go.setFont(font);
    btn_go.setTextureParam( X_OFFSET+40+2*COL_WIDTH, Y_OFFSET+(i+1)*ROW_HEIGHT, 218, 35);
    btn_go.setLabelParam( "Go", 14, sf::Color::White, -10, -4 );

    choice_car.setFont(font);
    choice_car.setTextureParam( X_OFFSET+COL_WIDTH, Y_OFFSET+2*ROW_HEIGHT, 75, 30, sf::Color(245,245,245, 255) );
    choice_car.setLabelParam( 14, sf::Color::Black );
    choice_car.elements.push_back("<(1)>");

    choice_pcolor.setFont(font);
    choice_pcolor.setTextureParam( X_OFFSET+COL_WIDTH, Y_OFFSET+3*ROW_HEIGHT, 100, 30, sf::Color(245,245,245, 255) );
    choice_pcolor.setLabelParam( 14, sf::Color::Black );
    choice_pcolor.elements.push_back("<Red>");
    choice_pcolor.elements.push_back("<Green>");
    choice_pcolor.elements.push_back("<Blue>");
    choice_pcolor.elements.push_back("<Black>");
    choice_pcolor.elements.push_back("<White>");

    p_colors.push_back( sf::Color::Red );
    p_colors.push_back( sf::Color::Green );
    p_colors.push_back( sf::Color::Blue );
    p_colors.push_back( sf::Color::Black );
    p_colors.push_back( sf::Color::White );

    choice_scolor.setFont(font);
    choice_scolor.setTextureParam( X_OFFSET+COL_WIDTH, Y_OFFSET+4*ROW_HEIGHT, 100, 30, sf::Color(245,245,245, 255) );
    choice_scolor.setLabelParam( 14, sf::Color::Black );
    choice_scolor.elements.push_back("<White>");
    choice_scolor.elements.push_back("<Black>");
    choice_scolor.elements.push_back("<Red>");
    choice_scolor.elements.push_back("<Blue>");
    choice_scolor.elements.push_back("<Green>");
    choice_scolor.elements.push_back("<None>");

    s_colors.push_back( sf::Color::White );
    s_colors.push_back( sf::Color::Black );
    s_colors.push_back( sf::Color::Red );
    s_colors.push_back( sf::Color::Blue );
    s_colors.push_back( sf::Color::Green );
    s_colors.push_back( sf::Color::Transparent );

	return 0;
}

void CPreSPState::leaveState() {
    if( next_state == STATE_SP ) {
        std::string t;
        name_input.getContent(t);
        g_game.SPConfig.player_name = t;
        map_input.getContent(t);
        g_game.SPConfig.map_path = t;
        g_game.SPConfig.car_type = choice_car.getChoice();
        unsigned int num;
        num = choice_pcolor.getChoice();
        if(num<p_colors.size())
            g_game.SPConfig.primary_color = p_colors.at(num);
        else
            g_game.SPConfig.primary_color = sf::Color::White;

        num = choice_scolor.getChoice();
        if(num<s_colors.size())
            g_game.SPConfig.secondary_color = s_colors.at(num);
        else
            g_game.SPConfig.secondary_color = sf::Color::Black;

    }
    manager.freeAll();
}

void CPreSPState::handleEvent( sf::Event& event ) {
    switch( event.type ) {
        // window closed
        case sf::Event::Closed:
            g_game.setRunning(false);
            break;

        // key pressed
        case sf::Event::KeyPressed:
            if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape ) {
                if( !name_input.isActive() && !map_input.isActive() )
                    btn_back.pressButton();
            }
            else if( event.key.code == sf::Keyboard::Return ) {   //Enter
                name_input.eventEnter();
                map_input.eventEnter();
            }
            if( event.key.code == sf::Keyboard::Left ) {
                if( name_input.isActive() )
                    name_input.moveCursorLeft();
                else if( map_input.isActive() )
                    map_input.moveCursorLeft();
                if( choice_car.isActive() )
                    choice_car.keyPress(event.key.code);
                if( choice_pcolor.isActive() )
                    choice_pcolor.keyPress(event.key.code);
                if( choice_scolor.isActive() )
                    choice_scolor.keyPress(event.key.code);
            }
            if( event.key.code == sf::Keyboard::Right ) {
                if( name_input.isActive() )
                    name_input.moveCursorRight();
                else if( map_input.isActive() )
                    map_input.moveCursorRight();
                if( choice_car.isActive() )
                    choice_car.keyPress(event.key.code);
                if( choice_pcolor.isActive() )
                    choice_pcolor.keyPress(event.key.code);
                if( choice_scolor.isActive() )
                    choice_scolor.keyPress(event.key.code);
            }
            if( event.key.code == sf::Keyboard::Delete ) {
                if( name_input.isActive() )
                    name_input.eventDel();
                else if( map_input.isActive() )
                    map_input.eventDel();
            }
            break;

        case sf::Event::MouseButtonPressed:
            if( event.mouseButton.button == sf::Mouse::Left ) {
                name_input.mouseDown( event.mouseButton.x, event.mouseButton.y);
                map_input.mouseDown( event.mouseButton.x, event.mouseButton.y);
                btn_back.mouseDown( event.mouseButton.x, event.mouseButton.y);
                btn_go.mouseDown( event.mouseButton.x, event.mouseButton.y);
                choice_car.mouseDown( event.mouseButton.x, event.mouseButton.y);
                choice_pcolor.mouseDown( event.mouseButton.x, event.mouseButton.y);
                choice_scolor.mouseDown( event.mouseButton.x, event.mouseButton.y);
            }
            break;

        case sf::Event::MouseButtonReleased:
            if( event.mouseButton.button == sf::Mouse::Left ) {
                btn_back.mouseUp( event.mouseButton.x, event.mouseButton.y);
                btn_go.mouseUp( event.mouseButton.x, event.mouseButton.y);
            }
            break;

        case sf::Event::TextEntered:
            if (event.text.unicode < 128) {
                name_input.textEntered(static_cast<char>(event.text.unicode));
                map_input.textEntered(static_cast<char>(event.text.unicode));
            }
            break;

        default:
            break;
    }

}

bool CPreSPState::checkName(std::string name) {
    if(name.length()>3)
        return true;
    else return false;
}

bool CPreSPState::checkFile(std::string path) {
    FILE* fp = fopen( path.c_str(), "r");
    if( fp == NULL )
        return false;
    else {
        fclose(fp);
        return true;
    }
}

void CPreSPState::step() {
    sf::Vector2i cursor = sf::Mouse::getPosition(*g_game.window);

    if( btn_back.check( cursor.x, cursor.y) )
        next_state = STATE_MENU;
    if( btn_go.check( cursor.x, cursor.y) ) {
        std::string s;
        map_input.getContent(s);
        map_valid = checkFile(s);
        name_input.getContent(s);
        name_valid = checkName(s);
        if( map_valid && name_valid )
            next_state = STATE_SP;
    }
}

void CPreSPState::render() {
    g_game.window->draw(background);

    for( unsigned int i=0; i<labels.size(); i++)
        g_game.window->draw( labels.at(i) );

    if( !name_valid )
        g_game.window->draw( label_name_inv );

    if( !map_valid )
        g_game.window->draw( label_map_inv );

    name_input.render(g_game.window);
    map_input.render(g_game.window);

    btn_back.render(g_game.window);
    btn_go.render(g_game.window);

    choice_car.render(g_game.window);
    choice_pcolor.render(g_game.window);
    choice_scolor.render(g_game.window);

    g_game.window->display();
}

//runs only if do_sleep is false, must not block
void CPreSPState::idle( sf::Time remaining_time ) {
}
