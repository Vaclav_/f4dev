#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include <sstream>

#include "../Game.h"
#include "SPState.h"
#include "../Util.h"
#include "../Physics/PhysicsParam.h"
#include "../Editor/Map/MapLoader.h"

int CSPState::loadState() {

    b2d_to_gfx = 25.0f;

    ctrl_reverse = false;

    next_state = STATE_SP;

    initGroundData();
    loadParamFromFile();

    velocity_iter = 8;
    position_iter = 3;

    world = NULL;

    sf::Texture* panel_tex;
    panel_tex = manager.getTexture("gfx/Glass.png");
    control_panel.setTexture(panel_tex);

    //world = new b2World(b2Vec2(0.0f,0.0f));
    bool ret = loadMap( g_game.SPConfig.map_path, map_info, static_objects, world, b2d_to_gfx);

    if( ret )
        g_game.debug << "Loaded map succesfully.";
    else
        g_game.error << "Error loading map.";

    if(world==NULL) {
        //TODO: proper map loading, and error handling
        g_game.error << "world pointer is NULL.";
        g_game.setRunning(false);
        return -1;
    }

    world->SetContactListener(&contact_listener);

	uint32 flags = 0;
	flags |= b2Draw::e_shapeBit;
	//flags |= b2Draw::e_jointBit;
	//flags |= b2Draw::e_aabbBit;
	//flags |= b2Draw::e_pairBit;
	//flags |= b2Draw::e_centerOfMassBit;
	debug_draw.SetFlags(flags);

    world->SetDebugDraw( &debug_draw );

    debug_draw.setFont( manager.getFont("gfx/DejaVuSansMono.ttf") );
    debug_draw.setScale( b2d_to_gfx);

    if( g_game.settings.reduced_gfx )
        debug_draw.setReducedMode();


    CCarDef car_def;
    car_def.drive_t = CAR_DRIVE_TYPE;
    car_def.car_mass = CAR_MASS;
    car_def.car_width = CAR_WIDTH;
    car_def.car_length = CAR_LENGTH;

    car_def.tire_rad = TIRE_RADIUS;
    car_def.tire_mass = TIRE_MASS;
    car_def.tire_width = TIRE_WIDTH;
    car_def.tire_length = TIRE_LENGTH;

    car_def.front_tire_x = FRONT_TIRE_X;
    car_def.front_tire_y = FRONT_TIRE_Y;
    car_def.rear_tire_x = REAR_TIRE_X;
    car_def.rear_tire_y = REAR_TIRE_Y;

    car_def.drive_target_ang_vel = DRIVE_TARGET_ANG_VEL;
    car_def.drive_torque_sat = DRIVE_TORQUE_SATURATION;
    car_def.rev_torque = REVERSE_TORQUE;
    car_def.max_brake_torque = MAX_BRAKE_TORQUE;

    b2Vec2 start_pos(0.0f, 0.0f);
    CStartPointObject* start_point;
    //get first starting point
    for( sf::Uint32 k=0; k<static_objects.size(); k++) {
        if( static_objects.at(k)->type == GAMEOBJ_STARTPOINT ) {
            start_point = (CStartPointObject*)static_objects.at(k);
            if(start_point->number == 1) {
                start_pos = start_point->position;
            }
        }
    }
    ///TODO: Load starting angle??
    car0 = new CCarPhysics( world, car_def, start_pos, -b2_pi/2.0f );

    sf::Mouse::setPosition( control_panel.position, *g_game.window);

    g_game.window->setView(camera.view);

    camera.setPosition( b2Vec2( 0.0f, 0.0f) );

    camera.view.zoom(2.5f);

    debug_draw.static_lines.push_back("Press Esc or q to return to main menu.");
    debug_draw.static_lines.push_back("\n");


    sf::Sprite sp;
    sf::RenderTexture render_tex;
    sf::Texture* car_combined_tex;
    sf::Texture t0, t1, t2;

    //err check!
    if(!t0.loadFromFile("gfx/CarBody25.png"))
        g_game.error << "Can't load texture.";
    if(!t1.loadFromFile("gfx/CarWindscreen25.png"))
        g_game.error << "Can't load texture.";
    if(!t2.loadFromFile("gfx/CarStripes25.png"))
        g_game.error << "Can't load texture.";

    if (!render_tex.create(115, 50))
        g_game.error << "Can't create render texture.";

    //g_game.SPConfig.secondary_color


    render_tex.clear( sf::Color::Transparent );
    sp.setTexture(t0);
    sp.setColor(g_game.SPConfig.primary_color);
    render_tex.draw(sp);
    sp.setTexture(t1);
    sp.setColor(sf::Color::White);
    render_tex.draw(sp);
    sp.setTexture(t2);
    sp.setColor(g_game.SPConfig.secondary_color);
    render_tex.draw(sp);
    render_tex.display();

    std::string tex_name = "car0_combined";
    car_combined_tex = manager.addTexture( render_tex.getTexture(), tex_name);
    car_combined_tex->setSmooth(true);

    car_texture = new CTexture(b2d_to_gfx);
    car_texture->setTexture(*car_combined_tex);

    sf::FloatRect t_size = car_texture->getLocalBounds();
    car_texture->setOrigin( t_size.width/2, t_size.height/2);
    car_texture->attach( car0->body );
    car_texture->setAngle( 90.0f);

    CCar* car0_obj = new CCar( car0, car_texture, TIRE_WIDTH, TIRE_LENGTH);

    dynamic_objects.push_back(car0_obj);

    sf::Font* font = manager.getFont("gfx/DejaVuSansMono.ttf");
    HUD_obj.initLabel( font, 800, 20, 18, sf::Color::White );
    HUD_obj.timer.restart();

    //find finish line in objects
    CFinishLineObject* f_line = NULL;

    unsigned int k;
    for( k=0; k<static_objects.size(); k++) {
        assert( static_objects.at(k) );
        if( static_objects.at(k)->type == GAMEOBJ_FINISHLINE ) {
            f_line = (CFinishLineObject*)static_objects.at(k);
            break;
        }
    }
    //find a start point
    CStartPointObject* start_p = NULL;
    for( k=0; k<static_objects.size(); k++) {
        assert( static_objects.at(k) );
        if( static_objects.at(k)->type == GAMEOBJ_STARTPOINT ) {
            start_p = (CStartPointObject*)static_objects.at(k);
            break;
        }
    }

    HUD_obj.init( f_line, start_p);

	return 0;
}

void CSPState::leaveState() {
    unsigned int i;

    world->SetContactListener(NULL);

    for( i=0; i<static_objects.size(); i++) {
        if( static_objects.at(i)!=NULL) {
            delete static_objects.at(i);
            static_objects.at(i) = NULL;
        }
    }

    std::list<CGameObject*>::iterator it;
    for( it=dynamic_objects.begin(); it!=dynamic_objects.end(); it++) {
        if( (*it) != NULL )
            delete (*it);
        (*it) = NULL;
    }

    if( world != NULL) delete world;
    world = NULL;

    //free textures and fonts
    manager.freeAll();
}

void CSPState::handleEvent( sf::Event& event ) {

    switch (event.type)
    {
        // window closed
        case sf::Event::Closed:
            g_game.setRunning(false);
            break;

        // key pressed
        case sf::Event::KeyPressed:
            if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape )
                next_state = STATE_MENU;
            else if( event.key.code == sf::Keyboard::X )
                camera.view.zoom(0.95f);
            else if( event.key.code == sf::Keyboard::C )
                camera.view.zoom(1.05f);
            else if( event.key.code == sf::Keyboard::D )    //toggle debug
                g_game.debug_on = !g_game.debug_on;
            break;
        default:
            break;
    }

}

void CSPState::step() {

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
        ctrl_reverse = true;
    else
        ctrl_reverse = false;

    debug_draw.checkIfRendered();

    unsigned int i;
    for( i=0; i<static_objects.size(); i++) {
        static_objects.at(i)->step( g_game.getFrameTime().asSeconds() );
    }
    //TODO: add same for dynamic objects

    car0->setSteering( control_panel.getThrottle(), control_panel.getBrake(), control_panel.getSteering(), ctrl_reverse );
    car0->step( g_game.getFrameTime().asSeconds() );
    world->Step( g_game.getFrameTime().asSeconds(), velocity_iter, position_iter);

    std::stringstream d_ctrl;

    if( g_game.settings.show_fps ) {
        d_ctrl << "FPS: " << g_game.fps_measured;
        debug_draw.dynamic_lines.push_back(d_ctrl.str());
    }

    std::stringstream gear_str;
    gear_str << "Gear: " << car0->drive->cur_gear + 1;
    debug_draw.dynamic_lines.push_back(gear_str.str());

    /*
    d_ctrl.str("");
    d_ctrl << "Throttle: " << control_panel.getThrottle() << " |";
    d_ctrl << "Brake: " << control_panel.getBrake() << " |";
    d_ctrl << "Steering: " << control_panel.getSteering();
    debug_draw.dynamic_lines.push_back(d_ctrl.str());*/


    tracks.check_tires( &car0->tires.at(0) );

    HUD_obj.update( car0->getBody()->GetPosition());

    /*std::stringstream ss;
    ss << "ang: " << ang;
    debug_draw.dynamic_lines.push_back(ss.str());*/
}

void CSPState::render() {
    g_game.window->clear(sf::Color( 90, 50, 25));

    g_game.window->setView(camera.view);

    camera.adjust( car0->body->GetPosition(), car0->body->GetLinearVelocity(), b2d_to_gfx);

    sf::FloatRect view_rect;
    const sf::Vector2f& view_center = g_game.window->getView().getCenter();
    const sf::Vector2f& view_size = g_game.window->getView().getSize();
    view_rect.left = view_center.x-view_size.x/2.0f;
    view_rect.top = view_center.y-view_size.y/2.0f;
    view_rect.width = view_size.x;
    view_rect.height = view_size.y;

    unsigned int i;
    for( i=0; i<static_objects.size(); i++) {
        static_objects.at(i)->render( g_game.window, view_rect );
    }
    //TODO: add same for dynamic objects

    tracks.render( g_game.window, view_rect);

    std::list<CGameObject*>::iterator it;
    for( it=dynamic_objects.begin(); it!=dynamic_objects.end(); it++) {
        if( (*it) != NULL )
            (*it)->render( g_game.window, view_rect );
    }

    if( g_game.debug_on && g_game.settings.reduced_gfx ) {
        world->DrawDebugData();
        debug_draw.renderWorld( g_game.window );
    }

    //render User Interface
    g_game.window->setView( g_game.window->getDefaultView() );

    if( g_game.debug_on ) {
        debug_draw.renderText( g_game.window );
    }

    control_panel.render();
    HUD_obj.renderTime( g_game.window );

    g_game.window->display();
}

//runs only if do_sleep is false, must not block
void CSPState::idle( sf::Time remaining_time ) {
}
