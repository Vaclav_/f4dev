#include <SFML/Graphics.hpp>
#include "../Game.h"
#include "PreMPState.h"
#include "../Util.h"
#include <vector>
#include <string>
#include <iostream>

int CPreMPState::loadState() {

    sf::Font* font;
    font = manager.getFont("gfx/DejaVuSansMono.ttf");
    if( font!=NULL ) {
    title.setFont(*font);

    title.setCharacterSize(14);
    title.setString("Press escape or q to return to main menu.");
    title.setColor(sf::Color::Yellow);
    title.setPosition( 20.0f, 10.0f);

    player_name.setFont(*font);
    player_name.setCharacterSize(14);
    player_name.setString("Player Name");
    player_name.setColor(sf::Color::Yellow);
    player_name.setPosition( 50.0f, 100.0f);

    host_label.setFont(*font);
    host_label.setCharacterSize(14);
    host_label.setString("Host Name");
    host_label.setColor(sf::Color::Yellow);
    host_label.setPosition( 50.0f, 200.0f);

    players_label.setFont(*font);
    players_label.setCharacterSize(14);
    players_label.setString("Players IN/MAX");
    players_label.setColor(sf::Color::Yellow);
    players_label.setPosition( 350.0f, 200.0f);

    hotkeys_label.setFont(*font);
    hotkeys_label.setCharacterSize(14);
    hotkeys_label.setString("Hotkeys\n(H) Host game\n(C) Connect\n(R) Refresh\n(q) Back");
    hotkeys_label.setColor(sf::Color::Yellow);
    hotkeys_label.setPosition( 500.0f, 250.0f);
    }

    games_panel.setSize(sf::Vector2f( 425.0f, 425.0f));
    games_panel.setPosition( 50.0f, 225.0f);
    games_panel.setFillColor(sf::Color(255,255,224,240));

    name_input.setFont(font);
    name_input.setTextureParam( 250, 100, 225, 30, sf::Color(245,245,245, 255) );
    name_input.setLabelParam( "", 14, 15, sf::Color::Black );
    //name_input.setActive();

    hostname_input.setFont(font);
    hostname_input.setTextureParam( 625, 250, 225, 30, sf::Color(245,245,245, 255) );
    hostname_input.setLabelParam( "", 14, 15, sf::Color::Black );

	return 0;
}

void CPreMPState::leaveState() {
    manager.freeAll();
}

void CPreMPState::handleEvent( sf::Event& event ) {

    switch (event.type)
    {
        // window closed
        case sf::Event::Closed:
            g_game.setRunning(false);
            break;

        // key pressed
        case sf::Event::KeyPressed:
            if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape ) {
                if( !name_input.isActive() && !hostname_input.isActive() )
                    next_state = STATE_MENU;
            }
            if( event.key.code == sf::Keyboard::H ) //Host
                if( !name_input.isActive() && !hostname_input.isActive() ) next_state = STATE_MP_LOBBY;
            //else if( event.key.code == sf::Keyboard::C ) //Connect
            //else if( event.key.code == sf::Keyboard::R ) //Refresh
            if( event.key.code == sf::Keyboard::Return ) {   //Enter
                name_input.eventEnter();
                hostname_input.eventEnter();
            }
            if( event.key.code == sf::Keyboard::Left ) {
                if( name_input.isActive() )
                    name_input.moveCursorLeft();
                else if( hostname_input.isActive() )
                    hostname_input.moveCursorLeft();
            }
            if( event.key.code == sf::Keyboard::Right ) {
                if( name_input.isActive() )
                    name_input.moveCursorRight();
                else if( hostname_input.isActive() )
                    hostname_input.moveCursorRight();
            }
            if( event.key.code == sf::Keyboard::Delete ) {
                if( name_input.isActive() )
                    name_input.eventDel();
                else if( hostname_input.isActive() )
                    hostname_input.eventDel();
            }
            break;

        case sf::Event::MouseButtonPressed:
            name_input.mouseDown( event.mouseButton.x, event.mouseButton.y);
            hostname_input.mouseDown( event.mouseButton.x, event.mouseButton.y);
            break;

        case sf::Event::TextEntered:
            if (event.text.unicode < 128) {
                name_input.textEntered(static_cast<char>(event.text.unicode));
                hostname_input.textEntered(static_cast<char>(event.text.unicode));
            }
            break;

        default:
            break;
    }

}

void CPreMPState::step() {
}

void CPreMPState::render() {
    g_game.window->clear(sf::Color::Black);
    g_game.window->draw(title);
    g_game.window->draw(player_name);
    g_game.window->draw(host_label);
    g_game.window->draw(players_label);
    g_game.window->draw(hotkeys_label);
    g_game.window->draw(games_panel);
    name_input.render(g_game.window);
    hostname_input.render(g_game.window);
    g_game.window->display();
}

//runs only if do_sleep is false, must not block
void CPreMPState::idle( sf::Time remaining_time ) {
}
