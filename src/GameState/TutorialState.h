#ifndef TUTORIAL_STATE_H_
#define TUTORIAL_STATE_H_

#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "../Graphics/GUI.h"
#include "../Util.h"

//implements pre singleplayer state
//inherits from CGameState, see description there
class CTutorialState : public CGameState {

	public:
	CTutorialState() : do_sleep(true), next_state(STATE_TUTORIAL) {}
	~CTutorialState() {}

	int loadState();
	void leaveState();

	void handleEvent( sf::Event& event );
	void step();
	void render();

	//runs only if do_sleep is false, must not block
	void idle( sf::Time remaining_time );

	void setNextState( int new_next_state ) { next_state = new_next_state; }
	int getNextState() { return next_state; }

    //if true, the app sleeps until the next frame, else it runs the idle function
	bool do_sleep;

	protected:
	int next_state;		//next game state
	CTextureManager manager;
	sf::Sprite background;
	sf::Text label0;
};

#endif
