#ifndef MP_LOBBY_H_
#define MP_LOBBY_H_

#include <SFML/Graphics.hpp>
#include "GameState.h"
#include "../Graphics/GUI.h"
#include "../Util.h"
#include <list>

//implements pre singleplayer state
//inherits from CGameState, see description there
class CMPLobbyState : public CGameState {

	public:
	CMPLobbyState() : do_sleep(true), next_state(STATE_MP_LOBBY) {}
	~CMPLobbyState() {}

	int loadState();
	void leaveState();

	void handleEvent( sf::Event& event );
	void step();
	void render();

	//runs only if do_sleep is false, must not block
	void idle( sf::Time remaining_time );

	void setNextState( int new_next_state ) { next_state = new_next_state; }
	int getNextState() { return next_state; }

    //if true, the app sleeps until the next frame, else it runs the idle function
	bool do_sleep;

	protected:
	int next_state;		//next game state
	CTextureManager manager;
	std::string host_name;
	std::string game_name;
	std::string map_name;
	std::list<std::string> players_list;
	bool ready;
	bool players_changed;
	sf::Text title;
	sf::Text host_name_l;
	sf::Text game_name_l;
	sf::Text map_name_l;
    sf::Text hotkeys_label;
    sf::Text players_label;
    sf::Text ready_label;
	CInputBox chat_input;
    sf::RectangleShape chat_panel;
};

#endif
