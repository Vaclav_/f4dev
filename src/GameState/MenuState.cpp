
#include <SFML/Graphics.hpp>
#include "../Game.h"
#include "MenuState.h"
#include "../Util.h"
#include <vector>
#include <string>
#include <stdio.h>


int CMenuState::loadState() {

	next_state = STATE_MENU;

	std::vector<std::string> menu_labels;
	menu_labels.push_back("Tutorial");
	menu_labels.push_back("Singleplayer");
	menu_labels.push_back("Multiplayer");
	menu_labels.push_back("Editor");
	menu_labels.push_back("Settings");
	menu_labels.push_back("Credits");
	menu_labels.push_back("Quit");

    sf::Texture* loaded_tex = NULL;
    sf::Font* loaded_font = NULL;
    std::vector<int> btn_x_offsets;
    btn_x_offsets.push_back(-80);
    btn_x_offsets.push_back(-105);
    btn_x_offsets.push_back(-115);
    btn_x_offsets.push_back(-115);
    btn_x_offsets.push_back(-108);
    btn_x_offsets.push_back(-85);
    btn_x_offsets.push_back(-365);

	unsigned int i;
	for( i=0; i<MBUTTON_MAX; i++) {
		loaded_tex = manager.getTexture("gfx/button_RED_1024.png");
		if( loaded_tex != NULL )
			buttons[i].setTexture(loaded_tex);
        //else failed to load texture
        loaded_font = manager.getFont("gfx/DejaVuSansMono.ttf");
		if( loaded_font != NULL )
            buttons[i].setFont(loaded_font);

        if( i<menu_labels.size()-1 )
            buttons[i].setTextureParam( g_game.scrnW/2+btn_x_offsets.at(i), 235+i*70, 405, 65 );
        else if( i<menu_labels.size() )
            buttons[i].setTextureParam( g_game.scrnW/2+btn_x_offsets.at(i), 235+i*70+45, 405, 65, sf::Color(255,0,0) );

        if( i<menu_labels.size() )
            buttons[i].setLabelParam( menu_labels.at(i), 24, sf::Color::White, -18, -6 );
        else
            buttons[i].setLabelParam( "<>", 24 );
	}

    loaded_tex = manager.getTexture("gfx/main_menu_RED_1024.png");
    if( loaded_tex != NULL )
        background.setTexture(*loaded_tex);
    background.setPosition( 0.0f, 0.0f );

    //FILE* fp = fopen( "out.vfs", "rb");
    //CVFSStream stream(fp);
    //stream.open("obj/main.o");
    //fclose(fp);

	return 0;
}

void CMenuState::leaveState() {

	manager.freeAll();

}

void CMenuState::handleEvent( sf::Event& event ) {

    switch (event.type)
    {
        // window closed
        case sf::Event::Closed:
            g_game.setRunning(false);
            break;

        // key pressed
        case sf::Event::KeyPressed:
            if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape )
                buttons[MBUTTON_QUIT].pressButton();
            break;

        case sf::Event::MouseButtonPressed:
            if( event.mouseButton.button == sf::Mouse::Left ) {
                for( int i=0; i<MBUTTON_MAX; i++) {
                    buttons[i].mouseDown( event.mouseButton.x, event.mouseButton.y);
                }
            }
            break;

        case sf::Event::MouseButtonReleased:
            if( event.mouseButton.button == sf::Mouse::Left ) {
                for( int i=0; i<MBUTTON_MAX; i++) {
                    buttons[i].mouseUp( event.mouseButton.x, event.mouseButton.y);
                }
            }
        break;

        default:
            break;
    }
}

void CMenuState::step() {

    sf::Vector2i cursor = sf::Mouse::getPosition(*g_game.window);

    buttons[MBUTTON_EDITOR].check( cursor.x, cursor.y);
    if( buttons[MBUTTON_TUTORIAL].check( cursor.x, cursor.y) )
        setNextState(STATE_TUTORIAL);
    if( buttons[MBUTTON_SP].check( cursor.x, cursor.y) )
        setNextState(STATE_PRE_SP);
    if( buttons[MBUTTON_MP].check( cursor.x, cursor.y ) )
        setNextState(STATE_PRE_MP);
    if( buttons[MBUTTON_SETTINGS].check( cursor.x, cursor.y) )
        setNextState(STATE_SETTINGS);
    if( buttons[MBUTTON_CREDITS].check( cursor.x, cursor.y) )
        setNextState(STATE_CREDITS);
    if( buttons[MBUTTON_QUIT].check( cursor.x, cursor.y) )
        g_game.setRunning(false);

}

void CMenuState::render() {

    g_game.window->draw(background);

	int i;
	for( i=0; i<MBUTTON_MAX; i++)
		buttons[i].render( g_game.window );

	g_game.window->display();

}

//runs only if do_sleep is false, must not block
void CMenuState::idle( sf::Time remaining_time ) {
}

