#include <SFML/Graphics.hpp>
#include "../Game.h"
#include "TutorialState.h"
#include "../Util.h"
#include <vector>
#include <string>

int CTutorialState::loadState() {

    sf::Texture* loaded_tex = manager.getTexture("gfx/main_menu_RED_1024.png");
    if( loaded_tex != NULL )
        background.setTexture(*loaded_tex);
    background.setPosition( sf::Vector2f( 0.0f, 0.0f) );

    sf::Font* loaded_font = manager.getFont("gfx/DejaVuSansMono.ttf");
    if( loaded_font != NULL )
        label0.setFont(*loaded_font);

    label0.setCharacterSize(16);
    label0.setColor(sf::Color::White);
    label0.setPosition( 240.0f, 215.0f );
    label0.setString(
        "\t\n\
        A playable tutorial will be made later.\n\
        Controls:\n\
        You can control the car with the mouse, not the\n\
        buttons. The Y axis is for throttle and brake,\n\
        while the X axis is for steering. When the mouse\n\
        is at the center of your control panel, no control\n\
        is applied. In this position you can use reverse\n\
        gear by pressing button A."
    );



	return 0;
}

void CTutorialState::leaveState() {
    manager.freeAll();
}

void CTutorialState::handleEvent( sf::Event& event ) {

    switch( event.type ) {
        // window closed
        case sf::Event::Closed:
            g_game.setRunning(false);
            break;

        // key pressed
        case sf::Event::KeyPressed:
            if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape )
                next_state = STATE_MENU;
            break;
        default:
            break;
    }
}

void CTutorialState::step() {
}

void CTutorialState::render() {
    g_game.window->draw(background);

    g_game.window->draw(label0);

    g_game.window->display();
}

//runs only if do_sleep is false, must not block
void CTutorialState::idle( sf::Time remaining_time ) {
}
