#include <SFML/Graphics.hpp>
#include "../Game.h"
#include "MPLobbyState.h"
#include "../Util.h"
#include <vector>
#include <string>

int CMPLobbyState::loadState() {

    host_name = "Unkown";
    game_name = "Unkown";
    map_name = "Unkown";
    ready = false;
    players_changed = true;

    sf::Font* font;
    font = manager.getFont("gfx/DejaVuSansMono.ttf");
    if( font!=NULL )
        title.setFont(*font);
    title.setCharacterSize(14);
    title.setString("Press escape or q to return to main menu.");
    title.setColor(sf::Color::White);
    title.setPosition( 20.0f, 10.0f);

    host_name_l.setFont(*font);
    host_name_l.setCharacterSize(14);
    host_name_l.setString("Host name: "+host_name);
    host_name_l.setColor(sf::Color::White);
    host_name_l.setPosition( 50.0f, 100.0f);

    game_name_l.setFont(*font);
    game_name_l.setCharacterSize(14);
    game_name_l.setString("Game name: "+game_name);
    game_name_l.setColor(sf::Color::White);
    game_name_l.setPosition( 50.0f, 150.0f);

    map_name_l.setFont(*font);
    map_name_l.setCharacterSize(14);
    map_name_l.setString("Map name: "+map_name);
    map_name_l.setColor(sf::Color::White);
    map_name_l.setPosition( 50.0f, 200.0f);

    hotkeys_label.setFont(*font);
    hotkeys_label.setCharacterSize(14);
    hotkeys_label.setString("Hotkeys\n(R) Ready/unReady\n(q) Leave");
    hotkeys_label.setColor(sf::Color::White);
    hotkeys_label.setPosition( 550.0f, 100.0f);

    players_label.setFont(*font);
    players_label.setCharacterSize(14);
    players_label.setString("Players\n"+host_name);
    players_label.setColor(sf::Color::White);
    players_label.setPosition( 550.0f, 250.0f);

    ready_label.setFont(*font);
    ready_label.setCharacterSize(14);
    ready_label.setString("(R)eady");
    ready_label.setColor(sf::Color::White);
    ready_label.setPosition( 650.0f, 650.0f);

    chat_panel.setSize(sf::Vector2f( 425.0f, 425.0f));
    chat_panel.setPosition( 50.0f, 225.0f);
    chat_panel.setFillColor(sf::Color(245,245,245,255));

    chat_input.setFont(font);
    chat_input.setTextureParam( 60, 610, 300, 30, sf::Color(200,200,200,255) );
    chat_input.setLabelParam( "", 14, 35, sf::Color::Black );
    chat_input.setActive();

	return 0;
}

void CMPLobbyState::leaveState() {
}

void CMPLobbyState::handleEvent( sf::Event& event ) {


    switch (event.type)
    {
        // window closed
        case sf::Event::Closed:
            g_game.setRunning(false);
            break;

        // key pressed
        case sf::Event::KeyPressed:
            if( event.key.code == sf::Keyboard::Q || event.key.code == sf::Keyboard::Escape ) {
                if( !chat_input.isActive() )
                    next_state = STATE_PRE_MP;
            }
            if( event.key.code == sf::Keyboard::R ) {
                if( !chat_input.isActive() )
                    ready = !ready;
                //if host: start!
            }
            if( event.key.code == sf::Keyboard::Return ) {   //Enter
                if( chat_input.isActive() ) {
                    //TODO: send chat message!
                    //chat_input.getContent();
                    chat_input.erase();
                }
            }
            if( event.key.code == sf::Keyboard::Left ) {
                if( chat_input.isActive() )
                    chat_input.moveCursorLeft();
            }
            if( event.key.code == sf::Keyboard::Right ) {
                if( chat_input.isActive() )
                    chat_input.moveCursorRight();
            }
            if( event.key.code == sf::Keyboard::Delete ) {
                if( chat_input.isActive() )
                    chat_input.eventDel();
            }
            break;

        case sf::Event::MouseButtonPressed:
            chat_input.mouseDown( event.mouseButton.x, event.mouseButton.y);
            break;

        case sf::Event::TextEntered:
            if (event.text.unicode < 128) {
                chat_input.textEntered(static_cast<char>(event.text.unicode));
            }
            break;

        default:
            break;
    }

}

//char a[]="Aa";
//int i=0;
void CMPLobbyState::step() {
/*    i++;
    if(!(i%50)) {
        std::string s = a;
        players_list.push_back(s);
        a[1]++;
        players_changed = true;
    }
*/
}

void CMPLobbyState::render() {
    g_game.window->clear(sf::Color::Black);
    g_game.window->draw(title);
    g_game.window->draw(host_name_l);
    g_game.window->draw(game_name_l);
    g_game.window->draw(map_name_l);
    g_game.window->draw(hotkeys_label);
    if(players_changed) {
        std::list<std::string>::iterator i;
        std::string new_label_str = "Players\n";
        new_label_str += host_name;
        new_label_str += "\n";
        for ( i = players_list.begin(); i != players_list.end(); i++) {
            new_label_str += *i;
            new_label_str += "\n";
        }
        players_label.setString(new_label_str);
    }
    g_game.window->draw(players_label);
    if(ready) g_game.window->draw(ready_label);
    g_game.window->draw(chat_panel);
    chat_input.render(g_game.window);
    g_game.window->display();
}

//runs only if do_sleep is false, must not block
void CMPLobbyState::idle( sf::Time remaining_time ) {
}
