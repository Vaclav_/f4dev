#ifndef SETTINGS_H_INCLUDED
#define SETTINGS_H_INCLUDED

class CSettings {
    public:
    CSettings();
    bool fullscreen;
    bool reduced_gfx;
    bool show_fps;

    bool loadFromFile( const char* path );
    bool saveToFile( const char* path );
};


#endif // SETTINGS_H_INCLUDED
