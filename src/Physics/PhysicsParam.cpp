
#include <stdio.h>
#include "GroundType.h"
#include "PhysicsParam.h"

#include "../Game.h"

extern int scanWord( FILE* fp, int max_len, char* buf);

int loadParamFromFile() {

	FILE* fp = NULL;
	fp = fopen( "param.cfg", "r");
	if(fp==NULL) {
		g_game.error << "Couldn't open parameters file!";
		return -1;
	}

	g_game.debug << "Loading parameters...";

	float d;
	char in_buffer[300];

	std::string temp_str;

	while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {

		if( in_buffer[0] == '#' ) {	// # marks comment
			//read until \n
			while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {
				if( in_buffer[0] == '\n' )
					break;
			}
			continue;
		}

		//go back and get param name+float
		fseek( fp, -1, SEEK_CUR );
		scanWord( fp, 300, &in_buffer[0]);
		fscanf( fp, "%f", &d);

		temp_str = in_buffer;

		if(temp_str == "CAR_WIDTH") {
			CAR_WIDTH = d;
		} else if(temp_str == "CAR_LENGTH") {
			CAR_LENGTH = d;
		} else if(temp_str == "TIRE_WIDTH") {
			TIRE_WIDTH = d;
		} else if(temp_str == "TIRE_LENGTH") {
			TIRE_LENGTH = d;
		} else if(temp_str == "TIRE_RADIUS") {
			TIRE_RADIUS = d;
		} else if(temp_str == "FRONT_TIRE_X") {
			FRONT_TIRE_X = d;
		} else if(temp_str == "FRONT_TIRE_Y") {
			FRONT_TIRE_Y = d;
		} else if(temp_str == "REAR_TIRE_X") {
			REAR_TIRE_X = d;
		} else if(temp_str == "REAR_TIRE_Y") {
			REAR_TIRE_Y = d;
		} else if(temp_str == "CAR_MASS") {
			CAR_MASS = d;
		} else if(temp_str == "TIRE_MASS") {
			TIRE_MASS = d;
		} else if(temp_str == "MAX_FWD_FRICTION") {
			ground_data[GTYPE_ROAD].MAX_FWD_FRICTION = d;
		} else if(temp_str == "FWD_FRICTION_FALLBACK") {
			ground_data[GTYPE_ROAD].FWD_FRICTION_FALLBACK = d;
		} else if(temp_str == "MAX_LAT_FRICTION") {
			ground_data[GTYPE_ROAD].MAX_LAT_FRICTION = d;
		} else if(temp_str == "LAT_FRICTION_FALLBACK") {
			ground_data[GTYPE_ROAD].LAT_FRICTION_FALLBACK = d;
		} else if(temp_str == "TIRE_FWD_FRICTION_MULT") {
			ground_data[GTYPE_ROAD].TIRE_FWD_FRICTION_MULT = d;
		} else if(temp_str == "ROAD_TO_TIRE_TORQUE") {
			ground_data[GTYPE_ROAD].ROAD_TO_TIRE_TORQUE = d;
		} else if(temp_str == "FRICTION_DRAWBACK_CONST") {
			ground_data[GTYPE_ROAD].FRICTION_DRAWBACK_CONST = d;
		} else if(temp_str == "AIR_RESISTANCE_CONST") {
			AIR_RESISTANCE_CONST = d;
		} else if(temp_str == "DRIVE_TARGET_ANG_VEL") {
			DRIVE_TARGET_ANG_VEL = d;
		} else if(temp_str == "DRIVE_TORQUE_SATURATION") {
			DRIVE_TORQUE_SATURATION = d;
		} else if(temp_str == "MAX_BRAKE_TORQUE") {
			MAX_BRAKE_TORQUE = d;
		} else if(temp_str == "MAX_STEERING_ANGLE") {
			MAX_STEERING_ANGLE = d;
		} else if(temp_str == "STEERING_ANG_VEL") {
			STEERING_ANG_VEL = d;
		} else if(temp_str == "CAR_DRIVE_TYPE") {
			CAR_DRIVE_TYPE = d;
		} else if(temp_str == "CAR_RESTITUTION") {
			CAR_RESTITUTION = d;
		} else if(temp_str == "WALL_RESTITUTION") {
			WALL_RESTITUTION = d;
		} else if(temp_str == "CAR_FRICTION") {
			CAR_FRICTION = d;
		} else if(temp_str == "REVERSE_TORQUE") {
			REVERSE_TORQUE = d;
		} else if(temp_str == "BOX_SIZE") {
			BOX_SIZE = d;
		} else if(temp_str == "BOX_MASS") {
			BOX_MASS = d;
		} else if(temp_str == "BOX_RESTITUTION") {
			BOX_RESTITUTION = d;
		} else if(temp_str == "BOX_FRICTION") {
			BOX_FRICTION = d;
		} else if(temp_str == "BOX_FRICTION_DRAWBACK") {
			BOX_FRICTION_DRAWBACK = d;
		} else if(temp_str == "BOX_ANG_DAMPING") {
			BOX_ANG_DAMPING = d;
		} else if( temp_str == "TR_ANG_VEL_MIN" ) {
			TR_ANG_VEL_MIN.resize(5);
			TR_ANG_VEL_MIN.at(0) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MIN.at(1) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MIN.at(2) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MIN.at(3) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MIN.at(4) = d;
		} else if( temp_str == "TR_ANG_VEL_MAX" ) {
			TR_ANG_VEL_MAX.resize(5);
			TR_ANG_VEL_MAX.at(0) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MAX.at(1) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MAX.at(2) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MAX.at(3) = d;
			fscanf( fp, "%f", &d);
			TR_ANG_VEL_MAX.at(4) = d;
		} else if( temp_str == "TR_MAX_TORQUE" ) {
			TR_MAX_TORQUE.resize(5);
			TR_MAX_TORQUE.at(0) = d;
			fscanf( fp, "%f", &d);
			TR_MAX_TORQUE.at(1) = d;
			fscanf( fp, "%f", &d);
			TR_MAX_TORQUE.at(2) = d;
			fscanf( fp, "%f", &d);
			TR_MAX_TORQUE.at(3) = d;
			fscanf( fp, "%f", &d);
			TR_MAX_TORQUE.at(4) = d;
		} else if( temp_str == "TR_MIN_TORQUE" ) {
			TR_MIN_TORQUE.resize(5);
			TR_MIN_TORQUE.at(0) = d;
			fscanf( fp, "%f", &d);
			TR_MIN_TORQUE.at(1) = d;
			fscanf( fp, "%f", &d);
			TR_MIN_TORQUE.at(2) = d;
			fscanf( fp, "%f", &d);
			TR_MIN_TORQUE.at(3) = d;
			fscanf( fp, "%f", &d);
			TR_MIN_TORQUE.at(4) = d;
		} else {
			g_game.error << "Loading: unkown parameter:\n>>>>";
		}

		g_game.debug << temp_str << ": " << d << "\n";

		//read /n
		while( fscanf( fp, "%c", &in_buffer[0]) != EOF ) {
			if( in_buffer[0] == '\n' )
				break;
		}

	}

	TR_ANG_VEL_DIFF.resize(5);
	for( unsigned int n=0; n<5; n++) {
		if( n>=TR_ANG_VEL_MAX.size() || n>=TR_ANG_VEL_MIN.size() )
			break;
		TR_ANG_VEL_DIFF.at(n) = TR_ANG_VEL_MAX.at(n) - TR_ANG_VEL_MIN.at(n);
	}

	fclose( fp );

	g_game.debug << "Loaded parameters succesfully.";

	return 0;
}

