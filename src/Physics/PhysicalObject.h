#ifndef _PHYS_OBJECT_H_
#define _PHYS_OBJECT_H_

#include <Box2D/Box2D.h>

enum ePhysicalObject { POBJECT_UNKOWN=0, POBJECT_CAR, POBJECT_TIRE, POBJECT_BOX, POBJECT_WALL, POBJECT_GROUND};

class CPhysicalObject {

    public:
    virtual int getType() = 0;
    virtual void setBody( b2Body* s_body ) = 0;
    virtual b2Body* getBody() = 0;
    virtual void setGround( int g_type ) = 0;
    virtual int getGround() = 0;
    virtual void step( float step_time_sec ) = 0;

    virtual ~CPhysicalObject() {}
};

#endif
