#ifndef GROUND_TYPE_H_#define GROUND_TYPE_H_#include <Box2D/Box2D.h>#include <vector>#include <iostream>
#include <assert.h>#include "PhysicalObject.h"#include "CarPhysics.h"enum eGroundType { GTYPE_ROAD=0, GTYPE_DIRT, GTYPE_MAX, GTYPE_NONE};enum eCollEntityType {    //default type for all entities    COLLENTITY_GENERAL      = 0x0001,    COLLENTITY_TIRE         = 0x0002};enum eCollMaskType {    //doesn't collide with anything    COLLMASK_NOTHING        = 0,    //"tries" to collide with everything, default value    COLLMASK_GENERAL        = 0xFFFF,    COLLMASK_GROUND         = COLLENTITY_TIRE    //collide with tires only (sensor)};void initGroundData();
static eGroundType default_gnd = GTYPE_DIRT;
class CGroundSensor : public CPhysicalObject {    public:    int getType();    void setBody( b2Body* s_body );    b2Body* getBody();    void setGround( int g_type );    int getGround();    void step( float step_time_sec );    int type;    b2Body* body;    CGroundSensor( b2World* world, int gnd_type, std::vector<b2Vec2>& polygon_points );
    CGroundSensor( b2Body* b, int gnd_type);};//returns true if it's a tire and a ground object//if true, obj0 will be the ground, and obj1 the tirebool isTireAndGround( b2Contact* contact, CPhysicalObject*& obj0, CPhysicalObject*& obj1);class CContactListener : public b2ContactListener{    void BeginContact(b2Contact* contact) {

        assert( contact != NULL );
        assert( contact->GetFixtureA() != NULL );
        assert( contact->GetFixtureB() != NULL );
        CPhysicalObject* coll_obj[2];        if ( isTireAndGround( contact, coll_obj[0], coll_obj[1]) ) {            //0 is ground and 1 is the tire            coll_obj[1]->setGround( coll_obj[0]->getGround() );            //std::cout << "New ground: " << coll_obj[0]->getGround() << "\n";        }    }
    //EndContact: if no there is no more ground under the tire,
    //then set ground under it to default ground type of the map    void EndContact(b2Contact* contact) {

        assert( contact != NULL );
        assert( contact->GetFixtureA() != NULL );
        assert( contact->GetFixtureB() != NULL );
        CPhysicalObject* coll_obj[2];        if ( isTireAndGround( contact, coll_obj[0], coll_obj[1]) ) {            //0 is ground and 1 is the tire
            //If no ground under it, set to default
            b2Body* tire_body = coll_obj[1]->getBody();
            b2ContactEdge* edge = tire_body->GetContactList();
            //If no contact at all
            if( edge == NULL )
                coll_obj[1]->setGround( default_gnd );
            else {
                bool has_gnd = false;
                //if any is touching
                while(edge) {
                    if( edge->contact->IsTouching() ) {
                        has_gnd = true;
                        break;
                    }
                    edge = edge->next;
                }
                if(!has_gnd)
                    coll_obj[1]->setGround( default_gnd );
            }
        }    }};#endif