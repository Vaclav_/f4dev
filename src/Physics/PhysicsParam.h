#ifndef PHYSICSPARAM_H_INCLUDED
#define PHYSICSPARAM_H_INCLUDED

int loadParamFromFile();

extern float TIRE_WIDTH;
extern float TIRE_LENGTH;
extern float TIRE_RADIUS;
extern float CAR_WIDTH;
extern float CAR_LENGTH;
extern float FRONT_TIRE_X;
extern float FRONT_TIRE_Y;
extern float REAR_TIRE_X;
extern float REAR_TIRE_Y;

extern float CAR_MASS;
extern float TIRE_MASS;

extern int CAR_DRIVE_TYPE;
extern float CAR_RESTITUTION;
extern float WALL_RESTITUTION;
extern float CAR_FRICTION;
extern float REVERSE_TORQUE;

extern float AIR_RESISTANCE_CONST;

extern float DRIVE_TARGET_ANG_VEL;
extern float DRIVE_TORQUE_SATURATION;
extern float MAX_BRAKE_TORQUE;

extern float MAX_STEERING_ANGLE;
extern float STEERING_ANG_VEL;

//
extern float BOX_SIZE;
extern float BOX_MASS;
extern float BOX_RESTITUTION;
extern float BOX_FRICTION;
extern float BOX_FRICTION_DRAWBACK;
extern float BOX_ANG_DAMPING;

extern std::vector<float> TR_ANG_VEL_MIN;
extern std::vector<float> TR_ANG_VEL_MAX;
extern std::vector<float> TR_MAX_TORQUE;
extern std::vector<float> TR_MIN_TORQUE;
extern std::vector<float> TR_ANG_VEL_DIFF;

extern struct SGroundData {
	float MAX_FWD_FRICTION;
	float FWD_FRICTION_FALLBACK;
	float MAX_LAT_FRICTION;
	float LAT_FRICTION_FALLBACK;
	float TIRE_FWD_FRICTION_MULT;
	float ROAD_TO_TIRE_TORQUE;
	float FRICTION_DRAWBACK_CONST;
} ground_data[GTYPE_MAX];


#endif // PHYSICSPARAM_H_INCLUDED
