#include "Game.h"
#include "GameState/GameState.h"
#include <iostream>
#include <string>


CGame::CGame(void) :
    active_state(STATE_MENU),
    scrnW(1024),
	scrnH(768),
	scrnBpp(32),
	debug_on(true),
	fps_measured(0),
	b2d_to_gfx(25.0f),
	gfx_to_b2d(1.0f/b2d_to_gfx),
    running(true),
    game_name("game")
{
	setFrameRate(50);
	error.type_msg = "Error: ";
	debug.type_msg = "Debug: ";
	game_time.restart();
}

CGame::~CGame(void) {}

int CGame::init() {

    g_game.settings.loadFromFile( "settings.cfg" );

    if( g_game.settings.fullscreen )
        window = new sf::RenderWindow( sf::VideoMode( scrnW, scrnH), game_name.c_str(), sf::Style::Fullscreen );
    else
        window = new sf::RenderWindow( sf::VideoMode( scrnW, scrnH), game_name.c_str(), sf::Style::Titlebar | sf::Style::Close );

	if( window == NULL ) {
		error <<  "Can't open main window, pointer is NULL.";
		return -1;
	}
	if( !window->isOpen() ) {
		error <<  "Can't open main window, window is not open.";
		return -1;
	}

	window->setVerticalSyncEnabled(false);

	return 0;
}

void CGame::end() {
	//cleanup
}

void CGame::setFrameRate( int new_frame_rate ) {
	frame_rate = new_frame_rate;
	frame_time = sf::microseconds( 1000000.0f/frame_rate );
}

sf::Time CGame::getFrameTime() {
	return frame_time;
}

int CGame::getFrameRate() {
	return frame_rate;
}

bool CGame::isRunning() {
	return running;
}

void CGame::setRunning( bool game_running ) {
	running = game_running;
}

sf::Time CGame::getElapsedTime() {
    return game_time.getElapsedTime();
}
