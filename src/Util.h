#ifndef _UTIL_H_
#define _UTIL_H_

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>

static const char* VFS_FILE_PATH = "out.vfs";

class COutput {
    public:
    COutput() : turned_on(true), type_msg(""), is_newline(true), insert_newline(true) {
        std::cout.precision(1);
        std::cout << std::fixed;
    };
    COutput operator<<(const std::string& msg);
    COutput operator<<(const unsigned ui);
    COutput operator<<(const int i);
    COutput operator<<(const float f);
    COutput operator<<(const double d);
    COutput operator<<(const b2Vec2& vec2);

    bool turned_on;
    std::string type_msg;
    private:
    bool is_newline;
    bool insert_newline;
};

class CVFSStream : public sf::InputStream {
    public:

    CVFSStream( FILE* fp_init = NULL ) :
        vfs_fp(fp_init),
        file_offset(0),
        cur_pos(0) {
    }
    ~CVFSStream() {}

    int open( const std::string& filename);

    sf::Int64 read(void* data, sf::Int64 size);

    sf::Int64 seek(sf::Int64 position);

    sf::Int64 tell();

    sf::Int64 getSize();

private :
    FILE* vfs_fp;
    //Current file's offset in the VFS
    sf::Int64 file_offset;
    //Reading/Writing position only in current file
    sf::Int64 cur_pos;
    unsigned int file_size;

    int getString( FILE* fp, std::string& str);
    unsigned int getUInt32( FILE* fp);
};

//use this class, and NOT SFML directly to load textures/fonts!
//prevents to load a texture or a font twice
class CTextureManager {

	public:
    CTextureManager() {
        vfs = fopen( VFS_FILE_PATH, "rb");
    }
    ~CTextureManager() {
        if(vfs != NULL)
            fclose(vfs);
    }
	//if the texture at parameter path hasn't been loaded yet
	//then loads it and returns a pointer
	//if it was loaded already, finds it and returns a pointer
	sf::Texture* getTexture( std::string path );
	sf::Font* getFont( std::string path );
    sf::Texture* addTexture( const sf::Texture& tex, std::string& name );

    //delete textures/fonts -> free memory
    void freeTextures();
    void freeFonts();
    void freeAll();

	private:
    FILE* vfs;
    std::vector<CVFSStream> font_streams;
	std::vector<sf::Texture*> textures;
	std::vector<std::string> texture_paths;
	std::vector<sf::Font*> fonts;
	std::vector<std::string> font_paths;
};

//delay until cur_clock reaches time, precise
void delayUntil( sf::Clock& cur_clock, const sf::Time& time );

#endif

