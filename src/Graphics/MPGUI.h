#ifndef MPGUI_H_INCLUDED
#define MPGUI_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <string>
#include "../Game.h"
#include <list>
#include <string>

class CGameList {

    public:
    std::list<std::string> games;
    // players/100 = IN
    // players%100 = MAX
    std::list<int> players;

    void addGame( std::string game_name, int game_players );
    void removeGame( std::string game_name );

    void buttonDown( int cur_x, int cur_y );

    bool isActive();
    void getSelection( std::string game_name );

    void render();

    private:
    bool active;
    int selection;
    sf::Text games_label;
    sf::Text players_label;
};

//later: implement scrolling the window vertically?
class CChatWindow {
    public:
    void addMessage( std::string user_name, std::string message );
    void clear();

    void render();

    private:
    std::vector<std::string> player_names;
    std::vector<std::string> messages;
    sf::Text names_label;
    sf::Text messages_label;
};


#endif // MPGUI_H_INCLUDED
