#ifndef GRAPHICOBJECT_H_INCLUDED
#define GRAPHICOBJECT_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

enum eGraphicObjectType { GRAPHIC_TEXTURE, GRAPHIC_SHAPE, GRAPHIC_VERTEXARRAY, GRAPHIC_TIRETRACKS };

//interface for drawable objects attached to something
//loading is custom, so it allows to be
//implemented as sf::Sprite or sf::VertexArray either
//may or may not add animation
class CGraphicObject {

    public:
    CGraphicObject() {}
    virtual ~CGraphicObject() {}
    virtual int getType() = 0;
    //absolute position if not attached, relative if attached to something
    //in graphics coordinates
    virtual void setPos( const sf::Vector2f& new_pos ) = 0;
    virtual void setAngle( float deg ) = 0;
    virtual sf::Vector2f getPos() = 0;
    virtual float getAngle() = 0;

    //can be attached to only one object at a time
    //attach to physics object
    //if already attached, detach from CGraphicObject
    virtual void attach( b2Body* body ) = 0;
    //attach to graphics object
    //if already attached, detach from b2Body*
    virtual void attach( CGraphicObject* object ) = 0;
    //detach, convert relative position to world position (absolute)
    virtual void detach() = 0;

    //any calculations you want every frame
    virtual void step( float t_sec ) = 0;
    //draw
    virtual void render( sf::RenderWindow* r_win, sf::FloatRect& view_rect ) = 0;
};

//A drawable shape, either circle, convex polygon or a rectangle shape
//It has no texture, only outline and colors
class CGraphicShape : public CGraphicObject {

    public:
    CGraphicShape( sf::Shape* shape, float b2d_to_gfx ) :
        shape_p(shape),
        att_body(NULL),
        att_object(NULL),
        phys_to_tex(b2d_to_gfx) {
    }
    ~CGraphicShape() {
        if( shape_p != NULL )
            delete shape_p;
    }
    int getType() { return GRAPHIC_SHAPE; };
    //absolute position if not attached, relative if attached to something
    void setPos( const sf::Vector2f& new_pos );
    void setAngle( float deg );
    sf::Vector2f getPos();
    float getAngle();

    //can be attached to only one object at a time
    //attach to physics object
    //if already attached, detach from CGraphicObject
    void attach( b2Body* body );
    //attach to graphics object
    //if already attached, detach from b2Body*
    void attach( CGraphicObject* object );
    //detach, convert relative position to world position (absolute)
    void detach();

    //any calculations you want every frame
    void step( float t_sec );
    //draw
    void render( sf::RenderWindow* r_win, sf::FloatRect& view_rect );

    sf::Shape* shape_p;

    private:
    //update position to latest
    void update();
    //body or object it is attached to
    //if not attached, both are NULL
    b2Body* att_body;
    CGraphicObject* att_object;

    //this is the center of the shape
    sf::Transformable origin;
    //ratio between physics and texture sizes
    float phys_to_tex;
};

//a vertex array, can be rendered as any primitive type
//yet it's static, cannot be moved
class CVertexArray : public CGraphicObject {

    public:
    CVertexArray( sf::PrimitiveType t, float b2d_to_gfx ) :
        varray_type(t),
        phys_to_tex(b2d_to_gfx),
        last_size(0) {
    }
    ~CVertexArray() {}
    int getType() { return GRAPHIC_VERTEXARRAY; };
    //yet it's static, these won't work
    //but we must implement them
    void setPos( const sf::Vector2f& new_pos );
    void setAngle( float deg );
    sf::Vector2f getPos();
    float getAngle();

    //yet it's static, these won't work
    void attach( b2Body* body );
    void attach( CGraphicObject* object );
    void detach();

    //any calculations you want every frame
    void step( float t_sec );
    //draw
    void render( sf::RenderWindow* r_win, sf::FloatRect& view_rect );

    std::vector<sf::Vertex> vertices;

    private:
    //update position to latest
    void update();

    sf::PrimitiveType varray_type;
    //body or object it is attached to
    //if not attached, both are NULL

    //ratio between physics and texture sizes
    float phys_to_tex;

    unsigned int last_size;
    sf::FloatRect bounding_rect;
};

#endif // GRAPHICOBJECT_H_INCLUDED
