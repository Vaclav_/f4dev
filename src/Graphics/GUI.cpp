#include "GUI.h"
#include "../Game.h"

CButton::CButton() :
    //initialise variables
    state(BUTTON_UP),
    button_event(false),
	frame_cnt(0),
	x(0),
	y(0),
	sizeX(100),
	sizeY(100),
	textX(0),
	textY(0)
	{
    button_label.setString("_");
}

CButton::~CButton() {}

void CButton::setTexture( sf::Texture* tex ) {
	if( tex != NULL )
		sprite.setTexture( *tex );
}

void CButton::setFont( sf::Font* font ) {
    if( font != NULL )
        button_label.setFont( *font );
}

void CButton::setTextureParam( int pX, int pY, int sX, int sY, sf::Color col ) {
	x = pX;
	y = pY;
	sizeX = sX;
	sizeY = sY;
	sprite.setOrigin( sf::Vector2f( sX/2, sY/2) );
	sprite.setPosition( sf::Vector2f( pX, pY) );
	sprite.setColor( col );
	sf::FloatRect label_size;
	label_size = button_label.getLocalBounds();
    button_label.setOrigin( label_size.width/2, label_size.height/2 );
    button_label.setPosition( x+textX, y+textY );
}

void CButton::setLabelParam( std::string text, unsigned char textSize, sf::Color col, int offsetX, int offsetY ) {
    textX = offsetX;
    textY = offsetY;
    button_label.setString(text);
    button_label.setCharacterSize(textSize);
    button_label.setColor( col );
    sf::FloatRect label_size;
    label_size = button_label.getLocalBounds();
    button_label.setOrigin( label_size.width/2, label_size.height/2 );
    button_label.setPosition( x+textX, y+textY );
}

void CButton::render( sf::RenderWindow* r_window ) {

	if( state == BUTTON_UP )
		sprite.setTextureRect( sf::IntRect( 0, 0, sizeX, sizeY) );
	else if( state == BUTTON_DOWN )
		sprite.setTextureRect( sf::IntRect( 0, sizeY, sizeX, sizeY) );
	else if( state == BUTTON_HOVER )
        sprite.setTextureRect( sf::IntRect( 0, sizeY*2, sizeX, sizeY) );
    else
        g_game.error << "CButton::render: No such state: " << state << " .";

	r_window->draw(sprite);
	r_window->draw(button_label);
}

void CButton::mouseDown( int cX, int cY) {

    //if cursor is in the rectangle
    if( x-sizeX/2 < cX && cX < x+sizeX/2 && y-sizeY/2 < cY && cY < y+sizeY/2 )
		state = BUTTON_DOWN;

}

void CButton::mouseUp( int cX, int cY) {

    if( x-sizeX/2 < cX && cX < x+sizeX/2 && y-sizeY/2 < cY && cY < y+sizeY/2 ) {
        if( state == BUTTON_DOWN )
            button_event = true;
    }

	state = BUTTON_UP;

}

void CButton::pressButton( unsigned int press_time ) {
	frame_cnt = press_time;
	state = BUTTON_DOWN;
}

bool CButton::check( int cX, int cY) {

    if( state == BUTTON_UP ) {
        if( x-sizeX/2 < cX && cX < x+sizeX/2 && y-sizeY/2 < cY && cY < y+sizeY/2 )
            state = BUTTON_HOVER;
    } else if( state == BUTTON_HOVER )
        if( !(x-sizeX/2 < cX && cX < x+sizeX/2 && y-sizeY/2 < cY && cY < y+sizeY/2) )
            state = BUTTON_UP;

	if( frame_cnt > 0 ) {
		frame_cnt--;
		if ( frame_cnt==0 ) {
			state = BUTTON_UP;
			button_event = true;
		}
	}

	if( button_event ) {
		button_event = false;
		return true;
	} else return false;

    return false;
}


float char_size = 8.0f;

CInputBox::CInputBox() :
    active(false),
    content_changed(false),
    pos_x(0),
    pos_y(0),
    size_x(0),
    size_y(0),
    cur_pos(0),
    max_char(15),
    cur_blink_state(true),
    blink_period(1500), //in ms
    blink_cur_time(0)
    {
}

void CInputBox::setFont( sf::Font* font ) {
    if( font != NULL )
        render_text.setFont( *font );
}

//pX: x position; pY: y position (top left corner)
//sX: x size, sY: ySize (of the panel, NOT the texture)
void CInputBox::setTextureParam( int pX, int pY, int sX, int sY, sf::Color col ) {
    pos_x = pX;
    pos_y = pY;
    size_x = sX;
    size_y = sY;

    panel.setPosition( sf::Vector2f( pos_x, pos_y) );
    panel.setFillColor(col);
    render_text.setPosition( sf::Vector2f( pos_x+5, pos_y+size_y/2-10) );
    panel.setSize( sf::Vector2f( (float)size_x, (float)size_y));
    //TODO: fix cursor Y pos properly
    cursor.setSize( sf::Vector2f( 1.0f, size_y*0.8f) );
    cursor.setOrigin( sf::Vector2f( 0.0f, size_y*0.8f) );
    cursor.setPosition( sf::Vector2f( pos_x+6, pos_y+size_y/2+10) );
    cursor.setFillColor( sf::Color::Black );
}

//text properties
void CInputBox::setLabelParam( std::string default_text, unsigned char text_size, unsigned int set_max_char, sf::Color col ) {
    max_char = set_max_char;
    content = default_text;
    cur_pos = default_text.length();
    render_text.setCharacterSize(text_size);
    render_text.setColor(col);
    render_text.setString(content);
}

void CInputBox::textEntered( char ch ) {
    if( active ) {
        if(' ' <= ch && ch <= '~') {
            if( content.length() < max_char ) {
                content.insert( cur_pos, 1, ch);
                content_changed = true;
                cur_pos++;
                blink_cur_time = 0;
            }
        } else if( ch == 0x08 ) {   //backspace
            if( content.length()>0 && cur_pos > 0 ) {
                //remove one character
                content.erase( cur_pos-1, 1);
                content_changed = true;
                cur_pos--;
                blink_cur_time = 0;
            }
        } else if( ch == 0x1b ) {   //escape
            active = false;
        }
    }
}

void CInputBox::eventDel() {
    if( content.length()>0 && cur_pos<content.length() ) {
        //remove one character
        content.erase( cur_pos, 1);
        content_changed = true;
        blink_cur_time = 0;
    }
}

//mouse left click is held event, at screen coordinates x: cY, y: cY
void CInputBox::mouseDown( int cX, int cY) {
    //if cursor is in the rectangle
    if( pos_x < cX && cX < pos_x+size_x && pos_y < cY && cY < pos_y+size_y )
        setActive();
    else
        active = false;
}

	//activate by software
void CInputBox::setActive() {
	active = true;
	cur_pos = content.length(); //set cursor to the end of text
	blink_cur_time = 0;
}

void CInputBox::eventEnter() {
    if(active)
        active = false;
}

void CInputBox::moveCursorLeft() {
    if(cur_pos>0)
        cur_pos--;
    blink_cur_time = 0;
}

void CInputBox::moveCursorRight() {
    if(cur_pos<content.length())
        cur_pos++;
    blink_cur_time = 0;
}

bool CInputBox::isActive() {
    return active;
}

void CInputBox::CInputBox::erase() {
    content.erase( content.begin(), content.end() );
    content_changed = true;
    cur_pos = 0;
}

void CInputBox::getContent( std::string& save_content ) {
    save_content = content;
}

//draw the button
void CInputBox::render( sf::RenderWindow* r_window ) {
    if( isActive() ) {
        blink_cur_time += g_game.frame_time.asMilliseconds();
        if( blink_cur_time < blink_period/2 )
            cur_blink_state = true;
        else if( blink_cur_time < blink_period )
            cur_blink_state = false;
        else {
            cur_blink_state = true;
            blink_cur_time = 0;
        }
    }
    r_window->draw(panel);
    if(content_changed)
        render_text.setString(content);
    r_window->draw(render_text);
    if( isActive() && cur_blink_state ) {
        cursor.setPosition( sf::Vector2f( pos_x+6+char_size*cur_pos, pos_y+size_y/2+10) );

        r_window->draw(cursor);
    }
    content_changed = false;
}

//=========================================================
CCheckBox::CCheckBox() :
	state(false),
	tex_size_x(100), tex_half_size_y(100),
	x(0), y(0), size_x(100), size_y(100)
    {
}

void CCheckBox::setTexture( sf::Texture* tex ) {
    if(tex!=NULL)
        sprite.setTexture(*tex);
}

void CCheckBox::setTextureParam( int pX, int pY, int sX, int sY, sf::Color col ) {
    sprite.setPosition( pX, pY);
    sprite.setColor( col );
    tex_size_x = sX;
    tex_half_size_y = sY;
}

void CCheckBox::setArea( int pX, int pY, int sX, int sY) {
    x = pX;
    y = pY;
    size_x = sX;
    size_y = sY;
}

void CCheckBox::mouseDown( int cX, int cY) {
    //if cursor is in the rectangle
    if( x < cX && cX < x+size_x && y < cY && cY < y+size_y )
		state = !state;
}

void CCheckBox::render( sf::RenderWindow* r_window ) {

	if( !state ) //unchecked
		sprite.setTextureRect( sf::IntRect( 0, 0, tex_size_x, tex_half_size_y) );
	else
		sprite.setTextureRect( sf::IntRect( 0, tex_half_size_y, tex_size_x, tex_half_size_y) );

	r_window->draw(sprite);
}

bool CCheckBox::getState() {
    return state;
}

void CCheckBox::setState( bool s) {
    state = s;
}
