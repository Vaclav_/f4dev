#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

class CCamera {
    public:
    CCamera();
    void setPosition( const b2Vec2& pos );
    void adjust( const b2Vec2& car_pos, const b2Vec2& car_vel, float b2d_to_gfx );
    sf::View view;

    private:
    //previous vel_offsets
	std::vector<b2Vec2> smooth_buff;
	sf::Uint32 total_n;
	sf::Uint32 cur_n;
	float vel_scale;
	//the car's max distance from the centre
	sf::Vector2i max_distance;
};


#endif // CAMERA_H_INCLUDED
