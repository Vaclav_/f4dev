
#include "TireTracks.h"
#include "../Physics/PhysicsParam.h"

static sf::Color track_colors[GTYPE_MAX] = {
    sf::Color::Black,
    sf::Color( 45, 25, 12)
};
static sf::Texture* track_textures[GTYPE_MAX] = { NULL, NULL };

static sf::Vector2f texture_coords[GTYPE_MAX][4] = {
    {
        sf::Vector2f( 0.0f, 0.0f),
        sf::Vector2f( 0.0f, 10.0f),
        sf::Vector2f( 10.0f, 0.0f),
        sf::Vector2f( 10.0f, 10.0f)
    },
    {
        sf::Vector2f( 0.0f, 0.0f),
        sf::Vector2f( 0.0f, 10.0f),
        sf::Vector2f( 10.0f, 0.0f),
        sf::Vector2f( 10.0f, 10.0f)
    }
};

static const int FADE_ALPHA_RATE = 7;
static const sf::Time FADE_INTERVAL = sf::milliseconds(150);

static const sf::Time FADE_TIME = sf::seconds(2);

b2Vec2 rad2vec(float r, float m) {
   return m*b2Vec2(cos(r),sin(r));
}

//========================CTireTrackPart======================

//position, angle in radians
//Add only one position
void CTireTrackPart::addPos( const b2Vec2& pos, float ang ) {
    //Calculate the four corners of the tire
    b2Vec2 p1 = pos + rad2vec( ang-b2_pi/4.0f*3.0f, d_2);
    b2Vec2 p2 = pos + rad2vec( ang+b2_pi/4.0f*3.0f, d_2);
    b2Vec2 p3 = pos + rad2vec( ang-b2_pi/4.0f, d_2);
    b2Vec2 p4 = pos + rad2vec( ang+b2_pi/4.0f, d_2);
    b2Vec2* p[4];
    p[0] = &p1;
    p[1] = &p2;
    p[2] = &p3;
    p[3] = &p4;
    int i;
    if( texture != NULL ) {
        for( i=0; i<4; i++) {
            vertices.append(
                sf::Vertex( sf::Vector2f( p[i]->x, p[i]->y),
                sf::Color::White,
                texture_coords[gnd_type][i]
                )
            );
        }
    } else {
        for( i=0; i<4; i++) {
            vertices.append(
                sf::Vertex( sf::Vector2f( p[i]->x, p[i]->y),
                track_colors[gnd_type] )
            );
        }
    }
    prev_center = pos;
}

void CTireTrackPart::add( b2Vec2 pos, float ang_rad ) {

    //Convert to graphics coordinates
    pos.x *= g_game.b2d_to_gfx;
    pos.y *= g_game.b2d_to_gfx;

    assert( gnd_type < GTYPE_MAX );
    //If no points stored yet, just add all
    if( vertices.getVertexCount() == 0 ) {
        addPos( pos, ang_rad);
    } else {
        float MIN_DIST = d_2/2;
        float MAX_TRACK_DIST = d_2;

        float dist = b2Distance( prev_center, pos);

        if( dist < MIN_DIST ) {     //Don't add if too close
        }   //do nothing
        else if( dist > MAX_TRACK_DIST ) {  //too far: add interpolation
            float INT_STEP = MAX_TRACK_DIST/2;     //interpolation step
            b2Vec2 int_dir = pos - prev_center; //interpolation direction
            int_dir.Normalize();
            int_dir = INT_STEP*int_dir;
            b2Vec2 new_pos = prev_center + int_dir;
            addPos( new_pos, ang_rad);
            while( b2Distance( pos, new_pos) > MAX_TRACK_DIST ) {
                new_pos += int_dir;
                addPos( new_pos, ang_rad);
            }
        } else {   //distance is OK
            addPos( pos, ang_rad);
        }
    }
}

void CTireTrackPart::render( sf::RenderWindow* win, sf::FloatRect& view_rect) {
    //When the class is created, we add some vertces immediately
    //So this should never happen
    assert( vertices.getVertexCount() != 0 );
    if( texture != NULL )
        win->draw( vertices, texture );
    else
        win->draw( vertices );
}


//========================CTireTracks======================



//Check all tires and save tire tracks if the tire is skidding
//!TODO: OR the tire is on certain type of surface, eg. dirt, ice..
void CTireTracks::check_tires( CTirePhysics** tires_p ) {
    if( tires_p == NULL ) return;
    int i;
    //For all tires
    for( i=0; i<4; i++) {
        if( tires_p[i] == NULL ) return;

        //If the tire is skidding
        if( tires_p[i]->is_skidding_lat || tires_p[i]->is_skidding_fwd || tires_p[i]->is_skidding ) {
            //If the track list is yet empty
            if( tracks_p[i]->size() == 0 )
                tracks_p[i]->push_back( CTireTrackPart( TIRE_WIDTH/2, TIRE_LENGTH/2, tires_p[i]->getGround(), track_textures[tires_p[i]->getGround()]) );
            //If we have to start a new CTireTrackPart, because the old one is finished (closed)
            else if( tracks_p[i]->back().finished )
                tracks_p[i]->push_back( CTireTrackPart( TIRE_WIDTH/2, TIRE_LENGTH/2, tires_p[i]->getGround(), track_textures[tires_p[i]->getGround()]) );
            //The surface has changed, so we must start a new part
            //Also add current points to last Part, not to get a gap between the parts
            //And finish the last part
            else if( tracks_p[i]->back().gnd_type != tires_p[i]->getGround() ) {
                tracks_p[i]->back().add( tires_p[i]->body->GetPosition(), tires_p[i]->body->GetAngle());
                tracks_p[i]->back().finished = true;
                tracks_p[i]->back().fade_time = g_game.getElapsedTime() + FADE_TIME;
                tracks_p[i]->push_back( CTireTrackPart( TIRE_WIDTH/2, TIRE_LENGTH/2, tires_p[i]->getGround(), track_textures[tires_p[i]->getGround()]) );
            }

            //Add the tire tracks to the Tire Track Part
            tracks_p[i]->back().add( tires_p[i]->body->GetPosition(), tires_p[i]->body->GetAngle());
        } else if( tracks_p[i]->size() != 0 && !tracks_p[i]->back().finished ) {
            //If finished skidding, close this part
            tracks_p[i]->back().finished = true;
            tracks_p[i]->back().fade_time = g_game.getElapsedTime() + FADE_TIME;
        }
    }

    //Fading and removing...
    //For all tires
    for( i=0; i<4; i++) {
        //If empty, don't need to do anything with it
        if( tracks_p[i]->size() == 0 ) continue;

        //Start from the latest Tire Track Part
        //We can stop at the first part that doesn't need to be faded
        //Because the following parts are always newer
        bool to_remove;
        CTireTrackPart* cur_part;
        for( int cur_n=0; (unsigned int)cur_n<tracks_p[i]->size(); cur_n++ ) {

            cur_part = &tracks_p[i]->at(cur_n);
            //If finished adding points to this part already
            if( cur_part->finished ) {
                to_remove = false; //initial value
                //If it's time for a fade step
                if( cur_part->fade_time <= g_game.getElapsedTime() ) {
                    float alpha;
                    //Change the alpha of all vertices belonging to the Tire Track Part
                    for( unsigned int v=0; v<cur_part->vertices.getVertexCount(); v++ ) {
                        alpha = cur_part->vertices[v].color.a;
                        if( alpha > FADE_ALPHA_RATE ) {
                            alpha -= FADE_ALPHA_RATE;
                            cur_part->vertices[v].color.a = alpha;
                        } else {
                            //It would be alpha = 0, but it's no use rendering invisible stuff
                            //So remove it
                            to_remove = true;
                            break;
                        }
                    }   //for all vertices
                    if(to_remove) {
                        tracks_p[i]->pop_front();
                        //we want to set it 0, but it will be incremented at the end of this loop
                        cur_n = -1;
                    }
                    else
                        cur_part->fade_time += FADE_INTERVAL;
                } else  //No fading, no use to loop all elements, as they are newer
                    break;
            } else {    //if not finished
                break;
            }
        }   //for all parts
    }   //for all tires

}

void CTireTracks::step( float t_sec ) {

}

void CTireTracks::render( sf::RenderWindow* r_win, sf::FloatRect& view_rect ) {
    for( int i=0; i<4; i++) {
        for( unsigned int j=0; j<tracks_p[i]->size(); j++)
            tracks_p[i]->at(j).render( r_win, view_rect);
    }
}
