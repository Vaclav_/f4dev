
#include "GraphicObject.h"
#include "Texture.h"
#include "../Game.h"

#define RADTODEG 180.0f/3.14159265359f

CTexture::CTexture( float phys_to_tex_param ) : att_body(NULL), att_object(NULL) {
    phys_to_tex = phys_to_tex_param;
}

int CTexture::getType() { return GRAPHIC_TEXTURE; }

void CTexture::setPos( const sf::Vector2f& new_pos ) {
    //if not attached
    if( att_object == NULL && att_body == NULL )
        origin.setPosition( new_pos.x, new_pos.y);
    else {
        this->setPosition( new_pos.x, new_pos.y);
    }
}

void CTexture::setAngle( float deg ) {
    if( att_object == NULL && att_body == NULL ) {
        origin.setRotation(deg);
    } else {
        this->setRotation(deg);
    }
}

sf::Vector2f CTexture::getPos() {
    return (this->getPosition() + origin.getPosition());
}

float CTexture::getAngle() {
    return (this->getRotation() + origin.getRotation());
}
/*
sf::Transform CTexture::getWorldTransform() {
    //UNTESTED CODE
    sf::Transform orig_trans = origin.getTransform();
    orig_trans.combine(this->getTransform());
    return orig_trans;
}

sf::Transform CTexture::getLocalTransform() {
    return this->getTransform();
}
*/

void CTexture::attach( b2Body* body ) {
    if(body!=NULL) {
        att_body = body;
        att_object = NULL;
        //set offset to 0
        this->setPosition( 0.0f, 0.0f);
        this->setRotation(0.0f);
        this->setScale( 1.0f, 1.0f);
    }
}

void CTexture::attach( CGraphicObject* object ) {
    if(object!=NULL) {
        att_object = object;
        att_body = NULL;
        //set offset to 0
        this->setPosition( 0.0f, 0.0f);
        this->setRotation(0.0f);
        this->setScale( 1.0f, 1.0f);
    }
}

void CTexture::detach() {
    update();   //update position
    if(att_body!=NULL) {
        sf::Vector2f sc = this->getScale();
        origin.rotate(this->getRotation());
        origin.move(this->getPosition());
        origin.scale( sc.x, sc.y);
        this->setPosition( 0.0f, 0.0f);
        this->setRotation(0.0f);
        this->setScale( 1.0f, 1.0f);
    } else if(att_object!=NULL) {
        sf::Vector2f sc = this->getScale();
        origin.rotate(this->getRotation());
        origin.move(this->getPosition());
        origin.scale( sc.x, sc.y);
        this->setPosition( 0.0f, 0.0f);
        this->setRotation(0.0f);
        this->setScale( 1.0f, 1.0f);
    }
    att_body = NULL;
    att_object = NULL;
}

void CTexture::update() {
    if( att_body != NULL ) {
        b2Vec2 pos;
        float rot;
        pos = phys_to_tex*att_body->GetPosition();
        rot = RADTODEG*att_body->GetAngle();
        origin.setPosition( pos.x, pos.y);
        origin.setRotation(rot);
    } else if( att_object != NULL ) {
        sf::Vector2f pos;
        float ang;
        pos = att_object->getPos();
        ang = att_object->getAngle();
        origin.setPosition( pos );
        origin.setRotation( ang );
    }
}

void CTexture::step( float t_sec ) {
}

void CTexture::render( sf::RenderWindow* r_win, sf::FloatRect& view_rect ) {
    if( att_body != NULL ) {
        b2Vec2 pos;
        float rot;
        pos = phys_to_tex*att_body->GetPosition();
        rot = RADTODEG*att_body->GetAngle();
        origin.setPosition( pos.x, pos.y);
        origin.setRotation(rot);
    } else if( att_object != NULL ) {
        sf::Vector2f pos;
        float ang;
        pos = att_object->getPos();
        ang = att_object->getAngle();
        origin.setPosition( pos );
        origin.setRotation( ang );
    }

    sf::FloatRect bounds = getGlobalBounds();
    bounds = origin.getTransform().transformRect( bounds );
    //don't render if it's offscreen
    if( !bounds.intersects(view_rect) )
        return;

    sf::RenderStates r_states;
    r_states.transform = origin.getTransform();
    r_win->draw( *this, r_states);

//Debug bounding rectangles
#ifndef NDEBUG
    if( g_game.settings.reduced_gfx ) {
        float w_zoom = r_win->getView().getSize().x/(float)g_game.scrnW;
        sf::RectangleShape sh;
        sh.setSize( sf::Vector2f( bounds.width, bounds.height));
        sh.setPosition( bounds.left, bounds.top);
        sh.setOutlineThickness(1.0f*w_zoom);
        sh.setOutlineColor(sf::Color::Magenta);
        sh.setFillColor(sf::Color::Transparent);
        r_win->draw( sh );
    }
#endif
}
