
#ifndef _GUI_H
#define _GUI_H

#include <SFML/Graphics.hpp>
#include <string>
#include "../Game.h"
#include <list>
#include <string>

//if the button is pressed (down), or not (up)
//Hover: the mouse is over the button
enum eButtonState { BUTTON_UP = 0, BUTTON_DOWN, BUTTON_HOVER };

//a GUI button
class CButton {
public:
	CButton();
	~CButton();
	//this texture contains both the normal and the pressed texture
	//the normal should be in the top and the pressed texture in the bottom of the texture
	void setTexture( sf::Texture* tex );
	//font for the text on the button
	void setFont( sf::Font* font );
	//pX: x position; pY: y position (both are CENTER);
	//sX: x size, sY: ySize (of the button, NOT the texture)
	//col: coloring of the texture
	void setTextureParam( int pX, int pY, int sX, int sY, sf::Color col = sf::Color( 0xff, 0xff, 0xff) );
	//text properties on the button
	void setLabelParam( std::string text, unsigned char textSize, sf::Color col = sf::Color( 0xff, 0xff, 0xff), int offsetX = 0, int offsetY = 0 );
    //mouse left click is held event, at screen coordinates x: cY, y: cY
	void mouseDown( int cX, int cY);
	//mouse left click is released event, at screen coordinates x: cY, y: cY
	void mouseUp( int cX, int cY);
	//press the button "artifically", by software (eg. button hotkey is pressed)
	//press time: for how many frames is the button pressed
	void pressButton( unsigned int press_time = 10*50/g_game.getFrameRate() );

    //check if the button was pressed
    //also decrements press_time from pressButton(..)
    //so it must be called every frame!
    //true if the button was pressed and released (=clicked)
	bool check( int cX, int cY);
	//draw the button
	void render( sf::RenderWindow* r_window );

private:
	int state;
	bool button_event;
	sf::Text button_label;
	sf::Sprite sprite;
	unsigned int frame_cnt;
	int x, y;
	int sizeX, sizeY;
	int textX, textY;
};

//TODO: add parameters: text x, y pos
//TODO: scrolling long text vertically?
class CInputBox {
public:
	CInputBox();
	~CInputBox() {}

	//void setTexture( sf::Texture* tex );

	void setFont( sf::Font* font );
	//pX: x position; pY: y position (top left corner)
	//sX: x size, sY: ySize (of the panel, NOT the texture)
	void setTextureParam( int pX, int pY, int sX, int sY, sf::Color col = sf::Color( 0xff, 0xff, 0xff) );
	//text properties
	void setLabelParam( std::string default_text, unsigned char text_size, unsigned int set_max_char, sf::Color col = sf::Color( 0xff, 0xff, 0xff) );
	void textEntered( char ch );
    //mouse left click is held event, at screen coordinates x: cY, y: cY
	void mouseDown( int cX, int cY);
	//activate by software
	void setActive();

    void eventEnter();
    void eventDel(); //delete character
    void moveCursorLeft();
    void moveCursorRight();

    bool isActive();

    void erase();
    void getContent( std::string& save_content );

	//draw the button
	void render( sf::RenderWindow* r_window );

private:
	bool active;
	std::string content;
	bool content_changed;
	sf::Text render_text;
	//sf::Sprite panel;
    sf::RectangleShape panel;
    sf::RectangleShape cursor;
	int pos_x, pos_y;
	int size_x, size_y;
	//cursor positin, in characters
	unsigned int cur_pos;
	unsigned int max_char;
	bool cur_blink_state;
	unsigned int blink_period;  //in ms
	unsigned int blink_cur_time;
};

class CCheckBox {
    public:
    CCheckBox();
    void setTexture( sf::Texture* tex );
    void setTextureParam( int pX, int pY, int sX, int sY, sf::Color col = sf::Color( 0xff, 0xff, 0xff) );
    void setArea( int pX, int pY, int sX, int sY); //the clickable area
    void mouseDown( int cX, int cY);
	void render( sf::RenderWindow* r_window );
    bool getState();
    void setState( bool s);

    private:
	bool state;
    sf::Sprite sprite;
    int tex_size_x;
    int tex_half_size_y;
	int x, y;
	int size_x, size_y;
};

#endif
