#ifndef TIRETRACKS_H_INCLUDED
#define TIRETRACKS_H_INCLUDED

#include <deque>
#include <assert.h>
#include "GraphicObject.h"
#include "../Physics/GroundType.h"
#include "../Physics/CarPhysics.h"
#include "../Game.h"

b2Vec2 rad2vec(float r, float m = 1);

class CTireTrackPart {
    public:
    CTireTrackPart( float t_w, float t_l, int gnd_t, sf::Texture* tx = NULL ) :
        gnd_type(gnd_t),
        finished(false),
        t_width(t_w*g_game.b2d_to_gfx),
        t_len(t_l*g_game.b2d_to_gfx),
        texture(tx),
        vertices( sf::TrianglesStrip ),
        prev_center( 0.0f, 0.0f),
        fade_time( sf::seconds(0) )
        {
            b2Vec2 p( t_width, t_len);
            d_2 = p.Length();
    }

    int gnd_type;
    bool finished;          //remove time is set now, or it's still increasing
    float t_width;
    float t_len;
    float d_2;
    sf::Texture* texture;
    sf::VertexArray vertices;
    b2Vec2 prev_center;     //center of the last saved track/position
    //when to start fading
    //and when the next fade step is about to be done
    sf::Time fade_time;

    void addPos( const b2Vec2& pos, float ang );
    void add( b2Vec2 pos, float ang_rad );

    void render( sf::RenderWindow* win, sf::FloatRect& view_rect);

};


class CTireTracks : public CGraphicObject {

    public:
    CTireTracks () {
        tracks_p[0] = &tracks_fl;
        tracks_p[1] = &tracks_fr;
        tracks_p[2] = &tracks_rl;
        tracks_p[3] = &tracks_rr;
    }
    ~CTireTracks () {}
    int getType() { return GRAPHIC_TIRETRACKS; }

    //These functions are intentionally not implemented
    void setPos( const sf::Vector2f& new_pos ) {}
    void setAngle( float deg ) {}
    sf::Vector2f getPos() { return sf::Vector2f( 0.0f, 0.0f); }
    float getAngle() { return 0.0f; }
    void attach( b2Body* body ) {}
    void attach( CGraphicObject* object ) {}
    void detach() {}

    void check_tires( CTirePhysics** tires_p );

    void step( float t_sec );
    void render( sf::RenderWindow* r_win, sf::FloatRect& view_rect );

    private:
    std::deque<CTireTrackPart> tracks_fl;
    std::deque<CTireTrackPart> tracks_fr;
    std::deque<CTireTrackPart> tracks_rl;
    std::deque<CTireTrackPart> tracks_rr;
    std::deque<CTireTrackPart>* tracks_p[4];
};


#endif // TIRETRACKS_H_INCLUDED
