
#include "../Game.h"
#include "Camera.h"

CCamera::CCamera() :
    total_n(25),
    cur_n(0),
    vel_scale( 0.1f),
    max_distance( 250.0f, 0.0f)
    {
        view.setSize( g_game.scrnW, g_game.scrnH);
        view.zoom( 1.0f);
        //view.setCenter(g_game.scrnW/2, g_game.scrnH/2);
        smooth_buff.resize(total_n);
}

void CCamera::setPosition( const b2Vec2& pos ) {
    unsigned int i;
    for( i=0; i<total_n; i++)
        smooth_buff.at(i) = pos;

    view.setCenter( pos.x, pos.y);
}

void CCamera::adjust( const b2Vec2& car_pos, const b2Vec2& car_vel, float b2d_to_gfx ) {

    float w_zoom = g_game.window->getView().getSize().x/(float)g_game.scrnW;
    b2Vec2 vel_offset = vel_scale*b2d_to_gfx*w_zoom*car_vel;

    float max_world_dist = max_distance.x*w_zoom;

    if( vel_offset.Length() > max_world_dist ) {
        vel_offset.Normalize();
        vel_offset *= max_world_dist;
    }

	smooth_buff.at(cur_n) = vel_offset;
	cur_n++;
	if( cur_n >= total_n )
		cur_n = 0;

	b2Vec2 smoothed_pos( 0.0f, 0.0f);
	sf::Uint32 k;
	for( k=0; k<total_n; k++) {
		smoothed_pos += smooth_buff.at(k);
	}

	smoothed_pos.x /= total_n;
	smoothed_pos.y /= total_n;
	smoothed_pos += b2d_to_gfx*car_pos;

    view.setCenter( smoothed_pos.x, smoothed_pos.y);
}

