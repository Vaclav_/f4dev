
#include "GraphicObject.h"
#include "../Game.h"

#define RADTODEG 180.0f/3.14159265359f

//====================CGraphicShape========================
void CGraphicShape::setPos( const sf::Vector2f& new_pos ) {
    //if not attached
    if( att_object == NULL && att_body == NULL )
        origin.setPosition( new_pos.x, new_pos.y);
    else {
        shape_p->setPosition( new_pos.x, new_pos.y);
    }
}

void CGraphicShape::setAngle( float deg ) {
    if( att_object == NULL && att_body == NULL ) {
        origin.setRotation(deg);
    } else {
        shape_p->setRotation(deg);
    }
}


sf::Vector2f CGraphicShape::getPos() {
    return (shape_p->getPosition() + origin.getPosition());
}

float CGraphicShape::getAngle() {
    return (shape_p->getRotation() + origin.getRotation());
}


void CGraphicShape::attach( b2Body* body ) {
    if(body!=NULL) {
        att_body = body;
        att_object = NULL;
        //set offset to 0
        shape_p->setPosition( 0.0f, 0.0f);
        shape_p->setRotation(0.0f);
        shape_p->setScale( 1.0f, 1.0f);
    }
}

void CGraphicShape::attach( CGraphicObject* object ) {
    if(object!=NULL) {
        att_object = object;
        att_body = NULL;
        //set offset to 0
        shape_p->setPosition( 0.0f, 0.0f);
        shape_p->setRotation(0.0f);
        shape_p->setScale( 1.0f, 1.0f);
    }
}


void CGraphicShape::detach() {
    update();   //update position
    if(att_body!=NULL) {
        sf::Vector2f sc = shape_p->getScale();
        origin.rotate(shape_p->getRotation());
        origin.move(shape_p->getPosition());
        origin.scale( sc.x, sc.y);
        shape_p->setPosition( 0.0f, 0.0f);
        shape_p->setRotation(0.0f);
        shape_p->setScale( 1.0f, 1.0f);
    } else if(att_object!=NULL) {
        sf::Vector2f sc = shape_p->getScale();
        origin.rotate(shape_p->getRotation());
        origin.move(shape_p->getPosition());
        origin.scale( sc.x, sc.y);
        shape_p->setPosition( 0.0f, 0.0f);
        shape_p->setRotation(0.0f);
        shape_p->setScale( 1.0f, 1.0f);
    }
    att_body = NULL;
    att_object = NULL;
}

void CGraphicShape::update() {
    if( att_body != NULL ) {
        b2Vec2 pos;
        float rot;
        pos = phys_to_tex*att_body->GetPosition();
        rot = RADTODEG*att_body->GetAngle();
        origin.setPosition( pos.x, pos.y);
        origin.setRotation(rot);
    } else if( att_object != NULL ) {
        sf::Vector2f pos;
        float ang;
        pos = att_object->getPos();
        ang = att_object->getAngle();
        origin.setPosition( pos );
        origin.setRotation( ang );
    }
}


void CGraphicShape::step( float t_sec ) {
}

void CGraphicShape::render( sf::RenderWindow* r_win, sf::FloatRect& view_rect ) {

    if( att_body != NULL ) {
        b2Vec2 pos;
        float rot;
        pos = phys_to_tex*att_body->GetPosition();
        rot = RADTODEG*att_body->GetAngle();
        origin.setPosition( pos.x, pos.y);
        origin.setRotation(rot);
    } else if( att_object != NULL ) {
        sf::Vector2f pos;
        float ang;
        pos = att_object->getPos();
        ang = att_object->getAngle();
        origin.setPosition( pos );
        origin.setRotation( ang );
    }

    sf::FloatRect bounds = shape_p->getGlobalBounds();
    bounds = origin.getTransform().transformRect( bounds );
    //don't render if it's offscreen
    if( !bounds.intersects(view_rect) )
        return;

    sf::RenderStates r_states;
    r_states.transform = origin.getTransform();
    r_win->draw( *shape_p, r_states);

//Debug bounding rectangles
#ifndef NDEBUG
    if( g_game.settings.reduced_gfx ) {
        float w_zoom = r_win->getView().getSize().x/(float)g_game.scrnW;
        sf::RectangleShape sh;
        sh.setSize( sf::Vector2f( bounds.width, bounds.height));
        sh.setPosition( bounds.left, bounds.top);
        sh.setOutlineThickness(1.0f*w_zoom);
        sh.setOutlineColor(sf::Color::Magenta);
        sh.setFillColor(sf::Color::Transparent);
        r_win->draw( sh );
    }
#endif
}


//============CVertexArray==============================
void CVertexArray::setPos( const sf::Vector2f& new_pos ) {}

void CVertexArray::setAngle( float deg ) {}

sf::Vector2f CVertexArray::getPos() {
    if(vertices.size()>0)
        return vertices.at(0).position;
    else return sf::Vector2f( 0.0f, 0.0f);
}
float CVertexArray::getAngle() {
    return 0.0f;
}

void CVertexArray::attach( b2Body* body ) {}
void CVertexArray::attach( CGraphicObject* object ) {}
void CVertexArray::detach() {}

void CVertexArray::step( float t_sec ) {}
//draw
void CVertexArray::render( sf::RenderWindow* r_win, sf::FloatRect& view_rect ) {
    unsigned int cur_size = vertices.size();
    if( cur_size == 0 )
        return;
    //recalculate bounding box
    if( cur_size != last_size ) {
        last_size = cur_size;
        unsigned int i;
        sf::Vector2f top_l = vertices.at(0).position;
        sf::Vector2f bottom_r = vertices.at(0).position;
        for( i=0; i<cur_size; i++) {
            if( vertices.at(i).position.x < top_l.x )
                top_l.x = vertices.at(i).position.x;
            if( vertices.at(i).position.y < top_l.y )
                top_l.y = vertices.at(i).position.y;
            if( vertices.at(i).position.x > bottom_r.x )
                bottom_r.x = vertices.at(i).position.x;
            if( vertices.at(i).position.y > bottom_r.y )
                bottom_r.y = vertices.at(i).position.y;
        }
        bounding_rect.left = top_l.x;
        bounding_rect.top = top_l.y;
        bounding_rect.width = bottom_r.x - top_l.x;
        bounding_rect.height = bottom_r.y - top_l.y;
    }
    //No need to render if out of screen
    if( !bounding_rect.intersects(view_rect) )
        return;

    r_win->draw( &vertices.at(0), vertices.size(), varray_type);

//Debug bounding rectangles
#ifndef NDEBUG
    if( g_game.settings.reduced_gfx ) {
        float w_zoom = r_win->getView().getSize().x/(float)g_game.scrnW;
        sf::RectangleShape sh;
        sh.setSize( sf::Vector2f( bounding_rect.width, bounding_rect.height));
        sh.setPosition( bounding_rect.left, bounding_rect.top);
        sh.setOutlineThickness(1.0f*w_zoom);
        sh.setOutlineColor(sf::Color::Yellow);
        sh.setFillColor(sf::Color::Transparent);
        r_win->draw( sh );
    }
#endif
}
