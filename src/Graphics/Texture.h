#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

#include "GraphicObject.h"

class CTexture : public sf::Sprite, public CGraphicObject {
    public:
    CTexture( float phys_to_tex_param );
    ~CTexture() {}
    int getType();
    //absolute position if not attached, relative if attached to something
    void setPos( const sf::Vector2f& new_pos );
    void setAngle( float deg );
    sf::Vector2f getPos();
    float getAngle();

    //can be attached to only one object at a time
    //attach to physics object
    //if already attached, detach from CGraphicObject
    void attach( b2Body* body );
    //attach to graphics object
    //if already attached, detach from b2Body*
    void attach( CGraphicObject* object );
    //detach, convert relative position to world position (absolute)
    void detach();

    //any calculations you want every frame
    void step( float t_sec );
    //draw
    void render( sf::RenderWindow* r_win, sf::FloatRect& view_rect );


    private:
    //update position to latest
    void update();
    //body or object it is attached to
    //if not attached, both are NULL
    b2Body* att_body;
    CGraphicObject* att_object;

    //this is the center of the sprite
    sf::Transformable origin;
    //relative transformations are applied through the inherited sprite

    //ratio between physics and texture sizes
    float phys_to_tex;
};

#endif // TEXTURE_H_INCLUDED
