
#include "Control.h"
#include "Game.h"

CControlPanel::CControlPanel() :
    half_size(90),
    half_deadband(15),
    position( 815, 540),
    color_steering_deadband( sf::Color( 0, 0, 255, 200)),
    color_steering_active( sf::Color( 0, 0, 175, 125)),
    color_fwd_deadband( sf::Color( 0, 0, 0, 100)),
    color_fwd_throttle( sf::Color( 0, 255, 0, 125)),
    color_fwd_brake( sf::Color( 255, 0, 0, 125))
    {
    panel.setSize( sf::Vector2f( 2*half_size, 2*half_size) );
    panel.setOrigin( half_size, half_size);
    panel.setPosition( position.x, position.y);
}

void CControlPanel::setTexture(sf::Texture* tex) {
    if( tex != NULL ) {
        panel.setTexture(tex);
    }
}

float CControlPanel::getThrottle() {
    sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(g_game.window));
    int fwd_px = position.y-mouse_pos.y;
    if( -half_deadband <= fwd_px && fwd_px <= half_deadband )
        return 0.0f;
    int fwd_active_band = half_size-half_deadband;
    //normalizehttp://www.linuxquestions.org/questions/programming-9/non-blocking-sockets-in-perl-354499/
    float fwd_mag;
    if( fwd_px > 0.0f ) {
        fwd_mag = (fwd_px-half_deadband)/(float)fwd_active_band*100.0f;
    } else return 0.0f; //else is braking

    if( fwd_mag > 100.0f )
        fwd_mag = 100.0f;

    return fwd_mag;
}

float CControlPanel::getBrake() {
    sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(g_game.window));
    int fwd_px = position.y-mouse_pos.y;
    if( -half_deadband <= fwd_px && fwd_px <= half_deadband )
        return 0.0f;
    int fwd_active_band = half_size-half_deadband;
    //normalize
    float fwd_mag;
    if( fwd_px < 0.0f ) {
        fwd_mag = (fwd_px+half_deadband)/(float)fwd_active_band*100.0f;
    } else return 0.0f; //it's throttle

    if( fwd_mag < -100.0f )
        fwd_mag = -100.0f;
    //make it positive
    fwd_mag *= -1.0f;
    return fwd_mag;
}

float CControlPanel::getSteering() {
    sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(g_game.window));
    int side_px = position.x-mouse_pos.x;
    if( -half_deadband <= side_px && side_px <= half_deadband )
        return 0.0f;
    int side_active_band = half_size-half_deadband;
    //normalize
    float side_mag;
    if( side_px > 0.0f ) {
        side_mag = (side_px-half_deadband)/(float)side_active_band*100.0f;
    } else {
        side_mag = (side_px+half_deadband)/(float)side_active_band*100.0f;
    }
    if( side_mag > 100.0f )
        side_mag = 100.0f;
    else if( side_mag < -100.0f )
        side_mag = -100.0f;
    return side_mag;
}

void CControlPanel::render() {
    sf::Vector2i mouse_pos = sf::Mouse::getPosition(*(g_game.window));
    int steering_x;
    unsigned int steering_size_x;
    if( mouse_pos.x < position.x ) {
        steering_x = mouse_pos.x;
        steering_size_x = position.x-mouse_pos.x;
        if( steering_x < (int)(position.x-half_size) ) {
            steering_x = position.x-half_size;
            steering_size_x = half_size;
        }
    } else {
        steering_x = position.x;
        steering_size_x = mouse_pos.x-position.x;
        if( steering_size_x > half_size )
            steering_size_x = half_size;
    }
    if( mouse_pos.x != position.x ) {
        if( (int)steering_size_x <= half_deadband )
            steering_rect.setFillColor(color_steering_deadband);
        else
            steering_rect.setFillColor(color_steering_active);
        steering_rect.setSize( sf::Vector2f( (float)steering_size_x, (float)2*half_size ) );
        steering_rect.setPosition( steering_x, position.y-half_size);
        g_game.window->draw(steering_rect);
    }

    int fwd_y;
    int fwd_pos_y;
    if( mouse_pos.y < position.y ) {
        fwd_y = position.y-mouse_pos.y;
        fwd_pos_y = mouse_pos.y;
        if( fwd_pos_y < (int)(position.y-half_size) ) {
            fwd_pos_y = position.y-half_size;
            fwd_y = half_size;
        }
    } else {
        fwd_y = mouse_pos.y-position.y;
        fwd_pos_y = position.y;
        if( fwd_y > (int)half_size )
            fwd_y = half_size;
    }
    if( mouse_pos.y != position.y ) {
        if( fwd_y <= half_deadband )
            forward_rect.setFillColor(color_fwd_deadband);
        else if( fwd_pos_y < position.y )
            forward_rect.setFillColor(color_fwd_throttle);
        else
            forward_rect.setFillColor(color_fwd_brake);
        forward_rect.setSize( sf::Vector2f( (float)(2*half_size), (float)fwd_y ) );
        forward_rect.setPosition( position.x-half_size, fwd_pos_y);
        g_game.window->draw(forward_rect);
    }

    g_game.window->draw(panel);
}
