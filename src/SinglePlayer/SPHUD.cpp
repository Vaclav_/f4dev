
#include "SPHUD.h"
#include <assert.h>
#include <sstream>
#include <cmath>

void CSPHUD::renderTime( sf::RenderWindow* win ) {
    assert( win!=NULL );
    std::stringstream ss;
    ss << "Cur. lap " << getMinutes() << ":";
    unsigned int secs = getSeconds();
    if(secs < 10)
        ss << "0";

    ss << getSeconds() << ":";

    unsigned int ms = getMs();
    if(ms<100)
        ss << "0";

    ss << ms/10;

    time_label.setString(ss.str());
    //set origin/pos again?
    win->draw( time_label );

    std::stringstream ss2;

    ss2 << "Lap " << last_completed_lap;
    lap_label.setString(ss2.str());
    win->draw( lap_label );

    std::stringstream ss3;
    if(last_completed_lap<2) {
        ss3 << "Best lap  --";
    } else {

        ss3 << "Best lap " << (unsigned int)best_time.asSeconds()/60 << ":";
        ss3 << (unsigned int)best_time.asSeconds()%60 << ":";
        ss3 << best_time.asMilliseconds()%1000;
    }
    best_time_label.setString(ss3.str());
    win->draw( best_time_label );
}

void CSPHUD::initLabel( sf::Font* font, int x_pos, int y_pos, int char_size, sf::Color color) {
    if(font==NULL)
        return;
    time_label.setFont(*font);
    lap_label.setFont(*font);
    best_time_label.setFont(*font);
    if( char_size < 0 || char_size > 1000 )
        char_size = 16;

    time_label.setCharacterSize(char_size);
    lap_label.setCharacterSize(char_size);
    best_time_label.setCharacterSize(char_size);


    time_label.setColor(color);
    lap_label.setColor(color);
    best_time_label.setColor(color);

    //sf::FloatRect l_size;
    //l_size = time_label.getLocalBounds();
    //set origin to top right corner
    //time_label.setOrigin( l_size.width, 0 );

    lap_label.setPosition( (float)x_pos, (float)y_pos);
    best_time_label.setPosition( (float)x_pos, (float)y_pos+20.0f);
    time_label.setPosition( (float)x_pos, (float)y_pos+40.0f);
}

void CSPHUD::init( CFinishLineObject* fl, CStartPointObject* sp ) {
    finish_line = fl;

    //Check if clockwise or counter clockwise
    //...
}

//must increment cur_lap first
void CSPHUD::newLap() {
    if(cur_lap < 2)
        return;
    if(cur_lap<last_completed_lap)
        return;
    sf::Time l_time = timer.restart();
    lap_times.push_back(l_time);
    if( cur_lap == 2 )
        best_time = l_time;
    else if( l_time < best_time )
        best_time = l_time;
}

float CSPHUD::update( const b2Vec2& car_pos ) {
    float ang_car;
    float ang_fl;
    float new_ang;
    if( finish_line != NULL ) {
        b2Vec2 car_from_sp = car_pos - finish_line->point1;
        b2Vec2 finish_line_vect = finish_line->point2 - finish_line->point1;
        //angle from 0 to 2pi, 0 is ( 0, -1), counter clockwise
        ang_car = std::atan2( car_from_sp.x, car_from_sp.y) + b2_pi;
        ang_fl = std::atan2( finish_line_vect.x, finish_line_vect.y ) + b2_pi;

        new_ang = ang_fl - ang_car;
        if( new_ang < 0.0f )
            new_ang += 2.0f*b2_pi;
        else if( new_ang >= 2.0f*b2_pi )
            new_ang -= 2.0f*b2_pi;

        if(clockwise) {
            if( last_rad < -10.0f ) {
            } //first step, skip
            else {
                ///TODO: Doesn't work for second lap.. why?
                if( last_rad > 3.0f*b2_pi/2.0f && new_ang <= b2_pi/2.0f ) {
                    cur_lap++;
                    if(cur_lap>last_completed_lap) {
                        last_completed_lap = cur_lap;
                        newLap();
                    }
                } else if( last_rad <= b2_pi/2.0f && new_ang > 3.0f*b2_pi/2.0f ) {
                    cur_lap--;
                } //Note: can miss, if last_rad == new ang ?
            }
        } else {
            //...
        }

        last_rad = new_ang;
        //float new_rad =
    } else g_game.error << "CSPHUD: finish_line is NULL.";

    return cur_lap;
}
