#ifndef SPCONFIG_H_INCLUDED
#define SPCONFIG_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <string>
#include "../Object/GameObject.h"

//Class SinglePlayerConfig
class CSPConfig {
    public:
    CSPConfig() {}
    std::string player_name;
    std::string map_path;
    int car_type;
    sf::Color primary_color;
    sf::Color secondary_color;
};

class CSPStats {
    public:
    CSPStats() {}
    std::string map_name;
    std::vector<std::string> players;
    std::vector<sf::Time> times;
};

#endif // SPCONFIG_H_INCLUDED
