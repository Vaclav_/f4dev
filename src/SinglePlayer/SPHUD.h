#ifndef SPHUD_H_INCLUDED
#define SPHUD_H_INCLUDED

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <string>
#include <vector>
#include "../Object/GameObject.h"

class CSPHUD {
    public:
    CSPHUD() : clockwise(true), cur_lap(0), last_completed_lap(1), last_rad(-100.0f) {}

    bool clockwise;

    int cur_lap;
    int last_completed_lap;
    float last_rad;

    CFinishLineObject* finish_line;

    sf::Clock timer;
    std::vector<sf::Time> lap_times;
    sf::Time best_time;

    void renderTime( sf::RenderWindow* win );

    void init( CFinishLineObject* fl, CStartPointObject* sp );
    void initLabel( sf::Font* font, int x_pos, int y_pos, int char_size, sf::Color color = sf::Color::White );
    sf::Text best_time_label;
    sf::Text time_label;
    sf::Text lap_label;

    float update( const b2Vec2& car_pos );
    void newLap();

    unsigned int getMinutes() {
        float t = timer.getElapsedTime().asSeconds();
        t /= 60.0f;
        return (unsigned int)t;
    }
    unsigned int getSeconds() {
        float t = timer.getElapsedTime().asSeconds();
        return ((unsigned int)t)%60;
    }
    unsigned int getMs() {
        return(timer.getElapsedTime().asMilliseconds()%1000);
    }
};


#endif // SPMANAGER_H_INCLUDED
