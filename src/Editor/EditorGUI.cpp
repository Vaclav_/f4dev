
#include "EditorGUI.h"
#include "../Game.h"
#include <sstream>

static const float char_size = 8.0f;
static const sf::Color disabled_color = sf::Color( 100, 100, 100);

CNumInput::CNumInput() :
    max_value(0),
    enabled(true),
    max_digits(3),
    active(false),
    content_changed(false),
    pos_x(0),
    pos_y(0),
    size_x(0),
    size_y(0),
    cur_pos(0),
    cur_blink_state(true),
    blink_period(1500), //in ms
    blink_cur_time(0),
    panel_color(sf::Color::White)
    {
}

void CNumInput::setFont( sf::Font* font ) {
    if( font != NULL )
        render_text.setFont( *font );
}

//pX: x position; pY: y position (top left corner)
//sX: x size, sY: ySize (of the panel, NOT the texture)
void CNumInput::setTextureParam( unsigned int digits, int pX, int pY, int sY, sf::Color col ) {
    max_digits = digits;
    pos_x = pX;
    pos_y = pY;
    size_x = 10+digits*char_size;
    size_y = sY;

    panel.setPosition( sf::Vector2f( pos_x, pos_y) );
    panel.setFillColor(col);
    render_text.setPosition( sf::Vector2f( pos_x+5, pos_y+size_y/2-10) );
    panel.setSize( sf::Vector2f( (float)size_x, (float)size_y));
    //TODO: fix cursor Y pos properly
    cursor.setSize( sf::Vector2f( 1.0f, size_y*0.8f) );
    cursor.setOrigin( sf::Vector2f( 0.0f, size_y*0.8f) );
    cursor.setPosition( sf::Vector2f( pos_x+6, pos_y+size_y/2+10) );
    cursor.setFillColor( sf::Color::Black );

    panel_color = col;
}

bool CNumInput::keyPress( int key_code ) {
    if( key_code == sf::Keyboard::Return )
        eventEnter();
    else if( key_code == sf::Keyboard::Left )
        moveCursorLeft();
    else if( key_code == sf::Keyboard::Right )
        moveCursorRight();
    else if( key_code == sf::Keyboard::Delete )
        eventDel();
    else if( key_code == sf::Keyboard::Escape )
        active = false;

    return active;
}

//text properties
void CNumInput::setLabelParam( std::string default_text, unsigned char text_size, sf::Color col ) {
    content = default_text;
    cur_pos = default_text.length();
    render_text.setCharacterSize(text_size);
    render_text.setColor(col);
    render_text.setString(content);
}

void CNumInput::textEntered( char ch ) {
    if( active ) {
        if('0' <= ch && ch <= '9') {
            if( content.length() < max_digits ) {
                content.insert( cur_pos, 1, ch);
                content_changed = true;
                cur_pos++;
                blink_cur_time = 0;
            }
        } else if( ch == 0x08 ) {   //backspace
            if( content.length()>0 ) {
                //remove one character
                content.erase( cur_pos-1, 1);
                content_changed = true;
                cur_pos--;
                blink_cur_time = 0;
            }
        }
    }
    if( max_value!= 0) {
        if( getContent()>max_value ) {
            setContent( max_value );
        }
    }
}

void CNumInput::eventDel() {
    if( content.length()>0 && cur_pos<content.length() ) {
        //remove one character
        content.erase( cur_pos, 1);
        content_changed = true;
        blink_cur_time = 0;
    }
}

//mouse left click is held event, at screen coordinates x: cY, y: cY
bool CNumInput::mouseDown( int cX, int cY) {
    //if cursor is in the rectangle
    if(enabled) {
        if( pos_x < cX && cX < pos_x+size_x && pos_y < cY && cY < pos_y+size_y )
            select();
        else
            active = false;
    }
    return active;
}

	//activate by software
void CNumInput::select() {
	active = true;
	cur_pos = content.length(); //set cursor to the end of text
	blink_cur_time = 0;
}

void CNumInput::deSelect() {
    active = false;
}

void CNumInput::eventEnter() {
    if(active)
        active = false;
}

void CNumInput::moveCursorLeft() {
    if(cur_pos>0)
        cur_pos--;
    blink_cur_time = 0;
}

void CNumInput::moveCursorRight() {
    if(cur_pos<content.length())
        cur_pos++;
    blink_cur_time = 0;
}

bool CNumInput::isActive() {
    return active;
}

void CNumInput::enable() {
    enabled = true;
    blink_cur_time = 0;
    panel.setFillColor(panel_color);
}

    //returns true if it was active before
bool CNumInput::disable() {
    bool ret = active;
    enabled = false;
    deSelect();
    panel.setFillColor(disabled_color);
    return ret;
}

void CNumInput::CNumInput::erase() {
    content.erase( content.begin(), content.end() );
    content_changed = true;
    cur_pos = 0;
}

void CNumInput::setContent( int num) {
    if(!enabled) return;
    if( max_value != 0 ) {
        if( num > max_value ) num = max_value;
    }
    content.erase( content.begin(), content.end() );
    std::stringstream ss;
    unsigned int i;
    int top=1;
    for( i=0; i<max_digits; i++) {
        top*=10;
    }
    if(num>(top-1))
        num = top-1;
    if( num<0 )
        num = 0;
    ss << num;
    content = ss.str();
    content_changed = true;
}

int CNumInput::getContent() {
    int sum=0;
    int mult = 1;
    for( unsigned int i=0; i<content.length(); i++) {
        sum += mult*(content.at(content.length()-i-1)-'0');
        mult *= 10;
    }
    return sum;
}

//draw the button
void CNumInput::render( sf::RenderWindow* r_window ) {
    if( isActive() ) {
        blink_cur_time += g_game.frame_time.asMilliseconds();
        if( blink_cur_time < blink_period/2 )
            cur_blink_state = true;
        else if( blink_cur_time < blink_period )
            cur_blink_state = false;
        else {
            cur_blink_state = true;
            blink_cur_time = 0;
        }
    }
    r_window->draw(panel);
    if(content_changed)
        render_text.setString(content);
    r_window->draw(render_text);
    if( isActive() && cur_blink_state ) {
        cursor.setPosition( sf::Vector2f( pos_x+6+char_size*cur_pos, pos_y+size_y/2+10) );
        r_window->draw(cursor);
    }
    content_changed = false;
}

void CNumInput::setMax( int m) {
    max_value = m;
}

//====================================================
CChoiceBox::CChoiceBox() :
    enabled(true),
    active(false),
    content_changed(true),
    pos_x(0),
    pos_y(0),
    size_x(0),
    size_y(0),
    cur_element(0)
    {
}

void CChoiceBox::setFont( sf::Font* font ) {
    if( font != NULL )
        render_text.setFont( *font );
}

//pX: x position; pY: y position (top left corner)
//sX: x size, sY: ySize (of the panel, NOT the texture)
void CChoiceBox::setTextureParam( int pX, int pY, int sX, int sY, sf::Color color_pas, sf::Color color_act ) {
    pos_x = pX;
    pos_y = pY;
    size_x = sX;
    size_y = sY;
    col_active = color_act;
    col_passive = color_pas;
    panel.setPosition( sf::Vector2f( pos_x, pos_y) );
    panel.setFillColor(color_pas);
    render_text.setPosition( sf::Vector2f( pos_x+5, pos_y+size_y/2-10) );
    panel.setSize( sf::Vector2f( (float)size_x, (float)size_y));
}

//text properties
void CChoiceBox::setLabelParam( unsigned char text_size, sf::Color col ) {
    render_text.setCharacterSize(text_size);
    render_text.setColor(col);
}

void CChoiceBox::keyPress( int key_code ) {
    if( key_code == sf::Keyboard::Left )
        buttonLeft();
    else if( key_code == sf::Keyboard::Right )
        buttonRight();
    else if( key_code == sf::Keyboard::Escape )
        deSelect();
}

//mouse left click is held event, at screen coordinates x: cY, y: cY
bool CChoiceBox::mouseDown( int cX, int cY) {
    //if cursor is in the rectangle
    if(enabled) {
        if( pos_x < cX && cX < pos_x+size_x && pos_y < cY && cY < pos_y+size_y )
            select();
        else
            active = false;
    }
    return active;
}

	//activate by software
void CChoiceBox::select() {
	active = true;
}

void CChoiceBox::deSelect() {
	active = false;
}

void CChoiceBox::eventEnter() {
    if(active)
        active = false;
}

bool CChoiceBox::onList( int w ) {
    bool on_list = false;
    unsigned int i;
    for( i=0; i<disabled_elements.size(); i++) {
        if( disabled_elements.at(i) == w ) {
            on_list = true;
            break;
        }
    }
    return on_list;
}

int CChoiceBox::getNextLeft( int cur ) {
    if( cur>0)
        cur--;
    else
        cur = elements.size()-1;
    return cur;
}

int CChoiceBox::getNextRight( int cur ) {
    if( (unsigned int)cur<(elements.size()-1) )
        cur++;
    else
        cur = 0;
    return cur;
}

void CChoiceBox::buttonLeft() {

    if( !isActive() || !enabled) return;
    if( disabled_elements.size() == elements.size() ) return; //all disabled
    int new_element = getNextLeft(cur_element);

    while( onList(new_element) ) { //while next is disabled, keep getting a new one
        new_element = getNextLeft(new_element);
    }

    if( new_element != cur_element ) {
        content_changed = true;
        cur_element = new_element;
    }
}

void CChoiceBox::buttonRight() {

    if( !isActive() || !enabled) return;
    if( disabled_elements.size() == elements.size() ) return;
    int new_element = getNextRight(cur_element);

    while( onList(new_element) ) { //while next is disabled, keep getting a new one
        new_element = getNextRight(new_element);
    }

    if( new_element != cur_element ) {
        content_changed = true;
        cur_element = new_element;
    }

}

bool CChoiceBox::isActive() {
    return active;
}

void CChoiceBox::enable() {
    enabled = true;
    panel.setFillColor(col_passive);
}

bool CChoiceBox::disable() {
    bool ret = active;
    enabled = false;
    deSelect();
    panel.setFillColor(disabled_color);
    return ret;
}

void CChoiceBox::disable_one( int which ) {
    bool on_list = false;
    unsigned int i;
    for( i=0; i<disabled_elements.size(); i++) {
        if( disabled_elements.at(i) == which ) {
            on_list = true;
            break;
        }
    }
    if(!on_list)
        disabled_elements.push_back(which);

    if( which == cur_element ) { //choose another
        if( !(disabled_elements.size() == elements.size()) ) {
            int new_element = getNextRight(cur_element);
            while( onList(new_element) ) {
                new_element = getNextRight(new_element);
            }
            if( new_element != cur_element ) {
                content_changed = true;
                cur_element = new_element;
            }
        }
    }
}

void CChoiceBox::enable_one( int which ) {
    unsigned int i;
    int del_num = -1;
    for( i=0; i<disabled_elements.size(); i++) {
        if( disabled_elements.at(i) == which )
            del_num = i;
    }
    if( del_num >= 0 ) {
        disabled_elements.erase( disabled_elements.begin()+del_num, disabled_elements.begin()+del_num+1);
    }
}

void CChoiceBox::enable_all() {
    disabled_elements.erase( disabled_elements.begin(), disabled_elements.end() );
}

int CChoiceBox::getChoice() {
    return cur_element;
}

void CChoiceBox::setChoice( int c) {
    if(!enabled) return;
    cur_element = c;
    content_changed = true;
}

//draw the button
void CChoiceBox::render( sf::RenderWindow* r_window ) {
    if(enabled) {
        if( isActive() ) {
            panel.setFillColor(col_active);
        } else
            panel.setFillColor(col_passive);
    }
    r_window->draw(panel);
    if(content_changed && elements.size()>0) {
        render_text.setString(elements.at(cur_element));
        sf::FloatRect b_size = render_text.getLocalBounds();
        render_text.setOrigin( b_size.width/2, 0);
        render_text.setPosition( pos_x+size_x/2, pos_y+size_y/2-10 );
    }
    r_window->draw(render_text);
    content_changed = false;
}


//================================================================

CSelectBox::CSelectBox() :
    state(false),
	x(0),
	y(0),
	sizeX(100),
	sizeY(100),
	col_passive(sf::Color::White),
	col_active(sf::Color::Yellow)
	{
}

CSelectBox::~CSelectBox() {}

void CSelectBox::setTexture( sf::Texture* tex ) {
	if( tex != NULL )
		sprite.setTexture( *tex );
}

void CSelectBox::setTextureParam( int pX, int pY, int sX, int sY, sf::Color col_pas, sf::Color col_act ) {
	x = pX;
	y = pY;
	sizeX = sX;
	sizeY = sY;
	col_passive = col_pas;
	col_active = col_act;
	sprite.setOrigin( sf::Vector2f( sX/2, sY/2) );
	sprite.setPosition( sf::Vector2f( pX, pY) );
	sprite.setColor( col_pas );
}

void CSelectBox::render( sf::RenderWindow* r_window ) {

	if( state )
		sprite.setColor( col_active );
	else
		sprite.setColor( col_passive );

	r_window->draw(sprite);
}

bool CSelectBox::mouseDown( int cX, int cY) {
    //if cursor is in the rectangle
    if( x-sizeX/2 < cX && cX < x+sizeX/2 && y-sizeY/2 < cY && cY < y+sizeY/2 )
		state = true;
    //else
      //  state = false;
    return state;
}

void CSelectBox::select() {
    state = true;
}

void CSelectBox::deSelect() {
    state = false;
}

bool CSelectBox::isActive() {
    return state;
}
