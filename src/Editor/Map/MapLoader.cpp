
#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

#include "../../Game.h"
#include "MapLoader.h"
#include "../EditorObj.h"
#include "../../Graphics/GraphicObject.h"
#include "../../Physics/CarPhysics.h"
#include "../../Physics/GroundType.h"

#define RADTODEG 180.0f/b2_pi

static void loadFinishLine( CGameObject*& obj, b2Body* body, b2dJson& json, float b2d_to_gfx ) {
    CFinishLineObject* finish_obj;
    b2Vec2 p1 = body->GetPosition();
    b2Vec2 p2 = json.getCustomVector( body, "point2");
    finish_obj = new CFinishLineObject( p1, p2, b2d_to_gfx);
    obj = (CGameObject*)finish_obj;
}

static void loadStartPoint( CGameObject*& obj, b2Body* body, b2dJson& json, float b2d_to_gfx ) {
    CStartPointObject* start_p;
    b2Vec2 p = body->GetPosition();
    int n = json.getCustomInt( body, "number");
    start_p = new CStartPointObject( p, n, b2d_to_gfx);
    obj = (CGameObject*)start_p;
}

static void loadSprite( CGameObject*& obj, int editor_obj_type, b2Body* body, b2dJson& json, float b2d_to_gfx ) {

    b2Fixture* fx = body->GetFixtureList();
    if( fx == NULL ) {
        g_game.error << "loadGraphicObject: The body doesn't have a fixture.";
        obj = NULL;
        return;
    }
    int shape_type = fx->GetType();
    sf::Shape* sf_shape;
    CGraphicShape* gfx_shape;
    CPhysicalObject* phys_obj;
    CGraphicObject* gfx_obj;

    int cur_p;
    b2Vec2 b2_point;
    sf::Vector2f t_vect;
    sf::Transform rotation;

    int red, green, blue, alpha;

    if( shape_type == b2Shape::e_polygon ) {
        b2PolygonShape* b2_polygon;
        b2_polygon = (b2PolygonShape*)fx->GetShape();

        sf::ConvexShape* sf_conv_shape = new sf::ConvexShape(b2_polygon->GetVertexCount());

        if( editor_obj_type == EOBJ_GRAPHIC )
            rotation.rotate( body->GetAngle()*RADTODEG );
        for( cur_p = 0; cur_p<b2_polygon->GetVertexCount(); cur_p++ ) {
            b2_point = b2_polygon->GetVertex(cur_p);
            t_vect.x = b2d_to_gfx*b2_point.x;
            t_vect.y = b2d_to_gfx*b2_point.y;
            if( editor_obj_type == EOBJ_GRAPHIC )
                t_vect = rotation.transformPoint(t_vect);
            sf_conv_shape->setPoint( cur_p, t_vect);
        }

        sf_conv_shape->setPosition( b2d_to_gfx*body->GetPosition().x, b2d_to_gfx*body->GetPosition().y);

        sf_shape = (sf::Shape*)sf_conv_shape;
        red = json.getCustomInt( body, "fR");
        green = json.getCustomInt( body, "fG");
        blue = json.getCustomInt( body, "fB");
        alpha = json.getCustomInt( body, "fA");
        sf_shape->setFillColor(sf::Color( red, green, blue, alpha));
        red = json.getCustomInt( body, "lR");
        green = json.getCustomInt( body, "lG");
        blue = json.getCustomInt( body, "lB");
        alpha = json.getCustomInt( body, "lA");
        sf_shape->setOutlineColor(sf::Color( red, green, blue, alpha));
        sf_shape->setOutlineThickness(0.0f);

        gfx_shape = new CGraphicShape( sf_shape, b2d_to_gfx);
        gfx_obj = (CGraphicObject*)gfx_shape;
    }
    else if( shape_type == b2Shape::e_circle ) {
        b2CircleShape* b2_circle;
        b2_circle = (b2CircleShape*)fx->GetShape();
        float radius = b2_circle->m_radius*b2d_to_gfx;
        sf_shape = (sf::Shape*) new sf::CircleShape( radius );
        sf_shape->setOrigin( radius, radius );
        sf_shape->setPosition( b2d_to_gfx*body->GetPosition().x, b2d_to_gfx*body->GetPosition().y );

        red = json.getCustomInt( body, "fR");
        green = json.getCustomInt( body, "fG");
        blue = json.getCustomInt( body, "fB");
        alpha = json.getCustomInt( body, "fA");
        sf_shape->setFillColor(sf::Color( red, green, blue, alpha));
        red = json.getCustomInt( body, "lR");
        green = json.getCustomInt( body, "lG");
        blue = json.getCustomInt( body, "lB");
        alpha = json.getCustomInt( body, "lA");
        sf_shape->setOutlineColor(sf::Color( red, green, blue, alpha));
        sf_shape->setOutlineThickness(4.0f);

        gfx_shape = new CGraphicShape( sf_shape, b2d_to_gfx);
        gfx_obj = (CGraphicObject*)gfx_shape;
    }
    else if( shape_type == b2Shape::e_chain )
    {
        b2ChainShape* b2_chain;
        b2_chain = (b2ChainShape*)fx->GetShape();

        CVertexArray* v_array = new CVertexArray( sf::LinesStrip, b2d_to_gfx );

        if( editor_obj_type == EOBJ_GRAPHIC )
            rotation.rotate( body->GetAngle()*RADTODEG );

        red = json.getCustomInt( body, "fR");
        green = json.getCustomInt( body, "fG");
        blue = json.getCustomInt( body, "fB");
        alpha = json.getCustomInt( body, "fA");

        for( cur_p=0; cur_p<b2_chain->m_count; cur_p++) {
            v_array->vertices.push_back(sf::Vertex());
            t_vect.x = (*(b2_chain->m_vertices+cur_p)).x*b2d_to_gfx;
            t_vect.y = (*(b2_chain->m_vertices+cur_p)).y*b2d_to_gfx;
            if( editor_obj_type == EOBJ_GRAPHIC )
                t_vect = rotation.transformPoint(t_vect);
            //add shape rotation instead!
            v_array->vertices.back().position.x = t_vect.x + b2d_to_gfx*body->GetPosition().x;
            v_array->vertices.back().position.y = t_vect.y + b2d_to_gfx*body->GetPosition().y;
            v_array->vertices.back().color = sf::Color( red, green, blue, alpha);
        }

        gfx_obj = (CGraphicObject*)v_array;
    }
    else {
        g_game.error << "Unkown shape type.";
        obj = NULL;
        return;
    }

    if( editor_obj_type != EOBJ_GRAPHIC && editor_obj_type != EOBJ_GND ) {
        gfx_obj->attach( body );
        //should rather be called GeneralBody ?
        phys_obj = (CPhysicalObject*) new CBox( body );
    } else if( editor_obj_type == EOBJ_GND ){
        int ground_type = json.getCustomInt( body, "gnd_t");
        gfx_obj->attach( body );
        phys_obj = (CPhysicalObject*) new CGroundSensor( body, ground_type );
        b2Filter filter;
        filter.categoryBits = COLLENTITY_GENERAL;        filter.maskBits = COLLMASK_GROUND;
        fx->SetFilterData(filter);
    }

    if( editor_obj_type != EOBJ_GRAPHIC ) {
        obj = (CGameObject*)new CSimpleGameObject( gfx_obj, phys_obj );
    } else {
        obj = (CGameObject*)new CSimpleGameObject( gfx_obj, NULL );
    }
}

bool loadMap( std::string path, CMapInfo& map_info, std::vector<CGameObject*>& objects, b2World*& world, float b2d_to_gfx) {

    if( world != NULL ) {
        g_game.error << "loadMap: world pointer should be NULL but it isn't.";
    }

    b2dJson json;
    std::string err_msg = "";
    world = json.readFromFile( path.c_str(), err_msg);
    if( world == NULL ) {
        g_game.error << "loadMap: opening json file failed! Error message: " << err_msg << "\n";
        return false;
    }

    b2Body* body;
    int obj_type;
    std::vector<b2Body*> to_remove;

    for( body = world->GetBodyList(); body; body = body->GetNext() ) {
        objects.push_back(NULL);
        obj_type = json.getCustomInt( body, "type");
        if( obj_type == EOBJ_GRAPHIC || obj_type == EOBJ_DYNAMIC ||
           obj_type == EOBJ_GND || obj_type == EOBJ_STATIC ) {
            loadSprite( objects.back(), obj_type, body, json, b2d_to_gfx );
            if( obj_type == EOBJ_GRAPHIC )
                to_remove.push_back(body);
        } else if ( obj_type == EOBJ_START_POINT ) {
            loadStartPoint( objects.back(), body, json, b2d_to_gfx);
            to_remove.push_back(body);
        } else if ( obj_type == EOBJ_FINISH_LINE ) {
            loadFinishLine( objects.back(), body, json, b2d_to_gfx);
            to_remove.push_back(body);
        } else {
            g_game.error << "loadMap: No such Editor Object type (EOBJ).";
            objects.pop_back();
        }

        //something went wrong.. we shouldn't crash anyway
        if( objects.back() == NULL )
            objects.pop_back();
    }

    for( unsigned int i=0; i<to_remove.size(); i++) {
        world->DestroyBody(to_remove.at(i));
    }

    return true;
}


#endif // GAMEOBJECT_H_
