#ifndef MAPINFO_H_INCLUDED
#define MAPINFO_H_INCLUDED

class CMapInfo {
    public:
    CMapInfo() :
        map_name("Unkown"),
        max_players(10) {}
    std::string map_name;
    unsigned int max_players;
};

#endif // MAPINFO_H_INCLUDED
