#ifndef MAPLOADER_H_INCLUDED
#define MAPLOADER_H_INCLUDED

#include "Box2D/Box2D.h"
#include "MapInfo.h"
#include "../b2dJson/b2dJson.h"
#include "../../Object/GameObject.h"

bool loadMap( std::string path, CMapInfo& map_info, std::vector<CGameObject*>& objects, b2World*& world, float b2d_to_gfx);

#endif
